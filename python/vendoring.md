# Vendoring

- [Python で パッケージを vendoring しつつ AWS Lambda へデプロイ - Librabuch](https://librabuch.jp/blog/2016/04/python-vendoring-lambda-deploy/)
    - pip での vendoring
    - -t オプションを使う。
- [Embedding a Python library in my own package - Stack Overflow](http://stackoverflow.com/questions/18812614/embedding-a-python-library-in-my-own-package)

