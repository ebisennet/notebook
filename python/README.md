# python

## 公式資料

- [Welcome to Python.org](https://www.python.org/)

## Topics

- [Installing](installing.md)
- [Package Management](package-management.md)
    - [pip](pip/)
- [Python Packages](../pythonlib/)
- [Syntax](syntax/)
- [Reflection](reflection.md)
- Standard Library
    - argparse
        - [gem/git-style command line arguments in Python - Stack Overflow](http://stackoverflow.com/questions/9729919/gem-git-style-command-line-arguments-in-python)
- [pyenv](pyenv.md)
- virtualenv
    - bundler 相当？
    - pip install virtualenv
- Tools
    - [Python の Lint \(文法チェッカ\) まとめ \- flake8 \+ hacking を使う \- \- Qiita](http://qiita.com/kitsuyui/items/5ab4608003a29ff7689f)
- Versions
- [Vendoring](vendoring.md)
- 文字コード
    - [Python2で文字列を処理する際の心掛け \- Qiita](http://qiita.com/FGtatsuro/items/cf178bc44ce7b068d233)
    - [Python3で文字列を処理する際の心掛け \- Qiita](http://qiita.com/FGtatsuro/items/f45c349e06d6df95839b)
    - [標準入出力のエンコーディングを指定する3つの方法の使い分け \- methaneのブログ](http://methane.hatenablog.jp/entry/20120807/1344298924)
