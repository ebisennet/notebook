# pip

## pip 自体のインストール

``` bash
curl -sSL https://bootstrap.pypa.io/get-pip.py | sudo python
```

## アップグレード

pip 自体のアップグレード。

``` bash
pip install --upgrade pip
```

インストールパッケージのアップグレード。

``` bash
pip install -U PKGNAME
```

## 参考資料

- [Python - いつの間にかpipのインストールが楽になってた件 - Qiita](http://qiita.com/who_you_me/items/831d62f396e6d66dda66)

