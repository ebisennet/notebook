# Package Management

## 公式資料

- [PyPI - the Python Package Index : Python Package Index](https://pypi.python.org/pypi)
    - パッケージを登録する公式の中央リポジトリ。
    - pip や easy_install が参照する。

## 参考資料

- [Python パッケージ管理技術まとめ (pip, setuptools, easy_install, etc)](http://www.yunabe.jp/docs/python_package_management.html)

