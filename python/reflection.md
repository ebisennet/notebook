# Reflection

## 参考資料

- [Pythonにおけるリフレクション - Qiita](http://qiita.com/icoxfog417/items/bf04966d4e9706eb9e04)
    - `__import__` で動的な import
    - getattr でモジュールやオブジェクトから動的に定義を取得
    - inspect モジュールでメソッド一覧取得など
