# pyenv

rbenv 相当。

## 公式資料

- [GitHub - yyuu/pyenv-installer: This tools is used to install \`pyenv\` and friends.](https://github.com/yyuu/pyenv-installer)

## インストール

- `curl -sSL https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash`
- `~/.bash_profile` への追加

## python インストール

``` bash
yum install zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel
pyenv install 2.7.11
pyenv shell 2.7.11
```
