# Import

## モジュールの Import

``` python
import package_foo.foo

f = foo.FooClass()

print dir(foo)
```

## パッケージから複数モジュールの Import

``` python
from package_foo import foo
```
