# Class

## Instance Method

``` python
class Foo:
    def foo(self):
        print('foo')
```

引数に self がないと Class Method.

## 特殊メソッド : [データモデル — Python 2.7ja1 documentation](http://docs.python.jp/2.7/reference/datamodel.html#specialnames)

### `__new__`
### `__init__`
### `__getitem__`

インスタンスへの \[\] によるアクセスを実装。

### 演算子

### キャスト

### `__dir__`
