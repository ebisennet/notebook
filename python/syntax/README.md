# Syntax

## 公式資料

- 3.4
    - [Python 言語リファレンス — Python 3.4.3 ドキュメント](http://docs.python.jp/3.4/reference/index.html)
- 2.7
    - [Python 言語リファレンス — Python 2.7ja1 documentation](http://docs.python.jp/2.7/reference/index.html)
    - [Python チュートリアル — Python 2.7ja1 documentation](http://docs.python.jp/2.7/tutorial/index.html#tutorial-index)

## Topics

- Decorator
    - [Pythonのデコレータの使い方 - Life with Python](http://www.lifewithpython.com/2014/12/python-decorator-syntax.html)
- Comprehension
    - [Pythonの内包表記の使い方まとめ - Life with Python](http://www.lifewithpython.com/2014/09/python-list-comprehension-and-generator-expression-and-dict-comprehension.html)
        - リスト内包
        - ジェネレータ式
        - セット内包
        - 辞書内包
        - フィルター
        - 多重ループ
- [Class](class.md)
- [Import](import.md)

