# nginx

## 公式資料

- [Alphabetical index of directives](http://nginx.org/en/docs/dirindex.html)

## Topics

- [ディレクティブ一覧](directives/)

## 参考資料

- [nginx で帯域制限をかける(limit_rate を使ってみる) | レンタルサーバー・自宅サーバー設定・構築のヒント](http://server-setting.info/centos/nginx-limit_rate.html)

