# Headers

| Name                            | Remarks |
|---------------------------------+---------|
| Accept                          |         |
| Cache-Control                   |         |
| Connection                      |         |
| Content-Length                  |         |
| Content-Type                    |         |
| Host                            |         |
| Pragma                          |         |
| Proxy-Authorization             |         |
| Proxy-Connection                |         |
| User-Agent                      |         |
| [X-User-Agent](X-User-Agent.md) |         |


