# Tools

- [awscli](awscli/)
- [aws-shell](aws-shell/)
- awless
    - [awless](https://github.com/wallix/awless)

## AWS Toolkit for Eclipse

- [AWS Toolkit for Eclipse | アマゾン ウェブ サービス（AWS 日本語）](http://aws.amazon.com/jp/documentation/awstoolkiteclipse/)
    - [AWS Toolkit for Eclipse をインストールする - AWS Toolkit for Eclipse](http://docs.aws.amazon.com/ja_jp/AWSToolkitEclipse/latest/GettingStartedGuide/tke_setup_install.html)

