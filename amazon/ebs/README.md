# EBS

## 公式資料

- [Amazon EBSのアップデート – 新機能エラスティックボリュームが全てを変える \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-ebs-update-new-elastic-volumes-change-everything/)
    - インスタンスを停止せずにサイズやタイプを変更可能に。

## 参考資料

- [Amazon EBSのアップデート – 新たなスループット最適化ボリュームとコールドボリューム | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-ebs-update-new-cold-storage-and-throughput-options/)
- [【遂に来た！】EBS でボリュームサイズを変更できるようになりました（ボリュームタイプ変更も） ｜ Developers\.IO](http://dev.classmethod.jp/cloud/aws/release-change-ebs-volume-size/)
