# Managed Services

## 公式資料

- [AWS Managed Services](https://aws.amazon.com/jp/managed-services/)

## 参考資料

- [新しく登場した「AWS Managed Services」は、大企業向けマネージドサービス実現を支援するサービス － Publickey](http://www.publickey1.jp/blog/16/aws_managed_services.html)
