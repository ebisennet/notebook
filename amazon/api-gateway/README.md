# API Gateway

バックエンドシステムの API をインターネットに公開するサービス。

内部的にはフロントに CloudFront が配置されている。

## 公式資料

- [Amazon API Gateway （API を容易に作成・管理） | AWS](https://aws.amazon.com/jp/api-gateway/)
- [Amazon API Gateway のドキュメント | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/documentation/apigateway/)
- [Amazon API Gateway とは? - Amazon API Gateway](http://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/welcome.html)
    - 開発者ガイド
- [Amazon Web Services ブログ: Amazon API Gateway](http://aws.typepad.com/aws_japan/amazon-api-gateway/)
    - 公式ブログのAPI Gatewayカテゴリ

## Topics

- Features
    - [Throttling](throttling.md)
    - Cache
    - [API Key](api-key.md)
    - [Cognito 認証](cognito-auth.md)
    - [カスタム認可（Custom Authorizer）](custom-authorizer.md)
    - カスタムドメイン
        - 2017年3月 ACM に対応開始。
            - [Amazon API Gateway Integrates with AWS Certificate Manager \(ACM\)](https://aws.amazon.com/about-aws/whats-new/2017/03/amazon-api-gateway-integrates-with-aws-certificate-manager-acm/)
- API
- リソース
- メソッド
- ステージ
- モデル
- [Integration](integration/)
- [マッピングテンプレート](mapping-template.md)
- [カスタムドメイン](custom-domain.md)
- 制限
    - API Gateway 全体でソフトリミット1000RPSがかかっているので必要なら申請して数値を増やす
    - [Amazon API Gateway の制限と料金表 - Amazon API Gateway](http://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/limits.html)
- 他サービスとの連携
    - CloudWatch と統合可能
    - CloudWatch Logs と統合可能
- [ツール](tools/)
- [SDK生成](sdk-generation/)
- 関連
    - [serverless/serverless](https://github.com/serverless/serverless)
- Swagger
    - [【AWS】【API Gateway】SwaggerでREST APIを一元管理 - Qiita](http://qiita.com/sl2/items/839411b60326e16d9009)
    - [Amazon API Gateway Swagger ImporterでAPIを一発で定義する ｜ Developers.IO](http://dev.classmethod.jp/cloud/api-gateway-swagger-importer/)
- Catch-all Path Variables
    - １つのリソースで複数のパスに対応できる。
- ANY Method
    - メソッドをそのままバックエンドに転送できる。
- Lambda Proxy Integration
    - Lambda 関数にリクエスト・レスポンスを転送する。
    - リクエストの詳細が event で渡るようになる。
        - [API およびProxy Resourceを通じた Lambda プロキシ統合を作成する \- Amazon API Gateway](http://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html)
    - ステージ変数なども渡るようになる。
        - 通常の Lambda 統合の場合、明示的にマッピングテンプレートを作成しないとステージ変数は Lambda からは参照できない。
        - 通常の Lambda 統合の場合、 Context にも入らない。
            - [Context オブジェクト \(Node\.js\) \- AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/nodejs-prog-model-context.html)
    - [AWS 謹製 aws\-serverless\-express を使って APIGateway \+ Lambda \+ Node \+ Express で RESTful サービスの雛形を最速で作る \- Qiita](http://qiita.com/exabugs/items/594d3cd417e99dc29387)
- HTTP Endpoint Integration
    - API Gateway を HTTP Proxy として使えるように。
- Update (2016-09)
    - [API Gateway Update – New Features Simplify API Development | AWS Blog](https://aws.amazon.com/jp/blogs/aws/api-gateway-update-new-features-simplify-api-development/)
        - Catch-all Path Variables
        - ANY Method
        - Lambda Function Integration
        - HTTP Endpoint Integration
    - サーバーレスWebアプリが実現できるようになっている
        - [AWS Lambda と Amazon API Gateway で Express アプリケーションを実行 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/running-express-applications-on-aws-lambda-and-amazon-api-gateway/)

## 参考資料

- [Amazon API Gatewayのパフォーマンスオプションを理解する ｜ Developers.IO](http://dev.classmethod.jp/cloud/api-gateway-performance/)
    - キャッシュやスロットリングの設定。
    - API Gateway の呼び出しには TLS1.2 を使う必要がある。
- [「Amazon API Gateway 経由でちょっとしたアクセスを流してみた」 \#aws \#jawsug \#api \#駅すぱあとWebサービス - uchimanajet7のメモ](http://uchimanajet7.hatenablog.com/entry/2015/07/22/130342)
    - キャッシュやスロットリングの設定。
    - ステージだけでなくメソッドごとにも設定可能。
- [AWS Lambda / Amazon API Gateway Deep Dive](http://www.slideshare.net/keisuke69/aws-lambda-amazon-api-gateway-deep-dive)
    - P.34
        - API Gateway 自身も CloudFront を使っている（L7, L3 および Syn flood のDDoS防護用途）
            - この CloudFront は設定変更不可能。
        - API Gateway の前段に CloudFront を配置することは可能。
    - P.39
        - バックエンドへのタイムアウトは 29sec
- [Lambda+Amazon API Gatewayでバイナリレスポンスを返す方法 - Qiita](http://qiita.com/edvakf@github/items/c86ea650bc127a562b35)
    - `$util.base64Decode()`
