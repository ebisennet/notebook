# SDK生成

## iOS SDK

- [Amazon API Gateway の API のクライアント SDK を生成して iOS アプリから呼び出す ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/api-gateway-invoke-from-ios-app/)

Swift 生成も対応している。

Objective-C で生成した時の注意事項。

- 生成された各ソースファイルのファイル名が 255 文字以下でないといけない。
    - 255文字を越えると生成した SDK の zip ファイルを展開できない。

## Android SDK

- [Amazon API Gateway の API のクライアント SDK を生成して Android アプリから呼び出す ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/api-gateway-with-android/)
