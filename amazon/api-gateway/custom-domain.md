# カスタムドメイン

API Gateway で定義した API をカスタムドメインで運用できる。

サブドメイン運用時にCloudFrontを挟むことになる。

## 公式資料

- [API Gateway でカスタムドメイン名を使用する - Amazon API Gateway](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-custom-domains.html)

## 参考資料

- [Amazon API Gateway を独自ドメインで利用する ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/api-gateway-with-custom-domain/)
    - Base Path の設定は必要
