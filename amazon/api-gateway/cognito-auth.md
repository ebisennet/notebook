# Cognito 認証

API Gateway で提供する API を Cognito 認証済みユーザーに制限できる。

- Cognito 側で ID Pool に関連づける IAM Role に API 呼び出し権限が必要。

## 参考資料

- [Amazon API Gateway の API を Cognito で認証して呼び出す ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/api-gateway-cognito-auth/)
