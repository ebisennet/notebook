# Throttling

- API全体、あるいはAPIごとにRPSを制限する。
    - クライアントごとではない。
- AWSアカウント全体としての制限もあるので、こちらは上限緩和申請が必要。
- 制限にかかると 429 Too Many Requests を返す。
- SDK はリトライ機能を持つ。
- スロットリングのアルゴリズムはトークンバケット。
    - 参考：[トークンバケット - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%88%E3%83%BC%E3%82%AF%E3%83%B3%E3%83%90%E3%82%B1%E3%83%83%E3%83%88)

## 公式資料

- [API リクエストのスロットリングの管理 - Amazon API Gateway](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/api-gateway-request-throttling.html)
