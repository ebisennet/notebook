# カスタム認可（Custom Authorizer）

- Lambda を使って認可が行える。
    - 認証は別途行うことになる。
- 認可トークンを渡す方法設定可能
    - リクエストの特定ヘッダーやパラメーターなど
- API 認可成功時にポリシードキュメントを返す必要がある。
- API Gateway の API を OAuth などで保護可能。
    - [Amazon API Gateway の Custom Authorizer を使い、OAuth アクセストークンで API を保護する - Qiita](http://qiita.com/TakahikoKawasaki/items/b372ab49da0a9aedb76a)

## 公式資料

- [Introducing custom authorizers in Amazon API Gateway | AWS Compute Blog](https://aws.amazon.com/jp/blogs/compute/introducing-custom-authorizers-in-amazon-api-gateway/)
- [Amazon API Gateway カスタム認証を有効にする - Amazon API Gateway](http://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/use-custom-authorizer.html)
    - カスタム認可使う場合の一連の設定。

## 参考資料

- [Amazon API Gateway で Custom Authorization を使ってクライアントの認可を行う ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/api-gateway-custom-authorization/)
    - 認可としてのフローがわかりやすい。
- [Amazon API Gateway の Custom Authorizerを使い、User PoolsのユーザでAPI認証を行う - Qiita](http://qiita.com/horike37/items/7e0984e7729099032930)
