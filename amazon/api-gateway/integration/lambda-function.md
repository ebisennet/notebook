# Lambda Function

- [Lambda 関数の同期呼び出しを行う - Amazon API Gateway](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/getting-started.html)
- [AWS Lambda プロキシとして API Gateway API を作成する - Amazon API Gateway](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/integrating-api-with-aws-services-lambda.html)
- [API Gateway + Lambda にFormからPOSTする時のマッピングテンプレートを作成しました ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/sugano-013-api-gateway/)

