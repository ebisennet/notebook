# Mock Integration

リクエストに対して以下のようなレスポンスを返す。

- リクエスト JSON の statusCode プロパティで指定したステータスコードを返す。

## 公式資料

- [Introducing Mock integration: generate API responses from API Gateway directly](https://aws.amazon.com/jp/about-aws/whats-new/2015/09/introducing-mock-integration-generate-api-responses-from-api-gateway-directly/)
- [API Gateway でメソッドのモック統合を設定する - Amazon API Gateway](https://docs.aws.amazon.com/ja_jp/apigateway/latest/developerguide/how-to-mock-integration.html)

## 参考資料

- [Amazon API GatewayでMockを定義してみる - Qiita](http://qiita.com/Keisuke69/items/0852d536110b2fa6e087)
