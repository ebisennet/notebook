# RDS

## 公式資料

- [Amazon RDS（クラウドでのリレーショナルデータベースサービス） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/rds/)
- [Amazon Relational Database Service (Amazon RDS) とは? - Amazon Relational Database Service](http://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Welcome.html)
    - ユーザーガイド

## Topics

- DBエンジン別
    - [MySQL](mysql/)
    - MariaDB
    - PostgreSQL
    - SQL Server
    - Oracle
- DBパラメーターグループ
    - [DB パラメータグループを使用する - Amazon Relational Database Service](http://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_WorkingWithParamGroups.html)
- DBオプショングループ
    - [オプショングループを使用する - Amazon Relational Database Service](http://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_WorkingWithOptionGroups.html)
- DBサブネットグループ
    - [VPC 内の Amazon RDS DB インスタンスの使用 - Amazon Relational Database Service](http://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_VPC.WorkingWithRDSInstanceinaVPC.html)

## 参考資料

- [Amazon RDS のメンテナンスにどう立ち向かうべきか](http://blog.manabusakai.com/2016/01/rds-maintenance/)
    - 「Multi-AZ 構成にしていれば自動的にフェイルオーバーしますが、60 〜 120 秒のダウンタイムは避けれません。」
    - 「フェイルオーバーしている間はプライマリとスタンバイの両方にアクセスできない」
    - 「やはりフェイルオーバーしている間は ユーザーからの更新クエリを受け付けないのが鉄則 です。」
