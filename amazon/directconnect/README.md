# DirectConnect

## 公式資料

- [AWS Direct Connect（AWS クラウドとの専用線接続サービス） | アマゾン ウェブ サービス (AWS 日本語)](https://aws.amazon.com/jp/directconnect/)
- [AWS Direct Connect ドキュメント | AWS](https://aws.amazon.com/jp/documentation/directconnect/)
- [AWS Direct Connect とは何ですか - AWS Direct Connect](http://docs.aws.amazon.com/ja_jp/directconnect/latest/UserGuide/Welcome.html)
    - ユーザーガイド
- [AWS Direct Connect now provides Amazon CloudWatch Monitoring](https://aws.amazon.com/jp/about-aws/whats-new/2017/06/aws-direct-connect-now-provides-amazon-cloudwatch-monitoring/)
    - CloudWatch での監視が可能に。

## Topics

- パブリック接続
    - [AWS Solutions Architect ブログ: Direct Connectを利用して専用線経由でS3やDyamoDBへアクセスする (AWS Direct Connect のパブリック接続)](http://aws.typepad.com/sajp/2014/12/aws-direct-connect-public.html)
- pricing
    - 

### Virtual Interface

- Public
- Private

## 参考資料

- [AWS Direct ConnectでAWSへの専用ネットワークを接続｜AWS専業のcloudpack](http://cloudpack.jp/service/plan/direct-connect.html)
- [Amazon Direct Connectの”実際の”接続プランガイド ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/direct-connect-guide/)
    - 物理接続
    - BGPによる論理接続
    - 占有プランと共有プランの違い
    - link to [AWS Direct ConnectのShared Virtual Interfacesとは ｜ Developers.IO](http://dev.classmethod.jp/cloud/illustrate-direct-connect-shared-virtual-interfaces/)
        - １つの Direct Connect を複数の AWS アカウントから使う
        - Direct Connect が占有プランでないと使えない？
