# CodePipeline

CI/CD サービス

## 公式資料

- [AWS CodePipeline (継続的デリバリーとリリースの自動化） | AWS](https://aws.amazon.com/jp/codepipeline/)
- [What Is AWS CodePipeline? - AWS CodePipeline](http://docs.aws.amazon.com/ja_jp/codepipeline/latest/userguide/welcome.html)
- [AWS 開発者用ツールのまとめ – CodeCommit、CodePipeline、CodeDeploy に追加した最近の機能強化 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-developer-tool-recap-recent-enhancements-to-codecommit-codepipeline-and-codedeploy/)
- [CodePipeline の更新 – CloudFormation スタックの継続的配信ワークフローの構築 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/codepipeline-update-build-continuous-delivery-workflows-for-cloudformation-stacks/)
