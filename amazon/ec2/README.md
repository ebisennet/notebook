# EC2

仮想マシン、および物理マシンのサービス。

## 公式資料

- [Amazon EC2 (仮想クラウドサーバー) | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/ec2/)
- [Amazon EC2 とは - Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/concepts.html)
    - ユーザーガイド

## Topics

- [料金](pricing.md)
- Dedicated Hosts
    - 物理マシンを占有できる。
- Instance Profile
    - AWS SDK for PHP
        - http://docs.aws.amazon.com/aws-sdk-php/v2/guide/credentials.html
    - AWS SDK for Java
        - http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-roles.html
    - [既存のAmazon EC2インスタンスにIAM Roleがアタッチできるようになりました \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-attach-an-aws-iam-role-to-an-existing-amazon-ec2-instance-by-using-the-aws-cli/)
- 関連サービス
    - [EC2 Systems Manager](../ssm)

## Memo: CPU

- [EC2インスタンスのスペックについて｜amazonクラウドの活用ならcloudpack](http://cloudpack.jp/service/ec2-instance-spec.html)
    - 参考：[IBM（ソフトウェア） プロセッサーバリューユニット(PVU)課金](http://www.networld.co.jp/product/ibm-software/pro_info/licence/pvu)
- [日報 \#107 - AWSのEC2インスタンスのコア数を考慮しながらLoadAverageをきめる - 俺の報告](http://tom-rc.hatenablog.com/entry/2015/01/23/003711)
- [いまさら聞けない Amazon EC2](http://www.slideshare.net/understeer/20140317-ec2-instancyetype)

## 参考資料

- [スポットインスタンスを安定して利用するための取り組み - Qiita](http://qiita.com/megadreams14/items/766ce04ca6cb418e95ca)
    - [パワーアップしたスポットインスタンス - Qiita](http://qiita.com/megadreams14/items/b2099f33dce3b5b91e45)
