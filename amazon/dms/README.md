# Database Migration Service

- [Amazon Web Services ブログ: 【AWS発表】プレビュー：AWS Database Migration Service](http://aws.typepad.com/aws_japan/2015/10/now-in-preview-aws-database-migration-service.html)

## 公式資料

- [AWS Database Migration Service（データベースを簡単かつ安全に AWS へ移行） | AWS](https://aws.amazon.com/jp/dms/)
- [料金 - AWS Database Migration Service | AWS](https://aws.amazon.com/jp/dms/pricing/)
- [Amazon Web Services ブログ: AWS Database Migration Service (DMS)が一般公開に！利用可能リージョンも拡大](http://aws.typepad.com/aws_japan/2016/03/aws-database-migration-service.html)
- [Getting Started with AWS Database Migration Service - AWS Database Migration Service](http://docs.aws.amazon.com/ja_jp/dms/latest/userguide/CHAP_GettingStarted.html)

## 参考資料

- [AWS Database Migration Serviceの紹介](http://www.slideshare.net/nisanshin/aws-database-migration-service)
