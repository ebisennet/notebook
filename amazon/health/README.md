# AWS Personal Health Dashboard

AWS の状態確認。

CloudWatch Events との連携は無料。 API の利用はサポートプランがビジネスプラン以上からに限定。

## 公式資料

- [AWS Personal Health Dashboard – AWS Service Health & Notifications](https://aws.amazon.com/jp/premiumsupport/phd/)
- [What Is AWS Health? \- AWS Health](https://docs.aws.amazon.com/ja_jp/health/latest/ug/what-is-aws-health.html)
    - ユーザーガイド。
- [Class: AWS\.Health — AWS SDK for JavaScript](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Health.html)
