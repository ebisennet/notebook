# CloudFormation

AWS リソースに対するオーケストレーションサービス。

## 公式資料

- [AWS CloudFormation （設定管理とオーケストレーション） | AWS](https://aws.amazon.com/jp/cloudformation/)
- [AWS CloudFormation Documentation](https://aws.amazon.com/jp/documentation/cloudformation/)
- [AWS CloudFormation とは - AWS CloudFormation](http://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/Welcome.html)
    - ユーザーガイド
- 機能追加
    - [New – AWS CloudFormation の変更点 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-change-sets-for-aws-cloudformation/)

## Topics

- [リソースタイプ](resource-types/)
- Transform
    - Serverless リソース
        - [Introducing Simplified Serverless Application Deployment and Management \| AWS Compute Blog](https://aws.amazon.com/jp/blogs/compute/introducing-simplified-serverless-application-deplyoment-and-management/)
