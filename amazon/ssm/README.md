# EC2 Systems Manager

EC2 インスタンスやオンプレミスサーバーのシステム構成管理を行う。
Agent を EC2 インスタンスにインストールしておくことで リモートからソフトウェアインストールや設定変更を行える。

当初は Windows の構成管理のみサポートしていたが Linux も対応し始め、Lambda など EC2 以外からも使える機能を提供したあたりで
旧名 Simple Systems Manager から EC2 Systems Manager に名称変更された。

- API の名前空間などは旧名の略称 SSM が使われている点に注意。
- ドキュメントや Management Console 上は EC2 の一部となっていることに注意。

## 公式資料

- [Welcome \- Amazon EC2 Systems Manager](http://docs.aws.amazon.com/ssm/latest/APIReference/Welcome.html)
    - API Reference
- [Amazon EC2 Systems Manager \- Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/systems-manager.html)
    - ユーザーガイド

## Topics

- [Run Command](run-command)
- [SSM エージェント](ssm-agent)
- パラメーターストア
    - [パラメータストア \- Amazon EC2 Systems Manager \| AWS](https://aws.amazon.com/jp/ec2/systems-manager/parameter-store/)
        - [タスクIAMロールとパラメータストアを利用したAmazon ECSアプリケーションの秘密情報管理 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/managing-secrets-for-amazon-ecs-applications-using-parameter-store-and-iam-roles-for-tasks/)
- ステートマネージャー
- インベントリ
- メンテナンスウィンドウ
- オートメーション
- ドキュメント
    - [Systems Manager ドキュメント \- Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/sysman-ssm-docs.html)

## 参考

- [Simple Systems Manager](http://dev.classmethod.jp/cloud/aws-ssm-simple-systems-manager/)
- [AWS Solutions Architect ブログ: AWS Black Belt Online Seminar「Amazon EC2 Systems Manager」の資料およびQA公開](http://aws.typepad.com/sajp/2017/02/aws-black-belt-online-seminaramazon-ec2-systems-manager-slides-and-qa.html)
