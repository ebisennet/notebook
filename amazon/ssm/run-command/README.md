# Run Command

EC2インスタンスやオンプレミス環境にあるサーバーをリモートから管理するために
対象インスタンスに対して任意のコマンドを実行できる。

- SSMエージェントのインストールが必要。
- コマンドの実行を指示する側の IAM 設定が必要。
- EC2 に関連づける IAM Role に Systems Manager で管理するための許可が必要。

## 公式資料

- [EC2 Run Commandアップデート – コマンドの管理と共有など | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/ec2-run-command-update-manage-share-commands-and-more/)
- [インスタンスをリモートで管理する - Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/execute-remote-commands.html)
    - ユーザーガイド
- [マネージドインスタンスの設定 - Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/managed-instances.html)
    - オンプレミス環境のサーバーを設定する手順。

## 参考資料

- [EC2 Run CommandがLinux対応したので簡易実行コマンドを作ってみた \- Qiita](http://qiita.com/kawaz/items/10921dcb9a5f7326bc10)
