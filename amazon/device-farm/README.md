# Device Farm

## 公式資料

- [AWS Device Farm （AWS クラウドで実際のデバイスを使用してアプリをテスト） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/device-farm/)
- [What Is AWS Device Farm? - AWS Device Farm](http://docs.aws.amazon.com/ja_jp/devicefarm/latest/developerguide/welcome.html)
- [Working with Test Types in AWS Device Farm - AWS Device Farm](http://docs.aws.amazon.com/ja_jp/devicefarm/latest/developerguide/test-types-intro.html)
    - Device Farm でサポートしているテストフレームワーク
    - iOS, Android 固有のテストフレームワーク以外にも、OSS のクロスプラットフォームなテストフレームワークもサポート。
        - Android 固有
            - Instrumentation
            - UI Automator
        - iOS 固有
            - XCTest
            - UI Automation
        - クロスプラットフォーム
            - Appium Java JUnit
            - Appium Java TestNG
            - Calabash
- [よくある質問 - AWS Device Farm | AWS](https://aws.amazon.com/jp/device-farm/faq/)
    - 機能概要について一通り書かれている。
- [AWS Device Farm のアップデート – デバイスにリモートアクセスしてインタラクティブなテストが可能に | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-device-farm-update-remote-access-to-devices-for-interactive-testing/)
    - ブラウザ上から対象デバイスを操作可能になる。

## Topics

- テストタイプ別
    - [Appium](appium.md) (iOS/Android)
    - [Calabash](calabash.md) (iOS/Android)
    - [Instrumentation](instrumentation.md) (Andoird)
    - [UI Automation](ui-automation.md) (iOS)
    - [UI Automator](ui-automator.md) (Android)
    - [XCTest](xctest.md) (iOS)
- テスト結果
    - 合否情報、クラッシュレポート、テストログ、デバイスログ、スクリーンショット、およびパフォーマンスデータ
- Tools
    - Jenkins Plugin
        - [AWS Device Farm Integration with Jenkins CI Plugin - AWS Device Farm](http://docs.aws.amazon.com/ja_jp/devicefarm/latest/developerguide/continuous-integration-jenkins-plugin.html)
- 料金
    - $0.17/Device/Min

## Memo

- どのテストタイプでも、アプリ本体とテストをそれぞれアップロードする形。
    - テストファイル
        - Appium : zip
            - jar および依存 jar を含む
        - Calbash :
        - Instrumentation : apk
            - テストケースを含む
        - UI Automation : js
        - XCTest : zip
            - project.xctest を含む
- テスト実行
    - 通常は数分以内に開始
    - デバイスが利用できない場合は、実行キューに登録される

## 参考資料

- [DeviceFarm - Amazon Device FarmをJenkinsCIに導入する - Qiita](http://qiita.com/oh_rusty_nail/items/b59aca5f7ec54c51c663)
    - Device Farm を呼び出す Jenkins プラグイン
