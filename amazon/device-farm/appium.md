# Appium

iOS/Android のテスト自動化ツール。

- [スマホ向け無料システムテスト自動化ツール（8）：SeleniumのUIテスト自動化をiOS／AndroidにもたらすAppiumの基礎知識とインストール方法、基本的な使い方 (1/4) - ＠IT](http://www.atmarkit.co.jp/ait/articles/1504/27/news025.html)

Appium 自体はクライアントとサーバーに分かれる。

## Appium サーバー

Appium サーバーから各プラットフォーム固有のテストが呼ばれる。

- [appium/intro.md at master · appium/appium · GitHub](https://github.com/appium/appium/blob/master/docs/en/about-appium/intro.md)
    - Apple's UIAutomation
    - Google's UiAutomator
    - Google's Instrumentation

## Appium クライアント

Appium クライアントでは各種言語でテストをかける。

- [appium/appium-clients.md at master · appium/appium · GitHub](https://github.com/appium/appium/blob/master/docs/en/about-appium/appium-clients.md)

Device Farm では Java のみ。

- Appium Java JUnit
- Appium Java TestNG
