# Instrumentation

Android SDK 付属のテスト自動化ツール。

- [Testing Fundamentals | Android Developers](http://developer.android.com/tools/testing/testing_android.html)

実機上で HTTP Server を動かすことでテスト実行を呼び出せる仕組み。 HTTP Server を組み込んだテスターアプリは、テスト対象アプリと同じ開発者証明書で署名されている必要がある。

Android 4.1 以上であれば [UI Automator](ui-automator.md) の方が良い。
