# UI Automator

Android 4.3 以上であれば使える標準のテスト自動化ツール。

[Testing UI for Multiple Apps | Android Developers](http://developer.android.com/training/testing/ui-testing/uiautomator-testing.html)

- [スマホ向け無料システムテスト自動化ツール（2）：Android SDK標準の何でもテストツールuiautomatorの基本的な使い方 (1/3) - ＠IT](http://www.atmarkit.co.jp/ait/articles/1410/07/news029.html)
- 他者のアプリでも自動操作できる。
- 手順としては以下
    1. テスト用Javaプロジェクト作成
    2. Java でテストを書いて、ビルドする。
    3. ビルド結果の jar を実機に転送
    4. adb shell で uiautomator コマンドを実行
