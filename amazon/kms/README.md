# KMS

## 公式資料

- [AWS Key Management Service \(暗号化キーを簡単に作成・管理\) \| AWS](https://aws.amazon.com/jp/kms/)
- [新発表 – AWS Key Management ServiceでのBring Your Own Keys機能 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-bring-your-own-keys-with-aws-key-management-service/)
