# Support

## 公式資料

- [AWS サポート｜AWS](https://aws.amazon.com/jp/premiumsupport/?nc1=f_dr)
- [AWS Support Documentation](http://aws.amazon.com/jp/documentation/aws-support/)
- [Welcome - AWS Support](http://docs.aws.amazon.com/ja_jp/awssupport/latest/APIReference/Welcome.html)

## プラン別

### Basic

- Free

### Developer

- $49/month

### Business

- Minimum Charge
    - $100/month
- Health API が使えるようになる。

### Enterprise

- Minimum Charge
    - $15,000/month

Business に専任の TAM によるサービスが追加される。
