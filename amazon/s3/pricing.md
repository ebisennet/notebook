# 料金

1GB = $0.033/Month と考えてOK.

- 低頻度アクセスストレージ $0.019/GB
    - 標準の 2/3 程度
- Glacier ストレージ $0.0114/GB
    - 標準の 1/3 程度

## 公式資料

- [料金 - Amazon S3 (クラウドストレージサービス Amazon Simple Storage Service) | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/s3/pricing/)

