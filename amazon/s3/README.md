# S3

## 公式資料

- [Amazon S3 (クラウドストレージサービス ) | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/s3/)
- [Amazon Simple Storage Service の使用開始 - Amazon Simple Storage Service](http://docs.aws.amazon.com/ja_jp/AmazonS3/latest/gsg/GetStartedWithS3.html)
    - 入門ガイド
- [Amazon S3 とは何ですか？ - Amazon Simple Storage Service](http://docs.aws.amazon.com/ja_jp/AmazonS3/latest/dev/Welcome.html)
    - 開発者ガイド
- [革新、S3ストレージ管理の4つの新機能 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/revolutionizing-s3-storage-management-with-4-new-features/)

## Topics

- [料金](pricing.md)

## 参考資料

- [S3 static site with SSL and automatic deploys using Travis](http://laszlo.nu/2016/11/25/s3-static-site.html)
