# EFS (Elastic File System)

Managed NFS.

## 公式資料

- [Amazon Elastic File System が 3 つのリージョンで利用可能に | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-elastic-file-system-production-ready-in-three-regions/)
    - us-east-1, us-west-2, eu-west-1
