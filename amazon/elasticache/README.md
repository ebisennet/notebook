# ElastiCache

インメモリキャッシュサービス。

以下の OSS のマネージドサービス。

- Redis
- Memcached

## 公式資料

- [Amazon ElastiCache （キャッシュ管理・操作サービス） | AWS](https://aws.amazon.com/jp/elasticache/)
- [Amazon ElastiCache アップデート – RedisのSnapshotをAmazon S3へエクスポートする事が可能になりました | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-elasticache-update-export-redis-snapshots-to-amazon-s3/)
