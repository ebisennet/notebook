# VPC Flow Logs

- [【新機能】VPC Flow LogsでVPC内のIPトラフィックを監視することができるようになりました! ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/introduce-to-vpc-flow-log/)
- [VPC フローログ - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/flow-logs.html)

