# VPC Peering

VPC 同士をつなぐ。

１つのAWSアカウント内の複数のVPC間での接続、およびクロスアカウントでのVPC間接続に対応。

- [VPC ピア機能とは - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/PeeringGuide/Welcome.html)
- [Amazon VPC Peeringの技術的考察と留意点 ｜ Developers.IO](http://dev.classmethod.jp/cloud/vpc-peering-restricts-and-considerations/)
    - AZ またぎは可能。
    - リージョン越えは不可能。
    - L3ネットワーク通信。
    - 2VPC間以外のルーティングは取り持てない。
    - Peering 経由で IGW / VGW を使えない。
- [異なるAWSアカウントでのVPCピア接続を試してみた ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/vpc-peering-different-awsaccount/)
- 設定
    - DNS 設定
        - DNS クエリーに対してパブリックIPを返すか、プライベートIPを返すか設定できる。

