# Subnet

- Public Subnet
    - Internet GW がアタッチされているサブネット。
    - Public Subnet 内の ENI は、Public IP も持つことになる。
- Subnet が持つネットワークセグメントは VPC が持つセグメントを分割したものとなる点に注意。
    - Subnetのネットワークアドレスは、VPCのネットワークアドレスのサブネットに制限される。
- Subnet の最初の４つのIPアドレスと最後の１つのIPアドレスは予約されている。
    - [VPC とサブネット - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Subnets.html)
        - 予約されているアドレスの情報の記載あり。
