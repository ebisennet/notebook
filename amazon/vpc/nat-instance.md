# NAT Instance

- AMIに amzn-ami-vpc-nat が含まれる
- 通常は NAT Gateway のほうが可用性などに優れている。

## 参考資料

- [Amazon VPCでELBとNATを使ってよりセキュアな環境を作る【5日目】 ｜ Developers.IO](http://dev.classmethod.jp/cloud/amazon-vpc-elb-nat/)
    - NATインスタンス構築時のEC2インスタンス設定について注意事項があり
    - NATインスタンスの Dest Check を Disable に。
