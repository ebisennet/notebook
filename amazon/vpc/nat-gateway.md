# NAT Gateway

- Managed NAT
    - AWSによって運用されているNAT
- 踏み台サーバーには使えない
    - NAT Instance は使える
- ネットワークACLで一時ポートの許可が必要
    - [VPC に推奨されるネットワーク ACL ルール - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Appendix_NACLs.html)
- [NAT ゲートウェイ - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/vpc-nat-gateway.html)
- [New – Managed NAT (Network Address Translation) Gateway for AWS | AWS Official Blog](https://aws.amazon.com/jp/blogs/aws/new-managed-nat-network-address-translation-gateway-for-aws/)
- [Amazon Web Services ブログ: 【新機能】マネージドNATゲートウェイが利用可能に](http://aws.typepad.com/aws_japan/2015/12/managednat.html)
- [【新機能】ついに登場！Amazon VPC NAT GatewayでNATがAWSマネージドに ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/introduce-to-amazon-vpc-nat-gateway/)

