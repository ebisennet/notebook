# Elastic Network Interface (ENI)

- いわゆる仮想NIC
- １つのEC2インスタンスに複数ENIの割り当て可能。
    - 最大数はインスタンスタイプに依存。
    - プライマリーENIはデタッチ不可能。
- １つのENIで複数のIPアドレスを持ちうる。
- [VPC で Elastic Network Interface を使用する - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_ElasticNetworkInterfaces.html)

