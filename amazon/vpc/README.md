# VPC

## 公式資料

- [Amazon VPC （仮想プライベートクラウド Amazon Virtual Private Cloud）| アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/vpc/)
- [料金 - Amazon VPC | AWS](https://aws.amazon.com/jp/vpc/pricing/)
    - VPN接続とNATゲートウェイで料金発生
- [Amazon Virtual Private Cloud（VPC）ドキュメント | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/documentation/vpc/)
- [Amazon VPC とは? - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Introduction.html)
    - ユーザーガイド
    - [シナリオ 3: パブリックおよびプライベートのサブネットを持つ VPC とハードウェア VPN アクセス - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Scenario3.html)
    - [シナリオ 4: プライベートサブネットのみを持つ VPC とハードウェア VPN アクセス - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Scenario4.html)

## 関連

- [Direct Connect](../directconnect/)

## 構成要素

- [VPC](vpc.md)
- [VPC Peering](vpc-peering.md)
- [Subnet](subnet.md)
- [Virtual Gateway (VGW)](vgw.md)
- [Internet Gateway (IGW)](igw.md)
- [NAT Gateway](nat-gateway.md)
- [NAT Instance](nat-instance.md)
- [Elastic Network Interface (ENI)](eni.md)
- [Network ACL](network-acl.md)

## Topics

- [VPC Flow Logs](vpc-flow-logs.md)

## VPC 内で稼働可能なサービス

- EC2
- S3
    - VPC Endpoint for S3
- RDS
    - [Virtual Private Cloud（VPC）および Amazon RDS - Amazon Relational Database Service](http://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/USER_VPC.html)
- ElastiCache
- ELB
- Redshift
- NAT
    - [シナリオ 2: パブリックサブネットとプライベートサブネットを持つ VPC（NAT） - Amazon Virtual Private Cloud](http://docs.aws.amazon.com/ja_jp/AmazonVPC/latest/UserGuide/VPC_Scenario2.html)
        - プライベートサブネットからのインターネットアクセスが必要な場合の構成。
            - パブリックサブネットに NAT インスタンス配置が必要。
            - プライベートサブネットから NAT インスタンスへのルーティングが必要。
        - パブリックサブネットがいわゆる DMZ として機能する。
- Lambda

## VPC 内で稼働しないサービス

- API Gateway

## Multi AZ

- VPC を Multi AZ で運用するには、AZごとに Subnet を切る
    - [今更 VPC で 複数の AZ をまたいだ ELB を試す（1） - ようへいの日々精進XP](http://inokara.hateblo.jp/entry/2013/12/31/010647)

## 参考資料

- [AWSマイスターシリーズ Amazon VPC VPN & Direct Connect](http://www.slideshare.net/AmazonWebServicesJapan/20130904-aws-meisterregeneratevpcdxvpn)
- [VPCの構成まとめ - Qiita](http://qiita.com/bobu_web/items/ab2c0d9180f016bdaa4a)
