# IAM

## 公式資料

- [AWS Identity and Access Management（IAM クラウドのアクセス・認証管理）| AWS](https://aws.amazon.com/jp/iam/)
- [AWS Identity and Access Management（IAM）ドキュメント | AWS](https://aws.amazon.com/jp/documentation/iam/)

## Topics

- User
- Group
- Role
- [Policy](policy.md)
- [MFA](mfa.md)
