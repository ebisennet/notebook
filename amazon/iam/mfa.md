# MFA

- [AWS Developer Forums: Windows PC as virtual MFA ? ...](https://forums.aws.amazon.com/message.jspa?messageID=530133)
    - Windows 用の TOTP アプリについて。
    - link to [Windows Authenticator for Google, Blizzard, Guild Wars, Rift](https://winauth.com/)
- SMS を使う場合は以下の記事のような状況を加味した上で。
    - [携帯電話ネットワークのハッキングは簡単か？ | Kaspersky Daily - カスペルスキー公式ブログ](https://blog.kaspersky.co.jp/hacking-cellular-networks/9681/)
