# Policy

## 公式資料

- [AWS Policy Generator](http://awspolicygen.s3.amazonaws.com/policygen.html)
    - ポリシー作成ツール
- [IAM ポリシーエレメントの参照 \- AWS Identity and Access Management](http://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/reference_policies_elements.html)
- [IAM ポリシーで使用できる AWS サービスアクションと条件コンテキストキー \- AWS Identity and Access Management](http://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/reference_policies_actionsconditions.html)

## 参考資料

- [IAMによるAWS権限管理 – IAMポリシーの記述と評価論理 ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/iam-policy-evaluation-logic/)
- [IAM Policy Statementにおける NotAction / NotResource とは？ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/notaction-and-notresource-in-iam-policy-statement/)

