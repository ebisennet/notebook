# Mobile Analytics

## 公式資料

- [Amazon Mobile Analytics（アプリケーション使用状況データの大規模収集・視覚化サービス） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/mobileanalytics/)

## Topics

- [料金](price.md)
- カスタムイベント
- カスタムダッシュボード
    - [Amazon Mobile Analytics で Custom Dashboard が作れるようになりました ｜ Developers\.IO](http://dev.classmethod.jp/cloud/aws/mobile-analytics-custom-dashboard/)

## 参考資料

- [Amazon Mobile Analyticsのいろは](http://www.slideshare.net/yasuhisaarakawa/amazon-mobile-analytics)
