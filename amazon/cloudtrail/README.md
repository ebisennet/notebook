# CloudTrail

AWS の API 呼び出し監査のためのサービス。

API利用履歴を保管できる。

## 公式資料

- [AWS CloudTrail (AWS API の呼び出し記録とログファイル送信） | AWS](https://aws.amazon.com/jp/cloudtrail/)
- [CloudTrail Supported Services - AWS CloudTrail](http://docs.aws.amazon.com/ja_jp/awscloudtrail/latest/userguide/cloudtrail-supported-services.html)
    - 監視できるもの

## Topics

- [料金](pricing.md)
- 連携サービス
    - S3
    - CloudWatch Logs

## 参考資料

- [CloudTrailとCloudWatch Logsの連携によるログのアラーム設定 ｜ Developers.IO](http://dev.classmethod.jp/cloud/cloudtrail-cloudwatch-logs-sns/)
