# AWS Certificate Manager

SSL証明書発行サービス。 無料。
証明書更新は自動で行われる。

ELB や CloudFront と組み合わせて使う。

CloudFront と組み合わせる場合は us-east-1 の ACM を使う必要がある点に注意。

現在は 3rd-party の証明書をインポートすることもできる。

## 公式資料

- [AWS Certificate Manager](https://aws.amazon.com/jp/certificate-manager/)
- [New – AWS Certificate Manager – Deploy SSL/TLS-Based Apps on AWS | AWS Official Blog](https://aws.amazon.com/jp/blogs/aws/new-aws-certificate-manager-deploy-ssltls-based-apps-on-aws/)
- [Welcome - AWS Certificate Manager](http://docs.aws.amazon.com/ja_jp/acm/latest/APIReference/Welcome.html)
    - API
- [What is AWS Certificate Manager? - AWS Certificate Manager](http://docs.aws.amazon.com/ja_jp/acm/latest/userguide/acm-overview.html)
    - ユーザーガイド

## 参考資料

- [AWS Certificate Manager が発行するサーバ証明書を調べてみた](http://blog.manabusakai.com/2016/01/aws-certificate-manager/)
- [AWS Certificate Manager 東京でも無料でSSL証明書が使用可能になりました！ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/acm-available-in-tokyo/)
    - 2016年5月から。
