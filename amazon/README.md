# Amazon

## 公式資料

- [AWS Service Health Dashboard](http://status.aws.amazon.com/)
- [AWS サポート のプラン選択 - AWS サポート | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/premiumsupport/signup/)
- [AWS シンプルアイコン - AWS アーキテクチャーセンター | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/architecture/icons/)
    - 資料作成用に。
- [Amazon Web Services ブログ](http://aws.typepad.com/aws_japan/)
- [AWS サービス制限 - アマゾン ウェブ サービス](http://docs.aws.amazon.com/ja_jp/general/latest/gr/aws_service_limits.html)
    - 各サービスの制限事項がまとまっている。
- [Release Notes : Amazon Web Services](https://aws.amazon.com/releasenotes)

## サービス別

| サービス名                                              | 備考 |
|---------------------------------------------------------+------|
| AWS Step Functions                                      |      |
| AWS Glue                                                |      |
| [AWS Batch](batch/)                                     |      |
| AWS CodeBuild                                           |      |
| AWS Shield                                              |      |
| AWS X-Ray                                               |      |
| Amazon Pinpoint                                         |      |
| [AWS Personal Health Dashboard](health/)                |      |
| [AWS Organizations](organizations/)                     |      |
| [API Gateway](api-gateway/)                             |      |
| [Aurora](aurora/)                                       |      |
| [Autoscaling](autoscaling/)                             |      |
| [Artifact](artifact/)                                   |      |
| [AWS Certificate Manager](acm/)                         |      |
| [Application Discovery Service](application-discovery/) |      |
| [CloudDirectory](clouddirectory/)                       |      |
| [CloudFormation](cloudformation/)                       |      |
| [CloudFront](cloudfront/)                               |      |
| CloudHSM                                                |      |
| [CloudTrail](cloudtrail/)                               |      |
| [CloudWatch](cloudwatch/)                               |      |
| [CodeCommit](codecommit/)                               |      |
| [CodeDeploy](codedeploy/)                               |      |
| [CodePipeline](codepipeline/)                           |      |
| [Cognito](cognito/)                                     |      |
| [Config](config/)                                       |      |
| [Database Migration Service](dms/) (DMS)                |      |
| [Device Farm](device-farm/)                             |      |
| [DirectConnect](directconnect/)                         |      |
| [DirectoryService](directoryservice/README.md)          |      |
| [EBS](ebs/)                                             |      |
| [EC2](ec2/)                                             |      |
| [ECS](ecs/)                                             |      |
| [EFS](efs/)                                             |      |
| [ElasticSearch Service](es/)                            |      |
| [ElastiCache](elasticache/)                             |      |
| [ELB](elb/)                                             |      |
| [EMR (Elastic MapReduce)](emr/)                         |      |
| [IAM](iam/)                                             |      |
| [Inspector](inspector/)                                 |      |
| [Kinesis](kinesis/)                                     |      |
| [KMS](kms/)                                             |      |
| [Lambda](lambda/)                                       |      |
| [Managed Services](managed-services/)                   |      |
| [Mobile Analytics](mobileanalytics/)                    |      |
| [QuickSight](quicksight/)                               |      |
| [RDS](rds/)                                             |      |
| [Redshift](redshift/)                                   |      |
| [S3](s3/)                                               |      |
| Service Catalog                                         |      |
| [Server Migration Service](server-migration-service/)   |      |
| [SES](ses/)                                             |      |
| [SNS](sns/)                                             |      |
| [SQS](sqs/)                                             |      |
| [Simple Systems Manager](ssm/)                          |      |
| [Storage Gateway](storagegateway/)                      |      |
| [STS](sts/)                                             |      |
| [VPC](vpc/)                                             |      |
| [WAF](waf/)                                             |      |
| [VMware Cloud on AWS](vmware-cloud/)                    |      |
| [WorkDocs](workdocs/)                                   |      |
| [WorkMail](workmail/)                                   |      |
| [Workspaces](workspaces/)                               |      |

## Topics

- [AWS 全般のリファレンス - アマゾン ウェブ サービス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/Welcome.html)
- [Support](support.md)
- Status
    - [AWS Service Health Dashboard](http://status.aws.amazon.com/)
- ARN
    - [Amazon リソースネーム（ARN）と AWS サービスの名前空間 - アマゾン ウェブ サービス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/aws-arns-and-namespaces.html)
- [無料利用枠](free-tier.md)
- [セキュリティ](security/)
- [可用性](availability.md)
- [Tools](tools/)
- [Serverless](serverless.md)
- [SDK](sdk/)
- [制限](limits.md)
- [クラウドデザインパターン](cloud-design-pattern.md)
- [リージョン](regions.md)
- API リクエストへの署名
    - [AWS API リクエストへの署名 - アマゾン ウェブ サービス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/signing_aws_api_requests.html)
- [AWS IP アドレスの範囲 - アマゾン ウェブ サービス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/aws-ip-ranges.html)
- パートナー関連
    - [新しいAWSコンピテンシー – AWS移行 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-aws-competency-aws-migration/)
- Use case
    - [旭硝子が明かす、AWS基幹系導入の壁：ITpro](http://itpro.nikkeibp.co.jp/atcl/column/15/081900193/?TOC=1)
    - [ソニー銀行のクラウド活用によるITコスト構造改革、その内容と効果 － Publickey](http://www.publickey1.jp/blog/15/it_9.html)
- エンタープライズ
    - [シリコンバレーNextレポート - 米Amazon、「価格10分の1」でエンタープライズ市場への攻勢強める：ITpro](http://itpro.nikkeibp.co.jp/atcl/column/15/061500148/102200029/)
    - [現在、最強のエンタープライズIT企業はAmazon AWSだ | TechCrunch Japan](http://jp.techcrunch.com/2015/10/13/20151012is-aws-the-most-important-enterprise-company/)
- Price List API
    - [Amazon Web Services ブログ: 【AWS発表】新しいAWS Price List API](http://aws.typepad.com/aws_japan/2015/12/new-aws-price-list-api.html)
- Cloud Adoption Framework
    - [AWS Cloud Adoption Framework](https://aws.amazon.com/jp/professional-services/CAF/)

## 参考資料

- [【備忘録】AWSの最新情報が公開される場所をまとめてみた ｜ Developers\.IO](http://dev.classmethod.jp/cloud/aws/latest-information-about-aws/)
