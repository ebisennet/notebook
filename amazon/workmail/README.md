# AWS WorkMail

## 公式資料

- [Amazon WorkMail （セキュリティに優れたマネージド型の企業向けE メールおよびカレンダーサービス） \| AWS](https://aws.amazon.com/jp/workmail/)

## 参考資料

- [AWS WorkMailを使ってみたら想像以上に便利だった \- Qiita](http://qiita.com/ysKey2/items/2b019337772f8499beec)
