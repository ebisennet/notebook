# WAF

CloudFront と組み合わせて使う。

## 公式資料

- [AWS WAF（ウェブアプリケーションファイアウォール）| アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/waf/)
- [Amazon Web Services ブログ: 【AWS発表】新サービス: AWS WAF](http://aws.typepad.com/aws_japan/2015/10/waf.html)

## 料金

- Web ACL １つにつき $5
- ルールごとに $1
- 100万リクエストごとに $0.60

## 参考資料

- [AWS WAFを使うためシステムにCloudFrontを導入した時の注意点まとめ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/setup-amazon-waf-and-cloudfront/)
    - WAFを使うだけの場合、CloudFrontのキャッシュは緩める

