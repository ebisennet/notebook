# Subscriptions

## プロトコル

以下がサポートされている。

- http / https
- mail / mail-json
- SMS
- Amazon SQS
- Lambda
- Application
    - Mobile Push

## エンドポイント

プロトコルによって URL だったり、メールアドレスだったりを指定する。

## Lambda 関数呼び出し

- [Amazon SNS 通知を使用した Lambda 関数の呼び出し - Amazon Simple Notification Service](https://docs.aws.amazon.com/ja_jp/sns/latest/dg/sns-lambda.html)
- リージョンまたぎ（別リージョンのSubscription登録）が可能らしい。
    - [CloudWatchのAlertをAWS Lambda経由でSlackに飛ばす - Qiita](http://qiita.com/tmtysk/items/7161b11e20ac5e2dfc01)
