# SNS

イベント通知の配信サービス。

## 公式資料

- [Amazon Simple Notification Service（プッシュ方式のメッセージ送信 Amazon SNS） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/sns/)
- [Amazon Simple Notification Service（SNS）ドキュメント | アマゾン ウェブ サービス（AWS 日本語）](http://aws.amazon.com/jp/documentation/sns/)
- [Welcome - Amazon Simple Notification Service](http://docs.aws.amazon.com/ja_jp/sns/latest/api/Welcome.html)
    - API Reference
- [Amazon Simple Notification Service とは - Amazon Simple Notification Service](http://docs.aws.amazon.com/ja_jp/sns/latest/dg/welcome.html)
    - 開発者ガイド

## Topics

- Pricing
    - [料金 - Amazon Simple Notification Service（プッシュ方式のメッセージ送信 Amazon SNS） | アマゾン ウェブ サービス（AWS 日本語）](http://aws.amazon.com/jp/sns/pricing/)
- [Topics](topics.md)
- [Subscriptions](subscriptions.md)
- [事例](case-studies.md)

## Lambda からの呼び出し

- [LambdaからSNS送信 - Qiita](http://qiita.com/web_se/items/7ba375a56391f693aab5)

## Mobile Push

以下を参照。

- [Mobile Push](mobile-push/)
