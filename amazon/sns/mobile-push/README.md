# Mobile Push

## 公式資料

- [Amazon SNS モバイルプッシュ API の使用 - Amazon Simple Notification Service](http://docs.aws.amazon.com/ja_jp/sns/latest/dg/mobile-push-api.html)
    - モバイルプッシュで使う API の概要。
    - CreatePlatformApplication は管理画面で作成。
    - CreatePlatformEndpoint をアプリから呼び出し。
    - Publish はサーバーアプリから呼び出し。

## Topics

- [アプリケーション登録](register-apps.md)


## Memo

- EndPoint にカスタムデータを紐付け可能だが、findBy がないので使いにくい。
    - [Amazon SNS: How to get EndpointArn by token(registrationId) using amazon .net sdk? - Stack Overflow](http://stackoverflow.com/questions/22501373/amazon-sns-how-to-get-endpointarn-by-tokenregistrationid-using-amazon-net-sd)
- CreatePlatformApplication
    - 登録時に以下の情報が必要。
        - APNs : .p12 ファイルとパスワード
        - APNs Sandbox : .p12 ファイルとパスワード
        - GCM (FCM) : API Key (Server Key)
    - 登録結果として PlatformApplicationArn

## 参考資料

- [AWS（SNS, Cognito）を用いてiOSアプリにプッシュ通知するまでの手順（アプリ編） - Qiita](http://qiita.com/MJeeeey/items/e6c927118748fc61a5bc)
    - この例では Platform Endpoint の作成をアプリで行っている。
