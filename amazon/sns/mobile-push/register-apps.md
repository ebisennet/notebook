# アプリケーション登録

## 公式資料

- [モバイルアプリの AWS への登録 - Amazon Simple Notification Service](http://docs.aws.amazon.com/ja_jp/sns/latest/dg/mobile-push-send-register.html)

## APNs / APNs Sandbox

.p12 ファイルとそのパスワードが必要。

## GCM

サーバー API キーが必要。
