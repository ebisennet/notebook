# 事例

- [AWS 導入事例: 株式会社トランスリミット \| AWS](https://aws.amazon.com/jp/solutions/case-studies/translimit/)
    - 1000万以上のユーザーに対するプッシュ通知
    - ゲームの事例なので通知の量としては相当な量がある
