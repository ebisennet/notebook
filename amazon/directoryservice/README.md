# AWS Directory Service

## 公式資料

- [AWS Directory Service（クラウド上の管理型ディレクトリ） \| AWS](https://aws.amazon.com/jp/directoryservice/)
- [AWS Microsoft ADとオンプレミスの資格情報を使用してAWS管理コンソールにアクセスする方法 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/how-to-access-the-aws-management-console-using-aws-microsoft-ad-and-your-on-premises-credentials/)
    - ADFS 使った SSO
