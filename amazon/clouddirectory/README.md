# Cloud Directory

階層データの管理サービス。
LDAP などと同じカテゴリ。

Cognito や AWS Organizations 内部ですでに使われている。

ドキュメント体系や Management Console 上は Directory Service の１つとして扱われているが、内部的には別のサービス。

## 公式資料

- [Amazon Cloud Directory – マネージド型クラウドネイティブディレクトリサービス – AWS](https://aws.amazon.com/jp/cloud-directory/)
- [Amazon クラウドディレクトリ – 階層データ用のクラウドネイティブなディレクトリ \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-cloud-directory-a-cloud-native-directory-for-hierarchical-data/)

## Topics

- 料金
    - [Amazon Cloud Directory の料金 – アマゾン ウェブ サービス \(AWS\)](https://aws.amazon.com/jp/cloud-directory/pricing/)

## 参考資料

- [Amazon Cloud Directoryがリリースされたので触ってみました – 管理コンソールから新規作成 – ｜ Developers\.IO](http://dev.classmethod.jp/cloud/amazon-cloud-directory-released/)
