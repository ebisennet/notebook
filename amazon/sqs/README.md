# SQS

## 公式資料

- [Amazon Simple Queue Service（メッセージキューサービス） \| AWS](https://aws.amazon.com/jp/sqs/)
- [Amazon Simple Queue Service とは \- Amazon Simple Queue Service](http://docs.aws.amazon.com/ja_jp/AWSSimpleQueueService/latest/SQSDeveloperGuide/Welcome.html)
    - 開発者ガイド
- [Welcome \- Amazon Simple Queue Service](http://docs.aws.amazon.com/ja_jp/AWSSimpleQueueService/latest/APIReference/Welcome.html)
    - API

## Topics

- FIFO
    - [Amazon Simple Queue Service の新機能 – 1 回だけの処理と重複排除機能を備えた FIFO キュー \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-for-amazon-simple-queue-service-fifo-queues-with-exactly-once-delivery-deduplication/)

## 参考資料

- 
