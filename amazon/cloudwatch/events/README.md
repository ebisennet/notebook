# CloudWatch Events

## 公式資料

- [Amazon CloudWatch Events とは? - Amazon CloudWatch Events](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html)
    - CloudWatch Events のユーザーガイド
- [Amazon Web Services ブログ: 【AWS発表】CloudWatch Events – AWSリソースの変更を監視](http://aws.typepad.com/aws_japan/2016/01/new-cloudwatch-events-track-and-respond-to-changes-to-your-aws-resources.html)

## Topics

- [料金](pricing.md)
- イベントタイプ
    - [Event Types for CloudWatch Events \- Amazon CloudWatch Events](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/EventTypes.html)
    - サービスのイベント
        - EBS, EC2, EC2 System Manager, ECS, AutoScaling, CodeDeploy, Health, KMS, Trusted Advisor
    - その他のイベント
        - Scheduled Events
        - API 呼び出し
        - Console サインイン

## 参考資料

- [AWS CloudWatch Events + Amazon SNSで、AWS管理コンソールのサインインを検知しメールで通知する ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/check-amc-signin-using-cloudwatch-events/)
