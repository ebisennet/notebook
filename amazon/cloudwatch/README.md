# CloudWatch

## 公式資料

- [AWS CloudWatch （クラウドおよびネットワークモニタリング）| AWS](https://aws.amazon.com/jp/cloudwatch/)
- [料金 - Amazon CloudWatch （リソースとアプリケーションのモニタリング） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/cloudwatch/pricing/)
- [Amazon CloudWatch ドキュメント | AWS](https://aws.amazon.com/jp/documentation/cloudwatch/)
- [Amazon CloudWatch とは? - Amazon CloudWatch](http://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html)
    - 開発者ガイド
- [Amazon CloudWatch の名前空間、ディメンション、メトリックスのリファレンス - Amazon CloudWatch](http://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/monitoring/CW_Support_For_AWS.html)
    - サポートしている各サービスで何を監視しているか
- [AWS の値下げ – CloudWatch メトリクスの料金 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-price-reduction-cloudwatch-custom-metrics/)

## Topics

- [サポート対象の AWS サービス - Amazon CloudWatch](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/DeveloperGuide/supported_services.html)
- CloudWatch を使う AWS サービス
    - API Gateway
        - [Amazon API Gatewayを使い倒す！詳細な機能の解説 | つかびーの技術日記](http://tech-blog.tsukaby.com/archives/1071)
            - ステージ単位、メソッド単位で CloudWatch設定が可能。
            - メトリクスは、5xxエラー数、キャッシュヒットミス、レイテンシーなど
    - AutoScaling
    - DynamoDB
    - EBS
    - EC2
    - ElastiCache
    - ELB
    - EMR
    - OpsWorks
    - RDS
    - RedShift
    - Route53
    - SNS
    - SQS
    - StorageGateway
    - SWF
- メトリクスの保存期間
    - ２週間 : １分間隔データ。
    - ２ヶ月 : ５分間隔データ。
    - １５ヶ月 : １時間間隔データ。
    - [Amazon CloudWatch の更新 – メトリックス保存期間の延長とユーザーインターフェイスの更新 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-cloudwatch-update-extended-metrics-retention-user-interface-update/)
- Zabbix との組み合わせ
    - [ZabbixによるAWS監視のコツ](http://www.slideshare.net/ShinsukeYokota/zabbixaws-46431083)
    - [AWS - ZabbixとFluentdを使ってCloudWatchの情報を長期保存する - Qiita](http://qiita.com/toshihirock/items/84954d927bb0e1eae470)
    - メトリクスを２週間以上保存する場合に有益
- アラーム
    - アクションとして以下が可能。
        - SNSへの通知
        - AutoScalingアクション
        - EC2アクション
- その他
    - [CloudWatch でディスク容量の監視 - Qiita](http://qiita.com/bsdhack/items/aab4a421c0b956add8ac)
        - カスタムメトリクスとして登録。
- アラーム設定の注意
    - [ELB + CloudWatchアラームを使ったEC2サービス監視プラクティス ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/elb-using-cloudwatch-alarm/)
        - EC2 自体が停止した場合に対応した条件
- collectd 対応
    - [新しい collectd の CloudWatch プラグイン | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-cloudwatch-plugin-for-collectd/)
    - collectd
        - OSS のシステム統計情報収集ソフト
        - [collectdの設定方法 - Qiita](http://qiita.com/28tomono/items/33d3520e81f39fe2f96f)
        - [Start page – collectd – The system statistics collection daemon](https://collectd.org/)

# CloudWatch Logs

以下を参照。

- [CloudWatch Logs](logs/)

# CloudWatch Events

以下を参照。

- [CloudWatch Events](events/)
