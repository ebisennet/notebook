# CloudWatch Logs

## 公式資料

- [Amazon CloudWatch Logs とは? - Amazon CloudWatch ログ](http://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html)
    - CloudWatch Logs のユーザーガイド

## Topics

- CloudWatch Logs を使う AWS サービス
    - Lambda
        - [AWS Lambda の Amazon CloudWatch ログへのアクセス - AWS Lambda](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/monitoring-functions-logs.html)
- CloudWatch Logs エージェント
    - [CloudWatch LogsでAmazonLinux上のApacheエラーログを監視する ｜ Developers.IO](http://dev.classmethod.jp/cloud/cloudwatch-logs-apache/)
- メトリックフィルター
    - 条件に合致したログをカウントして、CloudWatchのカスタムメトリクスとして出力。
    - [Amazon CloudWatch Logsによるログの収集とフィルタとアラーム設定 ｜ Developers.IO](http://dev.classmethod.jp/cloud/cloudwatch-logs/)
- アラーム
    - [Amazon CloudWatch Logsによるログの収集とフィルタとアラーム設定 ｜ Developers.IO](http://dev.classmethod.jp/cloud/cloudwatch-logs/)
- 料金
    - [料金 - AWS CloudWatch | AWS](https://aws.amazon.com/jp/cloudwatch/pricing/)
    - ログ取り込み：$0.50/GB
    - ログアーカイブ：$0.03/GB/Month
- ログ保存期間
    - デフォルト：無制限
    - 任意の期間が設定可能
- 連携サービス
    - Elasticsearch Service
        - [CloudWatch Logs データを Amazon Elasticsearch Service にストリーミング - Amazon CloudWatch ログ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/CWL_ES_Stream.html)
    - Kinesis
        - [サブスクリプションを使用したログデータのリアルタイム処理 - Amazon CloudWatch ログ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/Subscriptions.html)
    - Kinesis Firehose
        - [サブスクリプションを使用したログデータのリアルタイム処理 - Amazon CloudWatch ログ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/Subscriptions.html)
    - Lambda
        - [サブスクリプションを使用したログデータのリアルタイム処理 - Amazon CloudWatch ログ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/Subscriptions.html)
        - [【新機能】Amazon CloudWatch LogsのログデータをAWS Lambdaでリアルタイムに処理する ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/real-time-processing-log-data-with-lambda/)
    - S3
        - [ログデータを一括で Amazon S3 にエクスポートする - Amazon CloudWatch ログ](https://docs.aws.amazon.com/ja_jp/AmazonCloudWatch/latest/logs/S3Export.html)
            - from/to を指定して、指定した S3 の宛先に一括エクスポートできる。
            - エクスポートできるようになるまで最大１２時間かかる。
    - CloudTrail
        - [Amazon CloudWatchがJSONログとAWS CloudTrailとの統合をサポート](https://www.infoq.com/jp/news/2015/03/aws-cloudtrail-cloudwatch-json)

Cloud Watch Logs は Cloud Watch と提供リージョンが異なるので注意。

