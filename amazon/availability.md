# 可用性

可用性とSLAについて。

## SLAがあるサービス

以下については99.95%のSLAがある。

-   EC2
-   EBS
-   S3
-   RDS
-   CloudFront

以下については99.98%のSLAがある。（正確な定義は違うので注意）

-   R53

その他のサービスはSLAがない。

参考

-   [AWSのSLAについて調べてみた 2014年7月版 ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/aws_sla_201407/)
-   [AWSのSLAの考え方　あなたのサービスを守ってくれない！？](http://cloudtriage.terilogy.com/20150821/)

## 参考資料

-   [【特別企画】AWSをめぐる「驚き」と「誤解」～Amazon Web Services入門(1) - クラウド Watch](http://cloud.watch.impress.co.jp/docs/special/20140903_664732.html)
    -   AWS の SLA への考え方

