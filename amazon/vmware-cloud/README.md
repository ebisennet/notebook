# VMware Cloud on AWS

vSphere, vSAN, NSX, EXSi が AWS のベアメタル上で稼働する。

## 公式資料

- [Amazon Web Services and VMware](https://aws.amazon.com/jp/vmware/)

## 参考資料

- [［速報］「VMware Cloud on AWS」をAWSとVMwareが共同発表。Amazonクラウドのベアメタル上でVMware環境を提供、ハイブリッドクラウドを実現（更新終了） － Publickey](http://www.publickey1.jp/blog/16/vmware_cloud_on_aws.html)
- [AWSとVMwareの提携から見えてくるもの。最強のハイブリッド環境を手に入れたAmazonと、クラウドでの逆転を狙うVMware － Publickey](http://www.publickey1.jp/blog/16/awsvmware_2.html)
- [ASCII\.jp：VMware Cloud on AWSが市場に与えるインパクトと国内市場｜大谷イビサのIT業界物見遊山](http://ascii.jp/elem/000/001/249/1249854/)
