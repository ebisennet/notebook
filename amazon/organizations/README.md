# AWS Organizations

複数の AWS アカウントの管理ができる。

## 公式資料

- [AWS Organizations – Centrally Manage Multiple Accounts \- Amazon Web Services](https://aws.amazon.com/jp/organizations/)
- [Announcing AWS Organizations: Centrally Manage Multiple AWS Accounts \| AWS Security Blog](https://aws.amazon.com/jp/blogs/security/announcing-aws-organizations-centrally-manage-multiple-aws-accounts/)
- [AWS Organizationsを利用したアカウント作成の自動化 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/account-management-automation-using-aws-organizations/)
