# awscli

## 公式資料

- [AWS コマンドラインインターフェイス | AWS](https://aws.amazon.com/jp/cli/)

## 参考資料

- [【AWS】CLIの初期設定について（認証情報とコマンド補完） - TASK NOTES](http://www.task-notes.com/entry/20141026/1414322858)

