# Cognito Sync

## 公式資料

- [Amazon Cognito Sync - Amazon Cognito](https://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/cognito-sync.html)
    - 開発者ガイド

## Topics

- 

## Cognito Events

Cognito Sync のイベントに反応して Lambda を起動できる。

- [Amazon Cognito イベント - Amazon Cognito](https://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/cognito-events.html)

## Cognito Streams

Cognito Sync のデータ更新や同期時に、リアルタイムにデータ変更内容を Kinesis Stream にプッシュできる。

- [Amazon Cognito ストリーム - Amazon Cognito](https://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/cognito-streams.html)
- [Amazon Cognitoストリームを試してみた ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/try_amazon_cognito_stream/)
