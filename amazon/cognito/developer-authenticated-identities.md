# Developer Authenticated Identities

自前の認証システムをCognitoで使える。

- 自前のシステムから Cognito の API を呼ぶ必要あり。
- Web Identity として Open ID Connect Token が返るのでそれを用いて AWS STS から temporary credentials を発行する。

自前の認証システムのことを開発者プロバイダーと呼び、開発者プロバイダーに関連付けられた ID プールの ID を開発者が認証した ID と呼ぶ。

## 公式資料

- [開発者が認証した ID - Amazon Cognito](http://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/developer-authenticated-identities.html)

## 参考資料

- [Facebook / Google / Amazon だけじゃない！独自の認証システムも利用可能になりました！ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/amazon-cognito-developer-authenticated-identities/)
- [Amazon Cognitoを独自認証(Developer Authenticated Identities)で利用する - Qiita](http://qiita.com/imaifactory/items/b66cdf91118e3be6fe17)
    - 自前の認証サーバーとの関係がわかりやすい
