# Cognito

Cognito Identity と Cognito Sync の２つからなる。

Cognito Identity は内部的に STS も使う。

## 公式資料

- [Welcome - Amazon Cognito Sync](http://docs.aws.amazon.com/ja_jp/cognitosync/latest/APIReference/Welcome.html)
- [Amazon Cognito とは - Amazon Cognito](http://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/what-is-amazon-cognito.html)
    - 開発者ガイド

## Topics

- SDK
    - [AWS Cognito（2）JavaScriptによるクライアントの実装 | テクニカルタイムアウト](http://tech.blog.surbiton.jp/aws_cognito_2_javascript_client/)
- [Identity Pools](identity-pools.md)
- [Developer Authenticated Identities](developer-authenticated-identities.md)
- [User Pools](user-pools.md)
- ID Provider
    - Facebook
    - Amazon
    - Google
    - Twitter/Digits
    - OpenID Connect Provider
    - SAML ID Provider

## 参考資料

- [Kinesis Recorderを使ってiOSアプリからAmazon KinesisへPutRecord - Qiita](http://qiita.com/imaifactory/items/fe1d6821136c197f0fd2)
    - IdP として Facebook を使った Cognito 設定の例。
    - IAM Role を設定してアプリから直接 Kinesis にデータ投入している。

# Cognito Sync

以下を参照。

- [Cognito Sync](sync/)
