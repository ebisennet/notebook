# Identity Pools

いわゆる Cognito Identity を保持しているもの。

- 認証されたIDと認証されていないIDのそれぞれについて、異なる IAM ロールを指定できる。
- 認証されたIDのために、IdPとして以下が使用できる。
    - パブリックプロバイダー（Amazon, Facebook, Twitter, Google, Twitter/Digits, OpenID Connect Provider）
    - 開発者プロバイダー
    - Cognito User Pools
- 認証されていないIDは匿名ユーザーとして扱われる。

## 参考資料

- [Cognito の導入方法がより簡単になりました！ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/amazon-cognito-update-20150120/)
    - Identity Pool の作成
    - Mobile SDK による初期化など
