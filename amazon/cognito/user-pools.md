# User Pools

Cognito によるマネージド認証プロバイダー。

サインアップのUIなども提供してくれる。
Developer Authenticated Identities を使って実装していたことをマネージドでやってくれる。

## Memo
- FB や TW での認証でもなく、カスタムプロバイダーでもない。
- Identity Pools は別途作る必要がある。
- MAU によってコストかかるので注意。

## 公式資料

- [Amazon Cognito 向け User Pools | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-user-pools-for-amazon-cognito/)

## 参考資料

- [新機能 Amazon Cognito に待望のユーザー認証基盤「User Pools」が追加されました！ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/cognito-user-pool/)
- [Amazon Cognito User Poolsを使って、webサイトにユーザ認証基盤を作る - Qiita](http://qiita.com/horike37/items/1d522f66452d3abe1203)
