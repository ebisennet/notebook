# Serverless

AWS 活用してサーバーレスアーキテクチャにする事例など。

サーバーレスアーキテクチャで検索すると良い。

- [クラウドネイティブ化する未来](http://www.slideshare.net/keisuke69/ss-52562743)
    - Lambda, API Gatwaey
    - 認証周りやSDK生成など
- [(レポート) DVO209: JAWS: 高度にスケーラブルなサーバーレスフレームワーク \#reinvent ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/reinvent2015-dvo209/)
    - サーバーレスフレームワークの JAWS について
- [(レポート)ARC308: AWS Lambdaを使用している、サーバーのない企業:AWSによるストリーミングアーキテクチャー \#reinvent ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/reinvent-arc308/)
    - Lambda で RTMP-HLS ストリーミング変換の事例
    - Lambda の使い方について色々とテクニックがのっている
        - GitHub -> SNS -> Lambda で Lambda から Lambda デプロイ
        - Cognito Sync トリガーで Lambda 起動
- [Serverless Reference Architectures with AWS Lambda - All Things Distributed](http://www.allthingsdistributed.com/2016/06/aws-lambda-serverless-reference-architectures.html)
    - Lambda を使ったサーバーレス構成について、用途別の典型的な構成例。
