# Application Discovery Service

## 公式資料

- [AWS Application Discovery Service](https://aws.amazon.com/jp/application-discovery/)
- [New – AWS Application Discovery Service – クラウド移行計画 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-aws-application-discovery-service-plan-your-cloud-migration/)
- [AWS Application Discovery Service アップデート – VMware のエージェントレス検出 | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-application-discovery-service-update-agentless-discovery-for-vmware/)
