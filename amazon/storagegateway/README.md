# Storage Gateway

オンプレミス上のストレージサーバーとして提供。

EC2 から S3 への NFS 接続にも使える。

## 公式資料

- [Amazonクラウドが企業向けクラウドストレージ「AWS Storage Gateway」を発表。事実上の容量無限、そのままディザスタリカバリ － Publickey](http://www.publickey1.jp/blog/12/amazonaws_storage_gateway.html)

## Topics

- [EC2からNFS経由でS3オブジェクトを保管できるようになりました！ ｜ Developers\.IO](http://dev.classmethod.jp/cloud/aws/filesgateway-on-ec2/)
