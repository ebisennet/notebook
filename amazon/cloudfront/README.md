# CloudFront

いわゆる CDN サービス。現在は簡易な RPS としての機能も持つ。

- コスト重視の場合、CloudFrontを使った方がトータルの料金は安くなる。
- 性能重視の場合、CloudFrontを使うと料金は高くなる。

複数のオリジンを１つのドメインにまとめることができる。

RTMPやHLSを用いた動画のオンデマンド／ストリーミング配信機能も提供する。

## 公式資料

- [Amazon CloudFront CDN （コンテンツ配信とストリーミング） | AWS](https://aws.amazon.com/jp/cloudfront/)
- [リクエストとレスポンスの動作 - Amazon CloudFront](https://docs.aws.amazon.com/ja_jp/AmazonCloudFront/latest/DeveloperGuide/RequestAndResponseBehavior.html)

## Topics

- 暗号化スイート
    - [ELB, CloudFront が対応する SSL/TLS の暗号スイートとプロトコル](https://blog.manabusakai.com/2015/09/elb-cloudfront-cipher-suite/)

## 参考資料

- [CloudFrontのオリジンサーバによる機能の違い ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/difference-between-cf-origin/)

