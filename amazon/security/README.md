# セキュリティ

- [PCI DSS](pci-dss.md)
- DDoS 対応
    - [DDoS に対する AWS のベストプラクティス](http://media.amazonwebservices.com/jp/DDoS%20White%20Paper_Revised.pdf) (PDF)

## 公式資料

- [AWS クラウドセキュリティ | AWS](https://aws.amazon.com/jp/security/)
- [セキュリティリソース - AWS クラウドセキュリティ | AWS](https://aws.amazon.com/jp/security/security-resources/)
- [AWS クラウドコンプライアンス | AWS](https://aws.amazon.com/jp/compliance/)

## ホワイトペーパー

- <https://d0.awsstatic.com/International/ja_JP/Whitepapers/AWS%20Security%20Whitepaper.pdf>
- <http://media.amazonwebservices.com/jp/wp/AWS_Security_Best_Practices.pdf>
- <https://s3.amazonaws.com/awsmedia/jp/wp/AWS_Risk_and_Compliance_Whitepaper.pdf>
- [AWS Risk and Compliance Whitepaper](http://d0.awsstatic.com/whitepapers/International/jp/AWS_Risk_Compliance_Whitepaper_Aug_2015.pdf) (pdf)

## ペネトレーションテスト

- [侵入テスト - AWS クラウドセキュリティ | AWS](https://aws.amazon.com/jp/security/penetration-testing/)
    - 「AWS のポリシーでは、お客様が所有する EC2 と RDS のインスタンスのテストのみが許可されます。AWS の他のサービスや、AWS で所有しているリソースに対するテストは禁止されています。」
- [クラウド環境で稼動するサービスの脆弱性診断 | ラック公式ブログ | 株式会社ラック](http://www.lac.co.jp/blog/category/security/20160602.html)
    - 責任共有モデルについて。
