AWS の PCI DSS 対応状況

- EC2
- S3
- EBS
- VPC
- RDS
- ELB
- IAM
- DynamoDB
- SimpleDB
- EMR
- Direct Connect
- Glacier

## 参考資料

- [AWS PCI DSS レベル 1 よくある質問 - AWS コンプライアンス | アマゾン ウェブ サービス（AWS 日本語） - AWS コンプライアンス | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/compliance/pci-dss-level-1-faqs/)
- [PCI Compliance for Amazon CloudFront | AWS Official Blog](http://aws.amazon.com/jp/blogs/aws/pci-compliance-for-amazon-cloudfront/)
    - 2015-08-04
- [AWSが新しいPCI DSS 3.2を採用する最初のクラウドサービスプロバイダに | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-becomes-first-cloud-service-provider-to-adopt-new-pci-dss-3-2/)
    - 2016-07-28

