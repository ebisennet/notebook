# QuickSight

BI.

## 公式資料

- [Amazon QuickSight（AWS による高速のビジネスインテリジェンス） | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/quicksight/)
- [Amazon QuickSightが一般提供開始 – 高速で利用が簡単なビッグデータ用ビジネスアナリティクス \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-quicksight-now-generally-available-fast-easy-to-use-business-analytics-for-big-data/)

## Topics

- [料金](pricing.md)
