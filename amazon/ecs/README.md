# ECS (EC2 Container Service)

Docker コンテナのクラスタ実行サービス。

## 公式資料

- [Amazon EC2 Container Service (Docker コンテナ管理) | AWS](https://aws.amazon.com/jp/ecs/)
- [Amazon ECSでAuto Scaling | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/automatic-scaling-with-amazon-ecs/)
- [ECSタスクのためのIAMロールによってコンテナ利用アプリケーションをより安全にする | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/help-secure-container-enabled-applications-with-iam-roles-for-ecs-tasks/)
- [Amazon ECSイベントストリームで、クラスタの状態を監視 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/monitor-cluster-state-with-amazon-ecs-event-stream/)
- [Amazon ECS Task Placement Policyのご紹介 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/introducing-amazon-ecs-task-placement-policies/)
