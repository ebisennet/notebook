# リージョン

## 公式資料

- [リージョン - グローバルインフラストラクチャ | AWS](https://aws.amazon.com/jp/about-aws/global-infrastructure/regional-product-services/)
    - サービスごとにどのリージョンが対応しているか。
- [リージョンとアベイラビリティーゾーン - Amazon Elastic Compute Cloud](http://docs.aws.amazon.com/ja_jp/AWSEC2/latest/UserGuide/using-regions-availability-zones.html)
- [AWS のリージョンとエンドポイント - アマゾン ウェブ サービス](https://docs.aws.amazon.com/ja_jp/general/latest/gr/rande.html)

## アベイラビリティゾーンについて

- [AWSのAvailability Zoneとロケーションの関係 - くろの雑記帳](http://kurochan-note.hatenablog.jp/entry/2016/01/13/203539)
    - ゾーンと実際のデータセンターとの関係は、アカウントによって異なる可能性がある。
