# CodeDeploy

EC2インスタンス、および AWS Endpoint に接続可能なオンプレインスタンスに コードのデプロイを実施するためのサービス。

エージェントを対象マシンに入れて動かす。

- codedeploy-agent.noarch.rpm

## 公式資料

- [AWS CodeDeploy (EC2 インスタンスへのコードデプロイを自動化） | AWS](https://aws.amazon.com/jp/codedeploy/)
- [AWS 開発者用ツールのまとめ – CodeCommit、CodePipeline、CodeDeploy に追加した最近の機能強化 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-developer-tool-recap-recent-enhancements-to-codecommit-codepipeline-and-codedeploy/)
