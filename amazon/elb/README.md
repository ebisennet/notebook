# ELB

## 公式資料

- [Elastic Load Balancing（クラウドネットワークのロードバランサー） | AWS](https://aws.amazon.com/jp/elasticloadbalancing/)
- [Elastic Load Balancing とは - Elastic Load Balancing](http://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/userguide/what-is-load-balancing.html)
    - ユーザーガイド
- [Elastic Load Balancing Adds Support for Host\-based Routing and Increased Rules on its Application Load Balancer](https://aws.amazon.com/jp/about-aws/whats-new/2017/04/elastic-load-balancing-adds-support-for-host-based-routing-and-increased-rules-on-its-application-load-balancer/)
    - ホスト名によるルール記述が追加

## Topics

- [料金](pricing.md)
- 内部向けELB
    - [内部ロードバランサー - Elastic Load Balancing](http://docs.aws.amazon.com/ja_jp/ElasticLoadBalancing/latest/DeveloperGuide/elb-internal-load-balancers.html)
    - いわゆる内部LB
        - インターネットからアクセスできない
- インターネット向けELB
    - [インターネット向けロードバランサー - Elastic Load Balancing](http://docs.aws.amazon.com/ja_jp/ElasticLoadBalancing/latest/DeveloperGuide/elb-internet-facing-load-balancers.html)
    - 外部LB
- VPC内のELB
    - [Amazon VPCでELBとNATを使ってよりセキュアな環境を作る【5日目】 ｜ Developers.IO](http://dev.classmethod.jp/cloud/amazon-vpc-elb-nat/)
        - Public VPC に配置した ELB から、Private VPC 内で EC2 を Autoscaling する構成について。
    - [AWS - ELBをVPC内に設置する際のサブネット設計の注意点 - Qiita](http://qiita.com/tetor/items/4c9e1aa58da2c5755452)
        - CIDR は /27 以上のサイズのネットワークを指定
        - ８個の空きIPアドレス
        - サブネット内にELBが予約しているIPアドレスがある
        - AZごとにサブネットが必要
        - エフェメラルポートをNetworkACLで開けておく必要がある。
    - [今更 VPC で 複数の AZ をまたいだ ELB を試す（1） - ようへいの日々精進XP](http://inokara.hateblo.jp/entry/2013/12/31/010647)
        - 
- Security Group
    - [標準ロードバランサーのセキュリティグループを設定する - Elastic Load Balancing](http://docs.aws.amazon.com/ja_jp/elasticloadbalancing/latest/classic/elb-security-groups.html)
- Pre-Warming
    - アクセス増加が予想される時
    - サポートレベルがビジネス以上である必要
- Application Load Balancer
    - [Amazonクラウド、URLでルーティング可能な新ロードバランサー「Application Load Balancer」提供開始 － Publickey](http://www.publickey1.jp/blog/16/amazonurlapplication_load_balancer.html)
    - パスベースルーティングに対応。
    - HTTP/2 や WebSocket にも対応。
    - 従来のELBは、標準ロードバランサーと呼ばれるように。
    - 標準ロードバランサーより高速、かつ10%程度低価格。
- リクエストトレース
    - [Elastic Load Balancing でリクエストのトレースをサポート](https://aws.amazon.com/jp/about-aws/whats-new/2016/11/elastic-load-balancing-support-for-request-tracing/)

## 参考資料

- [AWSマイスターシリーズ Amazon Elastic Load Balancing (ELB)](http://www.slideshare.net/AmazonWebServicesJapan/20130612-aws-meisterregenerateelbpublic)
- [VPCでELBにPrivate SubnetのEC2インスタンスを紐付ける · mechamogera/MyTips Wiki · GitHub](https://github.com/mechamogera/MyTips/wiki/VPC%E3%81%A7ELB%E3%81%ABPrivate-Subnet%E3%81%AEEC2%E3%82%A4%E3%83%B3%E3%82%B9%E3%82%BF%E3%83%B3%E3%82%B9%E3%82%92%E7%B4%90%E4%BB%98%E3%81%91%E3%82%8B)
    - Public に ELB、Private に EC2 の組み合わせで注意するところ。
- [VPC にプライベートサブネットを作るのはエンジニアの思考停止か？](http://blog.manabusakai.com/2015/06/no-private-subnet/)
