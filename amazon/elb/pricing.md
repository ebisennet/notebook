# 料金

標準ロードバランサーとアプリケーションロードバランサーで異なる。

## 公式資料

- [AWS | Elastic Load Balancing | Classic Load Balancer の料金](https://aws.amazon.com/jp/elasticloadbalancing/classicloadbalancer/pricing/)
- [AWS | Elastic Load Balancing | Application Load Balancer の料金表](https://aws.amazon.com/jp/elasticloadbalancing/applicationloadbalancer/pricing/)

## 標準ロードバランサー

- ロードバランサー１つについて、１時間あたり $0.027
- 処理されるデータ1GBあたり $0.008

## アプリケーションロードバランサー

ロードバランサーキャパシティーユニット（LCU）の使用時間単位で費用が発生する。

- ロードバランサー１つについて、１時間あたり $0.0243
- LCUについて、１時間あたり $0.008

１つのLCUで以下の処理能力がある。

- １秒あたり最大２５個の新規接続
- １分あたり最大３０００個のアクティブ接続
- 最大 2.22 Mbpsの帯域

いずれかでLCUの処理能力を超えると、LCUが増える。
