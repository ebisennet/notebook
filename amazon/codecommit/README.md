# CodeCommit

Git リポジトリサービス。

- AWS の他のサービスと組み合わせると有効。
- Issue と Commit の関連付けなどは出来ない。

## 公式資料

- [Amazon CodeCommit（安全でスケーラブルなマネージド型ソースコントロールサービス） | AWS](https://aws.amazon.com/jp/codecommit/)
- [料金 - Amazon CodeCommit | AWS](https://aws.amazon.com/jp/codecommit/pricing/)
- [What Is AWS CodeCommit? - AWS CodeCommit](http://docs.aws.amazon.com/ja_jp/codecommit/latest/userguide/welcome.html)
    - ユーザーガイド
- [AWS 開発者用ツールのまとめ – CodeCommit、CodePipeline、CodeDeploy に追加した最近の機能強化 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/aws-developer-tool-recap-recent-enhancements-to-codecommit-codepipeline-and-codedeploy/)

## Topics

- Triggers
    - [Amazon Web Services ブログ: 【新機能】AWS CodeCommit の通知](http://aws.typepad.com/aws_japan/2016/03/codecommit-notifications.html)
    - [Amazon Web Services ブログ: 【新機能】AWS CodeCommit にリポジトリ トリガーが追加されました](http://aws.typepad.com/aws_japan/2016/03/codecommit-repository-trigger.html)
- Credential Helper
    - [aws\-cli/codecommit\.py at develop · aws/aws\-cli · GitHub](https://github.com/aws/aws-cli/blob/develop/awscli/customizations/codecommit.py)
    - [credentials API](https://www.kernel.org/pub/software/scm/git/docs/technical/api-credentials.html)

## 参考資料

- [CodeCommitのGitリポジトリへの接続方法 ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/using-codecommit-git-repository/)
    - 開発者ごとに IAM ユーザー作成が必要
    - ssh接続
        - IAM ユーザーに公開鍵登録
    - https接続
        - IAM アクセスキーが必要
        - credential helper のセットが必要
- [CodeCommit のGitリポジトリにIAM Roleで接続する ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/using-codecommit-git-repository-by-iam-role/)
    - EC2 につけた IAM Role から CodeCommit に接続できる。
    - SSHキーも IAM アクセスキーも不要。
    - git の credential helper のセットは必要。
- [AWS CodeCommit入門 - Qiita](http://qiita.com/shigure_onishi/items/cc17da328bac601eba82)
