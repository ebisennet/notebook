# node.js

v4.3.2 が使える。

## 公式資料

- [AWS Solutions Architect ブログ: AWS LambdaでNode.js 4.3.2が利用可能になりました](http://aws.typepad.com/sajp/2016/04/node-js-4-3-2-runtime-now-available-on-lambda.html)

## AWS Lambda で node.js を選択する理由

- Java について
    - Lambda のコンテナ単位で動く仕組みと相性が悪い。
        - コンテナが作成される度にクラスローディングのオーバーヘッド。
        - コンテナ破棄と同時に JIT コンパイル結果も破棄される。
    - 必要メモリ量が Python や node.js より多く、コストが増加する。
        - メモリ量設定のデフォルト値が Python / node.js の 128MB に対し、Java は 512MB。
- Python について
    - Lambda 上は性能面などで node.js と似たような特徴。
    - 日本においては JavaScript よりプログラミング人口が少ない。
