# Java

- Java 8

## 公式資料

- [Lambda 関数ハンドラー (Java) - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/java-programming-model-handler-types.html)

## 実績

- 2015年末、ソーシャルゲームでの活用事例
    - Java Lambda で数万〜数十万のアクセス
    - レスポンスの遅延はごく一部で最大20sec
        - オンプレでやっていた時は数分〜数十分レベル
    - 通常は最大2-3sec

## Memo

### lambda core

``` xml
<dependency>
  <groupId>com.amazonaws</groupId>
  <artifactId>aws-lambda-java-core</artifactId>
  <version>1.1.0</version>
</dependency>
```

## 参考資料

- [AWS LambdaがJava対応したので簡単チュートリアル - Qiita](http://qiita.com/Keisuke69/items/fc39a2f464d14480a432)
- [Lambda Function が Java で書けるようになりました！ ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/lambda-function-in-java/)

