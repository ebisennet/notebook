# 必要な IAM Role

## 実行

実行ログ術力のためにポリシーとして最低限、以下のアクセス許可が必要。

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
```

デフォルトで用意されているポリシー AWSLambdaBasicExecutionRole で定義済み。

上記のほか、信頼関係として以下を定義する必要がある。

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```

## 呼び出し側

lambda の呼び出し権限。

```json
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Action": [
            "lambda:InvokeFunction"
        ],
        "Resource": ["*"]
    }]
}
```

デフォルトで用意されているポリシー AWSLambdaRole で定義済み。

## VPC内Lambdaの実行権限

ENI の操作権限が必要。

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface"
      ],
      "Resource": "*"
    }
  ]
}
```

デフォルトで用意されているポリシー AWSLambdaVPCAccessExecutionRole で定義済み。
