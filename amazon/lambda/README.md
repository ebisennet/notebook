# Lambda

## 公式資料

- [AWS Lambda (サーバーレスでコードを実行・自動管理) | AWS](https://aws.amazon.com/jp/lambda/)
- [料金 - AWS Lambda (コードの実行、リソース自動管理プラットフォーム) | アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/lambda/pricing/)
- [Amazon Web Services ブログ: AWS Lambda](http://aws.typepad.com/aws_japan/aws-lambda/)
    - 公式ブログのLambdaカテゴリ
- [AWS Lambda とは - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/welcome.html)
    - 開発者ガイド
- [API Reference - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/API_Reference.html)
    - リファレンス
- [Node.js で Lambda 関数を作成するためのプログラミングモデル - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/programming-model.html)

## Topics

- AMI
    - [Lambda 実行環境と利用できるライブラリ - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/current-supported-versions.html)
- 実行エンジン別
    - [Node.js](nodejs/)
    - [Java](java/)
    - Python
        - Python 2.7
    - C#
- 概念
    - Event
    - Context
        - [Context オブジェクト \(Node\.js\) \- AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/nodejs-prog-model-context.html)
- Tips
    - CPUやネットワーク処理の改善のためにわざとメモリ割り当てを増やすテクニックあり
    - コンテナのリフレッシュは、Lambda関数アップデート時や設定変更時が基本
        - リフレッシュの頻度を期待すべきではない。
- トリガーイベント
    - CloudWatch Logs
    - Cognito
    - DynamoDB
    - Kinesis
    - S3
    - Scheduled Events
    - SES
    - SNS
- 連携サービス
    - CloudWatch Logs
    - [VPC](vpc.md)
    - Aurora
- サポート
    - [AWS Lambdaでサポートに問い合わせる前に（S3イベント編） - Qiita](http://qiita.com/Keisuke69/items/80df7211d989d136ce47)
        - x-amz-request-idとx-amz-id-2という2つのリクエストIDを取得しておく必要
- [IAM Role](iam-role.md)
- 制限
    - [AWS サービス制限 - アマゾン ウェブ サービス](http://docs.aws.amazon.com/ja_jp/general/latest/gr/aws_service_limits.html#limits_lambda)
    - [AWS Lambda の制限 - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/limits.html)
- 環境変数
    - [AWS Lambda Supports Environment Variables](https://aws.amazon.com/jp/about-aws/whats-new/2016/11/aws-lambda-supports-environment-variables/)

## Memo

- Java の実績は少ない
- [サーバレスアーキテクチャの「AWS Lambda」、米国東リージョンの一部のユーザーで障害。約3時間エラーや遅延など － Publickey](http://www.publickey1.jp/blog/16/aws_lambda3.html)
- Lambda 削除時
    - Event Source Mapping は削除される。
        - API Gateway の関連するメソッド定義も削除される。リソース定義は消えない。
            - 405 method not allowed になる？
    - ログや関連する IAM Role などは残る。

## 参考資料

- [【速報】AWS Lambdaの機能拡張が数多くきました！VPC対応、ロングランニング、スケジュール、バージョニング、Python対応！ \#reinvent ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/aws-lambda-extention-reinvent2015/)
- [AWS Lambdaを使うときに注意する４つのこと - Qiita](http://qiita.com/imafuku/items/55844535dcc8e3861bd0)
    - Java 遅い。
    - context.fail 呼び出しだと、３リトライが発生。 context.done にする必要がある。
- [AWS Lambdaの互換ソフトウェア開発に向けた「Project Kratos」始動 － Publickey](http://www.publickey1.jp/blog/16/aws_lambda_project_kratos.html)
- [GoogleがAWS Lambda対抗「Google Cloud Functions」を公開。Node.jsによるイベントベースのJavaScript環境。サーバレスアーキテクチャ普及へはずみ － Publickey](http://www.publickey1.jp/blog/16/google_cloud_functions.html)
- [AWS Lambda / Amazon API Gateway Deep Dive](http://www.slideshare.net/keisuke69/aws-lambda-amazon-api-gateway-deep-dive)
    - P.21
        - 冪等性はLambda関数の実装で保証する必要がある。
            - まれに１イベントに対して２回実行されるらしい。
    - via. [JAWS DAYS 2016の公開資料まとめ - Qiita](http://qiita.com/masumiya/items/7ea4c364a9e8cca37b5e)
- [Building, Testing and Deploying Java applications on AWS Lambda using Maven and Jenkins | AWS Compute Blog](https://aws.amazon.com/jp/blogs/compute/building-testing-and-deploying-java-applications-on-aws-lambda-using-maven-and-jenkins/)
    - Lambda 上の Java アプリを Maven + Jenkins で。
- [Serverless Reference Architectures with AWS Lambda - All Things Distributed](http://www.allthingsdistributed.com/2016/06/aws-lambda-serverless-reference-architectures.html)
    - Lambda を使ったサーバーレス構成について、用途別の典型的な構成例。
- [Lambda+RDSはアンチパターン - Qiita](http://qiita.com/teradonburi/items/86400ea82a65699672ad)
    - 検証が必要。
- [Best practices – AWS Lambda function \| cloudncode](https://cloudncode.blog/2017/03/02/best-practices-aws-lambda-function/)
