# VPC

Lambda から VPC 内のリソースにアクセスできる。

- 指定した Subnet 向けに ENI のセットアップが自動で行われ、Private IP が使用される。
- インターネットへのアクセスはデフォルトで出来ないので Managed NAT を構築する必要がある。
    - Private Subnet で EC2 を起動した時と同じ。
- *Public Subnet に配置してもインターネットにアクセスできない点に注意。*
    - [Amazon VPC 内のリソースにアクセスできるように Lambda 関数を構成する - AWS Lambda](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/vpc.html)
        - 「各 ENI には、指定されたサブネットの IP アドレス範囲からプライベート IP アドレスが割り当てられますが、パブリック IP アドレスは割り当てられません。」
        - 「VPC に添付されたインターネットゲートウェイを使用することはできません。」

## 公式資料

- [ステップ 2.2: 実行ロール (IAM ロール) を作成する - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/vpc-ec-create-iam-role.html)
    - Lambda から VPC 内のリソースアクセス
- [New – Access Resources in a VPC from Your Lambda Functions | AWS Official Blog](https://aws.amazon.com/jp/blogs/aws/new-access-resources-in-a-vpc-from-your-lambda-functions/)
    - ja: [Amazon Web Services ブログ: LambdaファンクションからのVPC内リソースへのアクセス](http://aws.typepad.com/aws_japan/2016/02/access-resources-in-a-vpc-from-your-lambda-functions.html)
- [AWS Solutions Architect ブログ: AWS Black Belt Tech Webinar 「AWS Lambda」資料公開](http://aws.typepad.com/sajp/2016/03/aws-black-belt-tech-webinar-aws-lambda-update.html)
    - ENI作成時に10-60sec
    - ENI や Subnet IP 不足の時に CloudWatch Logs に出力されない
    - 「ご要望として承ります。」とある
- [Amazon VPC 内のリソースにアクセスできるように Lambda 関数を構成する - AWS Lambda](http://docs.aws.amazon.com/ja_jp/lambda/latest/dg/vpc.html)
    - 必要なENI数の算出式あり。
    - 注意事項がまとまっている。

## 参考資料

- [AWS LambdaのVPCアクセスに関して少しだけ解説 - Qiita](http://qiita.com/Keisuke69/items/1d84684f0511a062e968)
    - ENI作成に伴うレイテンシが最大60secほどある。
- [AWS Lambda with VPCでのトラブルシュート事始め - Qiita](http://qiita.com/Keisuke69/items/59de5dbd471fbca70499)
- [Lambda + API Gateway で処理の実行がむちゃくちゃ遅くなるケースを調べた - Qiita](http://qiita.com/pm11op/items/eb148b23b062fe778bd1)
