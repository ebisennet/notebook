# 料金

## 公式資料

- [料金 - Amazon Aurora | AWS](https://aws.amazon.com/jp/rds/aurora/pricing/)

## 注意事項

- ストレージ
    - 3 AZ に合計６つのコピーが作成されるが、ストレージ料金は乗算しなくて良い。 （あくまでDBとしての使用量に依存する）
- Auroraインスタンス
    - 最小インスタンスが大きめ（db.r3.large）な点に注意。
    - 開発や負荷の低いプロダクト用にインスタンスクラスが追加されている。
        - [Amazon Auroraを開発・テストワークロードでご利用しやすいT2\.Medium DBインスタンスクラスをリリース \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/use-amazon-aurora-for-dev-test-workloads-with-new-t2-medium-db-instance-class/)
