# Aurora

- MySQL 5.6 かつ InnoDB 相当。
    - InnoDB 以外のテーブルは自動変換される。
- PostgreSQL サポート追加。
- リージョンに制限あり
    - Virginia, Oregon, Ireland, Tokyo, Sydney

## 公式資料

- [Amazon Aurora (MySQL 互換のリレーショナルデータベース） | AWS](https://aws.amazon.com/jp/rds/aurora/)
- [Amazon Web Services ブログ: InfoWorldレビュー – Amazon Aurora Rocks MySQL](http://aws.typepad.com/aws_japan/2015/12/infoworld-review-amazon-aurora-rocks-mysql.html)
- [Amazon RDS での Aurora - Amazon Relational Database Service](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/CHAP_Aurora.html)
    - Aurora の構成（DBクラスタ、クラスターボリュームなど）の解説
    - 下の方に MySQL との比較あり
- [Amazon Aurora DB クラスターの作成 - Amazon Relational Database Service](https://docs.aws.amazon.com/ja_jp/AmazonRDS/latest/UserGuide/Aurora.CreateInstance.html)
    - Aurora DB クラスター作成時の前提条件
    - 最低２つのAZに２つのサブネットを持つVPCが必要。
    - DBストレージは３つのAZにまたがったクラスターボリュームとして作成される。
- [Amazon AuroraでCross-Region Read Replicaがご利用頂けるようになりました | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-cross-region-read-replicas-for-amazon-aurora/)
- [Amazon Auroraにリーダーエンドポイントが追加されました – 負荷分散と高可用性向上 – | Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/new-reader-endpoint-for-amazon-aurora-load-balancing-higher-availability/)
    - 複数のRead Replica に対する接続を容易にする。
- [Amazon Auroraを開発・テストワークロードでご利用しやすいT2\.Medium DBインスタンスクラスをリリース \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/use-amazon-aurora-for-dev-test-workloads-with-new-t2-medium-db-instance-class/)
- [Amazon Auroraアップデート – 空間インデックス・ゼロダウンタイムパッチ \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-aurora-update-spatial-indexing-and-zero-downtime-patching/)

## Topics

- [料金](pricing.md)
- Lambda 連携
    - [Amazon Auroraアップデート – ストアードプロシジャーからLambda Functionの呼び出しと S3からのデータ読み込みに対応 – \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/amazon-aurora-update-call-lambda-functions-from-stored-procedures-load-data-from-s3/)
- S3 連携
    - [Amazon Aurora の新機能: AWS Lambda との統合と Amazon S3 から Aurora テーブルへのデータロード](https://aws.amazon.com/jp/about-aws/whats-new/2016/10/amazon-aurora-new-features-aws-lambda-integration-and-data-load-from-amazon-s3-to-aurora-tables/)

## Memo

- DB 接続時にはクラスターエンドポイントと個別のインスタンスへのエンドポイントが選べる。
    - アプリからの接続はクラスターエンドポイント。
    - SSL 通信で接続する場合は個別インスタンスのエンドポイントのみがサポート。

## 参考資料

- [Amazon RDS for Aurora について知りたい28のこと (28 Questions about Aurora) - yoshidashingo](http://yoshidashingo.hatenablog.com/entry/2014/11/13/223225)
- [Amazon Auroraをt2\.mediumで起動できるように！（ただし開発/テスト用） ｜ Developers\.IO](http://dev.classmethod.jp/cloud/aws/amazon-aurora-t2-medium/)
- [シャーディングされたシステムをAuroraに集約してリソースの消費を削減 \| Amazon Web Services ブログ](https://aws.amazon.com/jp/blogs/news/reduce-resource-consumption-by-consolidating-your-sharded-system-into-aurora/)
