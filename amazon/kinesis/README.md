# Kinesis

- Kinesis Firehose
- Kinesis Analytics
- Kinesis Stream

## 公式資料

- [Amazon Kinesis (フルマネージド型リアルタイム大規模ストリーミング処理）| アマゾン ウェブ サービス（AWS 日本語）](https://aws.amazon.com/jp/kinesis/)

## 参考資料

- [Kinesis Recorderを使ってiOSアプリからAmazon KinesisへPutRecord - Qiita](http://qiita.com/imaifactory/items/fe1d6821136c197f0fd2)
