# 真偽値

YAML 内では以下の記法で真偽値（いわゆるブーリアン型）を記述できる。

```yaml
# 以下のいずれも「真」の値
true
True
TRUE
yes
Yes

# 以下のいずれも「偽」の値
false
False
FALSE
no
No
```

## 備考

YAML 1.2 からは、プログラムが YAML に出力する際には `true` と `false` で出力することになっている。  
また、`yes` や `no` の記述がなくなっている。
