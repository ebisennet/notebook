# スカラー

文字列、あるいは数値などのような単一の値のことを示す。

YAML におけるスカラー値には以下がある。

- [文字列](string.md)
- [整数](integer.md)
- [浮動小数点](floating-point.md)
- [真偽値](boolean.md)
- [Null](null.md)
