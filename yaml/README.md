# yaml

各種プログラミング言語でよく扱われるデータ構造を簡単に記述できるテキストファイルのフォーマット。

XML などと比べて人の手で編集しやすくプログラムから入出力することも容易なため、設定ファイルのフォーマットなどによく採用されている。

## 公式資料

- [The Official YAML Web Site](http://yaml.org/)
- [YAML Ain’t Markup Language (YAML™) Version 1.2](http://www.yaml.org/spec/1.2/spec.html)
    - YAML 1.2 の spec.

## Topics

- [文字コード](character-encodings.md)
- [シーケンス](sequence.md)
- [マッピング](mapping.md)
- [スカラー](scalar.md)
    - [文字列](string.md)
    - [整数](integer.md)
    - [浮動小数点](floating-point.md)
    - [真偽値](boolean.md)
    - [Null](null.md)
- [コメント](comment.md)

## 参考資料

- [ansible使いのためのYAML入門 - @znz blog](http://blog.n-z.jp/blog/2014-06-21-ansible-yaml.html)
- [Rubyist Magazine - プログラマーのための YAML 入門 (初級編)](http://magazine.rubyist.net/?0009-YAML)
