# Features

## Topics

- [Notification](notification.md)
- [Passbook](passbook.md)

## Memo

- ATS
    - [iOS 9 で追加された App Transport Security の概要 ｜ Developers\.IO](http://dev.classmethod.jp/smartphone/iphone/ios-9-intro-ats/#exception)
    - [iOS 10 での ATS 対応について ｜ Developers\.IO](http://dev.classmethod.jp/smartphone/ats-in-ios-10/)
