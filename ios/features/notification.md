# Notification

## 公式資料

- [About Local Notifications and Remote Notifications](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Introduction.html)

## 参考資料

- [iOSのローカル通知・UILocalNotificationの設定まとめ | MIRAI STEREO](http://mirai-stereo.net/2014/06/08/objectivec-uilocalnotification-setting/)
- [ローカル通知機能を使用して、アプリ起動時・非起動時に関わらず通知する（停止処理もする）｜成長の果実](http://ameblo.jp/welx/entry-11179469765.html)
- [UILocalNotificationを使った通知の設定について — ios-practice 0.1 documentation](http://ios-practice.readthedocs.org/en/latest/docs/notification/)
- [APNS - iOS8からのプッシュ通知 - Qiita](http://qiita.com/FumitakaYamada/items/c394cc991d6f863b56fa)
    - API 変わっている
    - インタラクティブプッシュが機能追加されている
