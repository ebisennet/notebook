# Carthage

## 公式資料

- [GitHub \- Carthage/Carthage: A simple, decentralized dependency manager for Cocoa](https://github.com/Carthage/Carthage)

## インストール

```bash
brew install carthage
```

## Cartfile

```text
github "aws/aws-sdk-ios"
```

## 実行

```bash
carthage update --platform iOS
```

## 参考資料

- [Carthage導入 \- Qiita](http://qiita.com/kobad/items/dddab651c91b3dee9fbf)
