# NSURLSessionConfiguration

## 公式資料

- [NSURLSessionConfiguration \- Foundation \| Apple Developer Documentation](https://developer.apple.com/reference/foundation/nsurlsessionconfiguration)

## Properties

- connectionProxyDictionary
    - proxy 情報。
    - 設定するキーは以下を参照。
        - [CFProxySupport \- CFNetwork \| Apple Developer Documentation](https://developer.apple.com/reference/cfnetwork/1666631-cfproxysupport)
        - [Property Keys \- Core Services \| Apple Developer Documentation](https://developer.apple.com/reference/cfnetwork/1666631-cfproxysupport/1666709-property_keys)
