# Foundation

- Swift 3 で NS Prefix がなくなる。
    - [Swiftは結局どうオープンソースになったのか？ \- なるようになるかも](http://quesera2.hatenablog.jp/entry/2015/12/05/163855)

## 公式資料

- [Foundation \| Apple Developer Documentation](https://developer.apple.com/reference/foundation)

## Classes

- [JSONSerialization](JSONSerialization.md)
- [NSURLSessionConfiguration](NSURLSessionConfigurationNSURLSessionConfiguration.md)
