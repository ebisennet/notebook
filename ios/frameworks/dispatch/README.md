# Dispatch

GCD から名称変更？

## 公式資料

- [Dispatch \| Apple Developer Documentation](https://developer.apple.com/reference/dispatch)

## Example

### メインスレッドに移譲

```swift
DispatchQueue.main.async(execute: {
  // Do something...
})
```
