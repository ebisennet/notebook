# groovy

## 公式資料

- [The Groovy programming language](http://www.groovy-lang.org/)
- [GitHub - apache/groovy: Mirror of Apache Groovy](https://github.com/apache/groovy)

## Gradle

groovy でビルドスクリプトがかける。

- [Gradle User Guide](http://gradle.monochromeroad.com/docs/userguide/userguide.html)

## 参考

- [Groovy - Wikipedia](https://ja.wikipedia.org/wiki/Groovy)

