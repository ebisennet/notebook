# Pivotal

## 公式資料

- [Pivotal Software - YouTube](https://www.youtube.com/user/goPivotal)
    - Youtube account of Pivotal
- [Prerequisites to Deploying Operations Manager and Elastic Runtime | Pivotal Docs](http://docs.pivotal.io/pivotalcf/customizing/requirements.html)
    - Pivotal CF の requirements
    - 必要となる VM の数が多い
    - [Does A Reduction in the Number of VMs Bring Benefits? | Pivotal P.O.V.](https://blog.pivotal.io/pivotal-cloud-foundry/features/does-a-reduction-in-the-number-of-vms-bring-benefits)
        - 効率の良い構成についての追加情報
- [pivotal-cf · GitHub](https://github.com/pivotal-cf) (github org)
    - [pivotal-cf/weblogic-buildpack · GitHub](https://github.com/pivotal-cf/weblogic-buildpack)

## Topics

- Products
    - [Mobile Services](mobile-services/)
    - [Pivotal CF](pivotal-cf/)
    - Pivotal CF Suite for Pivotal CF
    - [Pivotal HD](pivotal-hd/)
    - Pivotal Spring Cloud
        - Service Registry
            - for microservices
        - Circuit Breaker
            - microservice monitoring
    - Pivotal Tracker
        - Official
            - [Pivotal Tracker | Agile Project Management](http://www.pivotaltracker.com/)
        - ITS/BTS
        - [CF BOSH - Pivotal Tracker](https://www.pivotaltracker.com/n/projects/956238)
            - Cloud Foundry BOSH の開発で使われている Pivotal Tracker
    - Pivotal Web Services (PWS)
        - PivotalCF のサービス提供
        - [PivotalのPWS：Pivotal、「Pivotal Web Services」を正式リリース - ＠IT](http://www.atmarkit.co.jp/ait/articles/1405/12/news117.html)
        - [Pivotal Web Services | Home](https://run.pivotal.io/)
- [Pivotal Labs](labs.md)

