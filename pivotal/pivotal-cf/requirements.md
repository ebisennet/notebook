# Requirements

Pivotal CF をインストールする際の要件についてまとめる。

## vSphere/vCenter 要件

vSphere にインストールする場合の要件は以下の通り。

- vSphere 6.0, 5.5, 5.1 のいずれかであること
- Standard Edition 以上であること
- vCenter と ESX へ https アクセスが可能であること
    - 使用TCPポート 443, 902, 903
- vSphere クラスタが構成済みであること
    - vSphere DRS を有効化している場合、オートメーションレベルが Partially automated か Fully automated であること
    - vSphere ホストが VT-X/EPT をサポートしていない場合、ハードウェア仮想化をオフにすること
- vSphere 上の Administrator System Role が使用可能であること

## Amazon Web Service (AWS) 要件

AWS 上にインストールする場合の最小構成は以下の通り。

### Elastic Load Balancer (ELB)

ELBインスタンス１つ

### Relational Database Service (RDS)

db.m3.large 以上のDBインスタンス１つ

- 100GBのストレージを割り当てる

### S3

２つのS3バケット

### EC2

以下のEC2インスタンス

- t2.micro インスタンス x 10
- c4.xlarge インスタンス x 1
- m3.large インスタンス x 2
- r3.xlarge インスタンス x (DEA１つに付き１つ)

## 共通要件

- （推奨）ワイルドカードドメインを作成できること
- （推奨）DHCPなしのネットワークが利用できること
- 十分な数のIPアドレス
    - VMインスタンスごとに１つ
    - 固定IPアドレスを要求するインスタンスごとに１つ
        - 最小で１３アドレス
    - errand（？）ごとに１つ
    - compilation worker（？）ごとに１つ
    - 動的IPアドレスを３６アドレス以上
- １つ以上のNTPサーバー
- CPU: 30
- Memory: 37GB
- Storage: 135GB

## 参考資料

- [Prerequisites to Deploying Operations Manager and Elastic Runtime | Pivotal Docs](http://docs.pivotal.io/pivotalcf/customizing/requirements.html)

