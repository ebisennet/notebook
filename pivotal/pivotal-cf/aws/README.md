# PivotalCF on AWS

AWS 上に PivotalCFをインストールする場合。

## CloudFormation を使う

CF Template が提供されている。

### [pcf-cftemplate.json](pcf-cftemplate.json)

- メインテンプレート
- 作成する AWS リソース
    - ELB x 2
        - http 用と ssh 用
    - IAM Policy
        - s3
    - S3 Bucket x 4
        - Buildpack 用、Droplet 用、パッケージ用、リソース用。

### [pcf.json](pcf.json)

- メインテンプレートから呼ばれるテンプレート。
- Ops Manager を展開するためのもの。
- 作成する AWS リソース
    - IAM User
        - Ops Manager 用
        - 以下のパーミッションを与える。
            - OpsManager 用の s3 bucket への全アクション
            - EC2 インスタンスの起動や削除など。
            - ELB への LB 列挙、およびEC2インスタンスの登録／登録解除。
    - RDS で MySQL インスタンス
        - BOSH 用
    - S3 Bucket x 1
    - InternetGateway x1
    - VPC x 1
    - Subnet x 2
    - NAT インスタンス x 1
        - EC2 インスタンスとして。
    - NAT インスタンス用 EIP x 1

