# Pivotal CF

アプリケーションの開発、配備、運用環境を提供するPaaSプロダクト。 OSS の Cloud Foundry 上に構築されている。

一言でいえば、Private Heroku。Heroku のように多数の言語（Java, Ruby, Python, Go, PHP, Node.js）で アプリケーションの開発ができ、各種サービス（DB, MQ, Mobile, Big Data, …）をアタッチできる。

インフラとして AWS, Azure, vSphere, vCloud Air, OpenStack に対応。

## 公式資料

- [Pivotal Cloud Foundry | Platform as a Service | Pivotal](http://pivotal.io/platform-as-a-service/pivotal-cloud-foundry)
- <https://network.pivotal.io/>
    - Pivotalのソフトウェアのダウンロードサイト。
    - アカウントの作成が必要。
    - ドキュメント閲覧では不要。

## Topics

- [Cloud Foundry](../../cloud-foundry/)
- Pivotal CF Elastic Runtime
- Pivotal CF Ops Manager
    - Pivotal の各種 Product を実行環境にインストールするために使う
- [Requirements](requirements.md)
- インフラ別
    - [AWS](aws/)
- ドメイン
    - ワイルドカードドメインが使えない場合、アプリごとに複数のドメイン設定が必要。
    - 例：Mobile Services の Push Notification の場合、５ドメイン必要

## 参考資料

- [PivotalがCloud FoundryベースのPivotal CFを発表](http://www.infoq.com/jp/news/2013/11/pivotal-cf)

