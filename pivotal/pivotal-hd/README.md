# Pivotal HD

Apache Hadoop や関連プロダクトに Pivotal が色々と機能追加して １つのパッケージとしてまとめたもの。

EMC Isilon との組み合わせなどが想定されている。

## 公式資料

- [Pivotal HD | Big Data | Pivotal](https://pivotal.io/jp/big-data/pivotal-hd)

