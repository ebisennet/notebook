# API Gateway

- API Aggregation, API Transformation
    - 既存APIの統合、ｎ回コール問題の解決
- API Gateway アプリの実装は Java or JavaScript で行う
    - Java8がNashorn（ナズホーン）というJS実行エンジンを持っている
    - Spring Boot アプリケーションとして開発する

## 公式資料

- [API Gateway | Pivotal Docs](http://docs.pivotal.io/mobile/apigateway/)
    - [Development Guide | Pivotal Docs](http://docs.pivotal.io/mobile/apigateway/development-guide/index.html)
        - 以下のサンプルへのリンクページとなっている
            - [cfmobile/api-gateway-samples · GitHub](https://github.com/cfmobile/api-gateway-samples)
        - [Pivotal CF Mobile Services · GitHub](https://github.com/cfmobile) (github org)
            - Samples and SDKs
- [API Gateway Index](http://cfmobile.github.io/docs-apigateway/)
    - API Reference

## 公式サンプル

- [api-gateway-samples/sample-soapxml at master · cfmobile/api-gateway-samples · GitHub](https://github.com/cfmobile/api-gateway-samples/tree/master/sample-soapxml)
    - SOAP backend API を JSON API にするサンプル。
    - Spring Boot アプリとして構成されていて、メインロジックは JS で書かれている。
    - メインコードは以下。
        - [api-gateway-samples/app.js at master · cfmobile/api-gateway-samples · GitHub](https://github.com/cfmobile/api-gateway-samples/blob/master/sample-soapxml/src/main/resources/app.js)
- [api-gateway-samples/sample-ratelimit at master · cfmobile/api-gateway-samples · GitHub](https://github.com/cfmobile/api-gateway-samples/tree/master/sample-ratelimit)
    - Rate Limit のサンプル。
    - Java 側で Redis にアクセスしている。
- [api-gateway-samples/sample-http at master · cfmobile/api-gateway-samples · GitHub](https://github.com/cfmobile/api-gateway-samples/tree/master/sample-http)
    - HTTP Client として動くサンプル。

