# Pivotal CF Mobile Services

Enterprise向けMobile Backend Services。 Pivotal CF 上にいわゆる MADP (Mobile Application Development Platform) を構築するオプション製品。

 *2016年2月に製品提供を中止した模様*

## 公式資料

- [Pivotal Cloud Foundry Mobile Services™ | Pivotal Docs](http://docs.pivotal.io/mobile/)
    - Mobile Services Docs Home
    - ドキュメント
- [An Introduction to the Pivotal CF Mobile Suite API Gateway | Pivotal P.O.V.](http://blog.pivotal.io/pivotal-cloud-foundry/news/an-introduction-to-the-pivotal-cf-mobile-suite-api-gateway)
    - 機能紹介
    - Rate Limit を実装する方法

## Topics

- [Push Notifications](push/)

 *以下は2016年2月に製品提供を中止した模様*

- [Data Sync](data-sync/)
- [API Gateway](api-gateway/)
- App Distribution
    - 開発したアプリを社内展開するためのサービス

それぞれにWebの管理画面やネイティブのクライアントSDKなどが提供される。 Pivotal CF Mobile Services の環境構築、導入支援のコンサルティングサポートあり。
