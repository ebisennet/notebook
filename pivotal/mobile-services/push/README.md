# Push Notifications

## 公式資料

- [Push Notification Service for Pivotal Cloud Foundry | Pivotal Docs](http://docs.pivotal.io/mobile/push/)
- [Development Guide | Pivotal Docs](http://docs.pivotal.io/mobile/push/development-guide.html)
    - 開発者向けのガイド
    - モバイルアプリ側のサンプルあり

## Requirements

- Pivotal RabbitMQ
- Redis
    - Pivotal Redis あるいは外部提供
- MySQL
    - Pivotal MySQL あるいは外部提供

## API

### Push

- [Push | Pivotal Docs](http://docs.pivotal.io/mobile/push/v1_3_2/api/push/)

Device UUID は Registration で発行されたもの。 APNs, GCM のトークンを PCF 上で管理するためのもの。

### Registration

- [Registration | Pivotal Docs](http://docs.pivotal.io/mobile/push/v1_3_2/api/registration/#register)

Push 通知のトークン登録、更新、取得。
