# Pivotal Labs

Pivotal 社が提供する Agile ソフトウェア開発のトレーニングプログラム。

San Francisco にある開発センターで Pivotal のエンジニアとともに開発を行う。

## 公式資料

- [Pivotal Labs | Agile Software Development](http://pivotallabs.com/)

## Scoping Session

- 長くても数日程度。
- アメリカでやる場合は、事前に日本である程度候補しぼっておいて。

## XP の徹底

- テストファースト
    - カバレッジ９０％以上
- オンサイトカスタマー
- ペアプロ
    - ペアで無い限り作業は進めない
    - Discusson が７割
    - PC は各自にあるが、モニターはミラーリングして利用

## Scrum

Scrum も取り入れている

- 全員参加の朝会
    - 他チーム（別のお客のチーム）もまじえて。

## 開発プロセスの標準化

- 人の流動性が高い
    - 一時的に他チームから人を借りたり。

## 日本での展開

- ９月から日本でも開始する
    - １チーム（Product Manager, Developer x 3, Designer）の来日
    - すでに日本のお客がついている模様

## 契約形態

- Time and Material
    - 受託開発ではない
    - 顧客が自律的にプロジェクトを実行できるようにサポートする
    - ユーザSIの実現が目的となる
        - 標準化された開発ノウハウと教育が一式まとめて提供される

