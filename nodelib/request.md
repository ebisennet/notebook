# request

HTTP リクエスト送信を簡単にするパッケージ。

## 公式資料

- [GitHub - request/request: Simplified HTTP request client.](https://github.com/request/request#readme)

## JSON 送受信

```javascript
const request = require('request');

request({
  method: 'POST',
  uri: 'https://example.com/',
  body,
  json: true,
}, (err, res, body) => {
  // body は可能であれば JSON.parse 済みのものとなる。
}
```
