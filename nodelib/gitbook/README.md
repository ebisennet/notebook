# gitbook

## 公式資料

- [GitbookIO/gitbook · GitHub](https://github.com/GitbookIO/gitbook)
- [GitBook · Writing made easy](https://www.gitbook.com/)

## Topics

- Files
    - README.md
    - SUMMARY.md
- [PDF](pdf.md)

## 参考資料

- [azu/gitbook-starter-kit · GitHub](https://github.com/azu/gitbook-starter-kit)
- [Markdownで書く電子書籍開発環境](http://azu.github.io/slide/niku_sushi/ebook_development.html)
- [GitBookを使って社内向けの文章を書く - ハードコイルド・ワンダーランド](http://weathercook.hatenadiary.jp/entry/2014/09/10/092001)
