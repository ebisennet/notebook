# PDF

PDF 出力する場合。

## 準備

``` bash
brew cask install calibre
```

パス通し設定は不要

## gitbook 実行

``` bash
gitbook pdf
```

## 参考資料

- [Gitbookでpdfとebookを作成する - Qiita](http://qiita.com/kyokomi/items/1375ba44aaa3e78e10fb)
    - 記事内の指摘の以下は改善されている
        - 目次のリンクは正常に使える
