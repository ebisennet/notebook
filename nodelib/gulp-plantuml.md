# gulp-plantuml

## Troubleshooting

### Mac で JAVA 関連の環境変数を設定しているとエラーで止まる。

- java コマンドが warning としてエラー出力に出すメッセージで止まる。
    - [java - Suppressing the "Picked up _JAVA_OPTIONS" message - Stack Overflow](http://stackoverflow.com/questions/11683715/suppressing-the-picked-up-java-options-message)

java コマンド実行オプションで -jar より前に headless 指定を追加するように改修が必要。

``` example
-Djava.awt.headless=true
```
