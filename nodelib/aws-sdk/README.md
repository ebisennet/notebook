# aws-sdk

## 公式資料

- [GitHub - aws/aws-sdk-js: AWS SDK for JavaScript in the browser and Node.js](https://github.com/aws/aws-sdk-js)
- [File: README — AWS SDK for JavaScript](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/index.html)

## サービス別

- lambda
    - [Class: AWS.Lambda — AWS SDK for JavaScript](http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html)

## Proxy を使う設定

- [Configuring the SDK in Node.js — AWS SDK for JavaScript](http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html#Configuring_a_Proxy)

```js
const proxy = require('proxy-agent');

AWS.config.update({
  httpOptions: { agent: proxy(process.env.http_proxy) }
});
```
