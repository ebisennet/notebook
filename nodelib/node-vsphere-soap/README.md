# node-vsphere-soap

## 公式資料

- [GitHub \- reedog117/node\-vsphere\-soap: Node\.js module for accessing VMware vCenter/ESXi hosts using SOAP](https://github.com/reedog117/node-vsphere-soap)
- [vSphere 5\.5 ドキュメント センター](http://pubs.vmware.com/vsphere-55/index.jsp?topic=%2Fcom.vmware.wssdk.apiref.doc%2Fright-pane.html)
