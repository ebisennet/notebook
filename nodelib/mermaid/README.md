# mermaid

## 公式資料

- [mermaid - Generation of diagrams and flowcharts from text in a similar manner as markdown.](http://knsv.github.io/mermaid/index.html)
- [GitHub - knsv/mermaid: Generation of diagram and flowchart from text in a similar manner as markdown](https://github.com/knsv/mermaid)

## 参考資料

- [mermaid.jsが素晴らしいけどなかなか使ってる人見かけないので実例晒す(追記あり) - Qiita](http://qiita.com/uzuki_aoba/items/a01f8b0b52ced69c8092)
