# proxy-agent

node.js 組み込みの http モジュールに対して、proxy を使えるようにする。

## 公式資料

- [GitHub - TooTallNate/node-proxy-agent: Maps proxy protocols to `http.Agent` implementations](https://github.com/TooTallNate/node-proxy-agent)
