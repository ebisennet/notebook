# vim

## Topics

- [ヘルプの参照](help.md)

## 基本操作

- [Vim Cheat Sheet - 日本語](http://vim.rtorr.com/lang/ja/)
- [vim - 文字列置換](http://www.ksknet.net/vi/post_42.html)

## git

- [VimでGitる素敵な方法 - Qiita](http://qiita.com/tashua314/items/35521f9bada5fe00cca7)
- [VimプラグインのGit界隈について - Qiita](http://qiita.com/sgur/items/63d09ad3185cc3f92819)

## ruby

- [Rubyプログラミングが快適になるVim環境を0から構築する - Qiita](http://qiita.com/mogulla3/items/42a7f6c73fa4a90b1df3)

## yaml

- [yaml編集時のスクロールが遅いのでvim-yamlを入れた - dackdive's blog](http://dackdive.hateblo.jp/entry/2015/02/19/180209)

## markdown

- [Vim + Markdown - Qiita](http://qiita.com/iwataka/items/5355bdf03d0afd82e7a7)

## 参考資料

- [Vim - Wikipedia](https://ja.wikipedia.org/wiki/Vim)
- [名無しのvim使い](http://nanasi.jp/)
    - [vimエディタ・スターターマニュアル #1 （ドキュメントの表記） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-description.html)
        - [vimエディタ・スターターマニュアル #2 （モードの切替） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-mode.html)
        - [vimエディタ・スターターマニュアル #3 （カーソルの移動） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-motion.html)
        - [vimエディタ・スターターマニュアル #4 （テキスト編集） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-editing.html)
        - [vimエディタ・スターターマニュアル #5 （検索、置換） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-search.html)
        - [vimエディタ・スターターマニュアル #6 （複数のバッファの管理） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-buffer.html)
        - [vimエディタ・スターターマニュアル #7 （ホームディレクトリのファイル構成） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-user-dir.html)
        - [vimエディタ・スターターマニュアル #8 （設定ファイル） — 名無しのvim使い](http://nanasi.jp/articles/howto/user-manual/user-manual-config.html)
