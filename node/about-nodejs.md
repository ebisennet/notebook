# node.js について

## node.js とは

- ブラウザの外で使える JavaScript
    - JavaScript 実行エンジンは Chrome の実行エンジンを使用している。
- C10K問題に対応しやすい
    - C10K問題＝クライアントからの同時接続が10,000台を超えるようなケース特有の性能問題。
- シングルスレッド
    - スレッド切り替えのオーバーヘッドがない。
    - マルチコアを活かすためにCPUコア数分だけプロセスをfork。
- ノンブロッキングI/O + イベントドリブン
    - シンブルスレッドで多数の通信I/Oを並行に処理するためにノンブロッキングI/Oを使って実現。
    - あるタスクがI/O待機中の時に他のタスクを実行する。タスクがなくなったらI/O完了イベントを待機。
- ボトルネックがCPUとなるプログラムには向いていない
    - CPUを明け渡さないタスクがあると、他の全てのタスクがブロックされる。
    - Webアプリは大抵 I/O がボトルネックとなるので問題になりにくい。
- 近年 Web アプリ開発関連で人気が出ている
    - 有名な JS ライブラリ（jQuery, underscore, lodash, backbone.js, ...）が npm で配布されるようになった。
        - npm = node.js に対する maven のような位置付けのパッケージ管理ツール。
    - node.js 製の Web アプリ開発ツールの増加。
        - gulp, grunt, browserify, webpack, autoprefixer, ...

## 参考資料

- [Node.js を5分で大雑把に理解する - Qiita](http://qiita.com/hshimo/items/1ecb7ed1b567aacbe559)
