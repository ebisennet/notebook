# インストール

## Binary from Official

公式 tarball

- <http://nodejs.org/dist/>

``` sh
VERSION='v6.3.1'

cd /opt
curl -sSL http://nodejs.org/dist/${VERSION}/node-${VERSION}-linux-x64.tar.gz | tar xzf -
ln -s node-${VERSION}-linux-x64 node
ln -s /opt/node/bin/* /usr/local/bin/
```

## RPM from NodeSource

入るバージョンは epel と同じっぽい

-   <https://rpm.nodesource.com/pub/el/7/SRPMS/>

``` sh
yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup | bash -
```

proxy 環境では curl と yum の設定が先に必要。
