# バージョン別

## 8.1.4

- [Node v8\.1\.4 \(Current\) \| Node\.js](https://nodejs.org/en/blog/release/v8.1.4/)

## 8.1.3

- [Node v8\.1\.3 \(Current\) \| Node\.js](https://nodejs.org/en/blog/release/v8.1.3/)

## 7.0.0

- [Node v7\.0\.0 \(Current\) \| Node\.js](https://nodejs.org/en/blog/release/v7.0.0/)

## 6.9.0 (LTS)

- [Node v6\.9\.0 \(LTS\) \| Node\.js](https://nodejs.org/en/blog/release/v6.9.0/)

## 6.0.0

- [Node.js v6.0 (Current) がリリースされました。 - from scratch](http://yosuke-furukawa.hatenablog.com/entry/2016/04/27/110027)
