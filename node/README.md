# node.js

## 公式資料

- [Node.js](https://nodejs.org/en/)
- [Node.js v6.3.1 Manual & Documentation](https://nodejs.org/dist/latest-v6.x/docs/api/)
- [Node.js v4.4.7 Manual & Documentation](https://nodejs.org/dist/latest-v4.x/docs/api/)
- [Node.js v0.10.26 Manual & Documentation](http://nodejs.jp/nodejs.org_ja/docs/v0.10/api/)
- [GitHub - nodejs/LTS: Node.js Foundation Long-term Support Working Group](https://github.com/nodejs/LTS)
    - node@4 End 2018-04-01
    - node@6 End 2019-04-01

## Topics

- [node.js について](about-nodejs.md)
- [インストール](installing.md)
- [バージョン別](versions.md)
- [ライブラリ別ノート](../nodelib/)
