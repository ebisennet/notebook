#+title: python-gitlab

* 公式資料
- [[https://github.com/gpocentek/python-gitlab][GitHub - gpocentek/python-gitlab: Python wrapper for the GitLab API]]

* 参考
- [[http://urashita.com/archives/2901][GitLabでGUI画面からでなく、python-gitlabを使ってバルクでユーザー登録する方法 | urashita.com]]
