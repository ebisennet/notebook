# maven

## 公式資料

- [Maven – Welcome to Apache Maven](http://maven.apache.org/)
- [The Central Repository Search Engine](http://search.maven.org/)
- [Index of /maven2/](https://repo.maven.apache.org/maven2/)
    - Central Repository
- [Git at Apache](http://git.apache.org/)
    - ソースコード

## Topics

- [インストール](installation.md)
- [plugins](plugins/)
- [settings.xml](settings/)
- mavenrc
    - `/etc/mavenrc`, `~/.mavenrc`
    - maven 実行時の環境変数を追加設定できる。
    - MAVEN_OPTS などの設定に使える。
- 3rd party artifacts の管理
    - [Mavenリポジトリで提供されていないサードパーティJarをどうするか - sinsengumi血風録](http://sinsengumi.net/blog/2012/12/maven%E3%83%AA%E3%83%9D%E3%82%B8%E3%83%88%E3%83%AA%E3%81%A7%E6%8F%90%E4%BE%9B%E3%81%95%E3%82%8C%E3%81%A6%E3%81%84%E3%81%AA%E3%81%84%E3%82%B5%E3%83%BC%E3%83%89%E3%83%91%E3%83%BC%E3%83%86%E3%82%A3jar/)
    - [Maven2の小技 ローカルのjarを依存ライブラリに含める - 現場のためのソフトウェア開発プロセス - たかのり日記](http://szk-takanori.hatenablog.com/entry/20080118/1200665416)
    - system スコープと $basedir を使ったプロジェクト内ファイル参照を組み合わせる。
- Depedency Mechanism
    - [Maven – Introduction to the Dependency Mechanism](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)
    - [maven2導入TIPSその１～孫依存の強制排除 - 凡人プログラマ](http://d.hatena.ne.jp/u1tnk/20080326/1206548754)
- ソースコード
    - [Maven – Source Repository](https://maven.apache.org/source-repository.html)
- Central Repository
    - https://repo.maven.apache.org/maven2
        - maven がアクセスする URL
    - maven 3.4 以前はハードコーディングされていたが、3.4 からは settings.xml の設定を使用する。
        - [ASF Git Repos - maven.git/blob - maven-core/src/main/java/org/apache/maven/repository/RepositorySystem.java](https://git-wip-us.apache.org/repos/asf?p=maven.git;a=blob;f=maven-core/src/main/java/org/apache/maven/repository/RepositorySystem.java;h=5766b9886ce6f82ea454b14eae57167ec28cf51c;hb=a2249ce21ca1e5f28858422c3018153b8eba4d3a#l60)
- [Maven – Guide to Configuring Default Mojo Executions](https://maven.apache.org/guides/mini/guide-default-execution-ids.html)
    - 実行するゴールの制御。
- [Maven – Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)
    - ライフサイクルについて。

## 参考資料

- [構成管理　実践入門　第5章 Maven2ベストプラクティス　社内リポジトリ　- 自分たちのライブラリ置き場](http://www.nulab.co.jp/kousei/chapter5/02.html)
    - webdav 及び scp で配置
- [Maven3のはじめかた - GitBook](https://www.gitbook.com/book/kengotoda/what-is-maven/details)
- [Maven – Remote repository access through authenticated HTTPS](https://maven.apache.org/guides/mini/guide-repository-ssl.html)
    - https でアクセスする場合のパラメーターなど。

