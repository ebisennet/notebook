# wagon-ssh

scp: スキーマを使えるようにする。

## 公式資料

-   [Apache Maven Wagon – Introduction](http://maven.apache.org/wagon/)

## Depencency

``` xml
<dependency>
  <groupId>org.apache.maven.wagon</groupId>
  <artifactId>wagon-ssh</artifactId>
  <version>2.10</version>
</dependency>
```

## 使用例

``` xml
<build>
  <extensions>
    <extension>
      <groupId>org.apache.maven.wagon</groupId>
      <artifactId>wagon-ssh</artifactId>
      <version>2.10</version>
    </extension>
  </extensions>
</build>
<distributionManagement>
  <repository>
    <id>private-repository</id>
    <name>Private Repository</name>
    <url>scp://private-repository/var/www/maven/repo</url>
  </repository>
</distributionManagement>
```

## 関連

- [wagon-ssh-external](wagon-ssh-external.md)

