# plugins

- [maven-assembly-plugin](maven-assembly-plugin/)
- [maven-compiler-plugin](maven-compiler-plugin.md)
- [maven-dependency-plugin](maven-dependency-plugin.md)
- [maven-deploy-plugin](maven-deploy-plugin.md)
- [maven-install-plugin](maven-install-plugin.md)
- [maven-replacer-plugin](maven-replacer-plugin/)
- [maven-resources-plugin](maven-resources-plugin.md)
- [wagon-http](wagon-http.md)
- [wagon-ssh](wagon-ssh.md)
- [wagon-ssh-external](wagon-ssh-external.md)
- [wagon-webdav](wagon-webdav.md)
- [wagon-webdav-jackrabbit](wagon-webdav-jackrabbit.md)
- maven-invoker-plugin

## 参考資料

- [GitHub \- apache/maven\-plugins: Mirror of Apache Maven plugins](https://github.com/apache/maven-plugins)
