# wagon-ssh-external

scpexe: スキーマを使えるようにする。

scp: スキーマと違い、Maven実行マシンの ssh config などが活かせる。

## 公式資料

- [Apache Maven Wagon – Introduction](http://maven.apache.org/wagon/)
- [Apache Maven Deploy Plugin - Deployment of artifacts in an external SSH command](https://maven.apache.org/plugins/maven-deploy-plugin/examples/deploy-ssh-external.html)
    - maven-deploy-plugin のページだが、wagon-ssh-externalの使用例あり。

## Depencency

``` xml
<dependency>
  <groupId>org.apache.maven.wagon</groupId>
  <artifactId>wagon-ssh-external</artifactId>
  <version>2.10</version>
</dependency>
```

## 使用例

``` xml
<build>
  <extensions>
    <extension>
      <groupId>org.apache.maven.wagon</groupId>
      <artifactId>wagon-ssh-external</artifactId>
      <version>2.10</version>
    </extension>
  </extensions>
</build>
<distributionManagement>
  <repository>
    <id>private-repository</id>
    <name>Private Repository</name>
    <url>scpexe://ssh-host/var/www/maven/repo</url>
  </repository>
</distributionManagement>
```

- repository.url で指定したホストがそのまま ssh コマンドのホスト名指定となる。
- repository.id 指定から settings.xml 側で設定が可能。
    - 以下を参照。
        - [Apache Maven Deploy Plugin - Deployment of artifacts in an external SSH command](https://maven.apache.org/plugins/maven-deploy-plugin/examples/deploy-ssh-external.html)

## 関連

- [wagon-ssh](wagon-ssh.md)

