# maven-compiler-plugin

## Dependency

``` xml
<dependency>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-compiler-plugin</artifactId>
  <version>3.5</version>
</dependency>
```

## 参考資料

- [MavenプロジェクトでJDKが1.5になってしまう問題を解決する設定 - Qiita](http://qiita.com/takahitonara/items/769c8775183b13c631c5)

