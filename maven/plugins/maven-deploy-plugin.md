# maven-deploy-plugin

## 公式資料

- [Apache Maven Deploy Plugin - Introduction](https://maven.apache.org/plugins/maven-deploy-plugin/index.html)
- [Index of /maven/plugins/tags/maven\-deploy\-plugin\-2\.8\.2](http://svn.apache.org/viewvc/maven/plugins/tags/maven-deploy-plugin-2.8.2/)

## SSH を使ったデプロイ

Maven3 から標準で scp: や dav: を使ったデプロイ先指定ができなくなっている。

以下のいずれかのアーティファクトを追加する必要がある。

- [wagon-ssh](wagon-ssh.md)
- [wagon-ssh-external](wagon-ssh-external.md)
- [wagon-webdav-jackrabbit](wagon-webdav-jackrabbit.md)

追加しない場合、サーバ側が PUT メソッドによるアップロードに対応している必要がある。 ただし、Apacheだと問題あり。

- [個人/社内 Maven Repository を作る - tototoshi の日記](http://tototoshi.hatenablog.com/entry/2015/03/12/205444)
    - Nginx では `create_full_put_path` の設定で対応可能。

## まとめて deploy

``` bash
mvn -Dmdep.copyPom=true dependency:copy-dependencies

for pom in target/dependency/*.pom; do mvn deploy:deploy-file -Durl=http://your/repo -Dfile="${pom%%.pom}.jar" -DgeneratePom=false -DpomFile="$pom"
```

- via. [deployment - Maven - deploy dependencies to remote repository - Stack Overflow](http://stackoverflow.com/questions/8840984/maven-deploy-dependencies-to-remote-repository)

dependency:copy-dependencies ゴールについては以下を参照。

- [maven-dependency-plugin](maven-dependency-plugin.md)

## Goals

### deploy:deploy

-   [Apache Maven Deploy Plugin - deploy:deploy](https://maven.apache.org/plugins/maven-deploy-plugin/deploy-mojo.html)

deploy フェーズのデフォルトゴール。

### deploy:deploy-file

-   [Apache Maven Deploy Plugin - deploy:deploy-file](https://maven.apache.org/plugins/maven-deploy-plugin/deploy-file-mojo.html)

指定したファイルを指定したリポジトリにデプロイするゴール。

``` bash
mvn deploy:deploy-file -Durl=scp://xxx.xxx/home/mvn/repo \
                       -DrepositoryId=my.repo \
                       -Dfile=./sample.jar \
                       -DgroupId=com.example \
                       -DartifactId=sample \
                       -Dversion=1.0.0  \
                       -Dpackaging=jar
```

WebDav 上にデプロイするためには [wagon-webdav-jackrabbit](wagon-webdav-jackrabbit.md) の設定が必要なので デプロイ用の pom.xml を用意した方がよい。

wagon-webdav-jackrabbit を使うためには url パラメーターで dav: スキーマ指定が必要な点に注意する。

#### 参考

- [mavenのインハウスリポジトリを作るよ - Qiita](http://qiita.com/NewGyu/items/71b50d036eef64be9b96)
- [Maven3でWebDavでjarをdeploy:deploy-fileしたら失敗する件 | shinodogg.com](http://shinodogg.com/?p=3551)

