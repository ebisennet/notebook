# wagon-http

## Dependency

``` xml
<dependency>
    <groupId>org.apache.maven.wagon</groupId>
    <artifactId>wagon-http</artifactId>
    <version>2.10</version>
</dependency>
```

## 自己署名証明書の使用

wagon-http の場合、環境変数からシステムプロパティーで設定できる。

``` bash
export MAVEN_OPTS=-Dmaven.wagon.http.ssl.insecure=true
```

- [How to configure Maven Wagon - Stack Overflow](http://stackoverflow.com/questions/31191007/how-to-configure-maven-wagon)

SSL認証周りのその他のオプションは以下を参照。

- [mavenでオレオレ証明書を付けてるリポジトリを使う - Qiita](http://qiita.com/thrakt/items/5f5413a24b43c426e47e)

