# maven-assembly-plugin

## 公式資料

- [Apache Maven Assembly Plugin – Introduction](http://maven.apache.org/plugins/maven-assembly-plugin/)

## 実行可能な jar 作成

dependencies も内包した jar を作成する。

``` xml
<build>
  <plugins>
    <plugin>
      <artifactId>maven-assembly-plugin</artifactId>
      <configuration>
        <archive>
          <manifest>
            <mainClass>com.example.Main</mainClass>
          </manifest>
        </archive>
        <descriptorRefs>
          <descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
      </configuration>
    </plugin>
  </plugins>
</build>
```

パッケージ作成

``` bash
mvn clean package assembly:single
```

実行

``` bash
java -jar path/to/jar
```

## 参考資料

- [Mavenでwarと実行可能jarを同時にビルドする - Qiita](http://qiita.com/mocchii/items/1660929982246c7f135b)
- [How to create runnable JAR file with Maven](http://www.javavids.com/video/how-to-create-runnable-jar-file-with-maven.html)
- [mavenで実行可能なjarファイルと依存ライブラリを含めたzipアーカイブを作成する – 寺子屋未満](http://terakonya.sarm.net/wordpress/2012/02/02/maven_build_assembly/)

