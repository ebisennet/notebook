# wagon-webdav-jackrabbit

## 公式資料

- [Apache Maven Wagon – Introduction](http://maven.apache.org/wagon/)
- [maven-wagon/wagon-providers/wagon-webdav-jackrabbit at master · apache/maven-wagon · GitHub](https://github.com/apache/maven-wagon/tree/master/wagon-providers/wagon-webdav-jackrabbit)

## Dependency

``` xml
<dependency>
  <groupId>org.apache.maven.wagon</groupId>
  <artifactId>wagon-webdav-jackrabbit</artifactId>
  <version>2.10</version>
</dependency>
```

## 使用例

``` xml
<build>
  <extensions>
    <extension>
      <groupId>org.apache.maven.wagon</groupId>
      <artifactId>wagon-webdav-jackrabbit</artifactId>
      <version>2.10</version>
    </extension>
  </extensions>
</build>
<distributionManagement>
  <repository>
    <id>private-repository</id>
    <name>Private Repository</name>
    <url>davs://private-repository/var/www/maven/repo</url>
  </repository>
</distributionManagement>
```

- dav: のみだと http でアクセスする。
- davs: の他に dav+https: といった表記もある。
- dav:https: は使えるが Deprecated

あるいは最小限の pom.xml を用意する。

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.example</groupId>
	<artifactId>deploy</artifactId>
	<packaging>pom</packaging>
	<version>1.0-SNAPSHOT</version>
	<build>
		<extensions>
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-webdav-jackrabbit</artifactId>
				<version>2.10</version>
			</extension>
		</extensions>
	</build>
</project>
```

## 自己署名証明書の使用

[wagon-http](wagon-http.md) と異なり、insecureの指定ができない。

Java として持つCA証明書ストアを複製し、自己署名証明書を追加した証明書ストアを信頼済みストアとして指定する必要がある。

- [java - Problems using Maven and SSL behind proxy - Stack Overflow](http://stackoverflow.com/questions/25911623/problems-using-maven-and-ssl-behind-proxy)
    - -Djavax.net.ssl.trustStore=path/to/keystore の指定
        - 標準のキーストアを読まなくなるので注意。
        - 複数のキーストアを指定することもできない。
            - [How to add multiple keystore paths to “java.net.ssl.trustStore”? - Stack Overflow](http://stackoverflow.com/questions/21707894/how-to-add-multiple-keystore-paths-to-java-net-ssl-truststore)
    - URL から証明書ストアを作成する場合は以下も参照。
        - [java/tools/keytool](../../java/tools/keytool.org)
- [java - Error - trustAnchors parameter must be non-empty - Stack Overflow](Http://stackoverflow.com/questions/6784463/error-trustanchors-parameter-must-be-non-empty)

## 参考資料

- [apache + WebDAV でMaven2リポジトリを構成するメモ - やさしいデスマーチ](http://d.hatena.ne.jp/shuji_w6e/20110619/1308481726)

