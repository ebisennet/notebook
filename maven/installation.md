# インストール

## CentOS

``` bash
yum install maven
```

### 関連ファイル

- /usr/bin/mvn
    - CLI から実行可能なシェルスクリプト
    - /usr/share/maven/bin/mvn が呼ばれる
- /usr/share/maven
    - デフォルトの MAVEN_HOME
- /usr/share/maven/bin/mvn
    - シェルスクリプト
    - Maven 起動の Java コマンドを呼び出す
- /usr/share/maven/conf/settings.xml
    - いわゆるグローバルな設定ファイル
    - テンプレートとして使う
- /etc/m2.conf
    - 
- /etc/maven/settings.xml
    - 
