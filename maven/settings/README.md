# settings.xml

## 公式資料

-   [Maven – Settings Reference](https://maven.apache.org/settings.html)

## mirrors

-   [maven のセントラルリポジトリがダウンしている場合のミラー設定 | Office IWATA Inc.](http://www.office-iwata.com/archives/94)

## Password 暗号化

-   [Maven – Password Encryption](http://maven.apache.org/guides/mini/guide-encryption.html)
-   [Maven minimal settings.xml for authenticated repository](http://little418.com/2009/07/maven-minimal-settingsxml-for-authenticated-repository.html)

