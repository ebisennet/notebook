# req

CSR を作成する。

オプションで以下も可能。

- 自己署名
    - x509コマンドの同時実行
        - `-x509` オプション
- 秘密鍵の作成
    - genrsaコマンドの同時実行
        - `-newkey`, `-keyout` オプション

## CSR 作成

```sh
openssl req -new -nodes -subj "/C=JP/ST=Tokyo/L=Minato-ku/O=All Nippon Airways Co.,Ltd/OU=/CN=..." -keyout server.key -out server.csr
```

## Ansible で自己署名証明書の作成

``` yaml
- name: create self-signed SSL cert
  shell: openssl req -new -nodes -x509 -subj "/C=US/ST=Oregon/L=Portland/O=IT/CN=${ansible_fqdn}" -days 3650 -keyout /etc/nginx/ssl/server.key -out /etc/nginx/ssl/server.crt -extensions v3_ca
  args:
    creates: /etc/nginx/ssl/server.crt
  notify: reload nginx
```

参考

- [Simply generating self-signed SSL Certs with Ansible | serialized.net](http://serialized.net/2013/04/simply-generating-self-signed-ssl-certs-with-ansible/)

## SHA-2対応CSR作成

- [Apache/SSL自己証明書の作成とmod sslの設定 - maruko2 Note.](http://www.maruko2.com/mw/Apache/SSL%E8%87%AA%E5%B7%B1%E8%A8%BC%E6%98%8E%E6%9B%B8%E3%81%AE%E4%BD%9C%E6%88%90%E3%81%A8mod_ssl%E3%81%AE%E8%A8%AD%E5%AE%9A)

`-sha256` オプションをつける。 署名時にも必要な点に注意。

## SAN を使ったマルチドメイン証明書作成

- [SAN対応 x.509 証明書を取得するためのCSRを作成する - Qiita](http://qiita.com/saitara/items/eda74ac6a950122b5f31)
- [13.2.19. Domain Options: Using IP Addresses in Certificate Subject Names (LDAP Only)](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sssd-ldap-domain-ip.html)
    - IP アドレスを指定する場合、SAN として IP:x.x.x.x を指定する。
- [Creating an SSL Certificate with Multiple Hostnames](http://apetec.com/support/generatesan-csr.htm)
    - 複数のSAN指定。
