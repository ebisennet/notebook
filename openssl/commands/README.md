# Commands

| コマンド                      | 説明                     |
|-------------------------------+--------------------------|
| [aes-256-cbc](aes-256-cbc.md) | 暗号化                   |
| [enc](enc.md)                 | 暗号化/復号化            |
| genrsa                        | キーペア作成             |
| [req](req.md)                 | CSR作成、内容表示        |
| [s_client](s_client.md)       | SSL/TLS クライアント     |
| [speed](speed.md)             | アルゴリズムスピード計測 |
| [x509](x509.md)               | 証明書管理               |
