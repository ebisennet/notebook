# s_client

## 証明書取得

``` bash
openssl s_client -connect HOST:443 </dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > HOST.crt
```

proxy は指定できないので、インターネットに直接つながるサーバーで。

``` bash
ssh GATEWAY openssl s_client -connect HOST:443 </dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > HOST.crt
```

## 証明書詳細表示

``` bash
openssl s_client -connect HOST:443 </dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' | openssl x509 -noout -text
```

## 参考資料

-   [OpenSSLをSSL/TLSクライアントとして使ってみる | Siguniang's Blog](https://siguniang.wordpress.com/2014/08/09/openssl-as-ssl-client/)
    -   プロトコルバージョン指定など

