# jira

課題管理システム。

## 公式資料

- [JIRA Software - Issue & Project Tracking for Software Teams | Atlassian](https://ja.atlassian.com/software/jira)
- [JIRA 7.0.0](https://docs.atlassian.com/jira/REST/latest/)
    - REST API Reference
- [ダウンロード JIRA Software | Atlassian](https://ja.atlassian.com/software/jira/download)
    - 評価版：３０日
    - ダウンロード時は登録不要
- [製品構成 | 見積 & 注文フォーム | Atlassian製品を購入](https://www.atlassian.com/purchase/product/jira-core)
    - JIRA Core
    - $10/10Users
- [Processing JIRA Software issues with Smart Commit messages - Atlassian Documentation](https://confluence.atlassian.com/bitbucket/processing-jira-software-issues-with-smart-commit-messages-298979931.html)
    - コミットメッセージの記述方法

## Topics

- [インストール](installation.md)
- [ショートカット](shortcuts.md)
- [API](api/)
- [Wiki](wiki.md)

## 参考資料

- [JIRAとSubversion（とEclipse）の連携 - リックソフト会社ブログ - サポートドキュメント](https://www.ricksoft.jp/document/blog/view-blog-post.action?pageId=58523717)
    - 定期的にインデックス作成する方法。
- [JIRA - テクニカル FAQ - サポートドキュメント](https://www.ricksoft.jp/document/display/FAQ/JIRA)
