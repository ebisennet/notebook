# インストール

## 公式資料

- [Installing JIRA applications - Atlassian Documentation](https://confluence.atlassian.com/adminjiraserver070/installing-jira-applications-749382623.html)
    - インストールガイド

## ダウンロード

- [ダウンロード JIRA Software | Atlassian](https://ja.atlassian.com/software/jira/download)

ダウンロードしたファイルに実行権限をつけて実行。

## Express Install

Express Install 実行時の作業ログ。

```
$ sudo ./atlassian-jira-software-7.1.1-jira-7.1.1-x64.bin
Unpacking JRE ...
Starting Installer ...
3 09, 2016 12:30:58 午前 java.util.prefs.FileSystemPreferences$1 run
情報: Created user preferences directory.

This will install JIRA Software 7.1.1 on your computer.
OK [o, Enter], Cancel [c]

Choose the appropriate installation or upgrade option.
Please choose one of the following:
Express Install (use default settings) [1], Custom Install (recommended for advanced users) [2, Enter], Upgrade an existing JIRA installation [3]
1
Details on where JIRA Software will be installed and the settings that will be used.
Installation Directory: /opt/atlassian/jira
Home Directory: /var/atlassian/application-data/jira
HTTP Port: 8080
RMI Port: 8005
Install as service: Yes
Install [i, Enter], Exit [e]


Extracting files ...


Please wait a few moments while JIRA Software starts up.
Launching JIRA Software ...
Installation of JIRA Software 7.1.1 is complete
Your installation of JIRA Software 7.1.1 is now ready and can be accessed
via your browser.
JIRA Software 7.1.1 can be accessed at http://localhost:8080
Finishing installation ...
```

インストール結果について。

- /etc/init.d/jira が作成される。
- 8080 は ANY Address へのバインド。

