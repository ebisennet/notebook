# Apache

## Installation

### CentOS7

- [apache - httpd Server not started: (13)Permission denied: make<sub>sock</sub>: could not bind to address :::88 - Stack Overflow](http://stackoverflow.com/questions/17079670/httpd-server-not-started-13permission-denied-make-sock-could-not-bind-to-ad)
    - ポート変更時に CentOS 7 の SELinux のデフォルト設定が使用できるポートを限定している。

## Configuration

### SSL Reverse Proxy

- [clmemo@aka: Apache の Reverse Proxy を SSL 経由で試す](http://at-aka.blogspot.jp/2013/03/apache-reverse-proxy-ssl.html)

SSL Terminate しない Reverse Proxy.

## Topics

- [Directives](directives/)

