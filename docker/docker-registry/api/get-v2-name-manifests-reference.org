#+title: GET /v2/<name>/manifests/<reference>

#+begin_src sh
  # タグ名
  curl -kv -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' https://aswort11:5000/v2/busybox/manifests/test

  # Digest
  curl -kv -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' https://aswort11:5000/v2/busybox/manifests/sha256:53327157116f984f0057a720ffc0d4f07b27fc54749cfde13655c2fcd9de5b27
#+end_src

- Accept ヘッダーをつけないとレスポンスが v1 形式になる。

* 正常系レスポンス
#+begin_example
  HTTP/1.1 200 OK
  Content-Length: 2389
  Content-Type: application/vnd.docker.distribution.manifest.v2+json
  Docker-Content-Digest: sha256:53327157116f984f0057a720ffc0d4f07b27fc54749cfde13655c2fcd9de5b27
  Docker-Distribution-Api-Version: registry/2.0
  Etag: "sha256:53327157116f984f0057a720ffc0d4f07b27fc54749cfde13655c2fcd9de5b27"
  Date: Wed, 26 Apr 2017 01:56:15 GMT

  {
     "schemaVersion": 2,
     "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
     "config": {
	"mediaType": "application/octet-stream",
	"size": 10354,
	"digest": "sha256:0c6459713d353fedbfc82d060e0f388a6c7e5fb1b9bcff4b08cd05dd7192cb3c"
     },
     "layers": [
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 71907148,
           "digest": "sha256:16dc1f96e3a1bb628be2e00518fec2bb97bd5933859de592a00e2eb7774b6ecf"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 20453837,
           "digest": "sha256:ae7edfc951fada8048f52ee10cc230f6284f65cddc5bb688ad1d3beabeddd6d1"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 227423245,
           "digest": "sha256:f470314425578258775ae479254d29dc63dce7faebcb7d728397e438bc2642bd"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 11024,
           "digest": "sha256:bc6ad4547eae714d1eadaaaedf5d2569e302ad991efacfff99929e255244a1a2"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 10922,
           "digest": "sha256:6852aa85b55070df2ccb319d2b662380bd98b0fdcf4129e901943918645bb24c"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 157,
           "digest": "sha256:fa8ec83b9be5ca44ee438f351cf2f6dd5ff4a597b879923408d684e1271984e4"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 22160,
           "digest": "sha256:50e115f0f0eb698037ccd1a03da40c2156e6df69b5a6e7461f1496f83946c694"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 1650,
           "digest": "sha256:301ef36b3b58ce7247d06741819ecb8585edc7994281a999f8635b26edcafb73"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 325,
           "digest": "sha256:88109b72df960f8bf42bd13f0280aa35d610e4f5b15290a08ae00eabaf41eb64"
	},
	{
           "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
           "size": 776,
           "digest": "sha256:f445f57823348cbb8cf7dbbc7b0444e767e4197267953eb73a80d96bd033ebd3"
	}
     ]
  }
#+end_example

- Docker-Content-Digest ヘッダーが他の操作で必要になる。
