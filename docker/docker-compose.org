
* Official
- [[https://docs.docker.com/compose/][Docker Docs]]
  - Overview
- [[https://docs.docker.com/compose/compose-file/][Compose file reference]]

* Installing
** Mac
brew install docker-compose

** CentOS7
GitHub 上のリリースページからダウンロードする。

- https://github.com/docker/compose/releases/

#+begin_src sh
  DOCKER_COMPOSE_VERSION=1.5.2
  curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
#+end_src

Proxy 指定する場合 -x, -U を指定する。

#+begin_src sh
  DOCKER_COMPOSE_VERSION=1.5.2
  curl -x PROXY_SERVER:PROXY_PORT -U PROXY_USER:PROXY_PASWORD -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
#+end_src

ダウンロードしたファイルに実行パーミッションをつける。

- [[http://fight-tsk.blogspot.jp/2015/04/centos7-docker-docker-compose.html][腹腹開発: CentOS7 上に Docker と Docker Compose をインストールする]]
