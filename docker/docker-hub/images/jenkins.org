#+title: jenkins

* Official
- [[https://hub.docker.com/_/jenkins/][Docker Hub]]

* Example
#+begin_src sh
  docker run --name jenkins -p 8080:8080 -d --restart always -v $HOME/.docker/volumes/jenkins_home:/var/jenkins_home jenkins
#+end_src

- restart オプションを指定してマシン再起動時にも再起動
- /var/jenkins_home をマウントして使う

* See also
- [[http://knjname.hateblo.jp/entry/2015/02/10/040833][Jenkinsの公式Dockerイメージ使ってみた - knjnameのブログ]]
