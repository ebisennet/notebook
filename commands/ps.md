# ps

## Linux

### ps auxef

-   e
    -   環境変数表示
-   f, --forest
    -   ツリー表示

### ps axef -o user:20:pid,comm

ユーザ名を省略しない。

### ps -eo euser,ruser,suser,fuser,f,comm,label

ユーザ名表示
