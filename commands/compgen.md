# compgen

## ディレクトリ

``` bash
compgen -d PATH

compgen -d -S / PATH
```

-   PATH は途中までのパスでも良い。
-   PATH 未指定の場合、カレントディレクトリ指定となる。
-   PATH が / で終わる場合はサブディレクトリが候補となる。

