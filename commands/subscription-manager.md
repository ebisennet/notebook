# subscription-manager

## 公式資料

- [Red Hat Enterprise Linux for Virtual Datacenter サブスクリプションを使用して VMware ゲストをサブスクライブする - Red Hat Customer Portal](https://access.redhat.com/ja/solutions/769473)
    - ゲストのうち１台で登録。
    - 登録後に作成された Subscription Pool を各ゲストVMにアタッチする。

## Proxy 設定

/etc/rhsm/rhsm.conf を参照。

## Commands

### attach

``` bash
subscription-manager attach --pool=<pool_id>
```

### auto-attach

### list

Subscription の一覧表示。

``` bash
subscription-manager list --available
```

### refresh

### register

Subscription 登録。

``` bash
subscription-manager register --username=<username> --password=<password>
```

### repos

Subscription 内のリポジトリの確認。

``` bash
subscription-manager repos
```

Subscription 内のリポジトリの有効化。

``` bash
subscription-manager repos --enable=rhel-7-server-extras-rpms 
subscription-manager repos --enable=rhel-7-server-optional-rpms
```

### unregister

Subscription 登録解除。
