#+title: createrepo

* 公式資料
- [[https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sec-Yum_Repository.html][Red Hat Customer Portal]]

* 参考
- [[http://dotnsf.blog.jp/archives/2807753.html][ローカル環境内に yum リポジトリを作成する : まだプログラマーですが何か？]]
  - 微妙に間違いあり？
- [[https://osdn.jp/magazine/14/01/10/090000/3][独自のRPMパッケージやyumリポジトリを作ってみよう 3ページ | OSDN Magazine]]
