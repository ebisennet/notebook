# defaults

## .DS_Store を作らない

``` bash
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
```

## 参考資料

- [Macギークなら知っておきたいdefaultsコマンドの使い方 | Macとかの雑記帳](http://tukaikta.blog135.fc2.com/blog-entry-209.html)
- [ターミナルから Mac を設定する（defaults write コマンド等） - Qiita](http://qiita.com/djmonta/items/17531dde1e82d9786816)

