# test

## 文字列長判定

- -n (nonzero)
- -z (zero)

## 参考資料

-   [if 文と test コマンド | UNIX & Linux コマンド・シェルスクリプト リファレンス](http://shellscript.sunone.me/if_and_test.html)

