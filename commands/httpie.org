#+title: httpie

* 公式資料
- [[https://github.com/jkbrzt/httpie][GitHub - jkbrzt/httpie: CLI HTTP client, user-friendly curl replacement with intuitive UI, JSON support, syntax highlighting, wget-like downloads, extensions, etc.]]

* インストール

#+begin_src sh
brew install httpie
#+end_src
