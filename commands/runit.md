# runit

## 公式資料

-   [runit - a UNIX init scheme with service supervision](http://smarden.org/runit/)

## 参考資料

-   [runit によるユーザ権限でのデーモン化 - Little Tech Notes](http://d.hatena.ne.jp/n_hayashi/20110422/1303454798)

