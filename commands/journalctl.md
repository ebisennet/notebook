# journalctl

## 指定ユニットのログ表示

``` example
sudo journalctl -u UNIT
```

## 参考資料

-   [journalctl 最低限覚えておくコマンド - Qiita](http://qiita.com/aosho235/items/9fbff75e9cccf351345c)

