# zip

## ディレクトリ圧縮

``` example
zip -r test.zip target
```

対象ディレクトリーの親ディレクトリーで実行すると良い。
