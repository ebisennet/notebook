# nmcli

設定変更後に network サービスの再起動をすること。

## 公式資料

-   [2.3. NetworkManager のコマンドラインツール nmcli の使用](https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/sec-Using_the_NetworkManager_Command_Line_Tool_nmcli.html)

## con

### add

### show <name>

指定した NIC の設定情報表示。

### modify

### delete

## 設定例

``` bash
nmcli c modify ethernet-eth0 ipv4.address 10.0.0.0/24 ipv4.gateway 10.0.0.254 ipv4.dns "10.0.0.1 10.0.0.2"
nmcli device connect eth0

nmcli c modify ethernet-eth1 ipv4.address 10.0.1.0/24 ipv4.gateway 0.0.0.0 ipv4.never-default yes
nmcli device connect eth1
```

## 参考資料

-   [nmcli コマンドで DNSサーバを変更できない時の対応方法（CentOS7） | あぱーブログ](https://blog.apar.jp/linux/3147/)
-   [複数のNICを持つ場合のデフォルトゲートウェイ - bit](http://iwsttty.hatenablog.com/entry/2015/03/01/152541)
    -   never-defaults を設定する。

