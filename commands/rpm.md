# rpm

## ファイル一覧

``` bash
rpm -ql PKGNAME
```

## インストールした日付順表

``` bash
rpm -qa --last
```

依存したパッケージも含めてパッケージを取得する場合は yumdownloader の --resolve を使う。

## パッケージ作成

-   spec ファイル例
    -   [amazon-ssm-agent/amazon-ssm-agent.spec at master · aws/amazon-ssm-agent · GitHub](https://github.com/aws/amazon-ssm-agent/blob/master/packaging/amazon-linux/amazon-ssm-agent.spec)

