# sed

## 基本

```bash
sed -e 's/test/xxx/g'
```

１行の中に複数の対象がある場合、正規表現の後ろの `g` が必要。

## 複数の置換を適用する場合

```bash
sed -e 's/test/xxx/; s/abc/yyy/'
```

## インプレース編集

```bash
sed -i.bak -e 's/test/xxx/; s/abc/yyy/' file1 file2
```

ファイルが編集され、元のファイルは拡張子 `.bak` 付きでバックアップされる。
