# pfctl

## Reset

``` example
sudo pfctl -f /etc/pf.cnf
```

## Debug

``` example
sudo pfctl -s state
```

通信を行ってから確認する

## /etc/sysctl.conf

net.inet.ip.forwarding=1 net.inet6.ip6.forwarding=1

## Tools

### IceFloor

-   [新・OS X ハッキング! (111) Yet Anotherなファイアウォール「PF」をラクに使う | マイナビニュース](http://news.mynavi.jp/column/osxhack/111/)
    -   GUI

## 参考資料

-   [Mac pfctl Port Forwarding - Sal Ferrarello](http://salferrarello.com/mac-pfctl-port-forwarding/)
-   [PF: OpenBSD パケットフィルター](http://ftp.tuwien.ac.at/.vhost/www.openbsd.org/xxx/faq/pf/ja/index.html)
-   [Port forwarding in Mavericks · GitHub](https://gist.github.com/kujohn/7209628)
-   [PF メモ - 魔術師見習いのノート](http://www.pied-piper.net/note/note.cgi?47)
    -   NAT 周り
-   [Using native OS X NAT with VirtualBox · Kamil Figiela](http://kfigiela.github.io/2014/11/07/using-native-os-x-nat-with-virutalbox/)

