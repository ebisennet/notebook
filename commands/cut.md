# cut

## Examples

``` bash
cut -d / -f 2
```

上記の場合

- デリミターは `/`
- ２カラム目の値を取り出し
