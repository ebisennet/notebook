# md5

## 指定ファイルの MD5 のみを出力

``` bash
md5 -q path/to/file
```

## openssl で代用

Linux では md5 コマンドが入っていないので openssl で代用。

``` bash
openssl md5 path/to/file | cut -f2 -d" "
```
