# ssh-keygen

## 秘密鍵から公開鍵生成

``` bash
ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
```

逆はできないので注意。
