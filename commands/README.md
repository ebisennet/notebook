# Commands

各 OS のコマンド別ノート。

| Command                                         | OS    | Keyword         |
|-------------------------------------------------+-------+-----------------|
| [ab](ab.md)                                     |       |                 |
| [arpwatch](arpwatch.md)                         |       | monitor         |
| [awk](awk.md)                                   |       |                 |
| [brctl](brctl.md)                               |       |                 |
| [cacti](cacti.md)                               |       | monitor         |
| [certutil](certutil.md)                         | Win   |                 |
| [compgen](compgen.md)                           |       |                 |
| [complete](complete.md)                         |       |                 |
| [createrepo](createrepo.md)                     |       | package manager |
| [curl](curl.md)                                 |       |                 |
| [cut](cut.md)                                   |       |                 |
| [date](date.md)                                 |       |                 |
| [defaults](defaults.md)                         | Mac   |                 |
| [dstat](dstat.md)                               |       |                 |
| [fdisk](fdisk.md)                               |       | file system     |
| [file](file.md)                                 |       |                 |
| [free](free.md)                                 |       |                 |
| [glances](glances.md)                           |       | monitor         |
| [hostnamectl](hostnamectl.md)                   |       |                 |
| [htop](htop.md)                                 |       | monitor         |
| [httpie](httpie.md)                             |       |                 |
| [iftop](iftop.md)                               |       | monitor         |
| [iostat](iostat.md)                             |       | monitor         |
| [iotop](iotop.md)                               |       | monitor         |
| [ip](ip.md)                                     |       |                 |
| [iptables](iptables.md)                         |       |                 |
| [iptraf](iptraf.md)                             |       | monitor         |
| [journalctl](journalctl.md)                     |       |                 |
| [logrotate](logrotate.md)                       |       |                 |
| [lsof](lsof.md)                                 |       | monitor         |
| [lvchange](lvchange.md)                         |       | lvm             |
| [lvcreate](lvcreate.md)                         |       | lvm             |
| [lvdisplay](lvdisplay.md)                       |       | lvm             |
| [lvextend](lvextend.md)                         |       | lvm             |
| [lvm](lvm.md)                                   |       | lvm             |
| [lvremove](lvremove.md)                         |       | lvm             |
| [md5](md5.md)                                   |       |                 |
| [mkfifo](mkfifo.md)                             |       |                 |
| [monit](monit.md)                               |       | monitor         |
| [monitorx](monitorx.md)                         |       | monitor         |
| [mount](mount.md)                               |       | file system     |
| [mpstat](mpstat.md)                             |       | monitor         |
| [munin](munin.md)                               |       | monitor         |
| [nagios](nagios.md)                             |       | monitor         |
| [nc](nc.md)                                     |       | network         |
| [nethogs](nethogs.md)                           |       | monitor         |
| [netstat](netstat.md)                           |       | monitor         |
| [nmcli](nmcli.md)                               |       |                 |
| [nmon](nmon.md)                                 |       | monitor         |
| [paste](paste.md)                               |       |                 |
| [perl](perl.md)                                 |       |                 |
| [pfctl](pfctl.md)                               | BSD   | network         |
| [prlimit](prlimit.md)                           | Linux | process         |
| [ps](ps.md)                                     |       | process         |
| [psacct](psacct.md)                             |       | monitor         |
| [pvcreate](pvcreate.md)                         |       | lvm             |
| [pvdisplay](pvdisplay.md)                       |       | lvm             |
| [pvmove](pvmove.md)                             |       | lvm             |
| [pvscan](pvscan.md)                             |       | lvm             |
| [rehash](rehash.md)                             |       | network         |
| [route](route.md)                               |       |                 |
| [rpm2cpio](rpm2cpio.md)                         |       |                 |
| [rpm](rpm.md)                                   |       |                 |
| [rsync](rsync.md)                               |       | file            |
| [runit](runit.md)                               |       |                 |
| [sar](sar.md)                                   |       | monitor         |
| [sarg](sarg.md)                                 |       | monitor         |
| [script](script.md)                             |       |                 |
| [service](service.md)                           |       |                 |
| [shellcheck](shellcheck.md)                     |       |                 |
| [shopt](shopt.md)                               |       |                 |
| [ssh-add](ssh-add.md)                           |       |                 |
| [ssh-agent](ssh-agent.md)                       |       |                 |
| [ssh-copy-id](ssh-copy-id.md)                   |       | ssh             |
| [ssh-keygen](ssh-keygen.md)                     |       |                 |
| [ssh](ssh.md)                                   |       |                 |
| [su](su.md)                                     |       |                 |
| [subscription-manager](subscription-manager.md) |       |                 |
| [sudo](sudo.md)                                 |       |                 |
| [suricata](suricata.md)                         |       | monitor         |
| [sysstat](sysstat.md)                           |       | monitor         |
| [systemctl](systemctl.md)                       |       |                 |
| [systemsetup](systemsetup.md)                   | Mac   |                 |
| [systemstats](systemstats.md)                   | Mac   |                 |
| [tc](tc.md)                                     |       |                 |
| [tcpdump](tcpdump.md)                           |       | monitor         |
| [test](test.md)                                 |       |                 |
| [top](top.md)                                   |       | monitor         |
| [tr](tr.md)                                     |       |                 |
| [typeset](typeset.md)                           |       |                 |
| [ulimit](ulimit.md)                             |       | process         |
| [uname](uname.md)                               |       |                 |
| [vgchange](vgchange.md)                         |       | lvm             |
| [vgcreate](vgcreate.md)                         |       | lvm             |
| [vgdisplay](vgdisplay.md)                       |       | lvm             |
| [vgextend](vgextend.md)                         |       | lvm             |
| [vgreduce](vgreduce.mv)                         |       | lvm             |
| [vgremove](vgremove.md)                         |       | lvm             |
| [vgrename](vgrename.md)                         |       | lvm             |
| [vgscan](vgscan.md)                             |       | lvm             |
| [vgsplit](vgsplit.md)                           |       | lvm             |
| [vmstat](vmstat.md)                             |       | monitor         |
| [watch](watch.md)                               |       |                 |
| [who](who.md)                                   |       |                 |
| [whoami](whoami.md)                             |       |                 |
| [xargs](xargs.md)                               |       |                 |
| [xfs_growfs](xfs_growfs.md)                     |       | file system     |
| [yum](yum.md)                                   |       | package manager |
| [yumdownloader](yumdownloader.md)               |       |                 |
| [zip](zip.md)                                   |       |                 |

## 参考資料

- [Advanced Bash-Scripting Guide](http://tldp.org/LDP/abs/html/index.html)
    - [External Filters, Programs and Commands](http://tldp.org/LDP/abs/html/external.html)
        - [Text Processing Commands](http://tldp.org/LDP/abs/html/textproc.html)

