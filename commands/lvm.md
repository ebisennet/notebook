# lvm

Logical Volume Manager.

- Kernel 2.4 からサポート。
- Kernel 2.6 からは LVM2 のサポート。

## 公式資料

- [LVM2 Resource Page](https://www.sourceware.org/lvm2/)

## 参考資料

- [3. LVM（Logical Volume Manager）(第2章ディスク管理～上級:基本管理コースII)](https://users.miraclelinux.com/technet/document/linux/training/2_2_3.html)
