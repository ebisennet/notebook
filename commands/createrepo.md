# createrepo

## 公式資料

- [Red Hat Customer Portal](https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sec-Yum_Repository.html)

## 参考資料

-   [ローカル環境内に yum リポジトリを作成する : まだプログラマーですが何か？](http://dotnsf.blog.jp/archives/2807753.html)
    -   微妙に間違いあり？
-   [独自のRPMパッケージやyumリポジトリを作ってみよう 3ページ | OSDN Magazine](https://osdn.jp/magazine/14/01/10/090000/3)

