# ulimit

Bash 組み込みコマンド。

システムリソースの上限の設定、表示。

## 全項目表示

``` bash
ulimit -a
```

## 関連

- [prlimit](prlimit.md)

