# vgrename

ボリュームグループ名のリネーム。

```bash
vgrename OLD_VOLUME_GROUP NEW_VOLUME_GROUP
```

Old Name として UUID を指定することもできる。
