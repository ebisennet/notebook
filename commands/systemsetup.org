#+title: systemsetup

#+begin_src sh
  sudo systemsetup -setdisplaysleep Never
  sudo systemsetup -setdisplaysleep 1
#+end_src
