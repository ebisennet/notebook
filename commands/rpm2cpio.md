# rpm2cpio

## RPM からのファイル取り出し

作業ディレクトリーにて。

``` example
rpm2cpio /path/to/rpm | cpio -id
```
