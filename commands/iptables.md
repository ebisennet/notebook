# iptables

## Files

-   /etc/sysconfig/iptables
    -   行単位で iptables 実行時の引数を記述

## FW への許可ポート設定追加

``` example
sudo iptables -A OS_FIREWALL_ALLOW -p tcp -m state --state NEW -m tcp --dport 10080 -j ACCEPT
```

Range 指定

``` example
sudo iptables -A OS_FIREWALL_ALLOW -p tcp -m state --state NEW -m tcp --dport 10000:19999 -j ACCEPT
```

## 設定の表示

``` example
sudo iptables -L
```

``` example
sudo iptables -L -n - v
```
