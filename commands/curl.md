# curl

## ダウンロード

``` example
curl -sSL http://...
```

色々な表示を抑制。 かつ、リダイレクト対応。

## Proxy

- -x http://user:password@host:port

その他の設定方法。

- ~/.curlrc
- 環境変数
    - http_proxy
    - https_proxy
    - [url-protocol]_PROXY
    - ALL_PROXY
    - NO_PROXY

## UA 指定

- -A NAME
- -H 'User-Agent: NAME'

## ユーザ名指定、パスワード指定

- -u $USER
    - パスワードは聞かれる
- -u $USER:PASS

## JSON 送信

``` bash
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"foo":"bar"}' https://example.com/api
```

## WebDAV 操作

ディレクトリ作成

```
curl -X MKCOL 'http://example.com/new_folder'
```

- 参考
    - [Using Curl commands with Webdav \| QED42](http://www.qed42.com/blog/using-curl-commands-webdav)

## Socks Proxy 指定

```
curl --socks5 host:port ...
```

## パラメーター渡し

```
curl -d 'foo=1' -d 'bar=2' ...
```

URL エンコードも考慮してくれる。
