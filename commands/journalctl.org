#+title: journalctl

* 指定ユニットのログ表示
: sudo journalctl -u UNIT

* 参考
- [[http://qiita.com/aosho235/items/9fbff75e9cccf351345c][journalctl 最低限覚えておくコマンド - Qiita]]
