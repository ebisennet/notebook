# yumdownloader

インストール済みパッケージのダウンロード。

## 公式資料

-   [yum を使用して、パッケージをインストールせずにダウンロードだけを行う方法 - Red Hat Customer Portal](https://access.redhat.com/ja/node/395763)

## インストール

``` bash
yum install yum-utils
```

## RPMダウンロード

``` bash
yumdownloader --resolve PKGNAME
```

現在の環境に対して不足する依存もダウンロードする。
