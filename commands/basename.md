# basename

ファイルパスからベース名を取り出す。

## 複数ファイルのベース名取得

```bash
basename -a /etc/*.d
```
