# shellcheck

## 公式資料

- [koalaman/shellcheck · GitHub](https://github.com/koalaman/shellcheck)
- [Home · koalaman/shellcheck Wiki · GitHub](https://github.com/koalaman/shellcheck/wiki)
    - エラーコード別のページなどがある。

## Emacs から使う場合

- [Flycheck: Quickstart](http://www.flycheck.org/manual/latest/Quickstart.html#Quickstart)
- [GitHub - flycheck/flycheck-pos-tip: Flycheck errors display in tooltip](https://github.com/flycheck/flycheck-pos-tip)

