# watch

指定したコマンドの定期実行。 出力はフルスクリーン表示。

## フルスクリーンにしたくない場合

watch コマンドを使わずワンライナーでループ記述。

``` bash
while : ; do du -sh /mnt/iso/OVF/ ; sleep 10 ; done
```
