# date

## YYYYMMDD-HHMMSS

``` bash
date "+%Y%m%d-%H%M%S"
```

先頭に + があることに注意。
