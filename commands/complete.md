# complete

## ホスト名補完

``` bash
complete -F _ssh_copy_id TARGET
```

## 参考資料

- [bash なんて書いたことない人が補完関数をとりあえず自作する - Qiita](http://qiita.com/sosuke/items/06b64068155ae4f8a853)

