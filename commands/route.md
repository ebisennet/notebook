# route

## ルーティング確認

``` bash
route
```

## ルーティング追加

``` bash
sudo route add -net 10.116.10.0/24 10.45.95.254
```

## 参考資料

-   [Macでルートの追加、ルーティングテーブルの確認 - Disce gaudere. 楽しむ事を学べ。](http://d.hatena.ne.jp/keroring/20120326/1332739760)

