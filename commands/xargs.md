# xargs

Mac と Linux でオプションが一部違うので注意。

コマンド省略すると echo 扱い。

## 指定行ずつ処理

``` bash
ls -1 | xargs -L 3 echo args:
```

```bash
ls -1 | xargs -I {} echo {} test
```

## 空ファイルに対する扱い

Mac と Linux で終了コードが違う。

Mac:

```
$ cat /dev/null | xargs grep xxx ; echo $?
0
```

Linux:

```
$ cat /dev/null | xargs grep xxx ; echo $?
123
```

## １項目ずつ処理

```bash
echo aaa bbb ccc | xargs -n 1 echo
```
