# shopt

## 再帰的なパス展開を有効にする

```
shopt -s globstar
```

globstar を有効化すると例えば `ls */**/*.txt` を実行した時のマッチする対象が以下のように変わる。  
（ `**` は globstar 有効時は ０個以上のディレクトリとマッチする。デフォルトは `*` と同じ）

| パス              | デフォルト | globstar 付き |
|-------------------+------------+---------------|
| a.txt             | -          | -             |
| dir/a.txt         | -          | ○              |
| dir/dir/a.txt     | ○         | ○            |
| dir/dir/dir/a.txt | -          | ○            |

## 参考資料

- [Pathname expansion \(globbing\) Bash Hackers Wiki](http://wiki.bash-hackers.org/syntax/expansion/globs)
