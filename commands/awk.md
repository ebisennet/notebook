# awk

## markdown リンク作成

``` bash
ls -1 | awk '{ print "[" $1 "](" $1 ")" }'
```

## デリミター変更

``` bash
cat test.csv | awk -F, '{ print $1 $3 }'
```

## Memo

``` bash
cat types.txt | cut -d : -f 3,5 | sed -e 's/:/\ /' | awk '{print "- [AWS::"$1"::"$2"]("$1"/"$2".md)" }' | pbcopy
```
