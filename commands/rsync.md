# rsync

## 参考資料

-   [Command Technica：rsyncで差分バックアップを行うための「--link-dest」オプション - ITmedia エンタープライズ](http://www.itmedia.co.jp/enterprise/articles/0804/25/news034.html)
-   [バックアップにrsync --link-destを使うと良い場合もあるよ | rutoの日記 | スラド](http://srad.jp/~ruto/journal/362588)
-   [rsync でファイルに変更がない場合にhardlinkを作成する --link-dest を利用して差分バックアップを取る - うまいぼうぶろぐ](http://hogem.hatenablog.com/entry/20130122/1358862685)
-   [Limiting rsync traffic](https://forum.pfsense.org/index.php?topic=53994.0)
    -   --bwlimit オプションによる Traffic Control

