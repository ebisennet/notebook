# yum

## 公式資料

- [yum - Trac](http://yum.baseurl.org/)

## Proxy

- [yum/wgetをproxy経由で使う方法(CentOS) - hogehoge foobar Blog Style5](http://d.hatena.ne.jp/mrgoofy33/20110125/1295966614)

/etc/yum.conf に以下を設定。

``` example
proxy=http://your.proxy.server:8080
proxy_username=username
proxy_password=password
```

リポジトリの定義ファイルごとに設定することも可能。

no proxy にする場合。

``` example
proxy=_none_
```

- [How to specify that Yum should not use proxy for specific domain - Super User](http://superuser.com/questions/393099/how-to-specify-that-yum-should-not-use-proxy-for-specific-domain)

## リポジトリ作成

- [createrepo](createrepo.md) 参照

## 関連コマンド

- repomanage
- repoquery
- reposync
- [rpm](rpm.md)
- verifytree

## Yum変数

-   [Red Hat Customer Portal](https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sec-Using_Yum_Variables.html)

## ダウンロードのみを行う

yum のプラグインである yum-plugin-downloadonly をセットする。

``` bash
yum install yum-plugin-downloadonly
```

実際にダウンロードをするには

``` bash
yum install --downloadonly --downloaddir=<directory> <package>
```

## キャッシュクリア

``` bash
yum clean all
```

プライベートリポジトリにパッケージを追加した後に yum search で見つからない時などに。

## リポジトリ限定

``` bash
yum --disablerepo=* --enablerepo=test7.2 list
```

## EPEL 追加

yum install epel-release

## 参考資料

- [yum.conf(5): config file for yum - Linux man page](http://linux.die.net/man/5/yum.conf)
    - 設定ファイルの記述についての man page.
- [Protected RPM repositories with yum and SSL](http://atodorov.org/blog/2011/09/15/protected-rpm-repositories-with-yum-and-ssl/)
    - SSL クライアント照明を使う場合
- [パッケージの管理｜yumとは](http://kazmax.zpp.jp/linux_beginner/yum.html)

