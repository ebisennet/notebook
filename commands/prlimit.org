#+title: prlimit

指定プロセスのリミットの設定・取得。

特定プロセスのリミット取得だけなら /proc/PID/limits を見た方が良い。
