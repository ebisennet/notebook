# perl

## url エンコード

```bash
ls -1 | perl -nlpe 's/([^ -~])/"%".unpack("H2",$1)/eg'
```

- via. [ワンライナー | 未分類 | hydroculのメモ](https://hydrocul.github.io/wiki/others/oneliner.html)
