# certutil

証明書関連のユーティリティ。

## base64 デコード

```
certutil -decode SRC DEST
```

## 参考資料

- [Windowsコマンド集 - certutil：ITpro](http://itpro.nikkeibp.co.jp/article/Windows/20051013/222768/)
