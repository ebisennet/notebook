# logrotate

## 設定ファイル

-   /etc/logrotate.conf
-   /etc/logrotate.d/*

## ディレクティブ

### compress

ログの gzip 圧縮指定。

### copytruncate

ログファイルをコピー後にオリジナルログファイルをtruncateする。

### create

ログをローテートした後にファイル作成する。

作成されるログファイルのパーミッション指定可能。

### dateext

ログファイルのファイル名に日付を付与する。

### maxage

ログを保持する期間を日数で指定する。

### missingok

ログファイルがなくてもエラーメッセージを出さない。

### notifempty

ログが空の時はローテートしない。 not if empty.

### rotate

何世代ローテーションするか。

### size

ログのローテーションを開始する条件となるログファイルのサイズ指定。

## 参考資料

- [ログローテートツール「logrotate」を使いこなしたい - ITmedia エンタープライズ](http://www.itmedia.co.jp/help/tips/linux/l0291.html)

