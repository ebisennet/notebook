# tc

Traffic Control コマンド。

## Memo

-   Man page が分かれているので注意。
    -   tc-tbf
        -   Token Bucket Filter を使う際の設定。

## 参考資料

-   [よくわかるLinux帯域制限 | GREE Engineers' Blog](http://labs.gree.jp/blog/2014/10/11266/)
-   [Linux TC (帯域制御、帯域保証) 設定ガイドライン | GREE Engineers' Blog](http://labs.gree.jp/blog/2014/10/11288/)
-   [シンプルな、クラスレスのキューイング規則](http://linuxjf.osdn.jp/JFdocs/Adv-Routing-HOWTO/lartc.qdisc.classless.html)
-   [Linuxでアウトバウンド帯域幅を制限する ｜ Developers.IO](http://dev.classmethod.jp/server-side/shaping_outbound_traffic_from_linux/)

