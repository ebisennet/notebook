#+title: pfctl

* Reset
: sudo pfctl -f /etc/pf.cnf

* Debug
: sudo pfctl -s state

通信を行ってから確認する

* /etc/sysctl.conf
net.inet.ip.forwarding=1
net.inet6.ip6.forwarding=1

* Tools
** IceFloor
- [[http://news.mynavi.jp/column/osxhack/111/][新・OS X ハッキング! (111) Yet Anotherなファイアウォール「PF」をラクに使う | マイナビニュース]]
  - GUI

* See also
- [[http://salferrarello.com/mac-pfctl-port-forwarding/][Mac pfctl Port Forwarding - Sal Ferrarello]]
- [[http://ftp.tuwien.ac.at/.vhost/www.openbsd.org/xxx/faq/pf/ja/index.html][PF: OpenBSD パケットフィルター]]
- [[https://gist.github.com/kujohn/7209628][Port forwarding in Mavericks · GitHub]]
- [[http://www.pied-piper.net/note/note.cgi?47][PF メモ - 魔術師見習いのノート]]
  - NAT 周り
- [[http://kfigiela.github.io/2014/11/07/using-native-os-x-nat-with-virutalbox/][Using native OS X NAT with VirtualBox · Kamil Figiela]]
