# systemctl

## Unit 一覧表示

``` example
sudo systemctl list-units
```

disabled のものは表示されない。 インストールされている unit を全て表示するには list-unit-files を使う。

``` example
sudo systemctl list-unit-files
```

## show

指定したユニットの設定を表示できる。

``` example
sudo systemctl show UNIT 
```

## status

## start

## stop

## enable

## disable

## daemon-reload

## restart

## mask

指定したユニットの定義を無視するように設定する。

disable にした時と異なり、他のユニットから依存関係で呼び出された時にエラーとしてのログが出力されない。
