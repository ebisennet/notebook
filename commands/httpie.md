# httpie

## 公式資料

-   [GitHub - jkbrzt/httpie: CLI HTTP client, user-friendly curl replacement with intuitive UI, JSON support, syntax highlighting, wget-like downloads, extensions, etc.](https://github.com/jkbrzt/httpie)

## インストール

``` bash
brew install httpie
```
