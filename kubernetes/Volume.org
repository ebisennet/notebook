#+title: Volume

Pod から使用する Volume について。

Docker の Volume に機能が追加されている。

* Official
- [[https://github.com/kubernetes/kubernetes/blob/master/docs/user-guide/volumes.md][kubernetes/volumes.md at master · kubernetes/kubernetes · GitHub]]

* Memo
- Docker と異なり、デフォルトでコンテナ内は readonly となる
  - Pod 起動時の定義で使用する Volume とそれらをどこにマウントするかを定義して初めて書き込み可能に。

* See also
- [[http://netmark.jp/2014/12/kubernetes-volume.html][KubernetesでのVolumeの扱い - netmark.jp]]
