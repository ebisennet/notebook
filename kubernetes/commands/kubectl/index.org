#+title: kubectl

Kubernetes Master Server の API を呼んで、Kubernetes Cluster を管理するためのCLI.

以前はシェルスクリプト ~cluster/kubectl.sh~ だったのが置き換わった。

| Command        |                                                                                              |
|----------------+----------------------------------------------------------------------------------------------|
| api-versions   | Print available API versions.                                                                |
| cluster-info   | Display cluster info                                                                         |
| config         | config modifies kubeconfig files                                                             |
| create         | Create a resource by filename or stdin                                                       |
| delete         | Delete a resource by filename, stdin, resource and name, or by resources and label selector. |
| describe       | Show details of a specific resource or group of resources                                    |
| exec           | Execute a command in a container.                                                            |
| expose         | Take a replicated application and expose it as Kubernetes Service                            |
| get            | Display one or many resources                                                                |
| help           | Help about any command                                                                       |
| label          | Update the labels on a resource                                                              |
| logs           | Print the logs for a container in a pod.                                                     |
| namespace      | SUPERCEDED: Set and view the current Kubernetes namespace                                    |
| patch          | Update field(s) of a resource by stdin.                                                      |
| port-forward   | Forward one or more local ports to a pod.                                                    |
| proxy          | Run a proxy to the Kubernetes API server                                                     |
| replace        | Replace a resource by filename or stdin.                                                     |
| rolling-update | Perform a rolling update of the given ReplicationController.                                 |
| run            | Run a particular image on the cluster.                                                       |
| scale          | Set a new size for a Replication Controller.                                                 |
| stop           | Gracefully shut down a resource by name or filename.                                         |
| version        | Print the client and server version information.                                             |
