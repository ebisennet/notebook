# 多重化

同じサーバーに接続するときに既存の接続を利用して接続する。

パスワード入力を省略できる。

## 設定

~/.ssh/config の最初に。

```
ControlMaster auto
ControlPersist yes
ControlPath ~/.ssh/mux/%C
```

~/.ssh/mux ディレクトリを作成しておく。

## ControlMaster

## ControlPath

OpenSSH 6.7 から %C が使える。

## 参考資料

- [OpenSSH/Cookbook/Multiplexing \- Wikibooks, open books for an open world](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Multiplexing)
- [最強のSSH踏み台設定 \- Qiita](http://qiita.com/kawaz/items/a0151d3aa2b6f9c4b3b8)
- [sshの接続を早くする(ControlMaster) - Qiita](http://qiita.com/tukiyo3/items/806c4502ed7522fbcf95)
- [OpenSSHのセッションを束ねるControlMasterの使いにくい部分はControlPersistで解決できる \- Dマイナー志向](http://d.hatena.ne.jp/tmatsuu/20120707/1341677472)
