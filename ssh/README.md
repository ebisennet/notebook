# ssh

## 公式資料

- [OpenSSH](http://www.openssh.com/)
- [GitHub - openssh/openssh-portable: Portable OpenSSH](https://github.com/openssh/openssh-portable)
  - Portable OpenSSH のソースコード

## Topics

- [環境変数](environments.md)
- [バージョン別](versions.md)
- [多重化](multiplexing.md)

## 設定ファイル

- 先に書いたものが優先される。
    - 共通設定を個別にカスタマイズする場合、共通設定を最後に書く。
    - 共通設定の個別カスタマイズを禁止する場合、共通設定を最初に書く。

## MaxStartups

一定確率で ssh 接続要求を拒否する設定ができる。

- [接続制限の設定](http://www14.plala.or.jp/campus-note/vine_linux/server_ssh/ssh_filter.html)

## ECDSA

OpenSSH-5.7以降で使える。

- [SSHの鍵をECDSAにした | n10の個人的なメモ](http://mirahouse.jp/n10/2014/07/05-143545.html)

## 多段接続

- [最強のSSH踏み台設定 \- Qiita](http://qiita.com/kawaz/items/a0151d3aa2b6f9c4b3b8)

## ProxyJump (7.3)

```
ProxyJump user@gateway
```

- ProxyCommand で `ssh user@gateway -W %h:%p` としていたのをさらに短縮する設定。
- カンマ区切りで多段接続設定。
- ssh コマンドラインオプション上は `-J` が対応する。

## Windows

- [windowsに公式なsshdをインストールし、linuxからwindowsにssh接続 ｜ Developers.IO](http://dev.classmethod.jp/cloud/aws/aws-windows-sshd/)
