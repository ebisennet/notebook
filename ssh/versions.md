# バージョン別

## 7.3

- Include
    - 設定ファイル内で別ファイルを読み込み。
- ProxyJump
    - ProxyCommand で `ssh -W %h:%p` を使っていたことをさらに短縮する。
    - ssh コマンドラインオプション上は `-J` が対応する。
- IdentityAgent
    - SSH_AUTH_SOCK 相当。

## 6.7

- ControlPath で %C が指定可能に。
    - [最強のSSH踏み台設定 \- Qiita](http://qiita.com/kawaz/items/a0151d3aa2b6f9c4b3b8)

## 5.7

- ECDSA サポート。
    - [SSHの鍵をECDSAにした | n10の個人的なメモ](http://mirahouse.jp/n10/2014/07/05-143545.html)
