# ruby

## 公式資料

- [オブジェクト指向スクリプト言語 Ruby](http://www.ruby-lang.org/ja/)
- [Ruby Reference Manual](http://docs.ruby-lang.org/ja/)
- [ライブラリ一覧 (Ruby 2.1.0)](http://docs.ruby-lang.org/ja/2.1.0/library/index.html)
    - [library _builtin (Ruby 2.1.0)](http://docs.ruby-lang.org/ja/2.1.0/library/_builtin.html)
- [Ruby-Doc.org: Documenting the Ruby Language](http://ruby-doc.org/)

## Topics

- [インストール](installation.md)
- [文法](syntax/)
- [メタプログラミング](meta.md)
- エディター
    - [flycheck でrubocopとruby-lintを使う - Qiita](http://qiita.com/aKenjiKato/items/9ff1a153691e947113bb)
- [ワンライナー](oneliner.md)
