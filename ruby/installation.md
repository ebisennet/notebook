# インストール

## gem インストールに必要なパッケージインストール

ネイティブコードのコンパイル環境が大抵必要になる。

RHEL の場合、以下のパッケージ。

- ruby-devel
    - Cent OS 7 からは EPEL 参照しないといけない。
    - RHEL 7 の場合、 subscription manager で optional repo を有効化する必要がある。
        - `subscription-manager repos  --enable rhel-7-server-optional-rpms`
        - via. [redhat - No ruby-devel in RHEL7? - Stack Overflow](http://stackoverflow.com/questions/30665912/no-ruby-devel-in-rhel7)
- gcc
