# ブロック

## ブロック渡し

```ruby
[1, 2, 3].map { |x| x * 2 }

# ブロックをProcオブジェクト化して使う場合、Procを渡す所で & が必要。
p = Proc.new { |x| x * 2 }
[1, 2, 3].map &p

# & でProc以外を指定すると #to_proc が呼ばれる。
# 以下は Symbol#to_proc が呼ばれる。
# 生成された Proc は最初の引数として receiver を受け取る。
%w(a b c).map(&:upcase)
```

## ブロック引数

```ruby
# 引数の最後のみ可能
def test(a, b, &block)
  block.call(a, b)
end

# yield 使うなら引数は省略可能。
def test(a, b)
  yield a, b
end
```

## 参考資料

- [class Proc (Ruby 2.0.0)](http://docs.ruby-lang.org/ja/2.0.0/class/Proc.html)
