# 文法

- [Hash リテラル](hash-literal.md)
- [Symbol リテラル](symbol-literal.md)
- [ブロック](blocks.md)
- [Lambda](lambda.md)

## 多値代入

```ruby
x, y, z = [1, 2, 3]

# 正規表現によるキャプチャー結果の取得。
# captures メソッドが使いやすい。
foo, bar = "xxxfooyyybarzzz".match(/(foo).*(bar)/).captures

# リスト処理（car, cdr）
first, *rest = list
```

一部要素を捨てるには `_` に代入で。
