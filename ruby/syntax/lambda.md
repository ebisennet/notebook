# Lambda

アロー記法が使えるのは Ruby 1.9 から。

```ruby
lambda = -> x { x * 2}
[1, 2, 3].map &lambda

# 複数引数はカッコが必要
lambda = -> (x, y) { x + y }

# 以下は & と -> の間にスペースがあるとNG
[1, 2, 3].map &-> x { x * 2}

# 以下は OK
[1, 2, 3].map(& -> x { x * 2})
```

## 参考資料

- [ruby 1.9系のラムダ記法 - 雑草SEの備忘録](http://normalse.hatenablog.jp/entry/2016/03/11/003653)
    - 1.9 と 2.0　以降でアロー記法で記述に若干違いがある。
