# Hash リテラル

Ruby 1.9 から JS ライクな記法が使える。

```ruby
x = { x: 1, y: 2 }
```

Hash のキーは Symbol となる。

