# メタプログラミング

## 参考資料

- [Rubyでメソッドを上書き(monkey patch)をする方法を調べてみた | 69log](https://blog.kazu69.net/2014/11/23/examined-how-to-override-monkey-patch-methods-in-ruby/)
- [NAMAKESUGI |includeとalias_methodによるインスタンスメソッドの書き換え](http://namakesugi.blog.fc2.com/blog-entry-115.html)
- [Rubyのinitializeメソッドの継承 | Ruby入門 - Worth Living](http://worthliv.com/ruby_rubyinitialize_1.html)

