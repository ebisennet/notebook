# ワンライナー

## 各行をトリムするフィルター

```bash
ruby -pne '$_.strip!'
```

## 正規表現置換フィルター

```bash
ruby -pne '$_.gsub!(/test/, "xxxx")'
```

## 参考資料

- [Rubyワンライナー入門 - maeharin log](http://maeharin.hatenablog.com/entry/20130113/ruby_oneliner)
- [Ruby One-liner - Qiita](http://qiita.com/ksomemo/items/10141bb774ac20d0dd38)
