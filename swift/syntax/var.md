# var

変数宣言。

## 基本

``` swift
var x, y, z

// 全て String
var x, y, z: String = "test"
```
