# enum

## 省略記法

パラメーター指定やプロパティ代入などで列挙型の値を指定する場合は、型名を省略可能。

```swift
AWSLogger.defaultLogger().logLevel = .Verbose
```

## 参考資料

- [enumからNSErrorを作成する \- Qiita](http://qiita.com/almichest/items/43a3b3ecb5481f44d359)
