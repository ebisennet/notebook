# try-catch

```swift
do {
  try x = foo()
} catch {
  return false
}
```

## try?

エラーハンドリング結果を Optional Value に変換する。

```swift
guard let x = try? foo() else {
  return false
}
```
