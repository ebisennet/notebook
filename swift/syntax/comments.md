# comments

```
// comment

/*
 * comment
 */
```

## Quick Help 用コメント

```
/// comment

/**
 * comment
 */
```

Xcode7 からコメント書式として [markdown](../../markdown) が使える。

- リストのインデントは SPC2 ?

Xcode では `Cmd+Opt+/` でコメントのテンプレートを追加できる。

## 参考資料

- [Swiftのドキュメントコメント \- Qiita](http://qiita.com/hironytic/items/12589b4735f4c95eec0c)
    - [Swift header documentation in Xcode 7 — Erica Sadun](http://ericasadun.com/2015/06/14/swift-header-documentation-in-xcode-7/)
