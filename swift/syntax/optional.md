# optional

## Optional Bindings

### if let ... { ... }

### guard let ... else ...

- 初期化式は Optional となる式。
- 変数は非 Optional となる。
- Optional.None に対しては else 節を実行する。

```swift
let data : Foo?

// data2 の型は Bar
guard let data2 = data as? Bar else {
  return false
}
```

## as?

- 右辺：非Optional
- 結果：Optional

Optional Bindings と組み合わせれば、変数は非Optional.

```swift
let data : Foo?

// data2 の型は Optional<Bar>
let data2 = data as? Bar
```

Unwrap と Downcast を同時にはできないので以下のように Unwrap -> Downcast の２段階で記述。

```swift
guard let anyRoot = json["root"] else {
  return false;
}

guard let root = anyRoot as? String else {
  return false;
}
```

## as!

- 右辺：非Optional
- 結果：非Optional

Forced Unwrapping ないし Forced Downcast されるので危険。

## 参考資料

- [どこよりも分かりやすいSwiftの"?"と"\!" \- Qiita](http://qiita.com/maiki055/items/b24378a3707bd35a31a8)
- [SwiftのOptional BindingのTips \- Qiita](http://qiita.com/hachinobu/items/2ecfc5999327fff20837)
