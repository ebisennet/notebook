# Syntax

## 公式資料

- [The Swift Programming Language \(Swift 3\): About Swift](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/index.html)
- [The Swift Programming Language \(Swift 3\): The Basics](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html)

## Topics

- [let](let.md)
- [var](var.md)
- [comments](comments.md)
- [optional](optional.md)
- [enum](enum.md)
- [try-catch](try-catch.md)
- convenience initializer
    - [convenienceイニシャライザとdesignated（指定）イニシャライザ \- Qiita](http://qiita.com/edo_m18/items/733d8c81fb00942e3167)
