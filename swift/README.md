# Swift

## 公式資料

- [The Swift Programming Language (Swift 2.1): The Basics](https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID309)
- [The Swift Programming Language (Swift 2.1): About the Language Reference](https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/AboutTheLanguageReference.html#//apple_ref/doc/uid/TP40014097-CH29-ID345)

## Topics

- [文法](syntax/)
- [CLI](cli.md)
- Objective-C
    - [SwiftとObjective\-Cで相互に連携する \- Qiita](http://qiita.com/edo_m18/items/861d090a5471f4f0eeae)
    - Objective-C Runtime Functions
        - [SwiftでObjective\-Cの黒魔術ってどうなった？ \- Qiita](http://qiita.com/fmtonakai/items/e9036dec4af2609b5715)
    - [Using Swift with Cocoa and Objective\-C \(Swift 3\.0\.1\): Basic Setup](https://developer.apple.com/library/content/documentation/Swift/Conceptual/BuildingCocoaApps/index.html#//apple_ref/doc/uid/TP40014216-CH2-ID0)
    - [iOS8\+SwiftでperformSelectorが使えない \- Qiita](http://qiita.com/takuyaaan/items/a62aa583e91f1a3509da)
        - Swift 3 で復活している
- Server APIs Projects
    - [Swift\.org \- Server APIs Project](https://swift.org/server-apis/)
- [バージョン別](versions.md)
 
## Memo

- SourceKit
    - [SourceKitについての話 - Qiita](http://qiita.com/ushisantoasobu/items/320db16691948a52b5e4)
- Internal
    - [SwiftのToll-Free Bridgeの実装を読む - Qiita](http://qiita.com/omochimetaru/items/a520387d2b2e63f8efb5)
    - [Swiftコンパイラの構造と基盤テクニック - Qiita](http://qiita.com/demmy/items/f08a65298d2f2caf1360)

## Memo

- [まだSwiftyJSONで消耗してるの？ - Qiita](http://qiita.com/koher/items/300d89136b515291dac4#%E3%81%8A%E3%81%BE%E3%81%91)
- [SwiftのOptional型を極める - Qiita](http://qiita.com/koher/items/c6f446bad54442a28bf4)
