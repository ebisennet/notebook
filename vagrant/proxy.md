# Proxy

Proxy 環境下で Vagrant を使用する場合の設定について。

ゲストOS内の色々な proxy 設定をしてくれる。

## 公式資料

- [vagrant-proxyconf - Proxy Configuration Plugin for Vagrant](http://tmatilai.github.io/vagrant-proxyconf/)

## プラグインのインストール

``` bash
vagrant plugin install vagrant-proxyconf
```

## Vagrant ファイルへの記述

``` ruby
Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = ENV['http_proxy']
    config.proxy.https    = ENV['https_proxy']
    config.proxy.no_proxy = ENV['no_proxy']
  end
  # ... other stuff
end
```

上記の設定を使う場合、以下の環境変数設定をする。

``` bash
export http_proxy=http://<USER>:<PASS>@<PROXY_SERVER>
export https_proxy=$http_proxy
export no_proxy=<NO_PROXY>
```

`<...>` は適宜置き換え。

## 環境変数による Proxy 設定

以下の環境変数で Vagrantfile 内の設定をオーバーライドできる。

``` bash
export VAGRANT_HTTP_PROXY=
export VAGRANT_HTTPS_PROXY=
export VAGRANT_FTP_PROXY=
export VAGRANT_NO_PROXY=
```

## 参考資料

- [VagrantのかんたんProxy設定 - Qiita](http://qiita.com/shouta-dev/items/7b8a462504f7d6c91e99)

