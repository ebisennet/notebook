# Commands

vagrant のサブコマンドについて。

## box

## cap

## connect

## destroy

## docker-logs

## docker-run

## global-status

## halt

マシン停止。

## help

## init

Vagrant ファイルを生成する。

## list-commands

## login

## package

## plugin

Vagrant プラグインのインストール。

## port

## powershell

## provider

## provision

マシンの構成管理を再実行する。

構成管理として chef, ansible, シェルスクリプトなどが使える。 構成管理の設定は Vagrant ファイル内に記述する。

- rsync はしないので必要であれば事前に別途実行。

## push

## rdp

## reload

## resume

## rsync

## rsync-auto

## share

## snapshot

## ssh

## ssh-config

## status

## suspend

## up

- 初回起動時、タイムアウトで失敗する場合がある。

## version
