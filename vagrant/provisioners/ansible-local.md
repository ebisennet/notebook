# ansible-local

ゲストマシン上で ansible を実行するための provisioner.

## 公式資料

-   [Ansible Local - Provisioning - Vagrant by HashiCorp](https://www.vagrantup.com/docs/provisioning/ansible_local.html)

