# ansible

ゲストマシンを ansible でプロビジョニングする。

hosts ファイル、inventory ファイルの自動生成機能を持っている。

## 公式資料

-   [Ansible - Provisioning - Vagrant by HashiCorp](https://www.vagrantup.com/docs/provisioning/ansible.html)

## Memo

-   グループ名 vagrants

## 参考

-   [AnsibleとVagrantで開発環境を構築する - さくらのナレッジ](http://knowledge.sakura.ad.jp/tech/2882/)
-   [Ansible で Vagrant ホストを再起動する - Qiita](http://qiita.com/janus_wel/items/cf5e0cf41ad771f92038)
    -   ゲストOS内でrebootする方法だとだめ。

