# Versions

## Vagrant 1.8

- [仮想環境の構築ツール「Vagrant 1.8」リリース。リンククローン対応、スナップショット機能も搭載 － Publickey](http://www.publickey1.jp/blog/15/vagrant_18.html)
  - 差分のよる仮想マシンイメージ
  - vagrant snapshot のサポート

