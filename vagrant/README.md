# Vagrant

## 公式資料

- [Vagrant](https://www.vagrantup.com/)
- [Discover Vagrant Boxes | Atlas by HashiCorp](https://atlas.hashicorp.com/boxes/search)
    - Box が探せる

## Topics

- [インストール](installation.md)
- [Commands](commands/)
- [Proxy](proxy.md)
- [Providers](providers/)
- [Provisioners](provisioners/)
- [Versions](versions.md)

## Memo

### Box

|                     |
|---------------------|
| box-cutter/fedora21 |
| box-cutter/fedora22 |
| centos/7            |

### Box ファイルの場所

~/.vagrant.d/boxes/

## 複数台起動

### １つの Vagrantfile で複数台起動

- [Vagrantで複数台同時起動するマルチマシン設定 | WEB ARCH LABO](http://weblabo.oscasierra.net/vagrant-malti-machine/)
- [Vagrant で複数のVM を立ち上げて、お互いに通信できるようにするには - Qiita](http://qiita.com/sho7650/items/cf5a586713f0aec86dc0)
    - virtualbox の新しいバージョンでは設定しなくても通信可能。

### 複数の Vagrantfile で同時起動

- ssh 接続の port forwarding は自動的にコンフリクトが解消される
    - 古いバージョンでは指定が必要だった
        - [Vagrant２つのゲストOSを同時に立ち上げる（※別のVagrantfile） - Qiita](http://qiita.com/betahikaru/items/d77f5891f222eba0c4fa)
    - うまく動かない時は、VM を再起動するとうまくいく
        - vagrant halt && vagrant up
- ネットワークは別のものを割り当てる。
    - 例
        - VM-A: 192.168.33.0/24
        - VM-B: 192.168.34.0/24
    - VirtualBox の Host only network が使われた時に VM ごとにネットワークを割り当てないとうまく通信できない。
    - ゲストOS間の通信は可能。
        - NATアダプター側を通ってホストOS経由での通信。
            - ホストOSを中心としたスター型。
        - あるゲストOSから見たら、他のゲストOSはインターネットごしにあるサーバと同じ位置付け。

## ssh 接続エラー

- [Vagrant 1.7+でSSH接続エラーが出た場合の対処法 | mawatari.jp](http://mawatari.jp/archives/solution-of-vagrant-ssh-connection-error)

## 参考資料

- [MacでのVagrantの使い方とコマンドについて（VirtualBoxに仮想マシンを作成） - TASK NOTES](http://www.task-notes.com/entry/20150527/1432695600)
- [windowsでvagrant & dockerそしてproxy対応してCoreOsからCentOS7コンテナに接続 - Qiita](http://qiita.com/astrsk_hori/items/70a0e65e41ebc1a13908)
- [VagrantのかんたんProxy設定 - Qiita](http://qiita.com/shouta-dev/items/7b8a462504f7d6c91e99)
- [VagrantでCentOS7の仮想マシンを作成する。 - nyasu<sub>h</sub>](http://nyasu1111.hateblo.jp/entry/2015/02/11/034103)
