# presentation

``` example
pandoc -t dzslides -i --self-contained -s -o presentation.html sublime.md
```

|                  |                                   |
|------------------|-----------------------------------|
| -t FORMAT        | dzslides s5 slidy reveal slideous |
| -i               | 箇条書きなどを徐々に表示          |
| --self-contained | １ファイルにする                  |
| -s               | HTML として完成した形にする       |

## 参考資料

-   [Safx: PandocでMarkdownからHTMLプレゼンテーションを作成する](http://safx-dev.blogspot.jp/2012/09/pandocmarkdownhtml.html)

