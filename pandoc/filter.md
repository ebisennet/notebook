# filter

## 公式資料

- [pandocfilters/examples at master · jgm/pandocfilters · GitHub](https://github.com/jgm/pandocfilters/tree/master/examples)
  - サンプル
- [pandoc-types: Types for representing a structured document | Hackage](http://hackage.haskell.org/package/pandoc-types)
  - AST の情報

## Memo

- フィルターには 1st arg として出力フォーマットが渡るのみ

## 参考資料
- [PythonでPandocのフィルターを書く - Qiita](http://qiita.com/takada-at/items/187923e57071c0aa73a7)
- [pandocでMarkdownを拡張しコードをインポート出来るfilterを書く | Web Scratch](http://efcl.info/2014/0301/res3692/)
  - link to [azu/pandoc<sub>importcodefilter</sub> · GitHub](https://github.com/azu/pandoc_import_code_filter)

