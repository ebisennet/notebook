# インストール

## 公式資料

- [Pandoc - Installing](http://pandoc.org/installing.html)

## Mac

``` bash
brew install pandoc
```

PDF 出力時には latex 環境が必要になる。
