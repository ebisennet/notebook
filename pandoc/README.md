# pandoc

多数のフォーマットに対応したファイル変換ツール。

- Input
  - markdown, reStructuredText, textile, HTML, DocBook, LaTeX, MediaWiki markup, TWiki markup, OPML, Emacs Org-Mode, Txt2Tags, Microsoft Word docx, EPUB, or Haddock markup
- Output
  - HTML
  - HTML Slideshow: slidy, reveal.js, slideous, s5, dzslides
  - Word: docx, odt
  - EPUB
  - DocBook, Info, man page
  - OPML
  - LaTeX
  - PDF
  - markdown, reST, asciidoc, MediaWiki, DokuWiki, Org-mode, Textile

## 公式資料

- [Pandoc - About pandoc](http://pandoc.org/)
- [jgm/pandoc · GitHub](https://github.com/jgm/pandoc)
  - ソースコード

## Topics

- [Installing](installing.md)
- [Filter](filter.md)
- Supporting Image
- [Presentation](presentation.md)

## Memo

``` bash
pandoc input.md -o output.html
```

まとめて変換。

``` bash
for i in *.md; do pandoc "$i" -o "${i%.md}.html" ; done
```

## 参考資料

- [HTML - 多様なフォーマットに対応！ドキュメント変換ツールPandocを知ろう - Qiita](http://qiita.com/sky_y/items/80bcd0f353ef5b8980ee)
- [Pandoc ユーザーズガイド 日本語版 - Japanese Pandoc User's Association](http://sky-y.github.io/site-pandoc-jp/users-guide/)
- [ドキュメント変換ツールPandoc：ユーザーズガイドを熟読して分かったマニアックな使い方 - Qiita](http://qiita.com/sky_y/items/5fd5c9568ea550b1d7af)
  - スタイルファイルの設定など
- [実務に使うプレーンテキスト→Microsoft Word変換、あるいはPandocを使い始めた話 - 技術memo](http://nenono.hatenablog.com/entry/2015/02/10/173516)
  - doc 出力時のテンプレート指定について記載あり
- [ドキュメント変換ツールPandoc：ユーザーズガイドを熟読して分かったマニアックな使い方 - Qiita](http://qiita.com/sky_y/items/5fd5c9568ea550b1d7af)
  - フィルター、スタイル指定、テンプレート
- [Pandoc+beamer+GraphvizでAWSの図を描く - Qiita](http://qiita.com/ooharak/items/78f79fe8a57668550a82)
  - svg を使っている
  - PDF 内に埋め込むことでベクターをキープ
- [Pandoc ユーザーズガイド 日本語版 - Japanese Pandoc User's Association](http://sky-y.github.io/site-pandoc-jp/users-guide/)
