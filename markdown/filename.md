# ファイル名

## ベースネーム

本来、決まりはないが markdown を扱うツールによっては制限がある。

- GitLab の場合、半角英数、`-`、`_`、`.` のみ。

また、ディレクトリ内のデフォルトのファイルを示す名前として `README` が使われる。

## 拡張子

拡張子として `.md` または `.markdown` を用いる。

参考：[RFC 7763 - The text/markdown Media Type](https://tools.ietf.org/html/rfc7763)
