# リンク

```markdown
[title](url)
```

URL のみを書いても自動的にリンクになる。

```markdown
https://www.google.co.jp/
```

## 使用例

```markdown
[Google](https://www.google.co.jp/)
```

[Google](https://www.google.co.jp/)

