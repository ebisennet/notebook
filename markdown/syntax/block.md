# ブロック

ソースコードなどを記述する。

行の先頭に空白４文字で示す方法と、シングルクォート３つの行で囲む方法がある。

```markdown
    function main() {
      return 1;
    }
```

    function main() {
      return 1;
    }

シングルクォートでソースコードのブロックを指定する場合はプログラム言語の指定もできる。

```markdown
    ```ruby
    def main
      return 'Hello';
    end
    ```
```

```ruby
def main
  return 'Hello';
end
```
