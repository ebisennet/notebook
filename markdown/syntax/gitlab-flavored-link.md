# GitLab拡張リンク

GitLab によって拡張されているリンク記法は以下のとおり。

## ユーザー/グループへのリンク

`@` + ユーザー名、またはグループ名で、対象ユーザー、グループへのリンクとなる。

```markdown
@user_name
@group_name
```

## Issue リンク

`#` + Issue 番号で、Issue へのリンクとなる。

```markdown
#123
```

## Merge Request リンク

`!` + Merge Request 番号で、Merge Request へのリンクとなる。

```markdown
!123
```

## Milestone リンク

`%` + Milestone 番号で、Milestone へのリンクとなる。
Milestone の名前でも良い。

```markdown
%123
%release-v2
```

## コミットリンク

コミットのリビジョンを書くと、対象コミットへのリンクとなる。

```markdown
9ab12345
```

## 別プロジェクトリンク

以下の書式で別プロジェクトの Issue, Merge Request にリンクできる。

```markdown
グループ名/プロジェクト名#Issue番号
グループ名/プロジェクト名!MergeRequest番号
グループ名/プロジェクト名%MileStone番号
グループ名/プロジェクト名@リビジョン
```

グループ名だけでなくユーザー名でも可能。
