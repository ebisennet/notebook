# 改行

Markdown では通常、改行は１つの空白に変換される。

```markdown
line1
line2
```

line1
line2

GitLab 拡張記法として、行末に２文字以上の空白を入れると改行として扱われる。

```markdown
line1  
line2
```

line1  
line2


