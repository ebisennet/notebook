# markdown

オリジナルの markdown.

## 公式資料

- [Daring Fireball: Markdown](https://daringfireball.net/projects/markdown/)

## Homebrew によるインストール

```bash
brew install markdown
```
