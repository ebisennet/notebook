# markdown

HTMLに変換可能なテキスト記法である Markdown について。

## 公式資料

- [Daring Fireball: Markdown](http://daringfireball.net/projects/markdown/)
    - オリジナルの Markdown
- RFC: [RFC 7763 - The text/markdown Media Type](https://tools.ietf.org/html/rfc7763)
    - Markdown の Media Type を定めた RFC.
- IANA: [https://www.iana.org/assignments/media-types/text/markdown](https://www.iana.org/assignments/media-types/text/markdown)

## Topics

- [ファイル名](filename.md)
- [文法](syntax/)
- [Tools](tools/)
- スライド生成
    - [Markdownでプレゼン用スライドをつくる方法いろいろ \- Qiita](http://qiita.com/shu223/items/7193ca05bb8a75fe4854)
    - [Markdown Slideshow \- Qiita](http://qiita.com/ogomr/items/0a92b8a517acb668de14)
    - [Markdownからスライド資料を作る](https://rcmdnk.com/blog/2014/02/19/computer-markdown/)

## 応用事例

- [Excelなテスト仕様書をMarkdown/GitHub/CircleCIに移行した話 - トレタ開発者ブログ](http://tech.toreta.in/entry/excel-to-markdown-github-circleci)
