# 内部動作

## 参考資料

- [Gitのコミットハッシュ値は何を元にどうやって生成されているのか - Mercari Engineering Blog](http://tech.mercari.com/entry/2016/02/08/173000)
    - [Initial revision of "git", the information manager from hell · git/git@e83c516 · GitHub](https://github.com/git/git/commit/e83c5163316f89bfbde7d9ab23ca2e25604af290#diff-f0bb13c0dd1f6ca9159716c0624f0679R37)
- [Git の仕組み \(1\) \- こせきの技術日記](http://koseki.hatenablog.com/entry/2014/04/22/inside-git-1)
    - [Git の仕組み \(2\) \- コミット・ブランチ・タグ \- こせきの技術日記](http://koseki.hatenablog.com/entry/2014/06/11/inside-git-2)
- [こわくない Git](http://www.slideshare.net/kotas/git-15276118)
    - 内部の仕組み解説。
