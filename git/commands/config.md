# config

git の設定を行う。

## 初期設定

```bash
git config --global user.name "User Name"
git config --global user.email example@example.com
```

git を使用するユーザーの情報を設定する。

ユーザー名とメールアドレスの情報はコミットに記録される。

## エイリアス

引数を取る場合、関数定義してその関数を実行するように設定する。

```
git config --global alias.test '!f() { echo "### $1" && cd "$1" && git pull; }; f'
```

## 参考資料

- [Git config | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!config)
