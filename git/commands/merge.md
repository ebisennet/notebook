# merge

他のブランチの内容を現在のブランチにマージする。

## 指定ブランチをマージ

```
git merge branch1
```

以下のように複数指定可能。

```
git merge branch1 branch2
```

## マージ作業の中断

```
git merge --abort
```

コンフリクト発生後、手作業によるマージを中断する。

マージの作業途中の内容は捨てることになる。

## 公式資料

- [Git \- 高度なマージ手法](https://git-scm.com/book/ja/v2/Git-%E3%81%AE%E3%81%95%E3%81%BE%E3%81%96%E3%81%BE%E3%81%AA%E3%83%84%E3%83%BC%E3%83%AB-%E9%AB%98%E5%BA%A6%E3%81%AA%E3%83%9E%E3%83%BC%E3%82%B8%E6%89%8B%E6%B3%95)

## 参考資料

- [Git merge | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-branches#!merge)
