# pull

リモートリポジトリからコミットを取得してマージする。

他の開発者によって修正された内容を取り込む。

内部的には [fetch](fetch.md) コマンドと [merge](merge.md) コマンドの組み合わせとなっている。

## 使用例

```bash
git pull
```

## 参考資料

- [Git pull | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/remote-repositories#!pull)
