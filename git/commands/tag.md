# tag

タグの作成や削除を行う。

## タグの作成

```bash
git tag TAGNAME
```

現在のコミット位置にタグ `TAGNAME` を作成する。

## タグの削除

```bash
git tag -k TAGNAME
```

タグ `TAGNAME` を削除する。
