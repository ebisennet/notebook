# add

ファイルの修正内容をインデックスに追加する。

インデックスに追加することで [commit](commit.md) コマンドで作成するコミットの対象となる。

## 使用例

```bash
git add test.txt
```

## 参考資料

- [Git add | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!add)
