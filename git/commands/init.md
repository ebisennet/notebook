# init

空の git リポジトリを作成する。

## 使用例

```bash
git init .
```

カレントディレクトリに新規の git リポジトリを作成する。

カレントディレクトリ内に `.git` ディレクトリが作成される。

## 参考資料

- [Git init | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!init)
