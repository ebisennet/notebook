# submodule

サブモジュールの管理を行う。

## add

サブモジュールの追加を行う。

```bash
git submodule add ssh://git@git-server/test.git test
```

URL で示したリモートリポジトリをサブディレクトリ `test` の位置に
サブモジュールとして追加する。

## init

サブモジュールの初期化を行う。

```bash
git submodule init
```

サブモジュールを持つ Git リポジトリをクローンした直後はサブモジュールが取得されていないため
最初に `git submodule init` で初期化を行う必要がある。

内部動作としては `.gitmodules` ファイルの記述から `git config` の設定を行う。
具体的には config 名 `submodule.$name.url` に URL が設定される。

`git config --get-regexp submodule` で設定内容を確認できる。

`git config` で値をセットすれば、 `.gitmodules` の記述と異なる URL を指定できる。

## update

サブモジュールの設定内容から実際にサブモジュールを取得する。

```bash
git submodule update
```

`git submodule init` で初期化された設定を元に、実際にサブモジュールのディレクトリ内で
リポジトリの内容を取得する。

update 前に init も同時に行うには `--init` オプションを指定する。

```bash
git submodule update --init
```

init 後にリモート URL 切り替えを行いたい場合は `--init` は指定してはいけない。

## status

サブモジュールの状態を表示する。

表示される状態は、サブモジュールに変更が入っているか、サブモジュールのRevision、パス、タグやブランチ。

`--cached` オプションをつけることでRevisionなどは、親リポジトリが参照しているRevisionの表示となる。
