# rev-list

コミットから辿れるコミットの一覧を表示する。

## 公式資料

- [Git - git-rev-list Documentation](https://git-scm.com/docs/git-rev-list)

## 参考資料

- [gitで削除したファイルを復活させる - Qiita](http://qiita.com/minamijoyo/items/0f1baa40376de26fec81)
