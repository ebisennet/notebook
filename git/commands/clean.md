# clean

作業ディレクトリから追跡対象外のファイルを削除する。

## Untracked files の削除

```
git clean -f
```

## Ignored files も一緒に削除

```
git clean -fx
```

## Dry Run

```
git clean -n
```

## 参考資料

- [Git clean | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/undoing-changes#!clean)
