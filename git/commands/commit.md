# commit

インデックスに登録した内容でコミットを作成する。

## 使用例

```bash
git commit
```

コミットメッセージを入力するためにエディタが起動する。

エディタでメッセージ入力を終えるとコミットが作成される。

## 参考資料

- [Git commit | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!commit)
- [Git commit --amend | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/rewriting-git-history#!commit-amend)
