# fetch

リモートリポジトリのコミットをダウンロードする。

## リモートから指定ブランチのダウンロード

```bash
git fetch origin master
```

## リモートから全てブランチのダウンロード

```bash
git fetch -a
```

## 参考

- [Git fetch | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/remote-repositories#!fetch)
- [Git - remote branch invisible to local repo, fetch isn't helping - Stack Overflow](http://stackoverflow.com/questions/31273639/git-remote-branch-invisible-to-local-repo-fetch-isnt-helping)
