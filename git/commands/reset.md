# reset

ブランチの位置を指定したコミットに再セットする。

## カレントブランチを１つ前のコミットにリセット

```bash
git reset HEAD^
```

## カレントブランチを指定ブランチの位置にハードリセット

```bash
git reset --hard origin/develop
```

## 参考資料

- [Git reset | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/undoing-changes#!reset)
