# remote

リモートリポジトリの設定を行う。

## リモートリポジトリの追加

```bash
git remote add test ssh://git@git-server/test.git
```

リモート名 `test` でリモートリポジトリ `ssh://git@git-server/test.git` を参照する。

リモートリポジトリが１つしかない場合、リモート名は通常 `origin` を使う。

ローカルのリポジトリから複数のリモートリポジトリの参照する時は上記のように `origin` 以外の名前をつけて区別する。

## リモートリポジトリのURL変更

```bash
git remote set-url origin ssh://git@git-server/test.git
```

リモート名 `origin` のリモートリポジトリを `ssh://git@git-server/test.git` に変更する。

サーバーのドメイン名が変わった時や Git リポジトリへのパスが変更された時に使用する。

## 参考資料

- [Git remote | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/remote-repositories#!remote)
