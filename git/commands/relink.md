# relink (retired)

ローカルの複数リポジトリ内の同じオブジェクトをハードリンクする。

v2.12.0 で削除された。

- [relink: retire the command · git/git@ed21e30 · GitHub](https://github.com/git/git/commit/ed21e30fef3609602481ba3630ccec636d459a21)
