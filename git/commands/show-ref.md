# show-ref

## リファレンス一覧表示

```
git show-ref
```

以下のように Commit ID とリファレンス名の一覧が表示される。

```
1da701dcb65aa2a5bdba0bcfcf5d028c9bfc22fc refs/heads/master
455d2c56b161dece445c83d54a335112aaaa49fa refs/remotes/origin/HEAD
...
```

## 現在のコミットからリファレンス一覧表示

```
git show-ref | grep "$(git rev-parse HEAD)" | cut -d\  -f2
```

リファレンスについてタグのみに絞るのであれば

```
git show-ref | grep "$(git rev-parse HEAD) refs/tags" | cut -d\  -f2
```

## 指定した名前のリファレンスを表示

```
git show-ref -- master
```

`--` の後に複数指定可能。

`master` であれば以下の２つがマッチする。

- `refs/heads/master`
- `refs/remotes/origin/master`

リモートブランチを除外したければ `--heads` オプションと組み合わせる。

マッチしたリファレンスが存在する場合、終了コードは 0 となる。  
存在しない場合、終了コードは 1 となる。
