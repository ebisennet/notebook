# log

コミットの履歴を表示する。

## 使用例

```bash
git log
```

## マージコミットのみ表示

```
git log --merges
```

作業ログを残す際などは以下のように `--no-pager` を使ってページャーなしで実行する。  
ログの量が多くなるのでレンジ指定する。

```
git --no-pager log --merges FROM..TO
```

## ログのグラフ表示

```
git config --global --add alias.tree 'log --graph --pretty='format:%C(yellow)%h%Creset %s %Cgreen(%an)%Creset %Cred%d%Creset''
```

`git tree` コマンドが使えるようになる。

## ログの markdown 形式出力

```
git config --global --add alias.markdown-log 'log --format="* %h %s"'
```

実行は `git markdown-log FROM..TO` と、出力範囲をレンジ指定する。

## 参考資料

- [Git log | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!log)
- [美しき git log \-\-graph のエイリアス \- Hack Your Design\!](http://blog.toshimaru.net/git-log-graph/)
- [一日のGitコミットをMarkdownに変換するワンライナー \- Qiita](http://qiita.com/yuku_t/items/7c2077e31b2480a2689e)
- [git log よく使うオプションまとめ \- Qiita](http://qiita.com/take4s5i/items/15d8648405f4e7ea3039)
