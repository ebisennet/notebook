# cat-file

git リポジトリ内の object の情報や中身を表示する。

object として commit, tree, blob がある。

## 任意の object を表示

-p で object のコンテンツをそのタイプに合わせた方法で表示する。

```sh
git cat-file-p <object-id>
```

### tree の出力例

```
$ git cat-file -p master^{tree}
...snip...
040000 tree 780b5e49399d2f72eddb56b055daacc119857bc2	files
040000 tree d5980aa9de0fe9de52cf5ead06f46b86e17b10c9	group_vars
040000 tree 802be9d2da846d5f1703c11eca343495ee0c546a	inventory
120000 blob e733c8af88d38bfbcb84da08460191ca75784f76	post_site.yml
040000 tree d564d0bc3dd917926892c55e3706cc116d5b165e	reports
160000 commit 5fa1c28a1bbd0cc4025887df43e5a1efa73ed2ca	shared_usage
100644 blob e6462507e0a4404f53e60bf2089aebc25fd2e8b8	site.yml
120000 blob 1515d3e6ddee36ec06db1a98d4cd7ee90ae03470	spec_filecheck_site.yml
040000 tree d054f9f5242c285e6d07b173ecec964c33de9273	spec_group_vars
```

属性、タイプ、object id、エントリー名。

commit はサブモジュール側の comit id が記録されている。

### commit の出力例

commit 情報が出力される。

```
 $ git cat-file -p master^{commit}
tree 5b9362646e03e089036ff8ca04d2281f15190bcf
parent f233ee26e89f0ca67b9e4365ee78124ab1cc5c85
parent 54e97bd4a7dd79dad386e3281c872fc92e5153eb
author kenichiro.kurosawa <kenichiro.kurosawa@ctc-g.co.jp> 1499135854 +0900
committer kenichiro.kurosawa <kenichiro.kurosawa@ctc-g.co.jp> 1499135854 +0900

Merge branch 'spec_fix_1682' into 'master'

[Serverspec] Weblogicパッチ(PSU p25388843)適用

See merge request !322
```

### blob の出力

対象のファイル内容がそのまま出力される。

## 任意のコミットの任意のファイル閲覧

```sh
git cat-file -p REV:PATH
```

- [git cat\-fileで任意のブランチの任意のファイルを閲覧できるので便利 · DQNEO起業日記](http://dqn.sakusakutto.jp/2013/06/git_cat-file.html)

## 指定コミットのサブモジュールのリビジョン表示

```sh
COMMIT=dev
git cat-file -p $(git cat-file -p "${COMMIT}" | head -1 | awk '{print $2}') | awk '$1 == "160000" {print $3}'
```
