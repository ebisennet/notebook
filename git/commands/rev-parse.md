# rev-parse

## Commit ID 表示

```sh
git rev-parse HEAD
```

## 参考資料

- [git rev\-parse でできること \- Qiita](http://qiita.com/Alice1017/items/ad061f33c8d42d52e079)
- [git rev\-parseを使いこなす \- Qiita](http://qiita.com/karupanerura/items/721962bb7da3e34187e1)
