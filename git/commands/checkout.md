# checkout

ブランチの切り替えを行う。

## 強制切り替え

```
git checkout -f REVISION
```

修正中のファイルなどは消える。

Untracked files が残るので必要なら git clean も実行する。

## 参考資料

- [Git checkout | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/undoing-changes#!checkout)
- [Git checkout | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-branches#!checkout)
