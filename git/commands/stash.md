# stash

修正途中の内容の一時退避操作。

## 一時退避

```bash
git stash
```

## 退避していた修正を復元

```bash
git stash pop
```
