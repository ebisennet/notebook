# clone

リポジトリのクローンを行う。

## 使用例

```bash
git clone ssh://git@git-server/test.git
```

カレントディレクトリに test ディレクトリが作成されて、その中にリポジトリの内容がクローンされる。

## 参考資料

- [Git clone | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!clone)
