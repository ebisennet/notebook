# merge-base

## 祖先チェック

```sh
git merge-base --is-ancestor <commit1> <commit2>
```

commit1 が commit2 の祖先かどうか。

exit code で結果を返す。

## 参考資料

- [git \- How can I tell if one commit is an ancestor of another commit \(or vice\-versa\)? \- Stack Overflow](http://stackoverflow.com/questions/18345157/how-can-i-tell-if-one-commit-is-an-ancestor-of-another-commit-or-vice-versa)
