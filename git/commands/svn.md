# svn

Subversion との連携を行う。

## 参考資料

- [git-svnでSVN→Gitへの移行をやってみたログ - Qiita](http://qiita.com/hidekuro/items/4727715fbda8f10b6b11)
- [SubversionからGitへの移行でチーム影響を最小限にする - Qiita](http://qiita.com/mookjp/items/7d6ac31521a671f8b806)
