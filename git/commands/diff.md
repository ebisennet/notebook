# diff

指定したコミット間で差分を表示する。

## ブランチ間での差分表示

```bash
git diff stg prd
```

ブランチ `stg` からブランチ `prd` への差分を表示する。

## 参考資料

- [忘れやすい人のための git diff チートシート - Qiita](忘れやすい人のための git diff チートシート - Qiita)
