# reflog

リファレンスログを管理する。

間違ってマージしていないブランチを削除した時や [reset](reset.md) でブランチを戻し過ぎた時など
ブランチ操作のミスを回復したい時に使う。

リファレンスログには、ブランチが以前指し示していたコミットが記録されているので
リファレンスログを表示して過去のコミット位置にブランチを移動、または作成することで
ブランチ操作のミスを回復できる。

## リファレンスログの表示

```bash
git reflog
```

## 参考資料

- [Git reflog | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/rewriting-git-history#!reflog)
- [いざという時のためのgit reflog - Qiita](http://qiita.com/yaotti/items/e37c707938847aee671b)
