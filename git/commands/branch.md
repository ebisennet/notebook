# branch

ブランチの操作を行う。

## ブランチを指定したコミットに移動する

```
git branch -f branch-A commit-1
```

カレントブランチの位置は移動できない。

- [git branchメモ \- 名前の変更、上書き、強制上書き、削除\(オプション\-m、\-f、\-dとか\) \- tweeeetyのぶろぐ的めも](http://tweeeety.hateblo.jp/entry/2014/10/23/222909)

## 参考資料

- [Git branch | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-branches#!branch)
