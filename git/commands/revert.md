# revert

コミットを打ち消すコミットを作成する。

## 使用例

```bash
git revert a1d89d4
```

コミット `a1d89d4` の差分を打ち消すコミットを作成する。

例えばコミット `a1d89d4` が「README.md の末尾に `Test` という行を追加する」内容であれば
「README.md の末尾の行を削除する」コミットが新規に作成される。

## 参考資料

- [Git revert | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/undoing-changes#!revert)
