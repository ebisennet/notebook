# ls-remote

リモートの reference 一覧を出力する。

リモートリポジトリを明示的に指定することもできる。
省略した場合はカレントディレクトリのリポジトリからリモートを決める。

## ブランチのみ表示

```
git ls-remote --heads git@example.com:example.git
```

出力例。

```
9ad97b3354848f9e4922832d0c4751c0da8c409e	refs/heads/master
785d1039b688223565ac31a5ccf72be17e2349f7	refs/heads/spec_apache_contents_fix
e9ee6affd8e9c36b4e7dd8a65d9079832d4ae47d	refs/heads/spec_oracle_client_32_fix
511d2251a790f12794c9dc57bca1b8647c610902	refs/heads/spec_rps_fix
48df591dc58dd52a99e1ca0c06a773cb350a2146	refs/heads/wls_shared_20170327_00
```

## タグのみ表示

```
git ls-remote --tags git@example.com:example.git
```

## 指定ブランチの有無を確認

```
git ls-remote --tags git@example.com:example.git BRANCH
```

指定したブランチがリモートになくても終了コードは 0 になるので注意。

出力の行数を見る必要がある。

## 参考資料

- [git - When cloning a branch : Remote branch not found in upstream origin - Stack Overflow](http://stackoverflow.com/questions/32578224/when-cloning-a-branch-remote-branch-not-found-in-upstream-origin)
