# status

ローカルのワークツリー、インデックスの状態を表示。

## ローカルの状態を表示する

```bash
git status
```

## 参考資料

- [Git status | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/git-basics#!status)
