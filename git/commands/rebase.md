# rebase

指定したコミットから指定したコミットまでの修正履歴を、別のコミットから伸ばしたことにする。

以下のようなケースで使用できる。

- master ブランチから feature ブランチ A を作って開発を進めていたが、master ブランチで他の人が push した内容を pull したので、ブランチ A を pull した後の master ブランチから作ったことにしたい。
- feature ブランチ A から別の feature ブランチ B を作って開発を進めたが、develop ブランチからブランチを作ったことにしたい。

内部的には reset と cherry-pick の組み合わせとして実装されている。

## カレントブランチの rebase

```bash
git rebase master
```

カレントブランチの一連のコミットを現在の `master` ブランチから作成したことにする。

`master` から派生したブランチで作業中に、別途、`master` ブランチで [pull](pull.md) や [merge](merge.md) を実行して
ブランチを派生させた時と `master` の位置が変わった時に、改めて現在の `master` 位置から派生したことにする。

## 指定ブランチの rebase

```bash
git rebase --onto develop feature-A feature-B
```

`feature-A` ブランチから派生した `feature-B` ブランチを `develop` ブランチから派生したことにする。

`feature-B` ブランチ作成時に `feature-A` ブランチから派生してブランチ作成したものの
`feature-B` が `feature-A` に依存しておらず、先に `feature-B` から `develop` に
マージしたくなった時などに使う。

## 参考資料

- [Git rebase | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/rewriting-git-history#!rebase)
- [Git rebase -i | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/rewriting-git-history#!rebase-i)
- [git rebase --onto を使って、まずは自分だけ幸せになる - Qiita](http://qiita.com/kyanro@github/items/d0111b07022494457b75)
- [git rebase --onto どこへ どこから どのブランチを - Qiita](http://qiita.com/sotarok/items/07c6b2cca5ed2f9a53a6)
- [初心者でもわかる！リベースの使い方を解説します | Git編：一歩踏み出すフロントエンド入門](http://liginc.co.jp/web/tool/79390)
- [git rebase -i はやっぱりイケてる件【git】【rebase 】【iオプション】 - DRYな備忘録](http://otiai10.hatenablog.com/entry/2012/11/10/013925)
- [gitのコミットの歴史を改変する\(git rebase\) 1 / 2 · けんごのお屋敷](http://tkengo.github.io/blog/2013/05/16/git-rebase-reference/)
- [gitのコミットの歴史を改変する\(git rebase\) 2 / 2 · けんごのお屋敷](http://tkengo.github.io/blog/2013/06/08/git-rebase-reference2/)
