# push

コミットをリモートリポジトリにアップロードする。

## ブランチのPUSH

```bash
git push origin branch1:branch2
```

ローカルの `branch1` ブランチを、リモート `origin` のリポジトリ上にある `branch2` ブランチにアップロードする。

```bash
git push origin master
```

ローカルの `master` ブランチを、リモート `origin` のリポジトリ上にある `master` ブランチにアップロードする。

ローカルとリモートで同名のブランチの場合、上記のように短縮記法が使える。

## タグのPUSH

```bash
git push origin tagname1:tagname2
```

ローカルの `tagname1` タグと同じコミットを指し示すタグをリモート `origin` のリポジトリ上に `tagname2` タグとして作成する。

ブランチの場合と同様に同名の場合、短縮記法が使える。

## リモートのブランチ/タグの削除

```bash
git push origin :feature
```

リモート `origin` のリポジトリ上にある `feature` ブランチを削除する。

## 参考資料

- [Git push | アトラシアン Git チュートリアル](https://www.atlassian.com/ja/git/tutorial/remote-repositories#!push)
