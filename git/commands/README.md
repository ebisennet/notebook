# Commands

git で使える各コマンドについて。

| コマンド                      | 説明                                                           |
|-------------------------------+----------------------------------------------------------------|
| [add](add.md)                 | ファイルの修正内容をインデックスに追加する                     |
| [blame](blame.md)             | ファイルの修正がどのコミットで行われたか確認する               |
| [branch](branch.md)           | ブランチの操作を行う                                           |
| [checkout](checkout.md)       | ブランチの切り替えを行う                                       |
| [cherry-pick](cherry-pick.md) | 指定したコミットを取り込む                                     |
| [clean](clean.md)             | 作業ディレクトリから追跡対象外のファイルを削除する             |
| [clone](clone.md)             | リポジトリのクローンを行う                                     |
| [commit](commit.md)           | インデックスに登録した内容でコミットを作成する                 |
| [config](config.md)           | git の設定を行う                                               |
| [diff](diff.md)               | 指定したコミット間で差分を表示する                             |
| [fetch](fetch.md)             | リモートリポジトリのコミットをダウンロードする                 |
| [init](init.md)               | 空の git リポジトリを作成する                                  |
| [log](log.md)                 | コミットの履歴を表示する                                       |
| [ls-remote](ls-remote.md)     |                                                                |
| [merge](merge.md)             | 他のブランチの内容を現在のブランチにマージする                 |
| [mv](mv.md)                   | ファイルの移動、名前変更を行う                                 |
| [pull](pull.md)               | リモートリポジトリからコミットを取得してマージする             |
| [push](push.md)               | コミットをリモートリポジトリにアップロードする                 |
| [rebase](rebase.md)           | 指定した一連のコミットを別の位置に追加する                     |
| [reflog](reflog.md)           | リファレンスログを管理する                                     |
| [remote](remote.md)           | リモートリポジトリの設定を行う                                 |
| [reset](reset.md)             | ブランチの位置を指定したコミットに再セットする                 |
| [rev-list](rev-list.md)       | コミットから辿れるコミットの一覧を表示する                     |
| [rev-parse](rev-parse.md)     |                                                                |
| [revert](revert.md)           | コミットを打ち消すコミットを作成する                           |
| [rm](rm.md)                   | ファイルの削除を行う                                           |
| [show-ref](show-ref.md)       |                                                                |
| [stash](stash.md)             | 修正途中の内容の一時退避操作                                   |
| [status](status.md)           | ローカルのワークツリー、インデックスの状態を表示               |
| [submodule](submodule.md)     | サブモジュールの管理を行う                                     |
| [subtree](subtree.md)         | サブツリーに関する操作を行う                                   |
| [svn](svn.md)                 | Subversion との連携を行う                                      |
| [tag](tag.md)                 | タグの作成や削除                                               |
| [worktree](worktree.md)       | 複数のワーキングツリーを管理する                               |
| for-each-ref                  |                                                                |

## Deprecated

| コマンド                      | 説明                                                           |
|-------------------------------+----------------------------------------------------------------|
| [relink](relink.md)           | ローカルの複数リポジトリ内の同じオブジェクトをハードリンクする |
