# git

## 公式資料

- [Git](https://git-scm.com/)
    - 公式サイト
- [Git \- Book](https://git-scm.com/book/ja/v2)
    - 書籍「Pro Git」日本語版
- [GitHub \- git/git: Git Source Code Mirror \- This is a publish\-only repository and all pull requests are ignored\. Please follow Documentation/SubmittingPatches procedure for any of your improvements\.](https://github.com/git/git)

## Topics

- [Commands](commands/)
- [Tools](tools/)
- Proxy
    - [Gitでプロキシのバイパスを設定する - なにか作る](http://create-something.hatenadiary.jp/entry/2014/07/30/081236)
        - `http_proxy` や `no_proxy` 環境変数を参照する。
- [https 接続](https-connections.md)
- Revision
    - [Git - revisions Documentation](https://git-scm.com/docs/revisions)
        - SHA1 ハッシュ
        - Git Commit とは限らない。
        - 名前によるエイリアスは refname を参照。
- [チュートリアル](tutorial.md)
- [移行](migration.md)
- [内部動作](internal.md)
- ワークフロー
    - [Git ワークフローとそのチュートリアル | アトラシアン](https://www.atlassian.com/ja/git/workflows#!workflow-overview)
- Practices
    - Monorepos
        - [Git で monorepo を扱う際の課題とヒント - Atlassian Japan Blog](http://japan.blogs.atlassian.com/2015/10/monorepos-in-git/)

## Memo

- Debug
    - `GIT_TRACE=1`
- http.ssl*
  - [.gitconfigのhttp設定のsslCAInfoはちゃんとサイト指定する - Qiita](http://qiita.com/eiryu/items/6bf8a8355a3194f2c8e3)
- sock
  - [Using a socks proxy with git for the http transport - Stack Overflow](http://stackoverflow.com/questions/15227130/using-a-socks-proxy-with-git-for-the-http-transport)
- コミットグラフ可視化
    - [Gitのコミットグラフを可視化できるGitGraph\.jsがおもしろい \- Qiita](http://qiita.com/oohira/items/827433d4ebcb69b45b7f)
    - [Gitgraph\.js をラップしてコマンドっぽく使えるようにした \- Qiita](http://qiita.com/opengl-8080/items/cdde8ecf8a0b1922ce54)

## 参考資料

- [How to make git push work with nginx (basic auth) behind nginx reverse proxy (HTTPS)? - Server Fault](http://serverfault.com/questions/586101/how-to-make-git-push-work-with-nginx-basic-auth-behind-nginx-reverse-proxy-ht)
- [Explain Git with D3](https://onlywei.github.io/explain-git-with-d3/)
- [誰得UNIX: ステージを理解して git をもっと便利に使う](http://daretoku-unix.blogspot.jp/2009/08/git.html)
- [GitLab flowから学ぶワークフローの実践 | 開発手法・プロジェクト管理 | POSTD](http://postd.cc/gitlab-flow/)
- [Git - Tutorial](http://www.vogella.com/tutorials/Git/article.html)
    - 英語。
    - 内部も含めて丁寧な解説。
- [Pretty git branch graphs - Stack Overflow](http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs)
    - git history のグラフ描画に使えるツールなど。
    - git なくても書けるツールもある。
- [Visualizing branch topology in git - Stack Overflow](http://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git)
    - git リポジトリ内のグラフを可視化するツールについて。
