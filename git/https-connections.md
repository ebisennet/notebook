# https 接続

https 接続時には自己署名証明書を使っている場合、設定が必要になる。

## 証明書の検証を無効化する方法

```bash
git config --global http.sslVerify false
```

OS 全体に適用する場合は以下の通り。

```bash
git config --system http.sslVerify false
```

## 信頼する証明書を設定する方法

## 参考資料

- [Git – Cloning and pushing via https (Linux and Windows) | vogella blog](http://blog.vogella.com/2010/08/09/git-https/)
    - http.sslVerify を設定する。
    - http.sslCAInfo を設定する。
- [SSLクライアント認証が必要なGitリポジトリをクローンする - 死ぬまでの暇潰し](http://aki2o.hatenablog.jp/entry/2015/01/15/053351)
