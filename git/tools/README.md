# Tools

- [Gitkraken](gitkraken.md)
- [SourceTree](sourcetree.md)
- [git-credential-osxkeychain](git-credential-osxkeychain.md)

## WinMerge

- [GitからWinMergeを起動してソースを比較し編集したい - For myself tomorrow](http://tigawa.github.io/blog/2014/02/12/difftool/)

## Git MultiSite

- [Git MultiSite | WANdisco](http://www.wandisco.com/git/multisite)
    - HA, DR 用途に使える。
    - プロプライエタリ

## その他

- Mac
    - [Mac で使える git mergetool をいろいろ試してみる - OpenDiff | そんなこと覚えてない](http://blog.eiel.info/blog/2013/06/26/git-mergetool-opendiff/)
    - [software recommendation - What's a good Mac equivalent of WinMerge? - Ask Different](http://apple.stackexchange.com/questions/3653/whats-a-good-mac-equivalent-of-winmerge)
- [Excel - OfficeドキュメントをGitで管理する下準備(テキストコンバータの比較) - Qiita](http://qiita.com/BeatsMe1978/items/4b954eb75d9af947cb25)
- git-ql
    - [GitHub \- cloudson/gitql: A git query language](https://github.com/cloudson/gitql)
