# git-credential-osxkeychain

Mac で Homebrew から git インストールすると付属している。

以下で有効化できる。

```
git config --global credential.helper osxkeychain
```

- [git を https 経由で使うときのパスワードを保存する - Qiita](http://qiita.com/usamik26/items/c655abcaeee02ea59695)

