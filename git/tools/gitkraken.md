# Gitkraken

## 公式資料

- [Git GUI Client for Windows Mac and Linux | Axosoft GitKraken](https://www.gitkraken.com/)

## インストール

### Mac

```bash
brew cask install gitkraken
```

