# SourceTree

## 公式資料

- [Free Mercurial and Git Client for Windows and Mac | Atlassian SourceTree](https://www.sourcetreeapp.com/)

## Proxy 設定

- [認証付きProxy環境下でもSourceTreeでPuTTYなSSH接続を - ペペロン頭脳](http://peperon-brain.hatenablog.com/entry/2015/02/05/031441)
- [GitをProxy環境下で使うときにハマった件 - ごずろぐ](http://gozuk16.hatenablog.com/entry/2014/06/06/200602)
  - `GIT_CURL_VERBOSE` によるデバッグ
- [いつの間にかSourceTreeが環境変数HTTP_PROXYを見なくなった？ - ごずろぐ](http://gozuk16.hatenablog.com/entry/2015/04/17/114552)

