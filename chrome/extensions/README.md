# Extensions

| Name                                        | Remarks                                                                 |
|---------------------------------------------+-------------------------------------------------------------------------|
| [Awesome Screenshot](awesome-screenshot.md) | スクリーンショット                                                      |
| [Create Link](create-link.md)               | 閲覧中のページのURLやタイトルを指定の書式でクリップボードにコピーする。 |
| [Postman](postman.md)                       | Web API テスト                                                          |
| [SwitchyOmega](switchyomega.md)             | Proxy の切り替え                                                        |
