# Create Link

閲覧中のページのURLやタイトルを指定の書式でクリップボードにコピーする。

## 公式資料

- [Create Link \- Chrome ウェブストア](https://chrome.google.com/webstore/detail/create-link/gcmghdmnkfdbncmnmlkkglmnnhagajbm)

## Markdown 用設定

- Name
    - Markdown
- Format
    - `[%text_md%](%url%)`

## Mediawiki 用設定

- Name
    - Mediawiki
- Format
    - `[%url%|%text%]`

## JIRA Wiki 用設定

- Name
    - JIRA Wiki
- Format
    - `[%text%|%url%]`
- Filter
    - `s/\[(ASW[^\]]+)\]/$1/`

## Redmine 用設定

- Name
    - Redmine
- Format
    - `"%text%":%url%`
- Filter
    - `s/ - "ASY JIRA"[^"]+//`

## Box 用設定

- Name
    - Box
- Format
    - `%url%`
- Filter
    - `s/ctcbox\.ent/app/`
