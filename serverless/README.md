# Serverless

## 参考資料

- [サーバレスアーキテクチャとは？ - プログラマになりたい](http://blog.takuros.net/entry/2015/10/19/081349)
- [サーバレスアーキテクチャ（ServerLess Architecture）とBaaSの違い - プログラマになりたい](http://blog.takuros.net/entry/2015/10/20/082757)
    - サーバ（メンテナンス）レス
- [Serverless Service Discovery – Part 1: Get Started | AWS Developer Blog](https://aws.amazon.com/jp/blogs/developer/serverless-service-discovery-part-1-get-started/)
- [Surviving the Zombie Apocalypse with Serverless Microservices | AWS Compute Blog](https://aws.amazon.com/jp/blogs/compute/surviving-the-zombie-apocalypse-with-serverless-microservices/)
- [「サーバレスとは何か」そして「サーバレスとPaaSの違い」とは。マーチン・ファウラー氏のブログに投稿された解説 － Publickey](http://www.publickey1.jp/blog/16/serverless_paas_2.html)
    - link to [Serverless Architectures](http://martinfowler.com/articles/serverless.html)
- [伊藤直也氏が語る、サーバーレスアーキテクチャの性質を解剖する（前編）。QCon Tokyo 2016 － Publickey](http://www.publickey1.jp/blog/16/qcon_tokyo_2016.html)
    - [伊藤直也氏が語る、サーバーレスアーキテクチャの性質を解剖する（中編）。QCon Tokyo 2016 － Publickey](http://www.publickey1.jp/blog/16/qcon_tokyo_2016_1.html)
    - [伊藤直也氏が語る、サーバーレスアーキテクチャの性質を解剖する（後編）。QCon Tokyo 2016 － Publickey](http://www.publickey1.jp/blog/16/qcon_tokyo_2016_2.html)
    - [Serverless Architecture // Speaker Deck](https://speakerdeck.com/naoya/serverless-architecture)
- [Serverless AWS構成でセキュアなSPAを目指す](https://www.slideshare.net/masayuki-kato/serverless-awsspa)
- Twilio Functions
    - [Twilioがサーバレスコンピューティング「Twilio Functions」公開。APIだけでなくアプリケーション実行環境も提供へ － Publickey](http://www.publickey1.jp/blog/17/twiliotwilio_functionsapi.html)
