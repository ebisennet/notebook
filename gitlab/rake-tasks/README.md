# Rake Tasks

## 公式資料

- [GitLab Documentation](http://doc.gitlab.com/ce/raketasks/README.html)

## コマンド

``` bash
gitlab-rake
```

タスク一覧

``` bash
gitlab-rake -T
```

## Topics

- [バックアップ・リストア](backup-restore.md)

## gitlab:import:repos

- [GitLab Documentation](http://docs.gitlab.com/ce/raketasks/import.html)

