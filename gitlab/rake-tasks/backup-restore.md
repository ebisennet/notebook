# バックアップ・リストア

## 公式資料

- [GitLab Documentation](http://docs.gitlab.com/ce/raketasks/backup_restore.html)

## バックアップ

1. gitlab-rake gitlab:backup:create

## リストア

1. tarball を配置
    - /var/opt/gitlab/backups
2. gitlab 開始
3. gitlab-ctl stop unicorn
4. gitlab-ctl stop sidekiq
5. gitlab-rake gitlab:backup:restore BACKUP=**********
    - BACKUP パラメーターに tarball のファイル名の数値部分を指定。
    - 途中 authorized_keys の復元について質問あり
6. gitlab-ctl start sidekiq
7. gitlab-ctl start unicorn

バックアップとリストアで同じバージョンのGitLabでないと失敗する。
