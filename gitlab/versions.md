# Versions

次のバージョンの情報は以下を参照。

- [GitLab Direction \| GitLab](https://about.gitlab.com/direction/)

過去のバージョン情報は以下を参照。

- [List of all GitLab releases \| GitLab](https://about.gitlab.com/release-list/)

## 9.3

- [GitLab 9\.3 Released with Code Quality and Multi\-Project Pipeline Graphs \| GitLab](https://about.gitlab.com/2017/06/22/gitlab-9-3-released/)
    - 2017-06-22

## 9.2

- [GitLab 9\.2 Released with Multiple Assignees For Issues and Pipeline Schedules \| GitLab](https://about.gitlab.com/2017/05/22/gitlab-9-2-released/)
    - 2017-05-22

## 9.0

- [GitLab 9\.0 Released with Subgroups and Deploy Boards \| GitLab](https://about.gitlab.com/2017/03/22/gitlab-9-0-released/)

## 8.17

- [GitLab 8\.17 Released \| GitLab](https://about.gitlab.com/2017/02/22/gitlab-8-17-released/)
    - 2017-02-22
    - GitLab Pages が CE でも有効に。
    - PlantUML 対応。
        - [PlantUML support for Markdown \(\!8588\) · Merge Requests · GitLab\.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8588)
## 8.16

- [GitLab 8\.16 Released with auto deploy on Google Container Engine and Prometheus monitoring as part of GitLab \| GitLab](https://about.gitlab.com/2017/01/22/gitlab-8-16-released/)
    - 2017-01-22
    - Time Tracking in CE and API
    - New issues search and filter interface
    - Deploy keys with write access

## 8.15

- [GitLab 8\.15 Released with Auto Deploy and Web Terminal \| GitLab](https://about.gitlab.com/2016/12/22/gitlab-8-15-released/)
    - 2016-12-11

## 8.14

- [GitLab 8\.14 Released with Time Tracking Beta and Chat Commands \| GitLab](https://about.gitlab.com/2016/11/22/gitlab-8-14-released/)
    - 2016-11-22
    - Time Tracking Beta (EE)
    - Chat Commands (experimental)
    - レビュー前のマージ抑止
    - JIRA 統合の改善

## 8.13

- [GitLab 8\.13 Released with Multiple Issue Boards and Merge Conflict Editor \| GitLab](https://about.gitlab.com/2016/10/22/gitlab-8-13-released/)
    - 2016-10-22
    - Multiple Issue Boards (EE)
    - New Issue from the Issue Board
    - Merge Conflict Editor
    - Group Labels
    - Ability to stop Review Apps
    - ...
- [GitLab 8\.13\.3, 8\.12\.8, 8\.11\.10, and 8\.10\.13 Released \| GitLab](https://about.gitlab.com/2016/11/02/cve-2016-9086-patches/)
    - Security Fix

## 8.12

- [GitLab 8\.12 Released with Cycle Analytics and Global Code Search \| GitLab](https://about.gitlab.com/2016/09/22/gitlab-8-12-released/)
    - 2016-09-22
    - Cycle Analytics
    - Global Code Search (EE)
    - Merge Request Versions
    - Preventing Secrets in your repositories (EE)
    - ...

## 8.11

- [GitLab 8.11 released with Issue Boards and Merge Conflict Resolution | GitLab](https://about.gitlab.com/2016/08/22/gitlab-8-11-released/)
    - 2016-08-22
    - Issue Board
    - GitLab 上でコンフリクト解決可能
        - 編集まではできない。 => 8.13 で対応
    - ブランチにユーザー/ロール単位で、マージやPUSHの制限が可能 (EE Only)
    - その他、パフォーマンスなど色々な改善
- [コードマージ時のコンフリクト自動解決とカンバン画面が加わった「GitLab 8.11」リリース － Publickey](http://www.publickey1.jp/blog/16/gitlab_811.html)

## 8.10

- [GitLab 8\.10 released with Wildcard Branch Protection and Manual Actions for CI \| GitLab](https://about.gitlab.com/2016/07/22/gitlab-8-10-released/)
    - 2016-07-22

## 8.9

- [GitLab 8\.9 released with File Locking, Environments, Priority Labels and more\! \| GitLab](https://about.gitlab.com/2016/06/22/gitlab-8-9-released/)
    - 2016-06-22

## 8.8

- 2016-05-22
- [GitLab 8.8 released with Pipelines and .gitignore templates | GitLab](https://about.gitlab.com/2016/05/22/gitlab-8-8-released/)
    - GitLab CI で Pipelines 機能
    - Docker Distribution が統合された。
        - [GitLab Container Registry | GitLab](https://about.gitlab.com/2016/05/23/gitlab-container-registry/)
            - 名称が紛らわしい
    - .gitignore テンプレート機能
    - ヘルスチェック機能（/health_check）
    - UI改善
- [GitリポジトリとDockerイメージレジストリを統合した「GitLab 8.8」リリース。開発や継続的統合のワークフローがシンプルに － Publickey](http://www.publickey1.jp/blog/16/gitdockergitlab_88.html)

## 8.7

- [GitLab 8\.7 released with Due Date for Issues and Remote Mirrors \| GitLab](https://about.gitlab.com/2016/04/22/gitlab-8-7-released/)
    - 2016-04-22

## 8.6

- [GitLab 8\.6 released with Deploy to Kubernetes and Subscribe to Label \| GitLab](https://about.gitlab.com/2016/03/22/gitlab-8-6-released/)
    - 2016-03-22

## 8.5

- [GitLab's fastest release ever: 8\.5, with Todos and Geo \| GitLab](https://about.gitlab.com/2016/02/22/gitlab-8-5-released/)
    - 2016-02-22

## 8.4

- [GitLab's 50th Release: 8\.4 \| GitLab](https://about.gitlab.com/2016/01/22/gitlab-8-4-released/)
    - 2016-01-22
- [「GitLab 8.4」リリース | OSDN Magazine](https://osdn.jp/magazine/16/01/25/230000)

## 8.3

- [GitLab 8.3 released with Auto-merge and GitLab Pages | GitLab](https://about.gitlab.com/2015/12/22/gitlab-8-3-released/)
    - Pages (EE Only)
    - JIRA Integration が全エディションで使用可能に。
    - Issue への重み付け (EE Only)
- [doc/update/8.2-to-8.3.md · master · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/update/8.2-to-8.3.md)
    - Migration

## 7.12

- [GitLab 7.12がアツい件 - プチ技術メモ](http://hiroponz.hateblo.jp/entry/2015/06/23/230827)
    - SAML認証に対応

