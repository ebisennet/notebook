# Deploy Key

## Public Deploy Key

- [Allow admin to create public deploy keys that are accessible to any project. (!469) · Merge Requests · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/469)
- Admin Area から登録すると Public Deploy Key となる。
- Public ではない Deploy Key は、登録者がオーナーとなる点に注意。

## 参考資料

- [GitLab: Read Onlyのgit アクセスを実現するDeploy Keyが便利！ - INNOBASE技術ブログ](http://blog.innobase.co.jp/entry/2014/10/22/113041)

