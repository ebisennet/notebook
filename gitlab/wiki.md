# Wiki

プロジェクトごとに Wiki を使うことが出来る。

- 日本語ファイル名は使用不可
- サブディレクトリ使用可能

## 8.4.x で使えない記法

- [gitlabhq/markdown.md at master · gitlabhq/gitlabhq · GitHub](https://github.com/gitlabhq/gitlabhq/blob/master/doc/user/markdown.md#wiki-specific-markdown)

上記ページの以下使えない。

- Wiki-specific Markdown
    - Wiki 内でディレクトリ構造を意識したリンクが作成できない。
        - Wiki ではなくプロジェクトリポジトリ内へのリンクとなってしまう。
- Multiline Blockquote
