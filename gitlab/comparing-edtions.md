# エディション比較

|                                              | CE | EE | Remarks                                                                                                                      |
|----------------------------------------------+----+----+------------------------------------------------------------------------------------------------------------------------------|
| GitLabからJIRAへの参照                       | OK | OK | [External issue tracker](http://doc.gitlab.com/ce/integration/external-issue-tracker.html) 機能を使う                        |
| GitLabからJIRAにコミット内容を登録           | OK | OK |                                                                                                                              |
| GitLabからJIRAのイシューを自動的に閉じる     | OK | OK |                                                                                                                              |
| 任意のGitフックスクリプトをGUIから設定       | NG | NG | ファイルシステム上で直接設定が必要                                                                                           |
| Gitフックによる簡易なルール設定をGUIから設定 | NG | OK | コミットメッセージなどを正規表現で制限がかけれる                                                                             |
| Merge Request の複数人によるレビュー         | NG | OK | [Feature Highlight: Merge Request Approvals](https://about.gitlab.com/2015/07/29/feature-highlight-merge-request-approvals/) |
| ユーザー/ロール単位のブランチ操作制限        | NG | OK |                                                                                                                              |

## 公式資料

- [Features \| GitLab](https://about.gitlab.com/features/)
