# Update

## 公式資料

- [doc/update.md · master · GitLab.org / omnibus-gitlab · GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/update.md)
    - バージョン別のアップデートドキュメント

## 手順

バージョンによって違う。

### 7.6.2 -> 8.4.10 の時

1. gitlab-ctl stop unicorn
2. gitlab-ctl stop sidekiq
3. yum install gitlab-ce-8.4.10-ce.0.el7.x86_64.rpm

## 参考資料

- [GitLabを7.9.2から7.12.0にアップデートした時の話 - Qiita](http://qiita.com/Siromitu26/items/440a7aa987c6bf6936f0)

