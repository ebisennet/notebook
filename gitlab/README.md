# gitlab

git リポジトリ管理。

## 公式資料

- [Create, review and deploy code together with GitLab open source git repo management software | GitLab](https://about.gitlab.com/)
- [GitLab High Availability | GitLab](https://about.gitlab.com/high-availability/)
    - 公式ページの HA についての記述
- [GitLab Documentation](http://doc.gitlab.com/)
    - [GitLab Documentation (CE)](http://doc.gitlab.com/ce/)
        - [Requirements](http://doc.gitlab.com/ce/install/requirements.html)
            - 要求するハードウェアスペックなど
        - [Protected Branches](http://doc.gitlab.com/ce/workflow/protected_branches.html)
- [gitlabhq/gitlabhq · GitHub](https://github.com/gitlabhq/gitlabhq)
- [gitlab/gitlab-ce - Packages](https://packages.gitlab.com/gitlab/gitlab-ce)
    - GitLab CE の APT/YUM リポジトリ
- [GitLab Documentation](http://doc.gitlab.com/ce/update/README.html)
    - Update 関連資料
- [doc/update · master · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/update)
    - GitLab の各種バージョン別のアップデートドキュメント
    - Omnibus は Omnibus としてのアップデートが別途ある
- [Help · GitLab](https://gitlab.com/help)
    - [Markdown · User · Help · GitLab](https://gitlab.com/help/user/markdown.md)
- [Press and Logos \| GitLab](https://about.gitlab.com/press/)

## Topics

- [インストール](installation/)
- [Omnibus Package](omnibus-package/)
- [Requirements](requirements.md)
- [Backup](backup.md)
- [Admin](admin.md)
- [Integrations](integrations/)
- [Availability](availability.md)
- [Rake Tasks](rake-tasks/)
- [Versions](versions.md)
- Features
    - CI
    - [Mattermost](mattermost.md)
    - [Pages (EE Only)](pages.md)
    - [Merge Request](merge-request.md)
    - [Webhook](webhook.md)
    - [Deploy Key](deploy-key.md)
    - [Wiki](wiki.md)
- [コマンド](commands/)
- [Update](update.md)
- [API](api/)
- [エディション比較](comparing-edtions.md)
- [Troubleshooting](troubleshooting.md)

## 使用ポート

- 80 or 443: nginx
- 8080: gitlab 本体

## Mirroring

- [samrocketman/gitlab-mirrors · GitHub](https://github.com/samrocketman/gitlab-mirrors)

## 比較

|                       | GitLab | GitHub | GitBucket |
|-----------------------|--------|--------|-----------|
| 逆方向の Pull Request | NG     |        | OK        |

## 参考資料

- [GitHubクローン「GitLab」を日本語化して使う方法 - Qiita](http://qiita.com/ksoichiro/items/f63196aa0a7f07c1862d)
    - 日本語化パッチ
- [Gitlabのインストールとアップデート - Qiita](http://qiita.com/takanemu/items/fca47833e6dd88ee5630)
- [huacnlee/gitlab-mail-receiver · GitHub](https://github.com/huacnlee/gitlab-mail-receiver)
