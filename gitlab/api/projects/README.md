# Projects

## 公式資料

- [Projects \- GitLab Documentation](https://docs.gitlab.com/ce/api/projects.html)

## プロジェクト情報取得例

```sh
curl -k -sSL -H 'PRIVATE-TOKEN: ZgNvyEryELyuS2wynEyr' 'https://10.39.229.8/api/v3/projects/INF_CODE%2Fshared_usage' | json_reformat
```

出力例：

```json
{
    "id": 99,
    "description": "共通利用されるインフラコード。",
    "default_branch": "master",
    "tag_list": [

    ],
    "public": false,
    "archived": false,
    "visibility_level": 0,
    "ssh_url_to_repo": "git@10.39.229.8:INF_CODE/shared_usage.git",
    "http_url_to_repo": "https://10.39.229.8/INF_CODE/shared_usage.git",
    "web_url": "https://10.39.229.8/INF_CODE/shared_usage",
    "name": "shared_usage",
    "name_with_namespace": "INF_CODE / shared_usage",
    "path": "shared_usage",
    "path_with_namespace": "INF_CODE/shared_usage",
    "issues_enabled": true,
    "merge_requests_enabled": true,
    "wiki_enabled": true,
    "builds_enabled": true,
    "snippets_enabled": true,
    "created_at": "2016-06-28T13:17:05.242+09:00",
    "last_activity_at": "2016-12-02T10:17:39.093+09:00",
    "shared_runners_enabled": true,
    "creator_id": 466,
    "namespace": {
        "id": 500,
        "name": "INF_CODE",
        "path": "INF_CODE",
        "owner_id": null,
        "created_at": "2016-06-25T19:20:41.536+09:00",
        "updated_at": "2016-06-25T19:20:41.536+09:00",
        "description": "インフラコード開発グループ",
        "avatar": {
            "url": null
        }
    },
    "avatar_url": null,
    "star_count": 2,
    "forks_count": 1,
    "open_issues_count": 37,
    "public_builds": true,
    "permissions": {
        "project_access": null,
        "group_access": {
            "access_level": 30,
            "notification_level": 3
        }
    }
}
```
