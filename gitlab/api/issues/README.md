# Issues

## 公式資料

- [Issues \- GitLab Documentation](https://docs.gitlab.com/ce/api/issues.html)

## Issue の取得例

```sh
curl -k -sSL -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" 'https://10.39.229.8/api/v3/projects/INF_CODE%2Fshared_usage/issues/600' | json_reformat
```

出力例：

```json
{
    "id": 600,
    "iid": 533,
    "project_id": 99,
    "title": "国際 apache アクセス制御",
    "description": "```\r\n  expected: \"SetEnvIf Remote_Addr (10.39.161.[1-4]|10.39.162.|10.39.163.) AllowStatus\"\r\n       got: \"SetEnvIf Remote_Addr (10.39.161.[1-4]|10.39.163.) AllowStatus\"\r\n```\r\n\r\n設計資料は`(10.39.161.[1-4]|10.39.162.|10.39.163.)`なのですが、実機は`(10.39.161.[1-4]|10.39.163.)`となっています。どの状態が正しいでしょうか？",
    "state": "closed",
    "created_at": "2016-11-16T12:10:35.984+09:00",
    "updated_at": "2016-11-30T13:40:42.380+09:00",
    "labels": [
        "Ansible",
        "相談・質問"
    ],
    "milestone": null,
    "assignee": {
        "name": "mikami",
        "username": "kenichiro.mikami",
        "id": 467,
        "state": "active",
        "avatar_url": "https://10.39.229.8/uploads/user/avatar/467/100px-Japanese_Crest_Futatsudomoe_1.svg.png",
        "web_url": "https://10.39.229.8/u/kenichiro.mikami"
    },
    "author": {
        "name": "mikami",
        "username": "kenichiro.mikami",
        "id": 467,
        "state": "active",
        "avatar_url": "https://10.39.229.8/uploads/user/avatar/467/100px-Japanese_Crest_Futatsudomoe_1.svg.png",
        "web_url": "https://10.39.229.8/u/kenichiro.mikami"
    }
}
```

iid が Web UI から見れる Issue ID となる。

直接 Issue ID で取得する場合は以下。

```sh
curl -k -sSL -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" 'https://10.39.229.8/api/v3/projects/INF_CODE%2Fshared_usage/issues?iid=533' | json_reformat
```

取得結果は配列として帰ってくる点に注意。

```json
[
    {
        "id": 600,
        "iid": 533,
        "project_id": 99,
        "title": "国際 apache アクセス制御",
        "description": "```\r\n  expected: \"SetEnvIf Remote_Addr (10.39.161.[1-4]|10.39.162.|10.39.163.) AllowStatus\"\r\n       got: \"SetEnvIf Remote_Addr (10.39.161.[1-4]|10.39.163.) AllowStatus\"\r\n```\r\n\r\n設計資料は`(10.39.161.[1-4]|10.39.162.|10.39.163.)`なのですが、実機は`(10.39.161.[1-4]|10.39.163.)`となっています。どの状態が正しいでしょうか？",
        "state": "closed",
        "created_at": "2016-11-16T12:10:35.984+09:00",
        "updated_at": "2016-11-30T13:40:42.380+09:00",
        "labels": [
            "Ansible",
            "相談・質問"
        ],
        "milestone": null,
        "assignee": {
            "name": "mikami",
            "username": "kenichiro.mikami",
            "id": 467,
            "state": "active",
            "avatar_url": "https://10.39.229.8/uploads/user/avatar/467/100px-Japanese_Crest_Futatsudomoe_1.svg.png",
            "web_url": "https://10.39.229.8/u/kenichiro.mikami"
        },
        "author": {
            "name": "mikami",
            "username": "kenichiro.mikami",
            "id": 467,
            "state": "active",
            "avatar_url": "https://10.39.229.8/uploads/user/avatar/467/100px-Japanese_Crest_Futatsudomoe_1.svg.png",
            "web_url": "https://10.39.229.8/u/kenichiro.mikami"
        }
    }
]
```

上記を組み合わせて指定した issue にメッセージを残す関数。

```bash
function post_comment () {
  local GITLAB_API_URL=https://10.39.229.8/api/v3
  local project=$(echo -n "${1%%#*}" | sed -e 's/\//%2F/g')
  local iid="${1##*#}"
  local msg=$2
  local id

  id=$(curl -k -sSL -H "PRIVATE-TOKEN: ${JENKINS_GITLAB_TOKEN}" "${GITLAB_API_URL}/projects/${project}/issues?iid=${iid}" | ruby -r json -e 'p JSON(STDIN.read)[0]["id"]' )
  curl -k -sSL -X POST -H "PRIVATE-TOKEN: ${JENKINS_GITLAB_TOKEN}" "${GITLAB_API_URL}/projects/${project}/issues/${id}/notes" -d "body=${msg}"
}

export JENKINS_GITLAB_TOKEN=...
post_commit "group/project#123" "Test message."
```

## Memo

- API 上 :id となっているところは、内部的な ID。
    - Web UI から見れる Issue ID ではないので注意。
    - :iid となっているところは Web UI から見れる Issue ID。
