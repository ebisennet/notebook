# 設定

GitLab Omnibus Package の設定ファイルの内容について。

`/etc/gitlab/gitlab.rb` の内容をまとめる。

## 全体

- external_url
    - GitLabに外部からアクセスするときのURL
    - GitLab が Listen するポートやプロトコルが決まる。
        - 別途指定することも可能。
    - 通知メールなどに埋め込まれる。

## GitLab Nginx

- [doc/settings/nginx.md · master · GitLab.org / omnibus-gitlab · GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/nginx.md)

