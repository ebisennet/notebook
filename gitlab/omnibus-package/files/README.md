# Files

GitLab Omnibus Pacakge に関連したファイルについてまとめる。

| Path                                                           | Remarks                              |
|----------------------------------------------------------------|--------------------------------------|
| /etc/gitlab/gitlab-secrets.json                                | token や salt などを格納したファイル |
| /etc/gitlab/gitlab.rb                                          | メインの設定ファイル                 |
| /etc/systemd/system/basic.target.wants/gitlab-runsvdir.service | Systemd のユニット定義               |
| /opt/gitlab/bin                                                | gitlabの各種コマンド                 |
| /opt/gitlab/embedded                                           |                                      |
| /opt/gitlab/embedded/cookbooks                                 |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab                          |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/attributes               |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/definitions              |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/files                    |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/libraries                |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/recipes                  |                                      |
| /opt/gitlab/embedded/cookbooks/gitlab/templates                |                                      |
| /opt/gitlab/etc                                                |                                      |
| /opt/gitlab/init                                               | embedded/bin/sv へのsymlink格納      |
| /opt/gitlab/service                                            | sv へのsymlink格納                   |
| /opt/gitlab/sv                                                 |                                      |
| /opt/gitlab/var                                                |                                      |
| /opt/gitlab/version-manifest.txt                               | インストールされるソフトウェアの情報 |
| /var/log/gitlab                                                |                                      |
| /var/log/gitlab/gitlab-shell                                   |                                      |
| /var/log/gitlab/sidekiq                                        |                                      |
| /var/opt/gitlab                                                |                                      |
| /var/opt/gitlab/backups                                        |                                      |
| /var/opt/gitlab/git-data                                       |                                      |
| /var/opt/gitlab/git-data/repositories                          | git リポジトリディレクトリー         |
| /var/opt/gitlab/gitlab-ci                                      |                                      |
| /var/opt/gitlab/gitlab-git-http-server                         |                                      |
| /var/opt/gitlab/gitlab-rails                                   |                                      |
| /var/opt/gitlab/gitlab-shell                                   |                                      |
| /var/opt/gitlab/gitlab-workhorse                               |                                      |
| /var/opt/gitlab/logrotate                                      |                                      |
| /var/opt/gitlab/nginx                                          |                                      |
| /var/opt/gitlab/postgresql                                     |                                      |
| /var/opt/gitlab/redis                                          |                                      |


