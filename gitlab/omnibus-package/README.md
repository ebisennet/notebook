# GitLab CE Omnibus Package

Ubuntu, Debian, CentOS (RedHat系含む) が対象。

## 公式資料

- [gitlabhq/omnibus-gitlab · GitHub](https://github.com/gitlabhq/omnibus-gitlab)
    - Omnibus Package のソースコード
- [GitLab Documentation](http://doc.gitlab.com/omnibus/)
    - Omnibus Package としてのドキュメント
    - "Configuration" の項に、各種設定に関するドキュメントがある。
- [Download GitLab Community Edition (CE) | GitLab](https://about.gitlab.com/downloads/)

## Topics

- [Installation](installation/)
- [Commands](commands.md)
- [Files](files/)
- [Configurations](configurations.md)
- Backup
    - [GitLab Documentation](http://doc.gitlab.com/omnibus/settings/backups.html)
        - Omnibus Package としてのバックアップ設定のドキュメント
        - CE 側のドキュメントにリンクしている
            - [GitLab Documentation](http://doc.gitlab.com/ce/raketasks/backup_restore.html)
- Update
    - [Updating GitLab via omnibus-gitlab](http://docs.gitlab.com/omnibus/update/README.html#updating-from-gitlab-66-and-higher-to-710-or-newer)
        - Omnibus Pakcage の各種アップデートについてのドキュメント。
        - GitLab 本体に比べてほとんどのバージョンでアップデート方法が変わらない。

## 参考資料

- [Vagrant内のCentOS 6.5にGitLab Omnibusをインストールしてみた話 - Qiita](http://qiita.com/tacck/items/d025f02e1db656a6fc66)
- [GitLab CE Omnibus のインストール - ダメ出し Blog](https://fumiyas.github.io/gitlab/install-omnibus.html)
- [CentOS 7にGitHubライクなGitLabを2分でインストールしてみた | urashita.com](http://urashita.com/archives/2870)
- [CentOS7へGitlabを公式ドキュメントに従いインストールする(およそ3分作業) | Coffee Breakにプログラミング備忘録](http://to-developer.com/blog/?p=1726)
    - インストール手順のドキュメント不足の指摘あり

