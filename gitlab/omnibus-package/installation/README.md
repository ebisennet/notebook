# インストール

- [CentOS](centos.md)

## 公式資料

- [Download GitLab Community Edition (CE) | GitLab](https://about.gitlab.com/downloads/)
    - ダウンロードページ
    - スクリプトが動かない場合は以下から手動でダウンロード
        - [gitlab/gitlab-ce - Packages](https://packages.gitlab.com/gitlab/gitlab-ce)

## ダウンロード

### curl

``` bash
GITLAB_VERSION=8.3.0
EPEL_VERSION=7
curl -sSL "https://packages.gitlab.com/gitlab/gitlab-ce/packages/el/${EPEL_VERSION}/gitlab-ce-${GITLAB_VERSION}-ce.0.el${EPEL_VERSION}.${uname -m}.rpm/download"
```

### 8.3.0

- RPM (CentOS, RedHat)
    - <https://packages.gitlab.com/gitlab/gitlab-ce/packages/el/7/gitlab-ce-8.3.0-ce.0.el7.x86_64.rpm>

## Memo

インストールスクリプトを使う方式は、公式サイトに間違いがあるケースがあるので注意。

RPM をダウンロードしておいたほうが確実。
