# CentOS

## 公式資料

- [gitlab/gitlab-ce - Installation](https://packages.gitlab.com/gitlab/gitlab-ce/install)
    - インストール手順

## GitLab 提供 yum リポジトリを使う場合

### 準備

#### proxy 設定

##### curl

$HOME/.curlrc を作成。

``` example
cat <<EOF>>$HOME/.curlrc
proxy="http://username:password@your.proxy.server:8080/"
EOF
```

##### yum

/etc/yum.conf に以下を設定。

``` example
cat <<EOF>>/etc/yum.conf
proxy=http://username:password@your.proxy.server:8080
EOF
```

### 依存パッケージインストール

``` bash
sudo yum install pygpgme yum-utils
```

先に EPEL を有効化していること。

### GitLab yum リポジトリの登録

/etc/yum.repos.d/gitlab_gitlab-ce.repo の作成

``` bash
[gitlab_gitlab-ce]
name=gitlab_gitlab-ce
baseurl=https://packages.gitlab.com/gitlab/gitlab-ce/el/7/$basearch
repo_gpgcheck=1
enabled=1
gpgkey=https://packages.gitlab.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt

[gitlab_gitlab-ce-source]
name=gitlab_gitlab-ce-source
baseurl=https://packages.gitlab.com/gitlab/gitlab-ce/el/7/SRPMS
repo_gpgcheck=1
enabled=1
gpgkey=https://packages.gitlab.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
```

### インストール

``` example
yum install -y gitlab-ce
```

### 設定

``` example
gitlab-ctl reconfigure
```

## 参考資料

- [CentOS7へGitlabを公式ドキュメントに従いインストールする(およそ3分作業) | Coffee Breakにプログラミング備忘録](http://to-developer.com/blog/?p=1726)

