# Backup

## 公式資料

- [gitlabhq/backup.rake at master · gitlabhq/gitlabhq · GitHub](https://github.com/gitlabhq/gitlabhq/blob/master/lib/tasks/gitlab/backup.rake)
    - rake task 定義
- [gitlabhq/lib/backup at master · gitlabhq/gitlabhq · GitHub](https://github.com/gitlabhq/gitlabhq/tree/master/lib/backup)
    - rake task から呼び出されているコード
- [GitLab Documentation](http://doc.gitlab.com/ce/raketasks/backup_restore.html)
    - gitlab-rake を使ったフルバックアップ
    - LVM + rsync を使ったバックアップ
        - /var/opt/gitlab を対象にする。
        - LVM については以下などを参照。
            - [3. LVM（Logical Volume Manager）(第2章ディスク管理～上級:基本管理コースII)](https://users.miraclelinux.com/technet/document/linux/training/2_2_3.html)

## 物理バックアップ

/var/opt/gitlab をバックアップすればよい。

### PostgreSQL の設定変更

/etc/gitlab/gitlab.rb で以下を設定して gitlab-reconfigure と再起動を実施する。

``` example
postgresql['wal_level'] = "archive"
```

### PostgreSQL でバックアップ開始宣言

``` bash
sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql -d gitlabhq_production \
  -c "SELECT pg_start_backup(now()::text)"
```

### PostgreSQL でバックアップ終了宣言

``` bash
sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql -d gitlabhq_production \
  -c "SELECT pg_stop_backup()"
```

WAL archiving に関する警告が出るが無視して良い。

## 参考

### gitlab-psql ユーザ

gitlab の postgresql 周りを調査する時は gitlab-sql ユーザーに切り替わって行う。

``` bash
sudo su - gitlab-psql
```

HOME は /var/opt/gitlab/postgresql となる。

DB 内を psql で直接調査する場合は以下のコマンド。

``` bash
sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql -d gitlabhq_production
```

### PostgreSQL の設定変更

postgresql.conf を生成している chef レシピのテンプレートは以下の場所にある。

- /opt/gitlab/embedded/cookbooks/gitlab/templates/default/postgresql.conf.erb

/etc/gitlab/gitlab.rb に無い設定項目がある。
