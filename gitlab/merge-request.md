# Merge Request

- [Option to disallow author to approve merge request (\#388) · Issues · GitLab.org / GitLab Enterprise Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ee/issues/388)
    - 自分自身で Accept を不許可にする設定は、2016-06-27時点でマージされていない。
- [Feature Highlight: Merge Request Approvals | GitLab](https://about.gitlab.com/2015/07/29/feature-highlight-merge-request-approvals/)
    - 承認者を複数に設定できる。
- [Merge Request Approvals - GitLab Documentation](http://docs.gitlab.com/ee/workflow/merge_request_approvals.html)
- ["Work In Progress" Merge Requests](http://docs.gitlab.com/ce/workflow/wip_merge_requests.html)
