# Jira 連携

## GitLab の JIRA 連携機能

EE だけだったが、GitLab 8.3 から全 Edition で使えるようになった。

Git コメントで Issue について記述することで JIRA 上のコメントとして自動投稿される。

- [GitLab Documentation](http://doc.gitlab.com/ee/project_services/jira.html)
    - EE の Jira Integration ドキュメント
- [GitLab Documentation](http://doc.gitlab.com/ce/project_services/jira.html)
    - CE の Jira Integration ドキュメント
- 必要なもの
    - Jira User/Password

## Webhook を使った連携

- [lesstif/gitlab-jira-integration · GitHub](https://github.com/lesstif/gitlab-jira-integration)
    - GitLab の Web フックから Jira の REST API を読んでコミットとイシューを関連付ける。
    - 必要なもの
        - Jira Server URL
        - Jira User/Password.
            - コメント対象プロジェクトへの書き込み権限付きがあること
        - GitLab Server URL
        - GitLab User, Token
            - Hook 設定対象のプロジェクト一覧を得る？
            - Hook で呼び出された時に Commit を調べる？
    - 設定作業
        - Jira
            - gitlab ユーザーを
        - GitLab
            - gitlab ユーザーを Jira 連携したいプロジェクトのメンバーに。
            - Webhook を設定。
        - Webhook 設定は GitLab API を使えばバッチ化可能。
        - バッチ実行は GitLab の System hooks

