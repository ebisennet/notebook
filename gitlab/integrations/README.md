# Integrations

GitLab と他のソフトウェア、サービスの連携機能。

## Topics

- [Jira 連携](jira.md)

## Service Templates

- Admin Area で設定できる。
- 新規プロジェクト生成時にテンプレートを設定した連携情報が適用される。
    - 既存プロジェクトには影響しない。

