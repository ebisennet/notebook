# Docker

## Official Image

- <https://hub.docker.com/r/gitlab/gitlab-ce/>
- [GitLab Documentation](http://doc.gitlab.com/omnibus/docker/)

Omnibus Package をベースにしている。 OS は Ubuntu ベース。

### 最小限

``` bash
docker run -d -p 80:80 --restart=always gitlab/gitlab-ce
```

### boot2docker 用、かつ、ボリュームなどの指定も追加

``` bash
# 22 は boot2docker ホストが使っているのでポートを変更
docker run -d \
  -p 443:443 -p 80:80 -p 10022:22 \
  --name gitlab \
  --restart always \
  --volume $HOME/DockerVolume/gitlab/config:/srv/gitlab/config \
  --volume $HOME/DockerVolume/gitlab/logs:/srv/gitlab/logs \
  --volume $HOME/DockerVolume/gitlab/data:/srv/gitlab/data \
  gitlab/gitlab-ce
```

### 初期アカウント

|     |          |
|-----|----------|
| ID  | root     |
| PW  | 5iveL!fe |

## 3rd-party Image

- <https://hub.docker.com/r/sameersbn/gitlab/>
    - [Dockerで5分くらいでGitLabを試す - Qiita](http://qiita.com/yacchin1205/items/fa774011d72ead599eb5)
    - [Docker GitLab by sameersbn](http://www.damagehead.com/docker-gitlab/)

