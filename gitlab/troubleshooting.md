# Troubleshooting

## Markdown

- 上位ディレクトリへのリンクができない。
    - GitLab 8.4 以上にアップデートが必要。
    - [Markdown parent relative links broken (\#7967) · Issues · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/issues/7967)
        - [Support a single directory traversal in RelativeLinkFilter (!2247) · Merge Requests · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/2247)
- WebUI 上で Markdown を編集した時にプレビューが見れない。
    - GitLab 8.11 以上にアップデートが必要。
    - [ACE editor causing 404 errors every time you try to edit a file in the WebUI (#3225) · Issues · GitLab.org / GitLab Community Edition · GitLab](https://gitlab.com/gitlab-org/gitlab-ce/issues/3225)
