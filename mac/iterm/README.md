# iTerm

## Topics

- [Shell Integration](shell-integration.md)
- [ショートカット](shortcuts.md)

## Dynamic Profiles

- [Dynamic Profiles - Documentation - iTerm2 - Mac OS Terminal Replacement](https://iterm2.com/documentation-dynamic-profiles.html)

## 同時実行

Menu "Shell" - "Broadcast Input"

- [iTerm2で複数タブへ同じ内容を同時に入力する - カメニッキ](http://tapira.hatenablog.com/entry/2015/06/08/190559)

## 参考資料

- [iTerm2 Version 3 betaの新機能を試してみた](http://swiftrithm.com/blog/iterm2-version3-beta/)
