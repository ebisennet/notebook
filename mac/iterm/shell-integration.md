# Shell Integration

## Badge

表示内容は Profile で設定可能。

```
\(user.gitBranch)\n\(session.username)@\(session.hostname)
```

git ブランチとユーザー名、ホスト名の表示。

user.gitBranch を使うためには以下を .bashrc で定義する。

```
function iterm2_print_user_vars() {
  iterm2_set_user_var gitBranch $((git branch 2> /dev/null) | grep \* | cut -c3-)
}
```

## 参考

- [iTerm2 3.0 - Shell統合 - Qiita](http://qiita.com/ksato9700/items/36a06de1c080167e1538)
