# mac

## Topics

- [Shortcuts](shortcuts.md)
- [brew](brew/)
- [Wine](wine/)
- [iTerm](iterm/)
- [Hyper](hyper/)
- QuickTime Player
    - [画面キャプチャ方法（Mac＆Win） \- Qiita](http://qiita.com/mocho/items/06cd109610ab256c6b6f)
- Tad
    - [Tad \- A Desktop Viewer App for Tabular Data](http://tadviewer.com/)

## Environment Variables

- [MacOSX - Macでの環境変数（environment variables）の設定方法 - Qiita](http://qiita.com/hoisjp/items/19374252db8e748c3407)
  - Yosemite は launchctl コマンドを agent で実行する
- [Setting environment variables via launchd.conf no longer works in OS X Yosemite/El Capitan? - Stack Overflow](http://stackoverflow.com/questions/25385934/setting-environment-variables-via-launchd-conf-no-longer-works-in-os-x-yosemite)
  - sh 経由で launchctl setenv を複数実行。

## 起動音

```
sudo nvram SystemAudioVolume=%80
```
