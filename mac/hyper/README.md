# Hyper

## 公式資料

- [Hyper™](https://hyper.is/)
- [GitHub \- zeit/hyper: A terminal built on web technologies](https://github.com/zeit/hyper)

## インストール

```sh
brew update
brew cask install hyper
```
