# Shortcuts

## Screenshots

| Shortcuts   |                        |
|-------------+------------------------|
| Cmd+Shift+3 | 画面全体をキャプチャ   |
| Cmd+Shift+4 | 範囲選択してキャプチャ |

## Zoom

| Shortcuts  |                     |
|------------+---------------------|
| Cmd+Opt+-  | 縮小                |
| Cmd+Opt+\^ | 拡大                |
| Cmd+Opt+8  | ズーム On/Off       |
| Cmd+Opt+\  | スムージング On/Off |



