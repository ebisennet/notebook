# gollum

## 公式資料

- [GitHub - gollum/gollum: A simple, Git-powered wiki with a sweet API and local frontend.](https://github.com/gollum/gollum)

## 参考資料

- [ぼっちWikiに最適なGollumについてのメモ - Qiita](http://qiita.com/tatzyr/items/5ea6328719d72135c6c3)
