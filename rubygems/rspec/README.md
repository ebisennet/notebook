# rspec

## 公式資料

- [RSpec: Behaviour Driven Development for Ruby](http://rspec.info/)
- [GitHub - rspec/rspec: RSpec meta-gem that depends on the other components](https://github.com/rspec/rspec)
- [RSpec · GitHub](https://github.com/rspec)
- [Publisher: RSpec - Relish](https://relishapp.com/rspec)
