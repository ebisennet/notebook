# rspec_junit_formatter

## 不正なXML出力

- [XML not fully escaped · Issue \#31 · sj26/rspec\_junit\_formatter · GitHub](https://github.com/sj26/rspec_junit_formatter/issues/31)

暫定対応は上記の Issue にある通りで xml 内の文字を置き換え。

```
ruby -pne '$_.gsub!(/\033/, "\uFFFD")' -i reports/*.xml
```

## 内部動作メモ

RSpecJUnitFormatter#xml_dump の戻り値を変換すればいい？

xml_dump_failed メソッドで以下のように出力している。

```ruby
xml_dump_example(example) do
  xml.failure message: exception.to_s, type: exception.class.name do
    xml.cdata! "#{exception.message}\n#{backtrace.join "\n"}"
  end
end
```

上記の cdata の出力が NG

