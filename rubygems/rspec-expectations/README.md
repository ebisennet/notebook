# rspec-expectations

## 公式資料

- [GitHub - rspec/rspec-expectations: Provides a readable API to express expected outcomes of a code example](https://github.com/rspec/rspec-expectations)
- [Built in matchers - RSpec Expectations - RSpec - Relish](https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers)

## 参考資料

- [RSpecのshouldはもう古い！新しい記法expectを使おう！ - Qiita](http://qiita.com/awakia/items/d880250adc8cdbe7a32f)
