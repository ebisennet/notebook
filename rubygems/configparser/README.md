# configparser

Python の ConfigParser と互換のある設定ファイル読み込み。

## 公式資料

- [GitHub - chrislee35/configparser: parses configuration files compatable with Python's ConfigParser](https://github.com/chrislee35/configparser)
- [File: README — Documentation for configparser (0.1.4)](http://www.rubydoc.info/gems/configparser/)
