# net-ssh

## 公式資料

- [GitHub - net-ssh/net-ssh: Pure Ruby implementation of an SSH (protocol 2) client](https://github.com/net-ssh/net-ssh)
- [net-ssh 4.0.0.alpha2](http://net-ssh.github.io/net-ssh/)

## 参考資料

- [RubyのNet::SSHでknown_hostsを無視する - Qiita](http://qiita.com/holysugar/items/b0858887a5ed6aa6b073)
