# コマンド

```
Usage: rspec [options] [files or directories]
```

- -I PATH
    - $LOAD_PATH 追加。
- -r PATH
    - require 指定。
- --dry-run
    - DRY RUN 実行。
