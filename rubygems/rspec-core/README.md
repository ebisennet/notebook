# rspec-core

## 公式資料

- [GitHub - rspec/rspec-core: RSpec runner and formatters](https://github.com/rspec/rspec-core)
- [File: README — Documentation by YARD 0.8.7.6](http://rspec.info/documentation/3.5/rspec-core/)
- [RSpec Core 3.5 - RSpec Core - RSpec - Relish](http://www.relishapp.com/rspec/rspec-core/docs)

## Topics

- [設定読み込み](configuration.md)
- [コマンド](command.md)
- [メタデータ](metadata.md)
- [Custom Formatter](custom-formatter.md)
- [モジュール](modules/)

## Memo

並列実行について。

- [These changes add handling of an additional parameter "\-\-parallel\-test" by bicarbon8 · Pull Request \#1527 · rspec/rspec\-core · GitHub](https://github.com/rspec/rspec-core/pull/1527)
    - 取り込まれていない。

## 参考資料

- [これを読むとRSpecの裏側がどうやって動いているのか分かるかもしれないぜ \- Qiita](http://qiita.com/joker1007/items/641961963ff459125f31)
- [Writing Macros in RSpec • Ben Mabey](http://benmabey.com/2008/06/08/writing-macros-in-rspec.html)
- [Define helper methods in a module \- Helper methods \- RSpec Core \- RSpec \- Relish](https://www.relishapp.com/rspec/rspec-core/v/3-5/docs/helper-methods/define-helper-methods-in-a-module)
