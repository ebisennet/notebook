# 設定読み込み

- [read command line configuration options from files - Configuration - RSpec Core - RSpec - Relish](https://www.relishapp.com/rspec/rspec-core/docs/configuration/read-command-line-configuration-options-from-files)

以下の順で設定が読まれ、先に読み込んだ内容を後に読み込んだ内容が上書きしていく。

1. `~/.rspec`
2. `.rspec`
3. `.rspec-local`
4. コマンドラインオプション
5. `SPEC_OPTS` 環境変数
