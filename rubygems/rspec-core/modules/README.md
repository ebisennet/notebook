# モジュール

- [GitHub - rspec/rspec-core: RSpec runner and formatters](https://github.com/rspec/rspec-core)

## RSpec

- configuration
    - グローバル設定の参照。

## RSpec::Core::Configuration

- load_spec_files
    - spec 読み込みの実体。

## RSpec::Core::ExampleGroup

- describe や it など、基本的な DSL 定義。

## RSpec::Core::Formatters

- Formatter が定義されている module.

## RSpec::Core::Formatters::BaseFormatter

- Formatter のベースクラス。

## RSpec::Core::RakeTask

- system 関数で別プロセスとして rspec コマンドを起動している。
- rspec_opts などが渡せる。
- テスト失敗時の動作は task_on_error などで変わる
    - デフォルトは exit する。
    - task_on_error = false で exit しなくなる。
        - 代わりに rake 自体の終了コードが 0 となる。

## RSpec::Core::SharedExampleGroup

- shared_example の定義を行っているモジュール。

## RSpec::Core::SharedExampleGroupModule

- shared_example の定義を行っているクラス。

## RSpec::Core::SharedExampleGroup::TopLevelDSL

- shared_example の DSL 定義を行っているモジュール。
- 同じソースファイルの最後あたりで定義の呼び出しを行っている。

## RSpec::Core::DSL

- 

## RSpec::Core::Runner

- RSpec の実行を提供するクラス。
- rspec コマンドがこのクラスの invoke メソッドを呼び出す。
    - コマンド経由の場合、@configuration は RSpec.configuration
- メインの処理として以下のメソッドあたり。
    - run
    - setup
    - run_specs
- 同一プロセスでの複数回実行についての記述もある。

## RSpec::Core::RubyProject

- root
    - プロジェクトのルートパスを返す。
    - プロジェクトのルートとして spec のあるディレクトリ探索を行う。
- add_to_load_path(*dirs)
    - ルートを基準にした $LOAD_PATH 追加メソッド。

## RSpec::Core::Configuration

- 各種設定の読み込み。
- lib や spec ディレクトリを $LOAD_PATH に追加している。

[rspec-core/configuration.rb at master · rspec/rspec-core · GitHub](https://github.com/rspec/rspec-core/blob/master/lib/rspec/core/configuration.rb)
