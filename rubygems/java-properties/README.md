# java-properties

## 公式資料

- [GitHub \- jnbt/java\-properties: Loader and writer for \*\.properties files](https://github.com/jnbt/java-properties)

## 書き込み

```ruby
hash = {:foo => "bar"}
JavaProperties.write(hash, "path/to/my.properties")
```
