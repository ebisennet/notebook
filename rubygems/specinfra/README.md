# specinfra

## 公式資料

- [Serverspec - Home](http://serverspec.org/)
- [GitHub - mizzy/specinfra: Command Execution Framework for serverspec, itamae and so on](https://github.com/mizzy/specinfra)

## Topics

- [Troubleshooting](troubleshooting.md)
- [モジュール](modules/)

## 参考資料

- [specinfraを使ってみよう - Qiita](http://qiita.com/sawanoboly/items/d1e7794739d9d31a5316)
