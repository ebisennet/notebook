# Troubleshooting

## host_inventory から値を取得できない

- backend が ssh の時に、ssh 接続に失敗しているとエラーとならずに空の値がセットされる模様。
- 対象が Mac の時は cpu の取得は実装されていない。
    - 該当ソース：[specinfra/inventory.rb at master · mizzy/specinfra · GitHub](https://github.com/mizzy/specinfra/blob/master/lib/specinfra/command/darwin/base/inventory.rb)
