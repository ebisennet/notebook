# モジュール

- [GitHub - mizzy/specinfra: Command Execution Framework for serverspec, itamae and so on](https://github.com/mizzy/specinfra)

## Specinfra

- command
- backend

## Specinfra::Backend::Base

Backend のベースクラス。

Backend を以下のものと結びつけている。

- Configuration
- HostInventory
- Example
- CommandFactory

Example は RSpec 由来。

このクラスから派生したクラスのインスタンスが Serverspec からは @runner として参照される。

## Specinfra::Backend::Ssh

- send_file
- with_env
- ssh_exec!
    - 実際の通信処理をしているメソッド
    - 実行結果として stdout, stderr, exit_status, exit_signal を返す。
- run_command
    - sudo の時、出力結果の先頭の空行削除がある。

## Specinfra::Command::Base

## Specinfra::Command::Base::File

ファイルの確認に関するコマンドを定義しているクラス。

ファイル種別のテストやファイルのコンテンツ取得など。

## Specinfra::Command::Linux

Linux 環境用のコマンドを定義したモジュールをサブモジュールに含む。

## Specinfra::Command::Linux::Base

## Specinfra::Command::Linux::Base::Inventory

Host Inventory のためのコマンドを含む。

## Specinfra::Configuration

- defaults
- os
- method_missing

## Specinfra::HostInventory

- ホストの情報を格納するクラス。
- Specinfra::Properties で :host_inventory というキーに格納される。

## Specinfra::Helper::Configuration

- subject
- build_configurations

## Specinfra::Helper::Properties

- property
- set_property

## Specinfra::Helper::Set

- set

## Specinfra::Properties

- プロパティを格納するシングルトンクラス。
