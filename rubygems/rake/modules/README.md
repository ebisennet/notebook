# モジュール

## Rake

rake_module.rb 内の定義。

- application
    - Rake::Application のグローバルインスタンス。
    - rake コマンドから実行した時に、Rakefile 内から参照可能。
- original_dir
- load_rakefile
- add_rakelib

## Rake::Application

- run
    - init, load_rakefile, top_level の順に実行する。

### 全タスク実行時の処理を追加

Rake.application の top_level を特異クラスを使ってオーバーライドすればよい。

```ruby
class << ::Rake.application
  def top_level
    # オリジナルの top_level 呼び出し
    super
    
    # 後処理
  end
end
```

## Rake::Task

- enhance
    - [Rake で任意のタスクの前後に別のタスクを実行する - HsbtDiary(2012-02-10)](https://www.hsbt.org/diary/20120210.html)
