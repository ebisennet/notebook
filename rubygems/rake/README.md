# rake

## 公式資料

- [Rake -- Ruby Make](http://docs.seattlerb.org/rake/)
- [GitHub - ruby/rake: A make-like build utility for Ruby.](https://github.com/ruby/rake)

## Topics

- [モジュール](modules/)

## 引数の受け取り方

- [rake でのコマンドライン引数の扱い - 君の瞳はまるでルビー - Ruby 関連まとめサイト](http://www.ownway.info/Ruby/rake/arguments)

## Memo

- 並列実行のためには `task` ではなく `multitask` を使う。
- オプション `-m` で全ての `task` が `multitask` として扱われる。
- multitask は各タスクがそれぞれの Thread で実行される。
