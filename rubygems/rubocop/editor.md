# エディター設定

## Atom

- [Ruby初心者に絶対オススメのAtomパッケージ『rubocop-auto-correct』。 - 日々、とんは語る。](http://d.hatena.ne.jp/tomoya/20151114/1447534411)

## Emacs

- [GitHub - bbatsov/rubocop-emacs: An Emacs interface for RuboCop](https://github.com/bbatsov/rubocop-emacs)

## Vim

- [RuboCopをVimで使う設定(Syntastic/プラグイン） | EasyRamble](http://easyramble.com/rubocop-with-vim.html)

