# rubocop

## 公式資料

- [GitHub - bbatsov/rubocop: A Ruby static code analyzer, based on the community Ruby style guide.](https://github.com/bbatsov/rubocop)
    - ソースコード
- [RuboCop: The Ruby Linter that Serves and Protects](http://rubocop.readthedocs.io/en/latest/)
    - [Integration with other tools - RuboCop: The Ruby Linter that Serves and Protects](http://rubocop.readthedocs.io/en/latest/integration_with_other_tools/)
    - [Configuration - RuboCop: The Ruby Linter that Serves and Protects](http://rubocop.readthedocs.io/en/latest/configuration/)

## Topics

- [設定](configuration.md)
- [コード自動修正](auto-correct.md)
- [エディター設定](editor.md)

## 参考資料

- [Rubocopを使ってコーディングルールへの準拠チェックを自動化 - Qiita](http://qiita.com/yaotti/items/4f69a145a22f9c8f8333)
- [RuboCop 関連記事まとめ #rubocop - Qiita](http://qiita.com/tbpgr/items/edbfecb6a6789dd54f47)
- [Rubocop がチェックしてくれるもの一覧表 (Style編) - Qiita](http://qiita.com/Yinaura/items/9928e32fa9fc9092f24c)
- [RuboCopの警告をコメントで無効化する方法 - Qiita](http://qiita.com/tbpgr/items/a9000c5c6fa92a46c206)
