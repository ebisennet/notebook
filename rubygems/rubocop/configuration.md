# 設定

## 設定ファイル

- [RuboCopの設定アレコレ - Qiita](http://qiita.com/necojackarc/items/8bc16092bbc69f17a16d)

## 設定ファイル自動生成

- [rubocop のしつけ方 - onk.ninja](http://blog.onk.ninja/2015/10/27/rubocop-getting-started)
    - `--auto-gen-config` オプションで設定ファイル生成する方法。
