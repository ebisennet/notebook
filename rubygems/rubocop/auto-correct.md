# コード自動修正

`rubocop` コマンド実行時にオプション指定でコードの自動修正を実行できる。

```bash
rubocop -a
```

## 参考資料

- [Rubocopを使ってコーディングルールへの準拠チェックを自動化 - Qiita](http://qiita.com/yaotti/items/4f69a145a22f9c8f8333)
    - コメント欄参照。
