# 基本的な使い方

## Gemfile の作成

``` ruby
source 'https://rubygems.org'
gem 'foo'
```

## インストール

``` bash
bundle install --path vendor/bundle
```

- `.gitignore` で vendor ディレクトリを無視するようにする。
- `Gemfile.lock`ファイルと `.bundle` ディレクトリを git リポジトリに追加する。

## コード

``` ruby
require 'foo'
```

## 実行

``` bash
bundle exec ruby a.rb
```

