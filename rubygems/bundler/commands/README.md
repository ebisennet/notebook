# Commands

## 公式資料

- [bundle(1) - Ruby Dependency Management](http://bundler.io/v1.12/man/bundle.1.html)

## 一覧

| コマンド                | 説明                                                          |
|-------------------------+---------------------------------------------------------------|
| [check](check.md)       |                                                               |
| [clean](clean.md)       |                                                               |
| [config](config.md)     |                                                               |
| [exec](exec.md)         | bundler でインストールした gem が提供するコマンドを実行する。 |
| [gem](gem.md)           |                                                               |
| [help](help.md)         |                                                               |
| [init](init.md)         |                                                               |
| [install](install.md)   | Gemfile に基づき gem をインストールする。                     |
| [list](list.md)         |                                                               |
| [lock](lock.md)         |                                                               |
| [open](open.md)         |                                                               |
| [outdated](outdated.md) |                                                               |
| [package](package.md)   |                                                               |
| [platform](platform.md) |                                                               |
| [show](show.md)         |                                                               |
| [update](update.md)     |                                                               |
| [viz](viz.md)           |                                                               |
