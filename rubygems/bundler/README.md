# bundler

## 公式資料

- [Bundler: The best way to manage a Ruby application's gems](http://bundler.io/)

## Topics

- [基本的な使い方](basic-usage.md)
- [Commands](commands/)
- [Gemfile](gemfile.md)
- Proxy
    - 環境変数 http_proxy を参照する。

## 参考

- [Bundlerの使い方 - Qiita](http://qiita.com/oshou/items/6283c2315dc7dd244aef)
