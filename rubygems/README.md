# rubygems

## 公式資料

- [RubyGems.org | your community gem host](https://rubygems.org/)
- [GitHub - rubygems/rubygems: Library packaging and distribution for Ruby.](https://github.com/rubygems/rubygems)

## Gems

| Gem名                                           | 説明                                                    |
|-------------------------------------------------+---------------------------------------------------------|
| [bundler](bundler/)                             |                                                         |
| [configparser](configparser/)                   | Python の ConfigParser と互換のある設定ファイル読み込み |
| [docker-api](docker-api/)                       | Docker API                                              |
| [java-properties](java-properties/)             |                                                         |
| [net-ssh](net-ssh/)                             |                                                         |
| [parallel_tests](parallel_tests/)               | RSpec 並列実行                                          |
| [pry](pry/)                                     |                                                         |
| [rake](rake/)                                   |                                                         |
| [rspec-core](rspec-core/)                       |                                                         |
| [rspec-expectations](rspec-expectations/)       |                                                         |
| [rspec](rspec/)                                 |                                                         |
| [rubocop](rubocop/)                             |                                                         |
| [specinfra](specinfra/)                         |                                                         |
| [rspec_junit_formatter](rspec_junit_formatter/) |                                                         |
