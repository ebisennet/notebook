# embulk

## 公式資料

- [Embulk — Embulk 0.8 documentation](http://www.embulk.org/docs/)
- [GitHub - embulk/embulk: Embulk: Pluggable Bulk Data Loader. http://www.embulk.org](https://github.com/embulk/embulk)

## 参考資料

- [Embulk(エンバルク)プラグインのまとめ - Qiita](http://qiita.com/hiroysato/items/da45e52fb79c39547f69)
