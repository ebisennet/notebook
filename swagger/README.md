# Swagger

OpenAPI Specification にリネームされている。

- [Swagger が OpenAPI にリネームされて Open API Initiative が誕生してた](https://r2.ag/swagger-to-openapi/)

最新 3.0

- [Community \| Capital One DevExchange](https://developer.capitalone.com/blog-post/welcoming-openapi-spec-v30/)

## 公式資料

- [Swagger – The World's Most Popular Framework for APIs.](http://swagger.io/)
- [Tools – Swagger](http://swagger.io/tools/)
- [Getting Started with Swagger – Swagger](http://swagger.io/getting-started/)
- [Swagger · GitHub](https://github.com/swagger-api) (github organization)
- [Open Source Integrations – Swagger](http://swagger.io/open-source-integrations/)
    - 3rd-party へのリンク。

Open API

- [Home \| Open API Initiative](https://openapis.org/)

## Topics

- Core Tools
    - Swagger Core
    - Swagger Codegen
    - Swagger UI
    - Swagger Editor
- Adjacent Tools
    - Swagger JS
    - Swagger Node
        - [GitHub - swagger-api/swagger-node: Swagger module for node.js](https://github.com/swagger-api/swagger-node)
    - Swagger-Socket
    - Swagger Parser
    - Swagger Inflector
        - [GitHub - swagger-api/swagger-inflector](https://github.com/swagger-api/swagger-inflector)
        - [Writing APIs with the Swagger Inflector – Swagger](http://swagger.io/writing-apis-with-the-swagger-inflector/)
- 3rd-party ツール
    - Swagger-JAXRS
        - [JAX-RS の REST API ドキュメントを Swagger を使って生成する - なにか作る](http://create-something.hatenadiary.jp/entry/2015/05/07/063000)
        - [Spring Rest API with Swagger - Integration and configuration](http://jakubstas.com/spring-jersey-swagger-configuration/#.V7WbqpMS-_A)
        - [Spring Rest API with Swagger – Creating documentation](http://jakubstas.com/spring-jersey-swagger-create-documentation/#.V7Wb35MS-_A)
        - [Integrating Swagger into JAX-RS with Java EE 6 specification - Jen-Ming Chung](http://jmchung.github.io/blog/2013/12/14/integrating-swagger-into-jax-rs-with-java-ee-6-specification/)
    - swagger-maven-plugin
        - [Swagger-maven-plugin](http://kongchen.github.io/swagger-maven-plugin/)
        - [GitHub - kongchen/swagger-maven-plugin: JAX-RS & SpringMVC supported maven build plugin, helps you generate Swagger JSON and API document in build phase.](https://github.com/kongchen/swagger-maven-plugin)
    - springfox
        - [SpringFox by springfox](https://springfox.github.io/springfox/)

## JSON Schema との組み合わせ

- [IBM Knowledge Center - Swagger](http://www.ibm.com/support/knowledgecenter/ja/SSMKHH_10.0.0/com.ibm.etools.mft.doc/bi12018_.htm)
    - 「Swagger 文書には、REST API 内の操作に送信される要求本文の構造を記述する JSON スキーマを組み込むことができ、.json スキーマによって、操作から戻されるあらゆる応答本文の構造を記述します。」
- [jsonschema - How to generate JSON-Schema from Swagger API Declaration - Stack Overflow](http://stackoverflow.com/questions/24118243/how-to-generate-json-schema-from-swagger-api-declaration)
    - JSON Schema Draft 4 のサブセットのみをサポート。
    - Swagger (1.2 and 2.0) does not support usage of many JSON structures, which are valid in terms of JSON Schema Draft 4 (the same applies to Draft 3).
    - Swagger does not support general XML data structures, only very restricted structures are allowed.

## 参考資料

- [Swaggerで始めるモデルファーストなAPI開発](http://www.slideshare.net/takurosasaki/swaggerapi)
    - Swagger Editor で編集
    - Swagger UI で API 呼び出し可能なドキュメント
    - Amazon API Gateway Importer への言及もある
- [Swaggerを使ってインタラクティブなWeb APIドキュメントをつくる - Qiita](http://qiita.com/weed/items/539f6bbade6b75980468)
- [swaggerの基礎。swaggerの設定ファイルの書き方とか - Qiita](http://qiita.com/magaya0403/items/0419d84d8df7784ac465)
- [Tony Tam氏に聞く- Open API InitiativeとSwaggerの最新情報](https://www.infoq.com/jp/news/2016/02/tony-tam-oai-initiative-swagger)
