# SELinux

## 無効化

``` bash
sudo setenforce 0
```

再起動後も有効にする場合、 /etc/selinux/config に設定。

## 参考資料

- [CentOS7.1 64bit SELinuxによるアクセス制御 | kakiro-web カキローウェブ](http://www.kakiro-web.com/linux/selinux.html)
