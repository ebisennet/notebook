# Configuration

ansible.cfg について。

## 公式資料

- [Configuration file — Ansible Documentation](http://docs.ansible.com/ansible/intro_configuration.html)
    - ansible.cfg で設定できる項目のドキュメント。
- [ansible/constants.py at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/lib/ansible/constants.py)
    - Configuration関連の定数定義のソースコード

## retry files 設定

- [How do you stop Ansible from creating .retry files in the home directory? - Stack Overflow](http://stackoverflow.com/questions/31318881/how-do-you-stop-ansible-from-creating-retry-files-in-the-home-directory)

## セクション別

- [defaults セクション](defaults.md)
- [privilege_escalation セクション](privilege_escalation.md)
- [paramiko_connection セクション](paramiko_connection.md)
- [ssh_connection セクション](ssh_connection.md)
- [accelerate セクション](accelerate.md)
