# defaults セクション

## action_plugins

## ansible_managed

## ask_pass

## ask_sudo_pass

## ask_vault_pass

## bin_ansible_callbacks

## callback_plugins

## callback_whitelist

## command_warnings

## connection_plugins

## deprecation_warnings

## display_args_to_stdout

## display_skipped_hosts

## error_on_undefined_vars

## executable

## filter_plugins

## force_color

## force_handlers

## forks

同時実行数。

デフォルトは5だが、大きな値を指定するのが一般的らしい。

## gathering

## hash_behaviour

## host_key_checking

ホストキーチェックを行うかどうか。

## hostfile

## inventory

Inventory file or directory を指定する。複数指定はできない。

パスの指定に環境変数を指定できる。

```
inventory = inventory/$ANSIBLE_ENV
```

環境変数で対象環境を指定する場合は、Playbookの対象ホストに all
を使わないこと。

-   環境変数指定忘れの際に localhost がマッチしてしまう。

環境変数 `ANSIBLE_INVENTORY` で設定を上書き可能。

## jinja2_extensions

## library

## local_tmp

## log_path

## lookup_plugins

## module_lang

## module_name

## module_set_locale (2.1)

## nocolor

## nocows

## pattern

## poll_interval

## private_key_file

## remote_port

## remote_tmp

## remote_user

## retry_files_enabled

## retry_files_save_path

## roles_path

追加で role を探すパスを複数指定可能。

## squash_actions

## stdout_callback

## strategy_plugins

## sudo_exe

## sudo_flags

## sudo_user

## system_warnings

## timeout

## transport

## vars_plugins

## vault_password_file

Vault のパスワードを記録したファイルを指定できる。

実行ファイルを指定した場合、その出力をパスワードとして使う。
