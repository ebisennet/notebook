# Ansible

## 公式資料
- [Ansible is Simple IT Automation](http://www.ansible.com/)
    - 公式サイト
- [Ansible Documentation](http://docs.ansible.com/)
    - ドキュメント
- [ansible/ansible · GitHub](https://github.com/ansible/ansible)
    - ansible 本体ソースコード

## Topics

- [Installing](installing.md)
- [Commands](commands/)
- [Concepts](concepts/)
- [Modules](modules/)
- [Variables](variables/)
- [Configuration](configuration/)
- [Jinja2](jinja2.md)
- [Block](block.md)
- [Loops](loops/)
- Conditionals
    - Loop と組み合わせると when は loop 内となる。
- Directives
    - [Directives Glossary — Ansible Documentation](https://docs.ansible.com/ansible/playbooks_directives.html)
- [Plugins](plugins/)
- [Async](async.md)
- [Tools](tools/)
- [Tips](tips.md)
- [Usecase](usecase.md)
- コーディング規約
    - [Ansible コーディング規約 (の例) — そこはかとなく書くよん。](http://tdoc.info/blog/2014/10/09/ansible_coding.html)
- コードカバレッジ
    - [Ansible向けのコードカバレッジツールKirbyを作りました - ks888の日記](http://ks888.hatenablog.com/entry/2015/09/06/223538)
- [Best Practices](best-practices.md)
- 環境切り替え
    - [Ansibleのインベントリファイルでステージを切り替える - Qiita](http://qiita.com/NewGyu/items/5de31d76d2488ab27ed6#comment-477545063579aa8bbf44)
    - [Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス - 双六工場日誌](http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF)
        - インベントリファイルの切り替え
        - 環境別のグループ
- Developer
    - [モジュール開発](developing-modules/)
    - [プラグイン開発](developing-plugins/)
- Proxy
    - [Setting the Environment (and Working With Proxies) — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_environment.html)
- デバッグ
    - [ansibleで特定のtaskを特定のhostに実行する - Qiita](http://qiita.com/346@github/items/00122556cb2bd6f57998)
        - -l (--limit) で実行対象を絞る。
        - --start-at で開始タスクを指定する。
        - --step でステップ実行。
- YAML
    - [ansible使いのためのYAML入門 - @znz blog](http://blog.n-z.jp/blog/2014-06-21-ansible-yaml.html)
- [Versions](versions.md)
- [Mac](mac.md)
- Windows
    - [Ansible for Windows](https://www.ansible.com/windows)
    - [Windows Support — Ansible Documentation](http://docs.ansible.com/ansible/intro_windows.html)
    - [AnsibleでWindowsを操作する準備をする - Qiita](http://qiita.com/yunano/items/f9d5652a296931a09a70)
    - [AnsibleでChocolateyを使ってWindowsアプリをインストールする - Qiita](http://qiita.com/yunano/items/5677685b61816f6e62a3)

## 参考資料

- [Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス - 双六工場日誌](http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF)
- [chefからansibleに乗り換えた5つの理由 | TechRacho](http://techracho.bpsinc.jp/yamasita-taisuke/2014_05_29/17567)
- [エージェントレスでシンプルな構成管理ツール「Ansible」入門 - さくらのナレッジ](http://knowledge.sakura.ad.jp/tech/3124/)
    - 入門記事としてちょうどいい
- [Ansible チュートリアル | Ansible Tutorial in Japanese](http://yteraoka.github.io/ansible-tutorial/)
    - 入門記事としてちょうどいい
    - ベストプラクティスについては記事内にリンクのある以下を参照
        - [Ansible オレオレベストプラクティス - Qiita](http://qiita.com/yteraoka/items/5ed2bddefff32e1b9faf)
- [今年1年間でAnsible界隈ではどのような変化が起こったのか。 - Qiita](http://qiita.com/volanja/items/2ffa1599fc9a1fae39a1)
- [Ansible Advent Calendar 2013 - Qiita](http://qiita.com/advent-calendar/2013/ansible)
- [ansible個人的メモ＆Tips - Qiita](http://qiita.com/yushin/items/d65929a3c972bcf50887)
- [Ansible Advent Calendar 2016 \- Qiita](http://qiita.com/advent-calendar/2016/ansible)
