# Mac

## Mac からの踏み台経由の実行

```
export ANSIBLE_SSH_ARGS=' -F ssh_config '
export ANSIBLE_SCP_IF_SSH=y
```

- `ANSIBLE_SCP_IF_SSH` は ansible.cfg で指定可能。

## 参考

- [AnsibleでSSH接続 - ソフトウェアエンジニアリング - Torutk](http://www.torutk.com/projects/swe/wiki/Ansible%E3%81%A7SSH%E6%8E%A5%E7%B6%9A)
