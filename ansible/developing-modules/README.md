# モジュール開発

## 公式資料
- [Developing Modules — Ansible Documentation](http://docs.ansible.com/ansible/developing_modules.html)

## Topics

-

## モジュールデバッグ

### 転送時のコードを確認

ansible 実行時に転送コードを残すように指定する。

```bash
ANSIBLE_KEEP_REMOTE_FILES=1 ansible-playbook -vvv
```

## Memo

- 単一ファイルであれば、python 以外でもモジュール開発は可能。
- python
  で開発したモジュールは、使用しているライブラリなどが結合してリモートに転送される。
- モジュールのパラメータとして message は使用できない？

## 参考資料

- [Ansibleモジュールのサンプルコードを実行してみた - Qiita](http://qiita.com/tanuki-project/items/fc99e19a3f1fd03cc24a)
- [Ansible の module の作成 | cloudpack技術情報サイト](http://blog.cloudpack.jp/2014/12/30/create-ansible-module/)
- [bashスクリプトではじめての自作Ansibleモジュール - Qiita](http://qiita.com/yunano/items/d58199eada0109d1e2fb)
  - 入出力を守れば bash スクリプトでも作成可能。
- [Ansible モジュール 作成・配布・貢献 - SSSSLIDE](http://sssslide.com/speakerdeck.com/yaegashi/ansible-moziyuru-zuo-cheng-pei-bu-gong-xian)

