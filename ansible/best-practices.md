# Best Practices

## 公式資料

- [Best Practices — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_best_practices.html)
    - 公式の Best Practices ドキュメント。

## Playbook の分割

- master として site.yml を作成
    - include で分割した yml を読み込む

## ディレクトリ構成
Playbook 用ディレクトリ

- group_vars
- host_vars
- library
- filter_plugins
- roles
