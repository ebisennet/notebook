# ansible

指定したインベントリで指定したモジュールや任意のコマンドを実行できる。

Playbook を用意しなくても実行できるタスクなので Ad-hoc Tasks, Ad-hoc Commands と呼ばれている。

## 公式資料
- [Introduction To Ad-Hoc Commands — Ansible Documentation](http://docs.ansible.com/ansible/intro_adhoc.html)

## 使用例

```bash
ansible all -m file -a "path=/path/to/file mode=600" -k -K --become
```

- -m `MODULE_NAME`
    - 使用モジュールの指定
- -a `MODULE_ARGS`
    - モジュールへの引数の指定

## 対象ホストの確認
```bash
ansible all --list-hosts
```
