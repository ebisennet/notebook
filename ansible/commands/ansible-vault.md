# ansible-vault

機密情報を含んだ yaml ファイルの暗号化／復号化を行う。

暗号化された yaml ファイルを使う時はパスワードが必要。

## 公式資料

- [Vault — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_vault.html)

## サブコマンド

### encrypt

指定したファイルを暗号化する。

```sh
ansible-vault encrypt path/to/secure.yml
```

### decrypt

指定した暗号化済みファイルを復号化する。

```sh
ansible-vault encrypt path/to/secure.yml
```

### edit

指定した暗号化ファイルを編集する。

```sh
ansible-vault edit path/to/secure.yml
```

対象ファイルを復号化した内容でエディターが起動する。
エディターで編集完了後に再び暗号化される。

### view

暗号化されたファイルの内容を出力する。

```sh
ansible-vault view path/to/secure.yml
```

### create

指定したファイル名で暗号化されたファイルを作成する。

```sh
ansible-vault create path/to/secure.yml
```

指定したパスでファイルを新規作成するためにエディターが起動する。
エディターで編集完了後に作成したファイルは暗号化される。

### rekey

指定した暗号化済みファイルのパスワードを変更する。

```sh
ansible-vault rekey path/to/secure.yml
```

現在のパスワードと変更後のパスワードの入力が必要。

Ansible 2.2.0 までバグがあるので注意。

## 環境変数

### ANSIBLE_VAULT_PASSWORD_FILE

パスワードが記録されたファイルのパスを指定する。

対象ファイルが実行可能なファイルの場合、そのファイルを実行して標準出力に出力された内容をパスワードとして扱う。

同等の内容を ansible.cfg でも指定可能。

## Troubleshooting

### rekey をするとファイルが壊れる

以下を参照。

- [ansible\-vault rekey losing file with env ANSIBLE\_VAULT\_PASSWORD\_FILE set · Issue \#18247 · ansible/ansible · GitHub](https://github.com/ansible/ansible/issues/18247)

Ansible 2.2.1 で解決している。

## 参考

- [Ansibleでパスワードを暗号化して管理する - Qiita](http://qiita.com/daeji/items/fe3859f4e469f0e97d3f)
