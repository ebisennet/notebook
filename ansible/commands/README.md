# Commands

| コマンド                                | 説明                                             |
|-----------------------------------------+--------------------------------------------------|
| [ansible](ansible.md)                   | Module を単体で実行する                          |
| [ansible-console](ansible-console.md)   | アドホックコマンドをインタラクティブに実行できる |
| [ansible-doc](ansible-doc.md)           | Module のドキュメントを閲覧する                  |
| [ansible-galaxy](ansible-galaxy.md)     | ansible galaxy の Role を使う                    |
| [ansible-playbook](ansible-playbook.md) | Playbook を実行する                              |
| [ansible-pull](ansible-pull.md)         |                                                  |
| [ansible-vault](ansible-vault.md)       | セキュアデータを暗号化／復号化する               |
