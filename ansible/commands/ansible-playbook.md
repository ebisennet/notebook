# ansible-playbook

Playbook を実行する。

## 公式資料

- [Intro to Playbooks — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_intro.html)
- [Check Mode (“Dry Run”) — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_checkmode.html)

## 指定した playbook を実行

```bash
ansible-playbook playbook.yml
```

## sudo のパスワードを指定して playbook 実行

```bash
ansible-playbook playbook.yml -K
```

最初にパスワードが聞かれる。

Playbook 内のタスクに `become_method: sudo` がある場合に使用する。 sudo 時にパスワードが不要の環境では不要。

参考。

- [Ansible, sudoパスワード要求を忘れただけでめんどくなる - Goldstine研究所](http://mosuke5.hateblo.jp/entry/2014/11/28/001748)

## Inventory の指定

```bash
ansible-playbook -i dev playbook.yml
```

-i で Inventory file を指定出来る。

## ローカル実行

```bash
ansible-playbook -i localhost, -c local playbook.yml
```

- -c local で ssh を使わない指定
- -i localhost, で localhost を指定
    - カンマをつけることで inventory file の作成を省略して直接 host 指定できる。

参考。

- [Ansibleチートシート（実行周り） - Qiita](http://qiita.com/unarist/items/39f5510f95c752c10df1)

## 実行するタスクを確認

```bash
ansible-playbook playbook.yml --list-tasks
```

実行はされない。

## 適用されるホストを確認

```bash
ansible-playbook playbook.yml --list-hosts
```

実行はされない。

## Dry Run

-C もしくは --check で指定。

## 変更時の差分確認

-D もしくは --diff で指定。

-C との組み合わせも可能。

差分が表示されないモジュールが多い点は注意。

## Syntax Check

--syntax-check で指定。

Palybook や Role の YAML にエラーがないかチェックする。

Jinja2 のテンプレート記述のエラーはチェックできないので注意。

## 適用されるホスト・グループを限定

-l で指定できる。

## Vault 付きの実行

### パスワードファイルを用いた実行指定

```bash
ansible-playbook playbook.yml --vault-password-file ~/.vault_pass
```

パスワードファイルが実行可能ファイルの場合、実行した出力がパスワードとして使われる。

パスワードファイルのパス指定は環境変数 `ANSIBLE_VAULT_PASSWORD_FILE` でも指定可能。

### 参考

- [Ansible-Vaultを用いた機密情報の暗号化のTips - Qiita](http://qiita.com/takuya599/items/2420fb286318c4279a02)
    - パスワードファイルの指定
