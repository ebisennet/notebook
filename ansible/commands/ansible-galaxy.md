# ansible-galaxy

Ansible Roles を中央リポジトリとなる Ansible Galaxy からインストールすることができる。

ロールは多数のタスクの実行をひとまとめにして再利用可能にしたもの。
ロールの中に自作モジュールを含めることもできる。

Ansible 1.8 からは、Ansible Galaxy 以外に GitHub など、その他のソースからインストール可能。

## 公式資料

- [Ansible Galaxy | Find, reuse, and share the best Ansible content](https://galaxy.ansible.com/)
  - Ansible Galaxy のサイト。
  - 公開されているロールを閲覧することができる。
- [Welcome to Ansible Galaxy](https://galaxy.ansible.com/intro)
  - ロール作成方法や変数名についての記述あり。
- [GitHub \- ansible/galaxy: Source code behind Galaxy \- http://galaxy\.ansible\.com](https://github.com/ansible/galaxy)

## サブコマンド

### init

新規ロール作成時に必要なディレクトリ構成を作成する。

`ansible-galaxy init sample` とした場合、以下のように構成される。

```
sample/
|-- README.md
|-- defaults
|   `-- main.yml
|-- files
|-- handlers
|   `-- main.yml
|-- meta
|   `-- main.yml
|-- tasks
|   `-- main.yml
|-- templates
`-- vars
    `-- main.yml
```

必要ならロールディレクトリ内に以下も作成する。

- library
  - 埋め込みモジュール用ディレクトリ

roles ディレクトリを指定するには `-p` を使用する。

### info

指定したインストール済みロールの情報を表示。

### install

指定したロールをダウンロードして Roles ディレクトリにインストールする。

複数のロールをダウンロードする際には以下のように install コマンドではなく -r オプションで
ダウンロードするロールを記述したテキストファイル、もしくはyamlファイルを指定する。

```
ansible-galaxy -r roles.txt
ansible-galaxy -r install_roles.yml
```

yaml指定のほうが、より細かい指定ができる。

- ダウンロード元として git リポジトリ指定
- git のタグ指定
- ダウンロード後のロール名指定
- ダウンロード先の roles ディレクトリ指定

### list

インストール済みのロール一覧を表示する。

### remove

## 参考

- [roleを作成してAnsible Galaxyに登録するワークフロー - Qiita](http://qiita.com/hnakamur/items/4c5abbbbbb5623ce46ad)
  - ansible-galaxy init でディレクトリ作成してから galaxy にロールを登録するまで。
- [Ansible Galaxyがオープンソース化。ローカルでAnsible Galaxyを実行してRolesを共有可能に － Publickey](http://www.publickey1.jp/blog/16/ansible_galaxyansible_galaxyroles.html)
