# ansible-console

Ansible 2.1 から使える。

## 参考資料

- [ansible-consoleを使おう - Qiita](http://qiita.com/yunano/items/faba4f3330594e41246b)
- [GitHub - dominis/ansible-shell: Interactive ansible shell](https://github.com/dominis/ansible-shell)
