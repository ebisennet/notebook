# Handler

- 特定のタスク実行時に、後で追加で実行したい処理を定義できる。
    - 設定ファイル変更後のサービス再起動など
- タスクから notify して実行する Handler を指定する。
- Handler はすべてのタスクが実行された後に呼び出される。

## 公式資料

- [Intro to Playbooks — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_intro.html#handlers-running-operations-on-change)

## Handler で複数タスク実行

Ansible 2.0 までなら include を組み合わせて対応できる。

```yaml
- name: restart service
  include: ../handlers/restart-service.yml
```

参考： [Add support for blocks in role handlers · Issue #14270 · ansible/ansible · GitHub](https://github.com/ansible/ansible/issues/14270#issuecomment-178805016)

上記は Ansible 2.1 だとエラーになる。

## 参考

- [Ansibleのハンドラが起動するのはタスクがchangedを返したときだけ - imagawa’s blog](http://imagawa.hatenadiary.jp/entry/2015/01/13/031317)

