# Dynamic Inventory

- 動的な Inventory.
- Inventory File が実行可能な場合に実行した結果から Inventory を決定する。
- ターゲットが Amazon EC2 などの場合

## 定義方法

以下のような構造の JSON を出力するスクリプトを作成する。

```
{
  "_meta": {
    "hostvars": {
      "host1": {
        "abc": 123
      }
    }
  },
  "group1": {
    "hosts": [
      "host1",
      "host2",
      "host3"
    ],
    "vars": {
      "foo": true
    },
    "children": [
      "group2",
      "group3"
    ]
  }
}
```

スクリプトに実行権限を付与する。
その上で、インベントリーファイルとして指定して実行する。

- _meta に全てのホストのホスト変数を定義。
    - ホスト名が間違えていてもエラーにはならない。

## 公式資料

- [Dynamic Inventory — Ansible Documentation](http://docs.ansible.com/ansible/intro_dynamic_inventory.html)
- [Developing Dynamic Inventory Sources — Ansible Documentation](http://docs.ansible.com/ansible/dev_guide/developing_inventory.html)
## 参考資料

- [Ansible - Dynamic Inventoryを使ってみる - Qiita](http://qiita.com/yamasaki-masahide/items/e05132f29b50245b6f26)
