# Playbook

- どうシステムを変更するかの設定をまとめたもの。 chef で言うところの recipe や cookbook になる。
- 基本的にはタスクやハンドラを記述したyamlファイルのことを指す。
    - 対象インベントリ、使用するロール、変数なども記述する

## 公式資料

- [Playbooks — Ansible Documentation](http://docs.ansible.com/ansible/playbooks.html)
- [Best Practices — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_best_practices.html)

## Playbook の include

```yaml
- hosts: localhost
  tasks:

- include: playbook1.yaml
- include: playbook2.yaml
```
