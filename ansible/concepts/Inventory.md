# Inventory

Ansible によってシステム構成対象となるサーバの一覧のこと。

## 公式資料

- [Inventory — Ansible Documentation](http://docs.ansible.com/ansible/intro_inventory.html)

## インベントリファイル

- インベントリについて記述したファイル。
- インベントリファイルのパスはコマンドのオプションやansible.cfgファイルで指定する。
    - ディレクトリ指定で複数のインベントリファイルが使える。
- デフォルトインベントリファイル
    - /etc/ansible/hosts

## インベントリファイルの yaml 記述

Undocumented な機能。

```yaml
group1:
  #
  # グループに属するホスト定義。
  #
  hosts:
    #
	# ホストとホスト変数記述。
	#
    host1:
      var1: group1/test1
  #
  # グループ変数定義。
  #
  vars:
    var1: group1

group2:
  #
  # 子グループ定義。
  # 子グループへの変数定義は不可。
  #
  children:
    group1:
```

## 参考資料

- [Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス - 双六工場日誌](http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF)
    - Inventory のグループを環境別定義に使っている。
- [Ansible inventoryパターン ｜ Developers.IO](http://dev.classmethod.jp/server-side/ansible/ansible-inventory-pattern/)
