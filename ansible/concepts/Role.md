# Role

再利用可能なタスクの集合となるもの。 Playbook の一部を分割してインクルードして利用する形。

## 公式資料

- [Playbook Roles and Include Statements — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_roles.html)

## Roles ディレクトリ

- 使用する Role が格納されるディレクトリ
- デフォルトパス
  - ./roles
  - /etc/ansible/roles
- ディレクトリ構成
  - defaults
  - files
  - handlers
  - meta
  - tasks
  - templates
  - vars

## モジュールの埋め込み

Role ディレクトリ内に library ディレクトリがあると モジュールが読み込まれる。

## Role の使用

``` yaml
roles:
  - name: role1
    param1: foo
    param2: var
```

## 参考資料

- [Ansibleのroleを使いこなす - Qiita](http://qiita.com/hnakamur/items/63f2d94badf89246e04a)
    - Role の役割が理解しやすい
    - Role の依存関係などについても記述あり

