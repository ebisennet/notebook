# Task

対象ホストに対して実行する処理のこと。 どんな処理を行うかはタスクで使用するモジュールによる。

## タスクの名前

## sudo

``` yaml
- service: name=nginx state=started
  become: yes
  become_method: sudo
```

## タスクの include

``` yaml
tasks:
  - include: tasks/main.yaml param1=foo
```

tasks/main.yaml:

``` yaml
- name: task1
  debug: msg=task1
- name: task2
  debug: msg=task2
```

## タスクのプロパティ

- always_run
- async
- become
- become_method
- changed_when
- delay
- environment
- failed_when
- ignore_errors
- include
- include_vars
- name
- notify
- poll
- register
- retries
- tags
- until
- when

