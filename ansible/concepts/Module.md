# Module

システムへの構成変更を行うもの。 chef で言うところの resource 相当。

- Core Modules と Extra Modules があり、どちらも ansible 付属
- [モジュール開発](../developing-modules/)
