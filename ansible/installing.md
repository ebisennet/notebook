# インストール

## Requirements

- Control Machine
    - Python 2.6 or 2.7
- Managed Node
    - Python 2.4 or later

## RHEL, CentOS, Fedora

### EPEL からのインストール

```
yum install epel-release
yum install ansible
```

- 2016-06月時点では 2.0 が入っている。

### pip からインストール

```
sudo yum install gcc python-dev
```

```
sudo pip install ansible
```

## Mac

### brew

```
brew install ansible
```

- 各種デフォルトパスは /usr/local になる。
    - ただし fact.d は /etc/ansible のまま。

### pip

python を brew で入れて pip からインストール。

```
brew install python
pip install ansible
```

- 各種デフォルトパスは /etc/ansible ベース。
- pip でインストールしたライブラリが必要な時に楽。

## Ansible でインストール

``` yaml
- name: Install EPLE
  yum:
    name: epel-release
- name: Install Ansible
  yum:
    name: ansible
    enablerepo: epel
```
