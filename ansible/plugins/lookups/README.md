# Lookups

## 公式資料

- [Using Lookups — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_lookups.html)
- [ansible/lib/ansible/plugins/lookup at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/tree/devel/lib/ansible/plugins/lookup)
  - 組み込み済みの Lookup plugins のソースコード
- [Loops — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_loops.html)
  - ループ処理を担うプラグインはこちらで説明されている。

## Topics

- [Ansibleのlookup pluginについて調べてみた - Qiita](http://qiita.com/yunano/items/4325935b8567572cc172)
  - with_* は lookup plugin が処理する。
    - with_items も lookup plugin の１つ。
- 環境変数参照が目的の場合、ansible.cfg 内から参照すれば十分なケースもある。

## Lookup plugin 別

|                          |                                                                      |
|--------------------------+----------------------------------------------------------------------|
| cartesian                |                                                                      |
| consul_kv                |                                                                      |
| credstash                |                                                                      |
| csvfile                  |                                                                      |
| dict                     |                                                                      |
| dig                      |                                                                      |
| dnstxt                   |                                                                      |
| env                      |                                                                      |
| etcd                     |                                                                      |
| file                     | 指定ファイルリストでループ。 item としてファイルのコンテンツが入る。 |
| [fileglob](fileglob.md) | ファイルの列挙。 item としてファイルのフルパスが入る。               |
| first_found              |                                                                      |
| flattened                |                                                                      |
| hashi_vault              |                                                                      |
| indexed_items            |                                                                      |
| ini                      |                                                                      |
| inventory_hostnames      |                                                                      |
| items                    |                                                                      |
| lines                    |                                                                      |
| list                     |                                                                      |
| nested                   |                                                                      |
| password                 |                                                                      |
| pipe                     |                                                                      |
| random_choice            |                                                                      |
| redis_kv                 |                                                                      |
| sequence                 |                                                                      |
| shelvefile               |                                                                      |
| subelements              |                                                                      |
| template                 |                                                                      |
| together                 |                                                                      |
| url                      |                                                                      |

## Memo

### lookup env と Jinja2 の default フィルターの組み合わせについて

`default` フィルターは undefined な値に対してのみデフォルト値を返すか、falsy な値（0, false, "", []）に対してもデフォルト値を返すか、最後のパラメーターで選択できる。

`lookup('env', ...)` の結果は常に文字列なので、`default` の最後のパラメーターで空文字列に対してもデフォルト値を返すようにする必要がある。
