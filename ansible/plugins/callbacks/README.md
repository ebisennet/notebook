# Callbacks

## 公式資料

- [ansible/lib/ansible/plugins/callback at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/tree/devel/lib/ansible/plugins/callback)

## Callback plugin 別

|                   |                                 |
|-------------------+---------------------------------|
| [junit](junit.md) | 実行結果を JUnit 形式で出力する |


## 参考資料

- [Ansibleのコールバックプラグインを使ってみる \| Siguniang's Blog](https://siguniang.wordpress.com/2014/09/05/play-with-ansible-callback-plugins/)
