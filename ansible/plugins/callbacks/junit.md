# junit

実行結果を JUnit 形式で出力するプラグイン。

Ansible 2.2 で追加。

## ansible.cfg

以下の設定が必要。

```
callback_whitelist = junit
```

プラグインの置き場所が違う時は以下も必要。

```
callback_plugins = ./plugins/callback
```

## 環境変数

環境変数 `JUNIT_OUTPUT_DIR` で出力先のディレクトリを指定できる。  
デフォルトは `~.ansible.log`

## Ansible 2.0 への適用

Ansible 2.0 で動かすにはプラグインの一部修正が必要。

```
modified   plugins/callback/junit.py
@@ -21,7 +21,7 @@ __metaclass__ = type
 import os
 import time
 
-from ansible.module_utils._text import to_bytes
+# from ansible.module_utils._text import to_bytes
 from ansible.plugins.callback import CallbackBase
 
 try:
@@ -182,7 +182,7 @@ class CallbackModule(CallbackBase):
         output_file = os.path.join(self._output_dir, '%s-%s.xml' % (self._playbook_name, time.time()))
 
         with open(output_file, 'wb') as xml:
-            xml.write(to_bytes(report, errors='surrogate_or_strict'))
+            xml.write(report.encode('utf-8', 'strict'))
 
     def v2_playbook_on_start(self, playbook):
         self._playbook_path = playbook._file_name
```

上記に加えて `junit_xml` モジュールが必要。
