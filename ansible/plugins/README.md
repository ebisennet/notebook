# Plugins

## 公式資料

- [ansible/lib/ansible/plugins at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/tree/devel/lib/ansible/plugins)

## Topics

- [Callbacks](callbacks/)
- [Lookups](lookups/)
- Strategies
    - v2 から。
    - [Strategies — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_strategies.html)

