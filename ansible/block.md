# Block

## 公式資料

- [Blocks — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_blocks.html)
- ソースコード
    - [ansible/block.py at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/lib/ansible/playbook/block.py)
        - block の定義を格納するクラス
    - [ansible/play_iterator.py at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/lib/ansible/executor/play_iterator.py)
        - block の実装

## 参考資料
- [blockでwith_itemsが使いたかった - Qiita](http://qiita.com/tboffice/items/4cb0a17a38359682afe9)
    - Loop との組み合わせはまだ。
