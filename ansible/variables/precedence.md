# 優先順位

以下の順位で優先順位がある。（後のほうが優先度が高い）

- role defaults
- inventory vars
- inventory group_vars
- inventory host_vars
- playbook group_vars
- playbook host_vars
- host facts
- registered vars
- set_facts
- play vars
- play vars_prompt
- play vars_files
- role and include vars
- block vars (only for tasks in block)
- task vars (only for the task)
- extra vars (always win precedence)

優先順位であって、評価順位ではない。

- 例えば role defaults の定義で inventory vars を参照できる。

## 公式資料

- [Variables — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)
