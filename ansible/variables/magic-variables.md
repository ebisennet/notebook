# Magic Variables

## 公式資料

- [ansible/__init__.py at stable-2.0 · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/stable-2.0/lib/ansible/vars/__init__.py#L355)
    - `_get_magic_variables` メソッド

## 変数

### ansible_check_mode (2.1)

Ansible 2.1 から使える。 `--check` オプション付きで Ansible を実行しているかどうか。

### ansible_hostname

### ansible_env

リモートの環境変数が入る facts.

リモートではなく ansible 実行環境の変数を取得する場合は 'env' lookup plugin を参照。

### ansible_play_batch (2.2)

### ansible_play_hosts (2.2)

### delegate_to

### environment

予約されている変数。

### groups

グループとターゲットホストのマッピング。

グループ名をキー、ターゲットホストの配列を値とした Dictionary が入っている。 グループの階層構造はフラットな構造に展開される。

以下のグループは特別なグループとなる。

- all
    - 全てのターゲットホストが入る。
- ungrouped
    - グループに属していないターゲットホストが入る。

### group_names

現在実行中のターゲットホストが、インベントリ上、どのグループに属しているか。

属しているグループ全てが入った配列となる。

### hostvars

### inventory_dir

### inventory_file

### inventory_hostname

インベントリに書かれたホスト名。

### inventory_hostname_short

インベントリに書かれたホスト名の最初の . まで。

ドメインとして書かれた場合の末端のサブドメイン名。

IP アドレス指定の場合、意味がない値となるので問題。

### play_hosts

### role_path

ロールの中でのみ有効な変数。 カレントのロールのパスを返す。

### role_name

## 参考

- [Ansible マジック変数の一覧と内容 - Qiita](http://qiita.com/h2suzuki/items/15609e0de4a2402803e9)

