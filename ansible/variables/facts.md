# Facts

設定対象のシステムの情報を格納した変数。

playbook 実行時に自動的に実行される [setup](../modules/setup.md) モジュールによって定義される。

- playbook の記述で setup モジュール実行をやめることもできる。

以下のドキュメントも参照。

- [setup](../modules/setup.md)
