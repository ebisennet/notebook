# Inventory Variables

インベントリファイルでホストやグループごとに任意の変数を定義できる。

ansibleがリモートホストにつなぐ方法など、インベントリで指定する変数もある。

タスクから参照する場合、明示的に設定されていない変数は null となることに注意。

## 公式資料

- [Inventory — Ansible Documentation](http://docs.ansible.com/ansible/intro_inventory.html)

## インベントリ変数

### ansible_connection

### ansible_ssh_host

### ansible_ssh_user

ansible が ssh するときのユーザー名。

### ansible_ssh_port

### ansible_ssh_pass

### ansible_ssh_private_key_file

### ansible_ssh_common_args

