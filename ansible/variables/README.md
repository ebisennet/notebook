# Variables

## 公式資料

- [Variables — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_variables.html)

## Topics

- [Facts](facts.md)
- [Magic Variables](magic-variables.md)
- [Inventory Variables](inventory-variables.md)
- [優先順位](precedence.md)

## 未整理

### playbook_dir

実行中 playbook の置かれているローカルのディレクトリパス。

- 実行対象がリモートの場合もローカルのパス。
