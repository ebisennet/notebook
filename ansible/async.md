# Async

タスクの実行を非同期にできる。（Block は不可）

## 公式資料

- [Asynchronous Actions and Polling — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_async.html)
- [async\_status \- Obtain status of asynchronous task — Ansible Documentation](http://docs.ansible.com/ansible/async_status_module.html)
    - 非同期実行したタスクの状態を取得できる。

## 参考資料

- [Ansible でバックグラウンドプロセスを起動する \| cloudpack\.media](https://cloudpack.media/12880)
- [Ansibleのplaybookで使用できるアトリビュートの一覧 \- Qiita](http://qiita.com/yunano/items/8494e785390360011a88)

