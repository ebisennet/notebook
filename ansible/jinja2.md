# jinja2

文法などについては以下を参照。

- [pythonlib/jinja2](../pythonlib/jinja2)

## 公式資料

- [Jinja2 filters — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_filters.html)
    - Ansible が追加している Jinja2 フィルターについてのドキュメント
- [ansible/lib/ansible/plugins/filter at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/tree/devel/lib/ansible/plugins/filter)
    - Ansible 提供の Jinja2 フィルターのソースコード。
    - "Other Useful Filters" に bool へのキャスト、basename など、使えるフィルターあり。

## 追加のフィルター

### to_json

### to_yaml

### to_nice_json

### to_nice_yaml
