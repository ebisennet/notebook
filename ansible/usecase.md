# Usecase

## Mac への適用

- [AnsibleでHomebrew, Cask, Atomエディターのパッケージを管理する - Qiita](http://qiita.com/hnakamur/items/1c27cf0df19fe57ec624)
    - localhost のインベントリ定義
    - localhost 用の playbook 作成と実行
    - ansible-galaxy で必要な Role をダウンロードする
- [Mac の開発環境構築を自動化する (2015 年初旬編) - t-wadaのブログ](http://t-wada.hatenablog.jp/entry/mac-provisioning-by-ansible)
- [HomebrewとAnsibleでMacの開発環境構築を自動化する | mawatari.jp](http://mawatari.jp/archives/mac-provisioning-by-homebrew-and-ansible)

## ローカル実行
- [Ansibleチートシート（実行周り） - Qiita](http://qiita.com/unarist/items/39f5510f95c752c10df1#-%E3%83%AD%E3%83%BC%E3%82%AB%E3%83%AB%E3%81%A7%E3%81%AE%E5%AE%9F%E8%A1%8C)
    - ローカル実行についての解説あり

## Serverspec との組み合わせ

- [Ansible と Serverspec を組み合わせて使う : あかぎメモ](http://blog.akagi.jp/archives/4535.html)
- [Ansibleの設定ファイルを使ってServerspecを実行するテンプレート作成用Gem(ansible~spec~)を作りました。 - Qiita](http://qiita.com/volanja/items/5e97432d6b231dbb31c1)

