# プラグイン開発

## 公式資料

- [Developing Plugins — Ansible Documentation](http://docs.ansible.com/ansible/developing_plugins.html)

## Topics

- [Action Plugins](action-plugins/)
- [Lookup Plugins](lookup-plugins/)
- [Callback Plugins](callback-plugins/)
- Connection Plugins
- Filter Plugins
- Vars Plugins

