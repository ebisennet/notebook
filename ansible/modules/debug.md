# debug

メッセージや変数を表示する。

## 公式資料

- [debug - Print statements during execution — Ansible Documentation](https://docs.ansible.com/ansible/debug_module.html)

## 変数表示

``` yaml
- name: debug
  debug:
    var: homedir
```

## 全変数表示

``` yaml
- name: debug
  debug:
    var: hostvars[inventory_hostname]
```

## 参考

- [Coderwall | Dump all variables](https://coderwall.com/p/13lh6w/dump-all-variables)
    - jinja2 で to_nice_json フィルターを使って全変数をファイルとして出力している。
