# htpasswd

BASIC認証用のファイルを作成する。

- Digest認証に対応したモジュールはないので、必要ならshellモジュールで htdigest コマンドを実行する。

## 公式資料

- [htpasswd - manage user files for basic authentication — Ansible Documentation](http://docs.ansible.com/ansible/htpasswd_module.html)

## 使用例

``` yaml
- name: 認証ファイル生成
  htpasswd:
    path: /etc/httpd/.htpasswd
    name: user
    password: password
```

## RHEL, CentOS での使用

EPEL を有効化した上で、python-passlib パッケージを入れておく必要がある。

``` yaml
- name: python-passlibインストール
  yum:
    name: python-passlib
    enablerepo: epel
```
