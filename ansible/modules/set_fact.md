# set_fact

変数に指定した値をセットできる。

## 公式資料

- [set_fact - Set host facts from a task — Ansible Documentation](https://docs.ansible.com/ansible/set_fact_module.html)

## 使用例

```yaml
- set_fact:
    foo: 1
    bar: "test"
```

他の Ansible モジュールとは異なり、特定のパラメーター名を指定するのではなく設定したい変数ごとに値を記述する。
