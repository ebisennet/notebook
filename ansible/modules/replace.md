# replace

正規表現にマッチした行を書き換える。

書き換え後の内容が再度、正規表現にマッチする場合、次回実行時に再び置き換えが発生する。

## 公式資料

- [replace - Replace all instances of a particular string in a file using a back-referenced regular expression. — Ansible Documentation](https://docs.ansible.com/ansible/replace_module.html)

## 使用例

```yaml
- name: Replace
  replace:
    dest: path/to/file
    regexp: '^([^\.])\.example\.com$'
    replace: '\1.test.com'
```

## 関連

- [lineinfile](lineinfile.md)
- [blockinfile](blockinfile.md)

## 参考

- [Ansibleでファイルの行を書き換える3つの方法 - Qiita](http://qiita.com/cognitom/items/57de72b739642041dcd5)

