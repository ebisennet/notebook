# file

ファイル、ディレクトリ、リンクの作成、パーミッション設定ができる。

## 公式資料

- [file - Sets attributes of files — Ansible Documentation](http://docs.ansible.com/ansible/file_module.html)

## symlink 作成

- symlink ファイルのパスを dest で指定。
- symlink ファイルの中身（＝リンク先）を src で指定。

### ローカル実行時に ansible プロジェクト内ファイルへリンク生成する例

``` yaml
- name: symlink to dotfiles
  file:
    src: "{{ playbook_dir }}/dotfiles/{{ item }}"
    dest: "{{ ansible_env.HOME }}/{{ item }}"
    state: link
  with_items:
    - .bashrc
    - .bash_profile
    - .ssh/config
```

### 実行ファイルへのリンクを /usr/bin にインストール

``` yaml
- name: symlink to an executable file
  file:
    src: "{{ playbook_dir }}/dotfiles/{{ item }}"
    dest: "{{ ansible_env.HOME }}/{{ item }}"
    state: link
  with_items:
    - .bashrc
    - .bash_profile
    - .ssh/config
```

## ファイルのパーミッション設定

``` yaml
- file:
    path: path/to/file
    state: file
    owner: user
    group: group
    mode: 0644
```

## ディレクトリ作成

``` yaml
- file:
    path: path/to/directory
    state: directory
    owner: user
    group: group
    mode: 0644
```

指定したパスのディレクトリが作成される。

パスの途中のディレクトリも作成される。
OS コマンドで言えば `mkdir -P` 相当。

この場合、owner/group/mode は作成される途中のディレクトリにも適用される。

すでに途中のディレクトリが存在する場合、末端のディレクトリにしか適用されない点に注意。 （べき等性が満たされない）

必要なら途中のディレクトリのパーミッション設定も明示的に行う。

## ディレクトリのパーミッション設定

``` yaml
- file:
    path: path/to/file
    state: directory
    owner: user
    group: group
    mode: 0644
    recurse: True
```

`state: direcotry` の時は `recurse` パラメーターを指定できる。

`recurse: True` にした場合、指定したディレクトリ内のファイルやディレクトリも再帰的にパーミッション設定される。
OS コマンドで言えば `chmod` や `chown` の `-R` オプション相当。

## touch 操作

state: touch で touch ができる。

常に changed になるので、空ファイル作成用途の場合、[copy](copy.md) モジュールを使う。

## ファイル・ディレクトリ削除

``` yaml
- file:
    path: path/to/file
    state: absent
```

ディレクトリの場合、再帰的な削除となるので注意。

# 参考

- [Ansible ～fileモジュール～ - Qiita](http://qiita.com/moiwasaki/items/e2c843e255c81a65746c)
