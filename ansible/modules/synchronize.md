# synchronize

rsync を使ったファイル転送を行う。

## 公式資料

- [synchronize - Uses rsync to make synchronizing file paths in your playbooks quick and easy. — Ansible Documentation](http://docs.ansible.com/ansible/synchronize_module.html)

## 使用例

```yaml
- name: copy files
  synchronize:
    src: path/to/src/
    dest: path/to/dest/
    archive: True
    owner: False
    group: False
```

- `src`
    - 転送元ファイル・ディレクトリのパスを指定する。
    - 相対パスの場合、Role 内で使う時は Role の `files` ディレクトリがベースとなる。
- `dest`
    - 転送先ファイル・ディレクトリのパスを指定する。
- `archive`
    - rsyncのアーカイブオプション（`-a`）指定をTrue/Falseで指定する。
    - 未指定の場合、`True` となる。
- `owner`
    - rsyncのオーナー名維持機能の指定をTrue/Falseで指定する。
    - 未指定の場合、`archive` パラメーターと同じ値となる。
- `group`
    - rsyncのグループ名維持機能の指定をTrue/Falseで指定する。
    - 未指定の場合、`archive` パラメーターと同じ値となる。

`archive`, `owner`, `group` の全てを省略すると転送元の owner, group が維持される点に注意する。

デフォルトではarchiveパラメーターが `True` になっているため、owner/groupが維持される。これを回避するためにownerパラメーターを `False` にするとrsyncコマンドにおける`--no-owner`を付与しrsyncを実行したユーザーがオーナーとなる。 

