# command

shell を経由しないコマンド実行。

通常 [shell](shell.md) モジュールを使った方がよい。

## 公式資料

- [command - Executes a command on a remote node — Ansible Documentation](https://docs.ansible.com/ansible/command_module.html)

