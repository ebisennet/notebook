# group

OS のグループを作成する。

## 公式資料

- [group - Add or remove groups — Ansible Documentation](https://docs.ansible.com/ansible/group_module.html)

## グループ作成

``` yaml
- name: create group
  group:
    name: group1
    gid: 999
```

- `name`
    - 作成するグループの名前を指定する。
- `gid`
    - 作成するグループのGIDを指定する。
