# yum

Yum を使ったパッケージのインストール、アンインストールなどが行える。

ローカルの rpm ファイルを指定したインストールも可能。

## 公式資料

- [yum - Manages packages with the yum package manager — Ansible Documentation](http://docs.ansible.com/ansible/yum_module.html)

## 使用例

``` yaml
- name: Apacheインストール
  yum:
    name: httpd
```

## EPELを使う

``` yaml
- name: python-passlibインストール
  yum:
    name: python-passlib
    enablerepo: epel
```

## EPELをインストール

``` yaml
- name: EPELインストール
  yum:
    name: epel-release
```

## 参考

- [Ansibleのyumにwith_itemsを指定するときの落とし穴と回避策 - Qiita](http://qiita.com/hnakamur/items/f8d8ceec3d0e8c4c3a86)
    - yum モジュールは with_items の処理が最適化される。
