# cloudformation

AWS CloudFormation Stack の作成・更新・削除。

テンプレートをパラメーターとして直接記述可能。

## 公式資料

- [cloudformation - Create or delete an AWS CloudFormation stack — Ansible Documentation](https://docs.ansible.com/ansible/cloudformation_module.html)
