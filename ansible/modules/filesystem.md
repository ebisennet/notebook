# filesystem

指定したデバイス上にファイルシステムを作成する。

## ファイルシステム作成

```yaml
- name: create filesystem
  filesystem:
    dev: /path/to/device
    fstype: ext4
    resizefs: True
```

- `dev`
    - ファイルシステムを作成する対象となるデバイスのパスを指定する。
- `fstype`
    - 作成するファイルシステムの種別（ext４など）を指定する。
- `resizefs`
    - ファイルシステムのサイズとブロックデバイスのサイズが異なる場合にリサイズする場合、 `True` を指定する。 それ以外の場合、 `False` を指定する。

## 公式資料

- [filesystem - Makes file system on block device — Ansible Documentation](http://docs.ansible.com/ansible/filesystem_module.html)
