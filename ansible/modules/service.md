# service

サービスの管理を行う。

## 公式資料

- [service - Manage services. — Ansible Documentation](https://docs.ansible.com/ansible/service_module.html)

## サービスを起動する

```yaml
- name: docker restart
  service:
    name: docker
    state: started
```

- `name`
    - 対象のサービス名を指定する。
- `state`
    - 起動する場合は `started` を指定する。
    - 停止する場合は `stoppted` を指定する。
    - 再起動する場合は `restarted` を指定する。

## 自動起動設定を変更する

```yaml
- name: service disabled
  service:
    name: firewalld.service
    enabled: False
```

- `name`
    - 対象のサービス名を指定する。
- `enabled`
    - 自動起動しないように設定する場合は `False` を指定する。
    - 自動起動するように設定する場合は `True` を指定する。

## トラブルシューティング

### enabled: yes に指定した時にエラーが起きる

ターゲット環境が systemd で動いている場合、 /etc/systemd/system の中に 通常 symlink が作成される位置に実ファイルが配置してあるとエラーが起きる。

symlink ではなく実ファイルを置くケースは unit 定義ファイルをカスタマイズするケース。
