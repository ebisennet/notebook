# sysctl

sysctl のエントリーを編集してカーネルパラメーターを設定する。

デフォルトで `/etc/sysctl.conf` に指定したカーネルパラメーターの設定が登録される。

カーネルパラメーターの設定に変化があった時は sysctl コマンドを呼び出して設定の反映も自動的に行う。

## カーネルパラメーター設定

```yaml
- name: set kernel parameter
  sysctl:
    name: "kernel.core_pattern"
    value: "/var/crash/core.%e.%t.%p"
```

- `name`
    - 設定するカーネルパラメーターの名前を指定する。
- `value`
    - 設定するカーネルパラメーターの値を指定する。

## 公式資料

- [sysctl - Manage entries in sysctl.conf. — Ansible Documentation](http://docs.ansible.com/ansible/sysctl_module.html)
