# bundler

## 公式資料

- [bundler - Manage Ruby Gem dependencies with Bundler — Ansible Documentation](https://docs.ansible.com/ansible/bundler_module.html)

## memo

- bundler 本体は別途 gem や yum などでインストールしておく必要がある。
- root ユーザーで実行すると失敗する。（bundlerの仕様）
