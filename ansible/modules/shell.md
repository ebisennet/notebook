# shell

command モジュールと異なり、シェル経由で実行できる。

## 公式資料

- [shell - Execute commands in nodes. — Ansible Documentation](https://docs.ansible.com/ansible/shell_module.html)

## 複数行に渡るコマンド実行の指定

``` yaml
- shell: >
    echo test \
         test &&
    echo test
```

## 指定ファイルがない時、コマンド実行

``` yaml
- shell: echo >> test.txt
  args:
    chdir: /path/to/work
    creates: test.txt
```

通常のモジュールと異なり、モジュール名を示す `shell` キーの後に実行したいコマンドを記述する。
シェル経由で実行されるため、シェルの変数展開やリダイレクトなどの記述が出来る。

モジュールのパラメーターは `args` キーの中に指定する。

- `chdir`
    - コマンド実行時のカレントディレクトリを指定する。
- `creates`
    - 任意のファイルのパスを指定する。
    - このパラメーターで指定したパスにファイルが存在した場合、コマンドは実行されない。

`creates` パラメーターを指定しないと毎回、必ず指定したコマンドが実行される。
その上で、コマンドの実行でターゲットサーバーに何も変化が起きなかった場合でもタスクの実行結果は常に `changed` となる。

- [stat](stat.org) モジュールを使わなくても shell だけで条件実行ができる。

## 実行結果の取得

```yaml
- shell: echo test \
    test &&
    echo test
  register: result

- debug:
    var: result
```

`register` ディレクティブにより実行結果を指定した変数に代入できる。

実行結果の代入された変数を [debug](debug.md) モジュールで表示した結果を以下に示す。

```
TASK [command] *****************************************************************
changed: [localhost]

TASK [debug] *******************************************************************
ok: [localhost] => {
    "result": {
        "changed": true,
        "cmd": "echo test test && echo test",
        "delta": "0:00:00.005076",
        "end": "2016-07-15 19:30:47.421877",
        "rc": 0,
        "start": "2016-07-15 19:30:47.416801",
        "stderr": "",
        "stdout": "test test\ntest",
        "stdout_lines": [
            "test test",
            "test"
        ],
        "warnings": []
    }
}
```

指定した変数には以下のような実行結果が入る。

- `changed`
    - 常に `true` が入る。
- `cmd`
    - 実行したコマンドが入る。
- `delta`
    - コマンドの実行時間が入る。
- `end`
    - コマンドの実行完了時間が入る。
- `rc`
    - コマンドの終了コードが入る。
    - 正常終了時は 0 となる。
- `start`
    - コマンドの実行開始時間が入る。
- `stderr`
    - コマンド実行した結果、標準エラー出力に出力された内容が入る。
- `stdout`
    - コマンド実行した結果、標準出力に出力された内容が入る。
- `stdout_lines`
    - `stdout` の内容を１行ごとに文字列にわけた配列が入る。

## 終了コードの拾い方

実行したコマンドの終了コードが０以外の場合、shellモジュールを使ったタスクは失敗扱いとなる。

以下のようにコマンドの最後に `echo $?` を実行してタスクの実行結果を取得することで、終了コードの判定ができる。

```yaml
- shell: diff a.txt b.txt ; echo $?
  register: result
  changed_when: False
  
- debug:
    msg: "差分あり"
  when: result.stdout != "0"
```

## shell 以外のモジュールを使うケース

shell モジュールから以下のコマンドを使う場合は、原則、対応するモジュールで置き換える。

| コマンド                      | 対応モジュール                                   |
|-------------------------------+--------------------------------------------------|
| find                          | [find](find.md)                                  |
| yum                           | [yum](yum.md)                                    |
| ln, mkdir, rmdir              | [file](file.md)                                  |
| useradd, usermod, userdel     | [user](user.md)                                  |
| groupadd, groupmod, groupdel  | [group](group.md)                                |
| git                           | [git](git.md)                                    |
| expect                        | [expect](expect.md)                              |
| systemctl, chkconfig, service | [service](service.md)                            |
| curl                          | [get_url](get_url.md), [uri](uri.md)             |
| mount, umount                 | [mount](mount.md)                                |
| nmcli                         | [nmcli](nmcli.md)                                |
| tar                           | [archive](archive.md), [unarchive](unarchive.md) |

## 指定ユーザーでコマンド実行

### become_user を使う

become_user でタスク実行ユーザーを指定することが出来る。

``` yaml
- shell: echo >> test.txt
  args:
    chdir: /path/to/work
    creates: test.txt
  become_user: USER
```

### become をオフにする

ansible が control node に ssh 接続した時のユーザーでよければ sudo をオフにして実行でも良い。

``` yaml
- shell: echo >> test.txt
  args:
    chdir: /path/to/work
    creates: test.txt
  become: false
```

### sudo を使う

グループまで指定したい時は明示的に sudo コマンドを使う。

``` yaml
- shell: sudo -u USER -g GROUP echo >> test.txt
  args:
    chdir: /path/to/work
    creates: test.txt
```

リダイレクトと sudo を組み合わせたい場合は、 tee コマンドを組み合わせる。
