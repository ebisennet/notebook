# authorized_key

SSH 公開鍵を登録、あるいは削除することができる。

## 公式資料

- [authorized_key - Adds or removes an SSH authorized key — Ansible Documentation](https://docs.ansible.com/ansible/authorized_key_module.html)

## 登録

``` yaml
- name: Add authorized keys
  authorized_key:
    user: "{{ ansible_ssh_user }}"
    key: "{{ lookup('file', lookup('env', 'HOME') + '/.ssh/id_rsa.pub') }}"
    state: present
```

- `user`
    - SSH公開鍵を登録するユーザー名を指定する。
- `key`
    - 登録するSSH公開鍵を指定する。

## 削除

``` yaml
- name: Remove authorized keys
  authorized_key:
    user: "{{ user }}"
    key: "{{ key }}"
    state: absent
```

- `user`
    - SSH公開鍵を削除するユーザー名を指定する。
- `key`
    - 削除するSSH公開鍵を指定する。
