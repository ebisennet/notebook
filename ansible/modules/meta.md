# meta

Ansible 内部状態に影響を与える特別なモジュール。

## 公式資料

- [meta - Execute Ansible ‘actions’ — Ansible Documentation](http://docs.ansible.com/ansible/meta_module.html)

## 使用例

```yaml
- meta: flush_handlers
```

パラメーターとして以下が指定できる。

- `flush_handlers`
    - `notify` によって実行待機状態となっている handler のタスクを実行する。
    - 内部的には `meta` タスクの場所に handler のタスクが追加されたような動きとなる。
- `refresh_inventory`
    - インベントリの再読込を行う。
    - ダイナミックインベントリを使っている時に使う。
- `noop`
    - 何もしない。
- `clear_facts` (2.1)
- `clear_host_errors` (2.1)
- `end_play` (2.2)
