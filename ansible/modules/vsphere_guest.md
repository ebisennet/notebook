# vsphere_guest

vSphere 上の VM の作成や管理ができる。

Ansible 1.6 から使えるようになったモジュール。

Ansible バージョンによって使えるパラメータが違うので注意。

- 公式サイトのリファレンスは stable バージョンでは使えないパラメータの記載がある。

## 公式資料

- [vsphere_guest - Create/delete/manage a guest VM through VMware vSphere. — Ansible Documentation](http://docs.ansible.com/ansible/vsphere_guest_module.html)
- [ansible-modules-core/vsphere_guest.py at devel · ansible/ansible-modules-core · GitHub](https://github.com/ansible/ansible-modules-core/blob/devel/cloud/vmware/vsphere_guest.py)
  - ソースコード

## pysphere の準備

pip で pysphere をインストールしておく必要がある。

``` bash
pip install -U pysphere
```

## VM作成

``` yaml
- hosts: localhost
  tasks:
    - vsphere_guest:
        vcenter_hostname: VCENTER_HOST
        username: VCENTER_USER
        password: VCENTER_PASSWORD
        guest: VM_NAME
        state: present
        vm_disk:
          disk1:
            size_gb: 1
            type: thin
            datastore: DATASTORE_NAME
        vm_nic:
          nic1:
            type: vmxnet3
            network: vm
            network_type: standard
        vm_hardware:
          memory_mb: 1024
          num_cpus: 1
          osid: centos64Guest
          scsi: paravirtual
        esxi:
          datacenter: DATACENTER_NAME
          hostname: ESXI_DOMAIN_NAME
```

-   ESXi のホスト名指定は vCenter に登録されているドメイン名である必要がある。

## テンプレートから起動

``` yaml
- hosts: localhost
  gather_facts: false
  tasks:
    - vsphere_guest:
        vcenter_hostname: VCENTER_HOST
        username: VCENTER_USER
        password: VCENTER_PASSWORD
        guest: VM_NAME
        from_template: yes
        template_src: TEMPLATE_NAME
        cluster: CLUSTER_NAME
        resource_pool: "/Resources"
```

- resource_pool はクラスタ内に作成したものを指定する場合、 "/Resources/PoolName" といった指定となる。
  - リソースプールを一つも作っていない場合、 "/Resources" のみで良い。
- power_on_after_clone パラメータは ansible 1.9.2 では未実装。

## Memo

- state と from_template は相互に排他的。
- from_template を使う時、 vm_extra_config は指定しても無視される。
- state: reconfigured を使う時は、ゲストOSがシャットダウンしていないといけない。

## 参考資料

- [Ansible で vSphere ESXi に仮想マシンをデプロイするには - らくがきちょう](http://sig9.hatenablog.com/entry/2015/04/13/235321)
- [Deploying Virtual Machines to vCenter with Ansible - Virtxpert](http://www.virtxpert.com/deploying-virtual-machines-vcenter-ansible/)

