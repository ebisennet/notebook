# git

git リポジトリのクローン、および指定したブランチやリビジョンのチェックアウトを行う。

## 公式資料

- [git - Deploy software (or files) from git checkouts — Ansible Documentation](http://docs.ansible.com/ansible/git_module.html)

## Git リポジトリのチェックアウト

```
- name: git clone
  git: 
    repo: git://example.com/example.git
    dest: /path//to/repo
    version: master
```

- `repo`
    - クローンするGitリポジトリのURLを指定する。
- `dest`
    - Gitリポジトリのクローンを作成するディレクトリを指定する。
- `version`
    - チェックアウトしたいブランチやリビジョンを指定する。
