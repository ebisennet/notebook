#  blockinfile

マーカー行で囲まれた範囲のブロックを書き換える。

マーカー行の内容を指定しないと以下の行がマーカーとなる。

```
# {mark} ANSIBLE MANAGED BLOCK
```

## 公式資料

- [blockinfile - Insert/update/remove a text block surrounded by marker lines. — Ansible Documentation](https://docs.ansible.com/ansible/blockinfile_module.html)

## ブロック設定

```yaml
- blockinfile:
    dest: path/to/file
    block: |
      test
    insertafter: *regexp*
```

insertafter / insertbefore で挿入位置を決めれる。

正規表現は正規表現のみを普通記述する。
`/` で囲んだり `\` を二重に書いたりする必要なし。

## ブロック削除

```yaml
- blockinfile:
    dest: path/to/file
    state: absent
```

## Memo

- ansible-playbook の `--diff` オプションをつけても差分内容が表示されない。

## 関連

- [lineinfile](lineinfile.md)
- [replace](replace.md)

