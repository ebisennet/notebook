# template

テンプレートを使ってリモートサーバー上にファイルを作成する。

## 公式資料

- [template - Templates a file out to a remote server. — Ansible Documentation](http://docs.ansible.com/ansible/template_module.html)

## サンプル

``` yaml
- name: template
  template:
    src: example.j2
    dest: path/to/example
    owner: owner
    group: group
    mode: 0644
```

- `src`
    - テンプレートファイルのパスを指定する。
    - 相対パスの場合、Role の templates ディレクトリ内のファイルとなる。
- `dest`
    - テンプレートを用いて作成するリモート上のファイルのパスを指定する。
- `owner`
    - リモート上のファイルの所有者を指定する。
- `group`
    - リモート上のファイルの所有グループを指定する。
- `mode`
    - リモート上のファイルパーミッションを指定する。
    - 数値で指定する場合に８進数指定するためには先頭に 0 が必要。
    - 文字列で指定する場合は、先頭に 0 がなくても８進数として解釈される。
