# ansible-vsphere

vSphere API を呼び出す Ansible Module です。

Core Module の VMware 用 Module と異なり pysphere ではなく pyvmomi を使っています。

put-file など Guest Operations Manager を使った操作もサポートしています。

- 詳細は ansible-doc コマンドでドキュメントを参照。

## 公式資料

- [ViaSat/ansible-vsphere · GitHub](https://github.com/ViaSat/ansible-vsphere)

## Memo

### spec の解釈

spec のプロパティは以下

- type
- value

value の内部は再帰的に dict から pyVmomi のオブジェクトへの変換処理が行われる。

- vSphere API の型名のキー
    - 指定した型のインスタンス生成
        - pyVmomi パッケージの vim に型名のキーがあるかどうかで判断されている。
    - プロパティ設定がない場合は YAML 上で明示的に空オブジェクトを指定する必要あり。
    - WSDL 定義上、型名が自明のものについても常に明示的な型指定が必要。
- ManagedObjectReference キー
    - type と name をプロパティに持つ。
    - type と name で Managed Object を指定できるが、同名があると問題になる。

## Troubleshooting

### サーバに接続できない

- pyvmomi をインストールしてあるか？
    - 接続先のサーバに必要。
    - エラーメッセージは "Could not connect to host" となり、他のケースと区別がつかないので注意。

## Inside

### class VsphereJsonEncoder

### class Vsphere

vSphere API 呼び出しのメインとなるクラス。

#### update_spec(self, spec)

spec を dictionary から vim オブジェクトに変換する。

#### clone_vm(self, guest, spec, devices)

- devices は使われていない。
  - spec 内に直接記述ことになる。
