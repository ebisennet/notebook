# selinux

## 公式資料

-   [selinux - Change policy and state of SELinux — Ansible Documentation](http://docs.ansible.com/ansible/selinux_module.html)

## SELinux をオフにする

``` yaml
- name: Install libselinux-python
  yum:
    name: libselinux-python
    state: installed

- name: disable SELinux
  sudo: yes
  selinux:
    state: disabled
```

再起動が必要になる。

Vagrant から Ansible を動かしている場合は再起動が問題になるので以下のようにする。

``` yaml
- name: Install libselinux-python
  yum:
    name: libselinux-python
    state: installed

- name: disable SELinux
  sudo: yes
  selinux:
    state: disabled

- name: reboot vagrant host
  command: 'vagrant reload'
  delegate_to: 127.0.0.1
  changed_when: false
```

vagrant provision ではなく ansible-playbook を使うこと。

- [備忘録: AnsibleでSELinuxをOFFにする方法](http://gyagya1111.blogspot.jp/2015/02/ansibleselinuxoff.html)
- [Ansible で Vagrant ホストを再起動する - Qiita](http://qiita.com/janus_wel/items/cf5e0cf41ad771f92038)
