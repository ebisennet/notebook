# stat

指定したファイルに関する情報を取得する。

取得結果を変数に登録することで、後続のタスクでファイルの有無などに合わせて条件分岐することが出来る。

## 公式資料

- [stat - retrieve file or file system status — Ansible Documentation](http://docs.ansible.com/ansible/stat_module.html)

## ファイル有無でタスク実行制御する

``` yaml
#
# 指定パスのファイルの情報を register 変数に登録する。
#
- name:
  stat:
    path: /path/to/file
  register: result

#
# 指定パスのファイルが存在した時のみ実行する。
#
- debug:
    msg: 'File exists'
  when: result.stat.exists

#
# 指定パスのファイルが存在しない時のみ実行する。
#
- debug:
    msg: 'File not exists'
  when: not result.stat.exists
```

- `path`
    - 情報を取得する対象のファイルパスを指定する。

取得した情報を登録した変数には以下の値が入る。

- `stat`
    - 指定したファイルの情報が設定される。
    - 以下のようなキーを持つ辞書型の値が設定される。
        - `exists`
            - 指定したファイルが存在するかどうか
        - `mtime`
            - ファイルの更新日時
        - `inode`
            - ファイルのinodeの値
        - `size`
            - ファイルのサイズ
        - `uid`
            - ファイルのUID
        - `pw_name`
            - ファイルの所有者のユーザー名
        - `gid`
            - ファイルのGID
        - `gr_name`
            - ファイルのグループ所有者のグループ名
        - `isdir`
            - ファイルがディレクトリかどうか
        - `ctime`
            - ファイルの作成日時
        - `mode`
            - ファイルのパーミッション
