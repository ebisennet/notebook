# setup

ターゲットインベントリから環境の情報を収集して変数から使えるようにする。

ターゲットインベントリに factor か ohai が入っていると `factor_` や `ohai_` で 始まる名前の変数も取得される。

## 公式資料

- [setup - Gathers facts about remote hosts — Ansible Documentation](http://docs.ansible.com/ansible/setup_module.html)
- [Variables — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_variables.html#information-discovered-from-systems-facts)
  - "Information discovered from systems: Facts"
- [ansible-modules-core/setup.py at devel · ansible/ansible-modules-core · GitHub](https://github.com/ansible/ansible-modules-core/blob/devel/system/setup.py)
    - Module のソースコード
- [ansible/facts.py at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/lib/ansible/module_utils/facts.py)
    - 実際に facts を集めている実体。

## 出力内容の確認

ansible コマンドで確認する例。

``` bash
ansible -m setup INVENTORY
```

## 変数

- `ansible_all_ipv4_addresses`
  - Array
- `ansible_all_ipv6_addresses`
  - Array
- `ansible_architecture`
- `ansible_default_ipv4`
  - Dict
- `ansible_default_ipv6`
  - Dict
- `ansible_devices`
  - Dict
- `ansible_distribution`
- `ansible_dns`
  - Dict
- `ansible_domain`
- `ansible_env`
  - Dict
- `ansible_fqdn`
- `ansible_{INTERFACE_NAME}`
  - INTERFACE_NAME: lo, eth0, eth1 など
- `ansible_hostname`
- `ansible_interfaces`
- `ansible_machine`
- `ansible_machine_id`
- `ansible_mounts`
- `ansible_os_family`
  - "RedHat"
      - RedHat, CentOS, Fedora など RedHat 系のディストリビューションであることを示す。
- `ansible_pkg_mgr`
- `ansible_processor`
- `ansible_processor_cores`
- `ansible_processor_count`
- `ansible_processor_threads_per_core`
- `ansible_processor_vcpus`
- `ansible_selinux`
- `ansible_service_mgr`
  - "systemd"
- `ansible_system`
- `ansible_user_dir`
- `ansible_virtualization_type`
- `module_setup`
  - true

## 参考資料

- [Ansibleを支えるfact: プラットフォームの情報を取得 — そこはかとなく書くよん。](http://tdoc.info/blog/2013/08/23/ansible_fact.html)
