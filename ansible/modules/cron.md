# cron

cron ジョブの登録、更新、削除を行う。

## 公式資料

- [cron - Manage cron.d and crontab entries. — Ansible Documentation](http://docs.ansible.com/ansible/cron_module.html)

## cron ジョブの登録、更新

```yaml
- name: set crontab
  cron:
    user: USER
    name: NAME
    job: JOB
    minute: *
    hour: *
    day: *
    month: *
    weekday: *
```

- `user`
    - ジョブを登録するユーザーを指定する。
- `name`
    - Ansible がジョブを認識するための名前を指定する。
    - 省略すると Ansible 実行のたびに新しいジョブが登録されることに注意。
- `job`
    - 実行するコマンドを指定する。
- `minute`, `hour`, `day`, `month`, `weekday`
    - ジョブを実行する時間を指定する。

## cron ジョブの削除

```yaml
- name: set crontab
  cron:
    user: USER
    name: NAME
	state: absent
```

- `user`
    - ジョブを 削除するユーザーを指定する。
- `name`
    - Ansible がジョブを認識するための名前を指定する。
- `state`
    - `absent` と書くことでジョブの削除を指定する。
