# Modules

## 公式資料

- [ansible/ansible-modules-core · GitHub](https://github.com/ansible/ansible-modules-core)
    - Core Modules
- [ansible/ansible-modules-extras · GitHub](https://github.com/ansible/ansible-modules-extras)
    - Extra Modules
    - [Extras Modules — Ansible Documentation](https://docs.ansible.com/ansible/modules_extra.html)
        - Extra Modules の位置付けについて
- モジュール一覧
    - [All Modules — Ansible Documentation](https://docs.ansible.com/ansible/list_of_all_modules.html)
    - [Cloud Modules — Ansible Documentation](https://docs.ansible.com/ansible/list_of_cloud_modules.html)

## Modules

| モジュール名                                  | 説明                                               |
|-----------------------------------------------+----------------------------------------------------|
| [add_host](add_host.md)                       |                                                    |
| [assert](assert.md)                           |                                                    |
| [authorized_key](authorized_key.md)           | ssh 公開鍵の追加、削除                             |
| [blockinfile](blockinfile.md)                 |                                                    |
| [bower](bower.md)                             |                                                    |
| [bundler](bundler.md)                         |                                                    |
| [cloudformation](cloudformation.md)           |                                                    |
| [cloudtrail](cloudtrail.md)                   |                                                    |
| [command](command.md)                         | コマンドの実行                                     |
| [composer](composer.md)                       |                                                    |
| [copy](copy.md)                               | 対象ホストにファイルコピー                         |
| [cron](cron.md)                               | cron の管理                                        |
| [debug](debug.md)                             | デバッグメッセージ出力                             |
| [easy_install](easy_install.md)               |                                                    |
| [ec2](ec2.md)                                 |                                                    |
| [ec2_eip](ec2_eip.md)                         |                                                    |
| [ec2_elb](ec2_elb.md)                         |                                                    |
| [ec2_eni](ec2_eni.md)                         |                                                    |
| [ec2_group](ec2_group.md)                     |                                                    |
| [ec2_key](ec2_key.md)                         |                                                    |
| [ec2_vpc](ec2_vpc.md)                         |                                                    |
| [ec2_vpcigw](ec2_vpcigw.md)                   |                                                    |
| [ec2_vpcnet](ec2_vpcnet.md)                   |                                                    |
| [ec2_vpcroutetable](ec2_vpcroutetable.md)     |                                                    |
| [ec2_vpcsubnet](ec2_vpcsubnet.md)             |                                                    |
| [expect](expect.md)                           |                                                    |
| [fail](fail.md)                               | Ansible 実行を失敗として終了する                   |
| [fetch](fetch.md)                             | 対象ホストからローカルにファイルコピー             |
| [file](file.md)                               | ファイルのパーミッション設定やディレクトリ作成など |
| [filesystem](filesystem.md)                   | ファイルシステムを作成する                         |
| [find](find.md)                               |                                                    |
| [gem](gem.md)                                 |                                                    |
| [get_url](get_url.md)                         |                                                    |
| [git](git.md)                                 | git の実行                                         |
| [group](group.md)                             | OS グループの管理                                  |
| [hostname](hostname.md)                       | ホスト名のセット                                   |
| [htpasswd](htpasswd.md)                       |                                                    |
| [iam](iam.md)                                 |                                                    |
| [iam_cert](iam_cert.md)                       |                                                    |
| [include_vars](include_vars.md)               |                                                    |
| [lineinfile](lineinfile.md)                   |                                                    |
| [lvg](lvg.md)                                 | LVM ボリュームグループの設定                       |
| [lvol](lvol.md)                               | LVM 論理ボリュームの設定                           |
| [maven_artifact](maven_artifact.md)           |                                                    |
| [meta](meta.md)                               | Ansible 内部状態に影響を与える特別なモジュール     |
| [nmcli](nmcli.md)                             | nmcli コマンドによるネットワークの管理             |
| [npm](npm.md)                                 |                                                    |
| [pear](pear.md)                               |                                                    |
| [pip](pip.md)                                 |                                                    |
| [raw](raw.md)                                 |                                                    |
| [rds](rds.md)                                 |                                                    |
| [rds_param_group](rds_param_group.md)         |                                                    |
| [rds_subnet_group](rds_subnet_group.md)       |                                                    |
| [redhat_subscription](redhat_subscription.md) |                                                    |
| [replace](replace.md)                         |                                                    |
| [route53](route53.md)                         |                                                    |
| [s3](s3.md)                                   |                                                    |
| [s3_bucket](s3_bucket.md)                     |                                                    |
| [script](script.md)                           |                                                    |
| [selinux](selinux.md)                         |                                                    |
| [service](service.md)                         | サービスの起動、停止など                           |
| [set_fact](set_fact.md)                       | 変数に指定した値をセット                           |
| [setup](setup.md)                             |                                                    |
| [shell](shell.md)                             | コマンドの実行                                     |
| [sns_topic](sns_topic.md)                     |                                                    |
| [sqs_queue](sqs_queue.md)                     |                                                    |
| [stat](stat.md)                               | 指定ファイルの情報取得                             |
| [sts_assume_role](sts_assume_role.md)         |                                                    |
| [synchronize](synchronize.md)                 |                                                    |
| [sysctl](sysctl.md)                           | sysctl.conf のエントリー管理                       |
| [template](template.md)                       | テンプレートファイルから対象ホストにファイル生成   |
| [unarchive](unarchive.md)                     | アーカイブファイルの展開                           |
| [uri](uri.md)                                 |                                                    |
| [user](user.md)                               | OS ユーザーの管理                                  |
| [vsphere_guest](vsphere_guest.md)             |                                                    |
| [wait_for](wait_for.md)                       |                                                    |
| [yum](yum.md)                                 | Yum によるパッケージ管理                           |

## モジュール別

- bigip
    - [Ansible を使って、big-ip を制御して、自動的にサービスを組み込む。その１ - Hack the World!](http://graceful-life.hatenablog.com/entry/2014/10/09/024458)
- [ansible-vsphere](ansible-vsphere.md)

