# copy

ファイルをローカルからリモートへコピーする。

変更対象ファイルのバックアップ指定も可能。

2.0 からはリモート内でのファイルコピーも可能。

- `remote_src: true` を指定する。

remote_src を使うと DRY RUN でもコピーされてしまうバグがあるので注意。
2.1 で修正されている。

- [Do not copy file if running on check mode by ewigkeit1204 · Pull Request \#3965 · ansible/ansible\-modules\-core · GitHub](https://github.com/ansible/ansible-modules-core/pull/3965)

## 公式資料

- [copy - Copies files to remote locations. — Ansible Documentation](http://docs.ansible.com/ansible/copy_module.html)

## ファイルコピー
``` yaml
- name: copy
  copy:
    src: path/to/src
    dest: /path/to/dest
    backup: yes
    owner: USER
    group: GROUP
    mode: 0600
```

- `src`
    - コピー元のファイルのパスを指定する。
    - 相対パスの場合、Role の files ディレクトリ内のファイルとなる。
- `dest`
    - ファイルコピー先のリモート上のパスを指定する。
- `owner`
    - コピー先のファイルの所有者を指定する。
- `group`
    - コピー先のファイルの所有グループを指定する。
- `mode`
    - コピー先のファイルパーミッションを指定する。
    - 数値で指定する場合に８進数指定するためには先頭に 0 が必要。
    - 文字列で指定する場合は、先頭に 0 がなくても８進数として解釈される。

force はデフォルトで yes なので注意。

## mode 指定の注意

mode 指定を変数展開で指定する時は、変数の値として文字列をセットする必要がある。

``` yaml
- name: copy
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    mode: "{{ item.mode }}"
  with_items:
    - src: path/to/src
      dest: path/to/dest
      mode: "0600"
```

文字列にしない場合、８進数としてパースされた結果の整数値が 変数展開時に文字列に変換され、mode に１０進数としての指定が入る。

## ファイル内容を指定

``` yaml
- name: copy
  copy:
    content: "Test"
    dest: /path/to/dest
    backup: yes
    owner: USER
    group: GROUP
    mode: 0600
```

- `content`
    - ファイルの内容を指定する。
    - `src` パラメーターの代わりとして使える。
