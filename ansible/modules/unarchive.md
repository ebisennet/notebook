# unarchive

アーカイブファイル（.tar, .tgz, .zip）を展開する。

デフォルトではファイルの展開前に、アーカイブファイルをローカルからリモートにコピーする。

内部で tar, gtar, unzip コマンドを呼び出す。

## 公式資料

- [unarchive - Unpacks an archive after (optionally) copying it from the local machine. — Ansible Documentation](http://docs.ansible.com/ansible/unarchive_module.html)

## ローカルのtarをリモートに展開

``` yaml
- name: unarchive
  unarchive:
    src: foo.tgz
    dest: /tmp/foo
```

## アーカイブファイルの展開

```yaml
- name: unpack archive
  unarchive:
    src: /path/to/archive.tgz
    dest: /path/to/dest
    copy: True
    creates: /path/to/file
  register: result
```

- `src`
    - 展開するアーカイブファイルのパスを指定する。
    - 相対パスで指定した場合、 Playbook のあるディレクトリを基準にする。
    - `copy` パラメーターが `True` の場合、ローカルのパスとして扱われる。
    - `copy` パラメーターが `False` の場合、リモートのパスとして扱われる。
- `dest`
    - アーカイブの展開先のディレクトリへの絶対パスを指定する。
    - このパラメーターで指定したパスにディレクトリがないとエラーとなる。
- `copy`
    - アーカイブファイルをローカルからリモートにコピーするかどうかを指定する。
    - `True` の場合、ローカルからリモートにアーカイブファイルをコピーしてから展開する。
    - `False` の場合、リモートサーバー上のアーカイブファイルを展開する。
- `creates`
    - アーカイブファイルの展開後のファイルを指定することで、展開済みファイルがある場合にこのモジュールの実行をスキップできる。
        - アーカイブから展開されたファイル以外の任意のファイルを指定しても良い。

`creates` パラメーターを指定しないと Ansible 実行のたびに、アーカイブファイルの展開が実行されてしまうので、原則、`creates` パラメーターを指定する。

- Ansible 2.1 からはアーカイブファイルと展開先の比較を行うので creates しない方が良いケースもある。

`register` で登録した変数には以下の値が入る。

- `changed`
    - ファイルが展開されたかどうか。
- `skipped`
    - タスクがスキップされたかどうか。
- `dest`
    - `dest` パラメーターが指定された値。
- `extract_results`
    - 内部で呼ばれたコマンドの実行結果。
- `gid`
    - `dest` パラメーターで指定されたディレクトリの gid。
- `group`
    - `dest` パラメーターで指定されたディレクトリのグループ名。
- `mode`
    - `dest` パラメーターで指定されたディレクトリのパーミッション。
- `owner`
    - `dest` パラメーターで指定されたディレクトリのオーナー名。
- `src`
    - アーカイブファイルのリモートマシン上のパス。
    - `copy` パラメーターが `True` の場合もローカルからコピーされた先のリモート上のパスとなる。
- `state`
    - `direcotry` 固定。
- `uid`
    - `dest` パラメーターで指定されたディレクトリの uid。
- `stat`
    - `creates` パラメーターが指定された時のみ設定される。
    - [stat](stat.md) モジュールの実行結果と同じ結果が入る。

## Troubleshooting

### No such file or directory のエラーが出る (Ansible 2.1)

tar や unzip コマンドの出力のパースに失敗することで出る。

タスク実行時に LANG を設定することで回避できるケース。

- [Ansible の unarchive で No such file or directory - Qiita](http://qiita.com/TakaakiOno/items/10271e56816862508fb3)

Mac 上で実行した時にエラーとなるケース。

- gnu-tar をインストールすることで回避できる。
