# maven_artifact

Maven リポジトリからアーティファクト（jarやwar）をダウンロードする。

## 公式資料

- [maven\_artifact \- Downloads an Artifact from a Maven Repository — Ansible Documentation](https://docs.ansible.com/ansible/maven_artifact_module.html)
