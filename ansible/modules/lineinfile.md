# lineinfile

ファイルの中に指定した内容の行があることを保証するモジュール。

対象の行がどの行の前/後にあるべきかパターンで指定可能。 指定しないと末尾に追加される。

## 公式資料

- [lineinfile - Ensure a particular line is in a file, or replace an existing line using a back-referenced regular expression. — Ansible Documentation](https://docs.ansible.com/ansible/lineinfile_module.html)

## 関連

- [blockinfile](blockinfile.md)
- [replace](replace.md)

## 参考

- [Ansibleでファイルの行を書き換える3つの方法 - Qiita](http://qiita.com/cognitom/items/57de72b739642041dcd5)
- [【Ansible】lineinfileのinsertbefore、insertafterについて | CentOS | 俺日記](http://blog.shinji.asia/ansible-lineinfile-insertbeforeetc/)

