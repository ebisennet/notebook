# fail

Ansible の実行を失敗とする。

タスクの実行条件をつけることで、特定の条件を満たしたときに Ansible の実行を中断するために使える。

## 公式資料

- [fail - Fail with custom message — Ansible Documentation](http://docs.ansible.com/ansible/fail_module.html)

## 変数定義のチェック

``` yaml
- fail:
    msg: "'foo' must be defined"
  when: foo is not defined
```
