# user

OSのユーザー作成や設定を行う。

## 公式資料
- [user - Manage user accounts — Ansible Documentation](https://docs.ansible.com/ansible/user_module.html)

## 基本

```yaml
- name: ユーザー作成
  user:
    name: user1
    uid: 999
    group: user1
    home: /path/to/home
    shell: /path/to/shell
```

デフォルトで home directory 作成。

## ユーザー作成

```yaml
- name: create user
  user:
    name: "{{ item.name }}"
    password: "{{ item.password )}}"
    uid: "{{ item.uid }}"
    home: "{{ item.home }}"
    shell: "{{ item.shell }}"
    group: "{{ item.group }}"
    groups: "{{ item.groups }}"
  no_log: True
```

- `name`
    - 作成するユーザーのユーザー名を指定する。
- `password`
    - 作成するユーザーのパスワードを指定する。
    - パスワードは暗号化された値を指定する必要がある。
- `uid`
    - 作成するユーザーのUIDを指定する。
- `home`
    - 作成するユーザーのホームディレクトリを指定する。
- `shell`
    - 作成するユーザーのログインシェルを指定する。
- `group`
    - 作成するユーザーが所属するプライマリーグループを指定する。
- `groups`
    - 作成するユーザーが所属するサブグループをカンマ区切りで指定する。
    - 全てのサブグループから所属を解除するには空文字列を指定する。
        - groups パラメーターの指定自体を削除すると、サブグループの設定を変更しない、扱いとなる。

パスワード変更時にパスワードが Ansible の実行ログに出力されるのを防ぐため `no_log: True` を指定する。

uid 変更時に対象のユーザーがプロセスを起動しているとエラーとなる。

## SSHキー生成

```yaml
- name: SSHキー生成
  user:
    name: user1
    generate_ssh_key: true
```

別途、キーペアを生成済みの場合、[authorized_key](authorized_key.md) モジュールを使う。

## パスワード設定

事前にハッシュ値を生成する場合、シェルで以下の 1 liner を実行して生成する。

```sh
python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.encrypt(getpass.getpass())"
```

ansible 内に閉じて実行したい場合

```yaml

```
