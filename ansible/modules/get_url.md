# get_url

指定URLからファイルダウンロード。

[file](file.md) モジュールのパラメータは全て使える。

checksum (2.0), sha256sum (1.3) の指定があれば、ファイル内容をチェックした上でスキップする。 指定がなければ、destのファイル有無のみでスキップチェックする。

## 公式資料

- [get_url - Downloads files from HTTP, HTTPS, or FTP to node — Ansible Documentation](http://docs.ansible.com/ansible/get_url_module.html)

## サンプル

``` yaml
- name: ダウンロード
  get_url:
    url: http://example.com/test.txt
    dest: /tmp/test.txt
    mode: 0660
```

## 参考

- [Ansible get_url モジュールで、冪等性を考えてみる - Qiita](http://qiita.com/h2suzuki/items/0f06ae8b765077ddfa7c)

