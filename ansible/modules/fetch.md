# fetch

リモートサーバーからローカルにファイルをダウンロードする。

http などで取得する場合は [get_url](get_url.md) を使う。

## 公式資料

- [fetch - Fetches a file from remote nodes — Ansible Documentation](http://docs.ansible.com/ansible/fetch_module.html)

## ファイルダウンロード

```yaml
- name: download
  fetch:
    src: path/to/src
    dest: path/to/dest
    flat: false
    validate_checksum: True
    fail_on_missing: False
  register: result
```

- `src`
    - ダウンロードするリモートサーバー上のファイルパスを指定する。
- `dest`
    - ダウンロードしたファイルを保存するローカル上のファイルパスを指定する。
- `flat`
    - ダウンロードしたファイルをフラッチに配置するか。
    - デフォルト動作は `dest` で作成したディレクトリ内に、ホスト名ディレクトリが作られ `src` で指定したパスの階層を再現しようとする。
- `validate_checksum`
    - ダウンロードした後に正常にダウンロードされたかチェックサムで検証するかどうかを指定する。
    - 未指定の場合、デフォルト動作は検証する。
- `fail_on_missing`
    - `src` で指定したファイルが存在しない時にエラーにするかどうかを指定する。
    - 未指定の場合、デフォルト動作はエラーにしない。

ダウンロードするだけであれば、通常 register をする必要性はない。
register で実行結果を変数に代入した場合、変数には以下のような値を持つ辞書型の値が設定される。

- `file`
    - `src` で指定したファイルのパス。
- `checksum`
    - ダウンロードしたファイルのチェックサム。
    - `src` で指定したファイルが存在しない時は未定義となる。
