# hostname

ホスト名をセットする。

## 公式資料

- [hostname - Manage hostname — Ansible Documentation](https://docs.ansible.com/ansible/hostname_module.html)

## 使用例

```yaml
- name: set hostname
  hostname:
    name: "{{ inventory_hostname_short }}"
```

- `name`
    - 設定するホスト名を指定する。
