# redhat_subscription

## 公式資料

- [redhat_subscription - Manage Red Hat Network registration and subscriptions using the subscription-manager command — Ansible Documentation](https://docs.ansible.com/ansible/redhat_subscription_module.html)

## 例

``` yaml
- name: RedHat Subscription 登録
  redhat_subscription:
    username: <RedHat Network username>
    password: <RedHat Network password>
```

## Memo

Extras や Optional チャネルを有効化するには別途以下の実行。

``` bash
subscription-manager repos --enable=rhel-7-server-extras-rpms 
subscription-manager repos --enable=rhel-7-server-optional-rpms
```
