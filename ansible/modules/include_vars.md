#  include_vars

指定したファイルから変数を読み込む。

## 公式資料

- [include_vars - Load variables from files, dynamically within a task. — Ansible Documentation](http://docs.ansible.com/ansible/include_vars_module.html)

