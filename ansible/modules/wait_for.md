# wait_for

TCP の Listen 待ちやロックファイルの削除待ちなどができる。

## 公式資料

- [wait_for - Waits for a condition before continuing. — Ansible Documentation](http://docs.ansible.com/ansible/wait_for_module.html)

