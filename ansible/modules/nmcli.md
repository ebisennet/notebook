# nmcli

nmcli コマンドを使ったネットワークの設定ができる。

## IPアドレス設定

```yaml
  - name: set ip
    nmcli:
      ifname: "eth0"
      type: ethernet
      conn_name: "ethernet-eth0"
      ip4: "10.0.0.1/24"
      gw4: "10.0.0.254"
```

- `ifname`
    - IP を設定するインターフェイス名を指定する。
- `type`
    - 設定対象のデバイスの種類として `ethenet` を指定する。
- `conn_name`
    - IP を設定するコネクション名を指定する。
- `ipv4`
    - 設定する IP アドレスを指定する。
- `gw4`
    - 設定する Gateway アドレスを指定する。

## 使用時の注意

nmcli モジュールを使ってネットワーク設定を行った場合、実際には何も変更されていなくても Ansible の実行結果は `changed` と報告される。

- 関連資料
    - [nmcli reports changed status even if nothing needs to change · Issue #1842 · ansible/ansible-modules-extras · GitHub](https://github.com/ansible/ansible-modules-extras/issues/1842)

## 公式資料

- [nmcli - Manage Networking — Ansible Documentation](http://docs.ansible.com/ansible/nmcli_module.html)
