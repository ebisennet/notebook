# Versions

## 公式資料

- [Releases · ansible/ansible · GitHub](https://github.com/ansible/ansible/releases)
    - Ansible の GitHub Releases ページ。
    - 過去のバージョンのリリース日確認に。

## Roadmap

- [ansible/docsite/rst/roadmap at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/tree/devel/docsite/rst/roadmap)

## 2.3

- [Red Hat Delivers Advanced Network Automation with Latest Version of Ansible](https://www.ansible.com/press/advanced-network-automation-with-2-3)
    - 2017-04-12 GA
    - Ansible からノードに接続する際の通信処理をプラグイン化。
        - [Networking Features Coming Soon in Ansible 2\.3](https://www.ansible.com/blog/networking-features-in-ansible-2-3)
- [ansible/ROADMAP\_2\_3\.rst at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/docsite/rst/roadmap/ROADMAP_2_3.rst)
- Single Encrypted Variable
    - [Vault — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_vault.html#single-encrypted-variable)
    - 通常の YAML ファイル内に値を暗号化した変数を組み込める。

## 2.2

- [Ansible 2\.2 Delivers New Automation Capabilities for Containers, Networks and Cloud Services](https://www.ansible.com/press/ansible-22-delivers-new-automation-capabilities-for-containers-networks-and-cloud)
    - 2016-11-01 GA
- [ansible/ROADMAP\_2\_2\.rst at devel · ansible/ansible · GitHub](https://github.com/ansible/ansible/blob/devel/docsite/rst/roadmap/ROADMAP_2_2.rst)
- include_role が追加
- jenins_job, jenkins_plugin が追加

## 2.1

- ansible.cfg
    - display_args_to_stdout
- [Ansible 2.1 新規機能紹介 - Qiita](http://qiita.com/toromoti/items/74579dc60e6a66a61856)
    - include の static 指定
    - debug strategy
    - loop_control
    - diff 表示の改善
    - ansible-shell の追加
        - ansible-console にリネームされてる？
    - copy モジュールが src ファイルとして vault で暗号化されたファイルをサポート

## 2.0

### 2.0.1.0

- 2016-02-25
- ASW Ansible バージョン

これ以降のセキュリティフィックス。

- 2.2.1 CVE-2016-9587
- 2.1.4 CVE-2016-9587
- 2.1.3 CVE-2016-8628 CVE-2016-8614
- 2.0.2 CVE-2016-3096
