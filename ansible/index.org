#+title: ansible

* Official
- [[http://www.ansible.com/][Ansible is Simple IT Automation]]
- [[http://docs.ansible.com/][Ansible Documentation]]
  - 
- [[https://github.com/ansible/ansible][ansible/ansible · GitHub]]
  - ansible 本体

* Topics
- [[file:installing.org][Installing]]
- [[file:commands/index.org][Commands]]
- [[file:concepts/index.org][Concepts]]
- [[file:modules/index.org][Modules]]
- [[file:variables/index.org][Variables]]
- [[file:configuration.org][Configuration]]
- [[file:jinja2.org][Jinja2]]
- Block
  - Loop との組み合わせはまだ。
  - [[http://qiita.com/tboffice/items/4cb0a17a38359682afe9][blockでwith_itemsが使いたかった - Qiita]]
- [[file:loops/index.org][Loops]]
- Conditionals
  - Loop と組み合わせると when は loop 内となる。
- [[file:plugins/index.org][Plugins]]
- [[file:tools/index.org][Tools]]
- [[file:requirements.org][Requirements]]
- [[file:tips.org][Tips]]
- [[file:usecase.org][Usecase]]
- コーディング規約
  - [[http://tdoc.info/blog/2014/10/09/ansible_coding.html][Ansible コーディング規約 (の例) — そこはかとなく書くよん。]]
- コードカバレッジ
  - [[http://ks888.hatenablog.com/entry/2015/09/06/223538][Ansible向けのコードカバレッジツールKirbyを作りました - ks888の日記]]
- [[file:best-practices.org][Best Practices]]
- 環境切り替え
  - [[http://qiita.com/NewGyu/items/5de31d76d2488ab27ed6#comment-477545063579aa8bbf44][Ansibleのインベントリファイルでステージを切り替える - Qiita]]
  - [[http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF][Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス - 双六工場日誌]]
    - インベントリファイルの切り替え
    - 環境別のグループ
- Developer
  - [[file:developing-modules/index.org][モジュール開発]]
  - [[file:developing-plugins/index.org][プラグイン開発]]
- Proxy
  - [[http://docs.ansible.com/ansible/playbooks_environment.html][Setting the Environment (and Working With Proxies) — Ansible Documentation]]
- Roadmap
  - [[https://github.com/ansible/ansible/blob/devel/ROADMAP.md][ansible/ROADMAP.md at devel · ansible/ansible · GitHub]]
- デバッグ
  - [[http://qiita.com/346@github/items/00122556cb2bd6f57998][ansibleで特定のtaskを特定のhostに実行する - Qiita]]
    - -l (--limit) で実行対象を絞る。
    - --start-at で開始タスクを指定する。
    - --step でステップ実行。
- YAML
  - [[http://blog.n-z.jp/blog/2014-06-21-ansible-yaml.html][ansible使いのためのYAML入門 - @znz blog]]
- [[file:versions.org][Versions]]

* Memo
** Mac からの踏み台経由の実行
#+begin_example
  export ANSIBLE_SSH_ARGS=' -F ssh_config '
  export ANSIBLE_SCP_IF_SSH=y
#+end_example

- -F オプションで指定するパスはシェルのパス展開されない。
  - 絶対パスか、相対パスのみ
- ANSIBLE_SCP_IF_SSH は ansible.cfg で指定可能。

参考
- [[http://www.torutk.com/projects/swe/wiki/Ansible%E3%81%A7SSH%E6%8E%A5%E7%B6%9A][AnsibleでSSH接続 - ソフトウェアエンジニアリング - Torutk]]

* See also
- [[http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF][Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス - 双六工場日誌]]
- [[http://techracho.bpsinc.jp/yamasita-taisuke/2014_05_29/17567][chefからansibleに乗り換えた5つの理由 | TechRacho]]
- [[http://knowledge.sakura.ad.jp/tech/3124/][エージェントレスでシンプルな構成管理ツール「Ansible」入門 - さくらのナレッジ]]
  - 入門記事としてちょうどいい
- [[http://yteraoka.github.io/ansible-tutorial/][Ansible チュートリアル | Ansible Tutorial in Japanese]]
  - 入門記事としてちょうどいい
  - ベストプラクティスについては記事内にリンクのある以下を参照
    - [[http://qiita.com/yteraoka/items/5ed2bddefff32e1b9faf][Ansible オレオレベストプラクティス - Qiita]]
- [[http://qiita.com/volanja/items/2ffa1599fc9a1fae39a1][今年1年間でAnsible界隈ではどのような変化が起こったのか。 - Qiita]]
- [[http://qiita.com/advent-calendar/2013/ansible][Ansible Advent Calendar 2013 - Qiita]]
