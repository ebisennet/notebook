# ansible-playbook-debugger

ansible 2.1 で公式ツールとなりそう。

## 公式資料

- [GitHub - ks888/ansible-playbook-debugger: A Debugger for Ansible Playbook](https://github.com/ks888/ansible-playbook-debugger)

## インストール

``` {.bash}
pip install ansible-playbook-debugger
```

## 参考

- [Ansible Playbookの短時間デバッグ方法](http://www.slideshare.net/ks888sk/ansible-playbook-debugtips)
    - -vv や debug モジュール
    - --start-at-task で途中から開始
- [Ansible Playbook向けのデバッガを作りました - ks888の日記](http://ks888.hatenablog.com/entry/2014/12/27/132835)
