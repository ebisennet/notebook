# aws-sdk-ios

## 公式資料

- [GitHub \- aws/aws\-sdk\-ios: Official mirror of version 2 of the AWS Mobile SDK for iOS\. For more information, see our web site:](https://github.com/aws/aws-sdk-ios)
- [AWS Mobile SDK for iOS v2\.4\.11 Reference](http://docs.aws.amazon.com/AWSiOSSDK/latest/)
    - 記載されていないプロパティもあるので注意。
- [AWS Mobile SDK for iOS — iOS Developer Guide](http://docs.aws.amazon.com/mobile/sdkforios/developerguide/index.html)

## Classes

- [AWSAPIGatewayClient](AWSAPIGatewayClient.md)
- [AWSNetworkingConfiguration](AWSNetworkingConfiguration.md)
- [AWSServiceConfiguration](AWSServiceConfiguration.md)
- [AWSNetworkingRequestInterceptor](AWSNetworkingRequestInterceptor.md)
- AWSSignatureV4Signer
- AWSNetworkingHTTPResponseInterceptor

## Carthage 使用時の設定

1. Add the following to your `Cartfile`:

        github "aws/aws-sdk-ios"

1. Then run the following command:
	
		$ carthage update

1. With your project open in Xcode, select your **Target**. Under **General** tab, find **Embedded Binaries** and then click the **+** button.


1. Click the **Add Other...** button, navigate to the `AWS<#ServiceName#>.framework` files under `Carthage` > `Build` > `iOS` and select them. Do not check the **Destination: Copy items if needed** checkbox when prompted.

	    * `AWSCore.framework`
	    * `AWSAPIGateway.framework`

1. Under the **Buid Phases** tab in your **Target**, click the **+** button on the top left and then select **New Run Script Phase**. Then setup the build phase as follows. Make sure this phase is below the `Embed Frameworks` phase.

        Shell /bin/sh
        
        bash "${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/AWSCore.framework/strip-frameworks.sh"
        
        Show environment variables in build log: Checked
        Run script only when installing: Not checked
        
        Input Files: Empty
        Output Files: Empty
