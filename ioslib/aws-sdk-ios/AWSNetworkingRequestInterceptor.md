# AWSNetworkingRequestInterceptor

## Example

```objc
## 
- (AWSTask *)interceptRequest:(NSMutableURLRequest *)request {
    [request setValue:[[NSDate aws_clockSkewFixedDate] aws_stringValue:AWSDateISO8601DateFormat2]
   forHTTPHeaderField:@"X-Amz-Date"];

    [request setValue:self.userAgent
   forHTTPHeaderField:@"User-Agent"];
    
    return [AWSTask taskWithResult:nil];
}
```
