# AWSAPIGatewayClient

API Gateway で生成した Client クラスが継承している基本クラス。

## Properties

- configuration
    - [AWSServiceConfiguration](AWSServiceConfiguration.md)
    - requestInterceptors を設定するときは、配列の最初に追加する。
        - 最後が signer でないといけない。
