# AWSNetworkingConfiguration

- [CocoaDocs\.org \- AWSNetworkingConfiguration Class Reference](http://cocoadocs.org/docsets/AWSCore/2.4.11/Classes/AWSNetworkingConfiguration.html)
- [aws\-sdk\-ios/AWSNetworking\.h at master · aws/aws\-sdk\-ios · GitHub](https://github.com/aws/aws-sdk-ios/blob/master/AWSCore/Networking/AWSNetworking.h)
- Interceptor や Selializer などを設定できる。

## Properties

- URL
- baseURL
- URLString
- HTTPMethod
- headers
- allowsCellularAccess
- sharedContainerIdentifier
- requestSerializer
- requestInterceptors
- responseSerializer
    - AWSAPIGatewayClient からは使われていない。
    - 他のサービスのクラスからは使われている。
- responseInterceptors
    - プロパティとして値の設定はできるが、インターセプト処理が実装されていない。
- retryHandler
- maxRetryCount
- timeoutIntervalForRequest
- timeoutIntervalForResource
