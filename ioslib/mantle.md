# mantle

## 公式資料

- [GitHub \- Mantle/Mantle: Model framework for Cocoa and Cocoa Touch](https://github.com/Mantle/Mantle)

## 参考資料

- [Safx: MantleでObjective\-CプロパティとJSON文字列との相互変換を自分で制御する](http://safx-dev.blogspot.jp/2013/12/mantleobjective-cjson-mantletip-mantle.html)
- [【Objective\-C】MantleでJSONをマッピング \- Qiita](http://qiita.com/KentaKudo/items/3688a8be8262ed272c36)
- 
