# バックアップ

## 対象別

- 設定
    - $JENKINS_HOME/\*.xml
        - Jenkins 全体の設定
    - $JENKINS_HOME/jobs/\*/config.xml
        - ジョブ定義
    - $JENKINS_HOME/users/\*\*
        - ユーザー定義
- プラグイン
    - $JENKINS_HOME/plugins/\*.[hj]pi
        - プラグインファイル
- その他
    - $JENKINS_HOME/userContent/\*\*
        - コンテンツ
    - $JENKINS_HOME/secrets/\*\*
        - 鍵情報など？

以下は通常バックアップ対象外。

- ビルド履歴
    - $JENKINS_HOME/jobs/\*/builds/\*\*
- 作業領域
    - $JENKINS_HOME/jobs/\*/workspace/\*\*

## バックアップ関連プラグイン

- [backup](plugins/backup.md)
- [thinBackup](plugins/thinBackup.md)

## バックアップ関連ツール

- [sue445/jenkins-backup-script · GitHub](https://github.com/sue445/jenkins-backup-script)
    - バックアップ作業のシェルスクリプト
    - Jenkins の設定とプラグインをバックアップするために以下をバックアップ
        - $JENKINS_HOME/\*.xml
        - $JENKINS_HOME/plugins/\*.[hj]pi
    - .pinned ファイルも
        - $JENKINS_HOME/jobs/\*/\*.xml
        - $JENKINS_HOME/users/\*
        - $JENKINS_HOME/secrets/\*

## 参考資料

- [Jenkinsのバックアップとリストアについてメモ - Qiita](http://qiita.com/tq_jappy/items/3654059d09a4896ae598)
