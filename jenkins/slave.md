# Slave

- [開発者（個人）のためのJenkins - Slave編 - Qiita](http://qiita.com/yasuhiroki/items/d618796a1ba1366c4f80)
- [Distributed builds - 日本語 - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JA/Distributed+builds#Distributedbuilds-OtherRequirements%2F%E3%81%9D%E3%81%AE%E4%BB%96%E5%BF%85%E8%A6%81%E4%BA%8B%E9%A0%85)
    - slave.jar 転送は以前はプラグインの機能だった
- JNLP 連携
    - [Jenkins導入から運用⑤　スレーブノードの設定 - phaaaaantomのメモ帳](http://phaaaaantom.hatenablog.jp/entry/2013/11/19/003702)
        - ブラウザから出なくても起動できる。
    - [JenkinsのSlaveノードを使ってNAT配下のサーバにアプリケーションをデプロイ | HAWSクラウドサービス](http://haws.haw.co.jp/tech/jenkins-slave-nat-deploy/)
        - JenkinsのSlave起動画面。
        - CLIからの起動方法も起動画面にのっている。
- 必要なツールは入れておく必要がある（git, svn も含めて）
- [Jenkins のスレーブを設定する - Qiita](http://qiita.com/tototoshi/items/d4e380fd40f0619bbf57)
    - ジョブの設定内容
