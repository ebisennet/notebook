# Jenkins ファイル構成

Jenkins 公式の RPM ファイルでインストールした時の関連ファイル。

## /lib

| パス                     | 説明         |
|--------------------------|--------------|
| /lib/jenkins/jenkins.war | Jenkins 本体 |

## /etc

| パス                     | 説明                       |
|--------------------------|----------------------------|
| /etc/init.d/jenkins      | init script                |
| /etc/sysconfig/jenkins   | 設定ファイル               |
| /etc/logrotate.d/jenkins | ログローテート設定ファイル |

## /var/lib

| パス                                             | 説明                                                    |
|--------------------------------------------------+---------------------------------------------------------|
| /var/lib/jenkins                                 | Jenkins のホームディレクトリ（ `$JENKINS_HOME` で参照） |
| /var/lib/jenkins/\*.xml                          | 各種設定ファイル                                        |
| /var/lib/jenkins/jobs/JOBNAME                    | ジョブごとのディレクトリ                                |
| /var/lib/jenkins/jobs/JOBNAME/builds             | ビルド履歴ディレクトリ                                  |
| /var/lib/jenkins/jobs/JOBNAME/builds/BUILDNUMBER | ビルド結果世代別ディレクトリ                            |
| /var/lib/jenkins/jobs/JOBNAME/config.xml         | ジョブ定義ファイル                                      |
| /var/lib/jenkins/jobs/JOBNAME/lastStable         |                                                         |
| /var/lib/jenkins/jobs/JOBNAME/lastSuccessful     | 最後に成功したビルド結果へのシンボリックリンク          |
| /var/lib/jenkins/jobs/JOBNAME/nextBuildNumber    | ビルド番号記録ファイル                                  |
| /var/lib/jenkins/jobs/JOBNAME/workspace          | 作業領域                                                |
| /var/lib/jenkins/jobs/JOBNAME/jobs/JOBNAME       | Folder Plugin によるフォルダー内のジョブ                |
| /var/lib/jenkins/nodes                           |                                                         |
| /var/lib/jenkins/plugins                         | プラグイン格納ディレクトリ                              |
| /var/lib/jenkins/plugins/PLUGINNAME              | プラグイン本体ディレクトリ（PLUGINNAME.jpi の展開先）   |
| /var/lib/jenkins/plugins/PLUGINNAME.hpi          | プラグインファイル（Hudson時代の拡張子）                |
| /var/lib/jenkins/plugins/PLUGINNAME.jpi          | プラグインファイル                                      |
| /var/lib/jenkins/plugins/PLUGINNAME.jpi.pinned   | プラグインバージョン固定ファイル                        |
| /var/lib/jenkins/proxy.xml                       | proxy設定                                               |
| /var/lib/jenkins/secret.key                      |                                                         |
| /var/lib/jenkins/secrets                         | シークレット情報格納ディレクトリ                        |
| /var/lib/jenkins/secrets/master.key              |                                                         |
| /var/lib/jenkins/updates                         |                                                         |
| /var/lib/jenkins/userContent                     |                                                         |

## /var/log

| パス                         | 説明         |
|------------------------------|--------------|
| /var/log/jenkins/jenkins.log | ログファイル |

## /var/run

| パス                 | 説明         |
|----------------------|--------------|
| /var/run/jenkins.pid | PID ファイル |

## /var/cache

| パス                   | 説明                 |
|------------------------|----------------------|
| /var/cache/jenkins     |                      |
| /var/cache/jenkins/war | jenkins.war の展開先 |


