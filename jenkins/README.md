# Jenkins

## 公式資料

- [CloudBees Jenkins Enterprise 15.5 User Guide](http://documentation.cloudbees.com/docs/cje-user-guide/index.html)
    - Enterprise のユーザガイド
    - 各種 Enterprise プラグインの使い方など
- [Jenkins](https://jenkins-ci.org/)
- [jenkinsci/jenkins · GitHub](https://github.com/jenkinsci/jenkins)
    - ソースコード
- [Home - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Home)
    - Wiki

## Topics

- [Plugins](plugins/)
- [Slave](slave.md)
- [Enterprise Edition](enterprise.md)
- [インストール](installation/)
- [設定](configure.md)
- [ユーザー管理](user-management.md)
- [バックアップ](backup.md)
- [ファイル構成](files.md)
- [ジョブ](jobs/)
- CLI
    - [コマンドラインから jenkins にプラグインをインストールする - Qiita](http://qiita.com/tototoshi/items/a2061a237faa21ee2f2c)
- [API](api/)
- SELinux 対応
    - [腹腹開発: SELINUX=enforcingのままでJenkinsを動かす](https://fight-tsk.blogspot.jp/2014/12/selinuxenforcingjenkins.html)

## Memo

- [Jenkins 1\.641 / Jenkins 1\.625\.3 からContent\-Security\-Policyが設定された \- R42日記](http://takahashikzn.root42.jp/entry/2015/12/16/020907)

## 使用ポート

- 8080: jenkins 本体
- 8009: AJP ポート

## 参考資料

- [Jenkinsユーザカンファレンス Tokyo 2015 メモ - Qiita](http://qiita.com/yasuhiroki/items/08a1666c0e48a915a304)
- [www.diva-portal.org/smash/get/diva2:647478/FULLTEXT01.pdf](http://www.diva-portal.org/smash/get/diva2:647478/FULLTEXT01.pdf)
    - active/active, active/standby

