# ジョブ

- [ジョブフィルター](job-filter.md)
- [環境変数](environments.md)
- [パラメーター](parameters.md)
- [ビルド](builds.md)

## ジョブ名制限

- 管理画面「システム設定」から「プロジェクト名の制限」で可能。

## Memo

- [Injecting Secrets into Jenkins Build Jobs – CloudBees Support](https://support.cloudbees.com/hc/en-us/articles/203802500-Injecting-Secrets-into-Jenkins-Build-Jobs)
    - Credentials をジョブ内から使う方法。
