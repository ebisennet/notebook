# ビルド

ビルド結果の確認。

## ビルド情報の編集

- [ほげめも: Jenkins のビルド情報の自動設定](http://blog.keshi.org/hogememo/2012/12/14/jenkins-setting-build-info)
    - curl で API 呼び出し
    - displayName と description が設定できる。
        - displayName は未設定だと "#${BUILD_NUMBER}"
