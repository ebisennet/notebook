# 環境変数

## 公式資料

- [Building a software project - 日本語 - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JA/Building+a+software+project)

## Jenkinsが設定する環境変数

- `BUILD_NUMBER`
    - 当該ビルドの番号。例 "153"
- `BUILD_ID`
    - 当該ビルドID。例 "2005-08-22_23-59-59" (YYYY-MM-DD_hh-mm-ss)
- `BUILD_DISPLAY_NAME`
    - The display name of the current build, which is something like "#153" by default.
- `JOB_NAME`
    - ビルドのプロジェクト名。例 "foo"
- `BUILD_TAG`
    - 文字列 "jenkins-${JOB_NAME}-${BUILD_NUMBER}"。簡易な識別子として、リソースファイルやjarファイルなどに付与するのに便利です。
- `EXECUTOR_NUMBER`
    - このビルドを実行している現在のエグゼキューターを識別する(同一マシンの中で)ユニークな番号。"ビルド実行状態"に表示されている数字ですが、1ではなく0から始まる数字です。
- `NODE_NAME`
    - ビルドが実行されるスレーブの名称。マスターで実行される場合は"master"。
- `NODE_LABELS`
    - ノードに設定されたラベルのリスト（スペース区切り)。
- `WORKSPACE`
    - ワークスペースの絶対パス。
- `JENKINS_HOME`
    - The absolute path of the directory assigned on the master node for Jenkins to store data.
- `JENKINS_URL`
    - JenkinsのURL。例 http://server:port/jenkins/
- `BUILD_URL`
    - このビルドのURL。 例 http://server:port/jenkins/job/foo/15/
- `JOB_URL`
    - このジョブのURL。 例 http://server:port/jenkins/job/foo/

## プラグインが設定する環境変数

- [Git Plugin](../plugins/git.md)
