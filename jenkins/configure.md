# 設定

## 関連ファイル

### `/etc/sysconfig/jenkins`

init script 内からロードされるスクリプト。

-   Jenkins 動作の設定をシェル変数としてセットしていく。
-   ただのシェルスクリプトなので、ulimit などコマンド実行による設定も可能。

#### 起動時オプション

以下で確認できる。

```
java -jar jenkins.war --help
```

#### 証明書のセット

SSL 証明書について Java Keystore 以外に PEM ファイルの指定が可能。

Keystore 形式の場合、Javaの [keytool](../java/tools/keytool.md) を使って作成する。

### `/etc/logrotate.d/jenkins`

logrotate の設定ファイル。

デフォルトで設定される内容は以下の通り。

```
/var/log/jenkins/jenkins.log /var/log/jenkins/access_log {
    compress
    dateext
    maxage 365
    rotate 99
    size=+4096k
    notifempty
    missingok
    create 644
    copytruncate
}
```

#### compress

ログの gzip 圧縮指定。

#### copytruncate

ログファイルをコピー後にオリジナルログファイルをtruncateする。

#### create

ログをローテートした後にファイル作成する。

作成されるログファイルのパーミッション指定可能。

#### dateext

ログファイルのファイル名に日付を付与する。

#### maxage

ログを保持する期間を日数で指定する。

#### missingok

ログファイルがなくてもエラーメッセージを出さない。

#### notifempty

ログが空の時はローテートしない。 not if empty.

#### rotate

何世代ローテーションするか。

#### size

ログのローテーションを開始する条件となるログファイルのサイズ指定。

## 参考資料

- [RPM・yumでインストールしたJenkinsの設定メモ - Qiita](http://qiita.com/mychaelstyle/items/1d90ec8b97f146b9ee45)
    - `JENKINS_JAVA_OPTIONS="-Xms256m -Xmx1024m -Djava.awt.headless=true"`
