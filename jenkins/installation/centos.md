# CentOS

## 公式資料

- [RedHat Repository for Jenkins](http://pkg.jenkins-ci.org/redhat/)
    - Jenkins の Yum リポジトリ
    - 個別の rpm ダウンロードも可能

## ダウンロード

バージョン指定して個別に rpm をダウンロードする場合。

``` bash
JENKINS_VER=1.646
curl -sSL http://pkg.jenkins-ci.org/redhat/jenkins-${JENKINS_VER}-1.1.noarch.rpm
```

## 参考資料

- [CentOSにyumでjenkinsをインストールする - 文系プログラマによるTIPSブログ](http://www.bunkei-programmer.net/entry/2014/06/11/233027)
- [CentOS7へのJenkinsのインストール手順 - Qiita](http://qiita.com/yukimunet/items/28c89370fccc86077dd2)
    - Jenkins の Yum リポジトリを使う
    - 自己署名証明書をセットした https 設定

