# ビルド結果の取得

`${BUILD_URL}api/xml` や `${BUILD_URL}api/json` から取得できる。

ビルドの後処理で取得するには [postbuild-task](../plugins/postbuild-task.md) プラグインを使って後処理でスクリプトが実行できるようにする必要がある。

## 例

```sh
curl -k -u "${BUILD_UPDATE_CREDENTIAL}" -s "${BUILD_URL}api/json" | json_reformat
```

出力：

```json
{
    "actions": [
        {
            "causes": [
                {
                    "shortDescription": "ユーザーctc-adminが実行",
                    "userId": "ctc-admin",
                    "userName": "ctc-admin"
                }
            ]
        }
    ],
    "artifacts": [

    ],
    "building": true,
    "description": null,
    "displayName": "#2",
    "duration": 0,
    "estimatedDuration": 349,
    "executor": {

    },
    "fullDisplayName": "ctc-test #2",
    "id": "2",
    "keepLog": false,
    "number": 2,
    "queueId": 3535,
    "result": "SUCCESS",
    "timestamp": 1481010521019,
    "url": "https://10.39.229.50:10443/job/ctc-test/2/",
    "builtOn": "",
    "changeSet": {
        "items": [

        ],
        "kind": null
    },
    "culprits": [

    ]
}
```

## 参考資料

- [Jenkins casual notification using Remote access API \- 烏賊様](http://ikasama.hateblo.jp/entry/2011/12/21/033421)
    - 後処理で通知処理。
    - ビルドステータスを API を使って取得。
