# API

## 公式資料

- [Remote access API - 日本語 - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JA/Remote+access+API)

## Topics

- [再起動](safeRestart.md)
- [ビルド結果の設定](buildConfig.md)
- [ビルド結果の取得](buildApi.md)

## 参考資料

- [Jenkins APIでジョブ情報を取得する - Qiita](http://qiita.com/tamikura@github/items/033ab9180561b6232509)
- [jenkins の API を使って serverspec を実行する - ようへいの日々精進XP](http://inokara.hateblo.jp/entry/2013/07/29/011850)
