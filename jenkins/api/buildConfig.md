# ビルド結果の設定

## Path

- `${BUILD_URL}configSubmit`

## Required Permissions

- 全体 Read
- ジョブ Read
- ビルド Update

## curl で設定する場合

```
BUILD_UPDATE_CREDENTIAL="$USER:$TOKEN"

function update_build () {
  local name=$1
  local description=$2

  cat <<EOF |
{"displayName":"#${BUILD_NUMBER} ${name}", "description":"${description}"}
EOF
  curl -u "${BUILD_UPDATE_CREDENTIAL}" -s "${BUILD_URL}configSubmit" -X POST --data-urlencode json@-
}
```

$TOKEN はユーザーの設定画面から参照可能。

上記の関数は `$BUILD_UPDATE_CREDENTIAL` を管理画面から設定する方法でも良い。

- 自己署名証明書を使った Jenkins には curl に -k オプションをつける。
- 外向きの URL を使うので `$no_proxy` の設定も必要。

エラーが起きた時は -v をつけて詳細を確認する。

## 参考資料

- [ほげめも: Jenkins のビルド情報の自動設定](http://blog.keshi.org/hogememo/2012/12/14/jenkins-setting-build-info)

