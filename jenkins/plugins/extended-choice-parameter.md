# extended-choice-parameter

複雑な構成のパラメーターを定義できるようになるプラグイン。

## Basic Parameter Types

基本のパラメーター定義。

値、デフォルト値、値の説明の３つのリストを定義することでそこから選択するパラメーターを定義できる。

各リストは以下の方法で取得できる。

1. リストを直接記述
2. プロパティファイルの指定したキーから取得
3. Groovy Script の実行結果
4. 指定した Goovy Script ファイルの実行結果

選択した結果が指定した区切り文字で結合して変数に設定される。

## Multi-level Parameter Types

## JSON Parameter Type

Groovy から JSON Schema で定義できる。

JSON Schema 定義より JSON Editor による画面が表示され、入力した結果が環境変数、あるいは指定ファイルに出力される。

### JSON Editor

- [GitHub \- jdorn/json\-editor: JSON Schema Based Editor](https://github.com/jdorn/json-editor)

## 公式資料

- [Extended Choice Parameter plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Extended+Choice+Parameter+plugin)
- [GitHub \- jenkinsci/extended\-choice\-parameter\-plugin: Jenkins extended\-choice\-parameter plugin](https://github.com/jenkinsci/extended-choice-parameter-plugin)
