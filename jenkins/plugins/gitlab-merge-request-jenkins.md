# gitlab-merge-request-jenkins

## 公式資料

- [Gitlab Merge Request Builder Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Gitlab+Merge+Request+Builder+Plugin)
- [GitHub - timols/jenkins-gitlab-merge-request-builder-plugin: A plugin that allows jenkins to build merge requests in Gitlab](https://github.com/timols/jenkins-gitlab-merge-request-builder-plugin)

## 参考資料

- [Gitlab Merge Request Builderを適用する - Qiita](http://qiita.com/Nkzn/items/00aa5772a416b1e0821f)
- [J( 'ｰ\`)し「たかしへ。Gitlab Merge Request Builder Pluginを入れてみたわよ」 - くりにっき](http://sue445.hatenablog.com/entry/2014/10/10/111601)

