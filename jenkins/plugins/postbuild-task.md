# postbuild-task

ジョブの後処理で任意のスクリプトが実行できる。

## 公式資料

- [Post build task \- Jenkins \- Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Post+build+task)

## 参考資料

- [Jenkins casual notification using Remote access API \- 烏賊様](http://ikasama.hateblo.jp/entry/2011/12/21/033421)
