# Multiple SCMs Plugin

## 公式資料

- [Multiple SCMs Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Multiple+SCMs+Plugin)

## 参考資料

- [Jenkinsで1つのジョブで複数のGitリポジトリをビルドする方法 - wadahiroの日記](http://wadahiro.hatenablog.com/entry/20120325/1332649690)

