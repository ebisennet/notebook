# gitlab-plugin

## 公式資料

- [gitlab-plugin](https://wiki.jenkins-ci.org/display/JENKINS/GitLab+Plugin)
- [GitHub - jenkinsci/gitlab-plugin: A Jenkins plugin that emulates GitlabCI](https://github.com/jenkinsci/gitlab-plugin)
    - 使い方など。

## 設定

1. Jenkins で対象ジョブのビルドトリガーから `Build when a change is pushed to GitLab.` とあるチェックを入れる。
2. ジョブを起動するための URL が表示されるので GitLab で Webhook を設定する。
    - SSL Verification は必要に応じてオフにする。

## Memo

- 依存で以下の plugin が入り、ワークスペースディレクトリの位置が変更されるので注意。
    - [Pipeline Step API Plugin \- Jenkins \- Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Pipeline+Step+API+Plugin)
