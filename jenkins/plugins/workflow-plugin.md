# workflow-plugin

Groovy スクリプトによる DSL でジョブ内でループ処理などが書ける。

## 公式資料

- [Pipeline Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Pipeline+Plugin)
    - 依存プラグインが多い。
    - 名称が変わっている。
        - 旧ページ : [Workflow Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Workflow+Plugin)
- [GitHub - jenkinsci/pipeline-plugin: Jenkins Pipeline plugin](https://github.com/jenkinsci/pipeline-plugin)
    - [pipeline-plugin/TUTORIAL.md at master · jenkinsci/pipeline-plugin · GitHub](https://github.com/jenkinsci/pipeline-plugin/blob/master/TUTORIAL.md)
        - チュートリアル

## 参考資料

- [Jenkins Workflow Pluginの使いかた - Qiita](http://qiita.com/tabbyz/items/71bd2a9a6ba14b514889)

