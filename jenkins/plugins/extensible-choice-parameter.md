# extensible-choice-parameter

ジョブ共有で使用できる選択パラメーターを定義できるようになるプラグイン。

選択パラメーターを Groovy Script でも定義できる。

## 公式資料

- [Extensible Choice Parameter plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Extensible+Choice+Parameter+plugin)

## 参考資料

- [Jenkins がもっともっともっと便利になるプラグイン 8 つ (フェンリル | デベロッパーズブログ)](http://blog.fenrir-inc.com/jp/2013/06/jenkins_plugins-2.html)
    - この記事自体が extended-choice-parameter と extgensible-... を間違えてる。

