# docker-build-step

Docker REST API を呼び出して Docker コマンドを実行するプラグイン。

- Jenkins のシステム設定からターゲットとなる Docker REST API の URL を指定可能。
- ジョブに Docker コマンドを指定したビルドステップを追加できる。

