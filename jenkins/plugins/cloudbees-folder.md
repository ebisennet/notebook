# CloudBees Folders Plugin

## 公式資料

- [CloudBees Folders Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/CloudBees+Folders+Plugin)

## Topics

- 新規フォルダー作成
    - 新規ジョブ作成から可能。
- ジョブの移動
    - ジョブのメニューから Move で可能。
