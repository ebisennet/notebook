# thinBackup

- バックアップ対象を選択できる。
- バックアップスケジュールを設定できる。
    - Cron like な設定。
- バックアップ前にJenkinsがIdleになるのを待機できる。
    - タイムアウトした場合、Jenkinsをバックアップ用にQuietモードになるよう強制できる。
- プラグイン本体はバックアップしない
    - プラグインIDとバージョンを記録した installedPlugins.xml を生成するので、プラグインを再インストールする形で復元可能。
- secrets ディレクトリはバックアップしない

以下、2016-01 時点で開発中のバージョン

- プラグイン本体のバックアップもある。
- 追加で任意のファイルのバックアップも指定可能。

## 公式資料

- [thinBackup - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/thinBackup)
- [jenkinsci/thin-backup-plugin · GitHub](https://github.com/jenkinsci/thin-backup-plugin)
    - ソースコード
    - [thin-backup-plugin/HudsonBackup.java at master · jenkinsci/thin-backup-plugin · GitHub](https://github.com/jenkinsci/thin-backup-plugin/blob/master/src/main/java/org/jvnet/hudson/plugins/thinbackup/backup/HudsonBackup.java)
        - バックアップ実装メイン
    - [thin-backup-plugin/HudsonRestore.java at master · jenkinsci/thin-backup-plugin · GitHub](https://github.com/jenkinsci/thin-backup-plugin/blob/master/src/main/java/org/jvnet/hudson/plugins/thinbackup/restore/HudsonRestore.java)
        - リストア実装メイン

## バックアップ対象

- ユーザーコンテンツ
    - $JENKINS_HOME/userContent/\*\*
    - 設定によってバックアップに含めるか選択可能
- Jenkins設定ファイル
    - $JENKINS_HOME/\*.xml
- ジョブ定義
    - $JENKINS_HOME/jobs/\*/config.xml
    - builds ディレクトリ
        - 設定によってバックアップに含めるか選択可能
- ユーザー情報
    - $JENKINS_HOME/users/\*\*
- プラグインリスト
    - インストールされているプラグインの Plugin ID とバージョンを記録したXMLファイル

## 参考資料

- [Jenkinsのバックアップとリストアについてメモ - Qiita](http://qiita.com/tq_jappy/items/3654059d09a4896ae598)

