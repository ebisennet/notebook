# Parameterized Trigger Plugin

## 公式資料

- [Parameterized Trigger Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Parameterized+Trigger+Plugin)

## 参考資料

- [Jenkinsによるパラメーター連携の自動化](http://changesworlds.com/2015/03/parameterized-trigger-by-jenkins/)
- [開発者（個人）のためのJenkins - Parameter編 - Qiita](http://qiita.com/yasuhiroki/items/daff66ab18b315ae4298)

