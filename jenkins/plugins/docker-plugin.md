# docker-plugin

ジョブ実行を docker コンテナ上で行うプラグイン。

ジョブ実行に使用するイメージと、コンテナを起動する Docker Server を設定して使用する。

デフォルトで設定されているイメージは、Ubuntu + OpenJDK となる。

## 公式資料

- [Docker Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Docker+Plugin)

