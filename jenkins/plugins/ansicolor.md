# ansicolor

ジョブの出力結果に色をつけることができるプラグイン。

## ジョブの設定

プラグインインストール後にジョブの設定画面で「ビルド環境」ー「Color ANSI Console Output」が増えるので
チェックを入れることでログに出力がカラー対応となる。

## 管理画面

プラグインインストール後にJenkinsの管理画面の「システム設定」画面に「ANSI Color」の項目が増えるので
そこで色の設定を変更することができる。

## 公式資料

- [AnsiColor Plugin \- Jenkins \- Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/AnsiColor+Plugin)

## 参考資料

- [Jenkins先生のログを色付けして見やすくする \- Qiita](http://qiita.com/yuichielectric/items/b0dd70d2c3e073eea862)
