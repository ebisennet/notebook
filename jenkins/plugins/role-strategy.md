# role-strategy

ロールによる権限設定、プロジェクト単位の権限設定が可能になるプラグイン。

管理画面上は "Role-based Authorization Strategy" という名称な点に注意。

プラグインインストール後、管理画面の「グローバルセキュリティの設定」から有効化できる。
再起動なしで有効化できるが、有効化した直後はカレントユーザーにのみ Admin 権限という状態になる。

管理画面に「Manage and Assign Roles」という項目が追加され、ロールの定義や
定義したロールのユーザーへの割り当てが管理できる。

ロールはプロジェクト単位でも定義でき、その場合はロールが適用されるプロジェクト名のパターンを指定する。

「CloudBees Folders」プラグインを使用している場合、ジョブ名へのパターン指定はフォルダーとジョブ名のそれぞれに適用される。
ジョブ閲覧権限があっても、それを格納しているフォルダーへの閲覧権限がないと閲覧不可能。

## 公式資料

- [Role-based Authorization Strategy](https://plugins.jenkins.io/role-strategy)
- [role-strategy](https://wiki.jenkins-ci.org/display/JENKINS/Role+Strategy+Plugin)
- [jenkinsci/role-strategy-plugin · GitHub](https://github.com/jenkinsci/role-strategy-plugin)

## 参考資料

- [Jenkinsで特定のプロジェクトだけ閲覧できるユーザを作成するには | TechRacho](http://techracho.bpsinc.jp/morimorihoge/2013_09_12/13642)
- [Jenkinsでユーザーに権限を付与してアクションを制限する \- Qiita](http://qiita.com/west-hiroaki/items/410a76c96ad57c8cb547)
