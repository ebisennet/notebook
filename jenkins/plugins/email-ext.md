# email-ext

メール通知をカスタマイズするプラグイン。

## 公式資料

- [Email-ext plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Email-ext+plugin)

## 参考資料

- [Jenkinsによるメール通知の自動化](http://changesworlds.com/2015/03/automation-of-mail-notification-by-jenkins/)
- [Jenkinsから送信されるメールをカスタマイズする | Ryuzee.com](http://www.ryuzee.com/contents/blog/4527)

