# backup

メンテナンスされていない？

- Latest Release Date
    - Aug 04, 2011

## 公式資料

- [Backup Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Backup+Plugin)
- [jenkinsci/backup-plugin · GitHub](https://github.com/jenkinsci/backup-plugin)

## 類似

- [thinBackup](thinBackup.md)

