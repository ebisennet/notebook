# ansible

## 公式資料

- [Ansible Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Ansible+Plugin)
- [GitHub - jenkinsci/ansible-plugin: Jenkins Ansible plugin](https://github.com/jenkinsci/ansible-plugin)
    - ソースコード

## Memo

- ssh 接続についてパスワード認証をする場合、sshpass コマンドがインストールされている必要がある。
- ジョブ設定時に各パラメータは、環境変数参照がそのまま使える。

