# ghprb

GitHub pull request builder plugin.

## 公式資料

- [GitHub pull request builder plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/GitHub+pull+request+builder+plugin)
- [jenkinsci/ghprb-plugin · GitHub](https://github.com/jenkinsci/ghprb-plugin)

## 参考資料

- [Jenkinsでプルリクエストをビルドする - Qiita](http://qiita.com/quattro_4/items/6b1962909ce868f12e5a)
- [Pull Request Builder PluginをJenkinsに導入する - Qiita](http://qiita.com/kompiro/items/a097b3b36aa30bc751c6)
