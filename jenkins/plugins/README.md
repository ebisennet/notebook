# Plugins

## 公式資料

- [Jenkins · GitHub](https://github.com/jenkinsci)
    - いろいろなプラグインが確認できる。
- [Jenkins Plugins](https://plugins.jenkins.io/)

## Plugins

| Plugin ID                                                       | プラグイン名                                      | 説明                                       |
|-----------------------------------------------------------------+---------------------------------------------------+--------------------------------------------|
|                                                                 | CloudBees Docker Workflow                         |                                            |
|                                                                 | CloudBees Docker Custom Build Envirionment Plugin | コンテナの中でビルド                       |
|                                                                 | CloudBees Docker Traceability                     |                                            |
|                                                                 | CloudBees Docker Hub Notification                 |                                            |
| [ansible](ansible.md)                                           | Ansible plugin                                    | Ansible Playbook のビルドステップ          |
| [ansicolor](ansicolor.md)                                       | AnsiColor Plugin                                  |                                            |
| [backup](backup.md)                                             |                                                   |                                            |
| [cloudbees-folder](cloudbees-folder.md)                         | CloudBees Folders Plugin                          |                                            |
| docker-build-publish                                            | CloudBees Docker Build and Publish Plugin         | Docker イメージのビルドステップ            |
| [docker-build-step](docker-build-step.md)                       |                                                   |                                            |
| [docker-plugin](docker-plugin.md)                               |                                                   |                                            |
| [email-ext](email-ext.md)                                       | Email Extension Plugin                            |                                            |
| [envinject](envinject.md)                                       | Environment Injector Plugin                       | ビルド設定で環境変数設定が可能になる       |
| [extended-choice-parameter](extended-choice-parameter.md)       | Extended Choice Parameter Plug-In                 |                                            |
| [extensible-choice-parameter](extensible-choice-parameter.md)   |                                                   |                                            |
| [extra-columns](extra-columns.md)                               |                                                   | リストビューの表示拡張。                   |
| [ghprb](ghprb.md)                                               | GitHub pull request builder plugin                |                                            |
| [git](git.md)                                                   |                                                   |                                            |
| [gitbucket](gitbucket.md)                                       |                                                   |                                            |
| [gitlab-merge-request-jenkins](gitlab-merge-request-jenkins.md) |                                                   |                                            |
| [gitlab-oauth](gitlab-oauth.md)                                 |                                                   |                                            |
| [gitlab-plugin](gitlab-plugin.md)                               |                                                   |                                            |
| [multiple-scms](multiple-scms.md)                               | Multiple SCMs Plugin                              |                                            |
| [nodejs](nodejs.md)                                             |                                                   |                                            |
| [parameterized-trigger](parameterized-trigger.md)               | Parameterized Trigger Plugin                      | パラメータの引き継ぎ、ビルド中のジョブ起動 |
| [project-description-setter](project-description-setter.md)     | Project Description Setter Plugin                 |                                            |
| [role-strategy](role-strategy.md)                               | Role Strategy Plugin                              |                                            |
| [schedule-build](schedule-build.md)                             |                                                   |                                            |
| [simple-theme-plugin](simple-theme-plugin.md)                   | Simple Theme Plugin                               | 指定した JS/CSS を適用する                 |
| [thinBackup](thinBackup.md)                                     | thinBackup                                        |                                            |
| [vsphere-cloud](vsphere-cloud.md)                               |                                                   |                                            |
| [weblogic-deployer-plugin](weblogic-deployer-plugin.md)         |                                                   |                                            |
| [workflow-plugin](workflow-plugin.md)                           |                                                   |                                            |

## Enterprise

| Plugin ID                                 |                          |
|-------------------------------------------+--------------------------|
|                                           | Folders Plugin           |
| [backup-scheduling](backup-scheduling.md) | Backup Scheduling Plugin |
|                                           | High Availability Plugin |

## 未整理

- [How to use Jenkins for Job Chaining and Visualizations | zeroturnaround.com](https://zeroturnaround.com/rebellabs/how-to-use-jenkins-for-job-chaining-and-visualizations/)

## 参考資料

- [Jenkinsをインストールしたら入れるプラグイン18個 - Qiita](http://qiita.com/shufo/items/4a128a0bf08affbecff8)
- [Jenkinsプラグイン探訪 - List View column plugins - ふぞろいのGENGOたち](http://d.hatena.ne.jp/tyuki39/20111216/1323966284)
