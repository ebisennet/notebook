# schedule-build

スケジュールビルドボタンを追加する。

指定した時間のジョブ実行をジョブキューに追加できる。

## 公式資料

- [Schedule Build Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Schedule+Build+Plugin)

## 注意

- プラグインをインストール、または有効化した後に Jenkins の再起動が必要。

