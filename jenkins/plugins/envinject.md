# envinject

ビルドの各タイミングで環境変数を設定できる。

- システムやJenkinsの設定による環境変数についてインジェクトするかどうか選べる。
- ビルドログ確認画面に実行時の環境変数の確認画面が追加される。
    - 画面左側の "Environment Variables"
    - 加えて、パスワードはデフォルトでマスクされる。

Jenkins 設定画面の「グローバルプロパティ」で設定した環境変数を無効化してしまう。

- [modify PATH variable in jenkins master - Stack Overflow](http://stackoverflow.com/questions/23502306/modify-path-variable-in-jenkins-master)

## 公式資料

- [EnvInject Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/EnvInject+Plugin)

## 参考資料

- [Jenkinsによる環境変数連携の自動化](http://changesworlds.com/2015/04/envinject-by-jenkins/)
    - 「問題点」についての記述は最新版では改善している。

