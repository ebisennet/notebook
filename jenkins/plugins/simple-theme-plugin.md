# simple-theme-plugin

指定した JS/CSS を適用して Jenkins の画面をカスタマイズできる。

インストールすると Jenkins 管理画面の「システムの設定」に「Theme」のセクションが追加される。

## 公式資料

- [Simple Theme Plugin \- Jenkins \- Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Simple+Theme+Plugin)

## 備考

- JS を適用した場合、Jenkins の各画面の head 要素末尾に script タグが追加される。
    - 画面内の要素を操作したい場合は onload イベントを使う必要がある。

## 参考資料

- [Jenkinsのテーマを変更してみる](http://chicken-yuta.com/article/programming/792/)
- [JenkinsのUIをカスタマイズする \| 84zume Works](https://84zume.wordpress.com/2013/12/23/customize-jenkins-ui/)
- [Jenkins がもっともっと便利になるプラグイン 8 つ \(フェンリル \| デベロッパーズブログ\)](http://blog.fenrir-inc.com/jp/2013/03/jenkins-2.html)
