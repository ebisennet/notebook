# project-description-setter

ビルド時にプロジェクトの説明をワークスペース上のファイルから設定する。

## 公式資料

- [Project Description Setter Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Project+Description+Setter+Plugin)

## 設定

Jenkins管理画面の「設定」ー「Project description setter defaults」。

## 参考資料

- [Jenkinsプラグイン探訪 - List View column plugins - ふぞろいのGENGOたち](http://d.hatena.ne.jp/tyuki39/20111216/1323966284)

