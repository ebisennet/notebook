# extra-columns

リストビューの表示拡張。

## 公式資料

- [Extra Columns Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Extra+Columns+Plugin)

## Memo

- [Editing or Replacing the All View - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Editing+or+Replacing+the+All+View)
    - "All" ビューに適用したい場合

## 参考資料

- [Jenkins がもっともっともっと便利になるプラグイン 8 つ (フェンリル | デベロッパーズブログ)](http://blog.fenrir-inc.com/jp/2013/06/jenkins_plugins-2.html)

