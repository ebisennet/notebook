# git

## 公式資料

- [Git](https://plugins.jenkins.io/git)
- [Git Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Git+Plugin)

## 差分をビルドログに出力

以下の内容のコマンド実行ステップを追加する。

``` bash
git diff $GIT_PREVIOUS_SUCCESSFUL_COMMIT $GIT_COMMIT
```

## Gitリポジトリから Web hooks でジョブ実行

- [Git Plugin - Jenkins - Jenkins Wiki](https://wiki.jenkins-ci.org/display/JENKINS/Git+Plugin#GitPlugin-Pushnotificationfromrepository)

手順。

1. ジョブのビルドトリガーで「SCMポーリング」を有効化。
    - スケジュールは入れない。
2. GitLab 側で以下のURLの Web hooks を設定。
    - `http://yourserver/git/notifyCommit?url=<URL of the Git repository>`
    - クエリーパラメーターとして以下も指定可能。
        - branches
        - sha1

url パラメーターの url は、ジョブの設定で Repository URL として入力した内容と一致する必要がある。
`https:`, `git:`, `ssh:` で始まらない形式（例：`git@example.com:repository.git`）でも良い。

HTTP レスポンスボディにどのジョブがSCMポーリングがスケジュールされたかのメッセージを返すので
curl コマンドなどでデバッグできる。

参考。

- [notifyCommitでgitレポジトリのポーリングを効率化 - Qiita](http://qiita.com/setoazusa/items/c19af227bd5dd2a442c8)

## 環境変数

- `GIT_COMMIT`
    - SHA of the current
- `GIT_BRANCH`
    - Name of the remote repository (defaults to origin), followed by name of the branch currently being used, e.g. "origin/master" or "origin/foo"
- `GIT_PREVIOUS_COMMIT`
    - SHA of the previous built commit from the same branch (the current SHA on first build in branch)
- `GIT_PREVIOUS_SUCCESSFUL_COMMIT`
    - SHA of the previous successfully built commit from the same branch.
- `GIT_URL`
    - Repository remote URL
- `GIT_URL_N`
    - Repository remote URLs when there are more than 1 remotes, e.g. GIT_URL_1, GIT_URL_2
- `GIT_AUTHOR_NAME` and `GIT_COMMITTER_NAME`
    - The name entered if the "Custom user name/e-mail address" behaviour is enabled; falls back to the value entered in the Jenkins system config under "Global Config user.name Value" (if any)
- `GIT_AUTHOR_EMAIL` and `GIT_COMMITTER_EMAIL`
    - The email entered if the "Custom user name/e-mail address" behaviour is enabled; falls back to the value entered in the Jenkins system config under "Global Config user.email Value" (if any)

## パラメーターでブランチ指定

ブランチ指定欄に $branch など環境変数の記述が可能なのでパラメーターによるブランチ指定も可能。

パラメータ指定のUIを専用のUIにするためには git-parameter プラグインを導入する。

参考

- [Building github branches with Jenkins – Julian Higman : Blog](http://julianhigman.com/blog/2012/02/22/building-github-branches-with-jenkins/)

