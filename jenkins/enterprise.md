# Enterprise Edition

## 参考資料

- [Enterprise Edition Features | CloudBees](https://www.cloudbees.com/products/cloudbees-jenkins-platform/enterprise-edition/features)
    - "Manage team-specific plugins" とあるが、Plugin バージョンの矯正の仕組みのことらしい。
- [エンタープライズプラグイン│Jenkins｜アプリテスト｜スマートフォンテスト｜株式会社シフト](http://softwaretest.jp/jenkins/enterprise.html)
    - Jenkinsのサポートを行っている会社による Enterprise プラグインの紹介ページ
- [CloudBeesがJenkinsエンタープライズ版をリリース](http://www.infoq.com/jp/news/2011/12/cloudbees-releases-jenkins-enter)
