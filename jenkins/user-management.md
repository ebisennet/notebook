#  ユーザー管理

## ログインの解除

`$JENKINS_HOME/config.xml` 内の useSecurity 要素の値を false にする。

- [Jenkinsの権限設定で失敗してログインできなくなってしまったときの対処方法 - おかひろの雑記](http://d.hatena.ne.jp/okahiro_p/20121210/1355118609)
- [JenkinsでAdmin権限を持つユーザのパスワードを全て忘れた時の復旧方法](http://gosyujin.github.io/2012/06/19/1340106125/)

## 参考資料

- [Jenkinsでユーザー管理してみる - yk5656 diary](http://d.hatena.ne.jp/yk5656/20140629/1405163688)
- [Jenkinsで特定のプロジェクトだけ閲覧できるユーザを作成するには | TechRacho](http://techracho.bpsinc.jp/morimorihoge/2013_09_12/13642)

