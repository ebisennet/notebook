# Troubleshooting

## Please write "set :request_pty, true" in your spec_helper.rb or other appropriate file.

指示に従って spec ファイル内に記述すれば良い。

最初の describe より前に記述する必要がある。

## Wrong sudo password! Please confirm your password on ...

SUDO 実行を指定する。

``` bash
ASK_SUDO_PASSWORD=1 bundle exec rake spec
```

highline gem が必要になる。

