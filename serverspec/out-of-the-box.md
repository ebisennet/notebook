# Out of the box

拡張をしていない素の Serverspec の動作について。

## `serverspec-init` コマンドによるプロジェクト作成

Serverspec はテストコードを書いて実行するための初期のプロジェクト構成を生成するために `serverspec-init` コマンドを提供している。

`serverspec-init` コマンドで生成される初期構成として以下のようなファイルが生成される。

- Serverspec 実行のための Rakefile
- Serverspec の動作設定などを記述する spec_helper.rb
- テスト内容が書かれた spec ファイル

## Rakefile

`serverspec-init` コマンドで生成されるデフォルトの Rakefile の内容は以下の通り。

```ruby
require 'rake'
require 'rspec/core/rake_task'

task :spec    => 'spec:all'
task :default => :spec

namespace :spec do
  targets = []
  Dir.glob('./spec/*').each do |dir|
    next unless File.directory?(dir)
    target = File.basename(dir)
    target = "_#{target}" if target == "default"
    targets << target
  end

  task :all     => targets
  task :default => :all

  targets.each do |target|
    original_target = target == "_default" ? target[1..-1] : target
    desc "Run serverspec tests to #{original_target}"
    RSpec::Core::RakeTask.new(target.to_sym) do |t|
      ENV['TARGET_HOST'] = original_target
      t.pattern = "spec/#{original_target}/*_spec.rb"
    end
  end
end
```

この Rakefile はプロジェクト構成について以下のような構成であることを想定している。

- テストコードは `spec` ディレクトリ内にテスト対象となるホスト名別のディレクトリを作って格納する。
- ホスト名別ディレクトリ内には `TESTNAME_spec.rb` （`TESTNAME` は任意のテスト名）という名前でテストコードのファイルを作成する。

Serverspec は元々、上記内容で要件を満たせない場合はこの Rakefile を拡張することを前提としている。
