# Serverspec

サーバのテストを自動化できる。

サーバテストは遠隔から ssh 経由などで実行可能。 ローカルでも実行可能。

rspec や specinfra 由来の仕組みをそのまま使うところも多いので、それらに対する理解も必要。

## 公式資料

- [Serverspec - Home](http://serverspec.org/)
- [Serverspec - tutorial](http://serverspec.org/tutorial.html)
- [Serverspec - Resource Types](http://serverspec.org/resource_types.html)
- [GitHub - mizzy/serverspec: RSpec tests for your servers configured by CFEngine, Puppet, Chef, Ansible, Itamae or anything else even by hand](https://github.com/mizzy/serverspec)
    - ソースコード
- [GitHub - mizzy/specinfra: Command Execution Framework for serverspec, itamae and so on](https://github.com/mizzy/specinfra)
    - Serverspec 内で使われている specinfra のソースコード
- [Serverspec - Advanced Tips](http://serverspec.org/advanced_tips.html)

## Topics

- [Out of the box](out-of-the-box.md)
- Ansible と組み合わせ
    - [Ansible と Serverspec を組み合わせて使う : あかぎメモ](http://blog.akagi.jp/archives/4535.html)
- Tools
    - [serverspec-runnerを使って複数のホストのテストレポートを作る - Qiita](http://qiita.com/hiracy/items/1ebc7b82f1a6ffa69ed0)
    - [serverspec-runnerにて独自リソースを使う - Qiita](http://qiita.com/hiracy/items/49e177624adef5fbee74)
- Custom Matchers
- [Custom Resources](custom-resources.md)
- [Backend](backend.md)
- [Troubleshooting](troubleshooting.md)
- 出力結果のカスタマイズ
    - [JenkinsとServerspecでサーバーの設定ファイルを定期監視する](http://toshihirock.blogspot.jp/2015/03/jenkinsserverspec.html)

## Memo

### `be_mode` の引数は10進数指定

最終的に以下の箇所で `to_s` されるので、数値で指定するする場合は１０進数でないといけない。 ８進数で記述すると誤動作する。

- specinfra/lib/specinfra/command/base/file.rb

### specinfra 由来の仕組み

- property
- host_inventory
- ...

## 適用対象の制御

環境別、マシン別などでテスト対象を制御したい場合。

- [Serverspec用のspec_helperとRakefileのサンプルをひとつ - Qiita](http://qiita.com/sawanoboly/items/98854fbb4b49e66f6c3c)

## 参考

- [「Serverspec」を使ってサーバー環境を自動テストしよう - さくらのナレッジ](http://knowledge.sakura.ad.jp/tech/2596/)
- [Serverspecの概要からインストールまで | Think IT（シンクイット）](http://thinkit.co.jp/story/2014/08/01/5139)
- [serverspecでiptables のルールを検証する - Qiita](http://qiita.com/kotaroito/items/121afeea2cc4048a4c56)
- [Serverspec ハンズオン資料（事前準備編） - Qiita](http://qiita.com/inokappa/items/43bb47745b3565b9b69f)
