# technotes

技術系ノート、作業時の覚書。

| ノート名                        | 概要                                       |
|---------------------------------+--------------------------------------------|
| [amazon](amazon/)               | Amazon Web Service                         |
| [ansible](ansible/)             | 構成管理ツール Ansible                     |
| [apache](apache/)               |                                            |
| [bash](bash/)                   |                                            |
| [chrome](chrome/)               |                                            |
| [commands](commands/)           | Unix 系の各種コマンドのノート              |
| [embulk](embulk/)               | データローダーツール Embulk のノート       |
| [git](git/)                     | ソースコード管理ツール Git のノート        |
| [gitlab](gitlab/)               |                                            |
| [go](go/)                       | プログラミング言語 Go のノート             |
| [http](http/)                   |                                            |
| [ios](ios/)                     | iOS プログラミングについて                 |
| [ioslib](ioslib/)               | iOS のライブラリについて                   |
| [jenkins](jenkins/)             |                                            |
| [jira](jira/)                   |                                            |
| [json](json/)                   |                                            |
| [mac](mac/)                     |                                            |
| [markdown](markdown/)           |                                            |
| [maven](maven/)                 | Java のビルドツール Maven についてのノート |
| [microservices](microservices/) |                                            |
| [nginx](nginx/)                 | Web Server である Nginx についてのノート   |
| [node](node/)                   |                                            |
| [nodelib](nodelib/)             | Node.js のライブラリについてのノート       |
| [openssl](openssl/)             |                                            |
| [pivotal](pivotal/)             |                                            |
| [python](python/)               |                                            |
| [ruby](ruby/)                   | プログラミング言語 Ruby のノート           |
| [rubygems](rubygems/)           | Ruby のライブラリについてのノート          |
| [selinux](selinux/)             |                                            |
| [serverless](serverless/)       |                                            |
| [serverspec](serverspec/)       |                                            |
| [ssh](ssh/)                     |                                            |
| [swagger](swagger/)             |                                            |
| [swift](swift/)                 | プログラミング言語 Swift のノート          |
| [vagrant](vagrant/)             |                                            |
| [vim](vim/)                     |                                            |
| [xcode](xcode/)                 |                                            |
| [yaml](yaml/)                   |                                            |
