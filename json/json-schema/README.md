# JSON Schema

## 公式資料

- [JSON Schema and Hyper-Schema](http://json-schema.org/)
- [GitHub - json-schema/json-schema: JSON Schema specifications](https://github.com/json-schema/json-schema)
- [GitHub - json-schema-org/json-schema-spec: The JSON Schema RFC sources](https://github.com/json-schema-org/json-schema-spec)

## Topics

- [JSON Schema Generator](http://jsonschema.net/#/)

## 参考資料

- [全てがJSONになる - ✘╹◡╹✘](http://r7kamura.hatenablog.com/entry/2014/06/10/023433)
- [JSONスキーマはじめの一歩 - Qiita](http://qiita.com/sagaraya/items/115c15c0df3e84ecbc7f)
- [JSON Schema生成ソフトウェア・ライブラリまとめ | NTT Communications Developer Portal](https://developer.ntt.com/ja/blog/e8d1542a-72ef-47c7-b7bb-aed22a747570)
- [GitHub - nijikokun/generate-schema: Effortlessly convert your JSON Object to JSON Schema, Mongoose Schema, or a Generic template for quick documentation / upstart.](https://github.com/Nijikokun/generate-schema)
