# インストール

## Mac

``` bash
brew install go
```

## 参考資料

- [Golang Macへのインストール〜Homebrewを使って〜 - Qiita](http://qiita.com/megu_ma/items/7208be8de52b712955a0)

