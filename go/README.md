# go

## Topics

- [インストール](installation.md)

## Env

``` bash
export GOROOT=/usr/local/opt/go/libexec
export GOPATH=$HOME
export PATH=$PATH:$GOPATH/bin
```

- [ghqを使ったローカルリポジトリの統一的・効率的な管理について - Kentaro Kuribayashi's blog](http://blog.kentarok.org/entry/2014/06/03/135300)

## Memo

- [gotypeコマンドでgolangのastの結果を視覚化する #golangjp - Qiita](http://qiita.com/atotto/items/acc451714a320d7f1dae)
- [golang のリファクタリングには gofmt が使える - Qiita](http://qiita.com/ikawaha/items/73c0a1d975680276b2c7)

## 参考資料

- [Introduction | build-web-application-with-golang](https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/index.html)

