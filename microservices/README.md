# Microservices

## 参考資料

- [マイクロサービスアーキテクチャにおけるオーケストレーションとコレオグラフィ \- Qiita](http://qiita.com/kawasima/items/17475a993e03f249a077)
- [マイクロサービスアーキテクチャにおけるAPIコールの仕方とHTMLレンダリング \- Qiita](http://qiita.com/kawasima/items/356d54e253c54d730fb0)
- [マイクロサービスはもう十分 \| プロダクト・サービス \| POSTD](http://postd.cc/enough-with-the-microservices/)
